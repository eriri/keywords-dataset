# Keywords Study

Guide to using web interface https://drustz.com/TextTestPP/ for initial study on keywords formulation are listed below.

1. Under `Settings`, upload custom phrase set in `.txt`. Example files can be found under `examples`.
2. Check `Use Enter for Next` so participants can press enter to go to the next sentence.
3. Participants should input all words they think that capture the fundamental idea present in the displayed text i.e. keywords, and each word should be separated by a space.
4. After completing all sentences, click `Analyse`, and download the `.json` log by changing the log type to `json` and clicking `Download log`.
5. Suggested time is 30 mins for 50 sentences of length 6 to 12 words.

## Test Data Generation and Analysis 
Using `generate_sentences(n, min_size, max_size)` in `utils.py`, we could generate `n` test sentences, selected using random sampling from Amazon's test dataset, of length `[min_size, max_size)`. It has already been set to exclude potential OOV words using POS tagging but it is recommended to manually check through all generated sentences for spelling errors, short forms or slangs etc. Examples generated can be found under `examples`. Uncomment `read_json(p, n)` to extract relevent information from `.json` log obtained. Information include:

1. List of POS tags, count and % of each POS according to https://universaldependencies.org/docs/u/pos/
2. Number and % of keywords, order of keywords found
3. Time taken for each trial

# Keywords Dataset

Sentences can be found in `sentences` folders and corresponding keywords extracted using different methods are stored in `yake`, `embedding` and `textrank` folders. Each `.p` file contains 1000 reviews. Both sentences and keywords are stored in dictionaries with sentence index starting as key.

For Yelp, Amazon's train dataset sentences and keywords extracted using Yake, sentence index does not reset for each file i.e. only for `sent_dict_1.p` index of the first sentence is `0` while for `sent_dict_2.p` index of the first sentence is the number of sentences in `sent_dict_1.p`. While for all others, sentence index restarts for each file i.e. index of the first sentence from both `sent_dict_1.p` and `sent_dict_2.p` is `0`. Hence when accessing the dictionary entries, it's best to avoid using key index.

Note that for short sentences, there may be no keywords extracted i.e. returns an empty list. 

## Example

```python
sent = pickle.load(open("pickles/yelp/train/sentences/sent_dict_1.p", "rb"))
keywords = pickle.load(open("pickles/yelp/train/yake/words_dict_1.p", "rb"))

print(sent[0])
# dr. goldberg offers everything i look for in a general practitioner.
print(keywords[0])
# ['goldberg', 'offers', 'general', 'practitioner']

```

