"""
Utils for study 1
"""
from pathlib import Path
from flashtext import KeywordProcessor
import numpy as np
import pickle
import spacy
import yake
import copy
import json
import csv

# Number of tests sentences to generate
n = 50
# Spacy NLP pipeline
nlp = spacy.load('en_core_web_sm')
# POS filters
pos_filters = {'PROPN', 'NUM', 'SYM', 'X'}
# POS dictionary
pos_dict = {'ADJ':0, 'ADV':0, 'INTJ':0, 'NOUN':0, 'VERB':0, 'ADP':0, 'AUX':0, 'CONJ':0, 'DET':0, 'PART':0, 'PRON':0, 'SCONJ':0}
# Yake parameters
max_ngram_size = 1
deduplication_thresold = 0.7
deduplication_algo = 'seqm'
numOfKeywords = 4
windowSize = 3

def read_csv():
    """Get results in csv, not used for now"""
    p = Path("res/test.csv")
    ln = 0
    with p.open() as data:
        for line in csv.reader(data):
            if ln > 0:
                print(line)
                break
            ln += 1

def read_json(p, n):
    """Get results in json"""
    with p.open() as f:
        data = json.load(f)
        for i in range(n):
            origin = data[i]['Present']
            print(i, origin)
            user_words = data[i]['Transcribed'].strip()
            print(user_words.split())
            tags, pos_dict_copy, tag_ratio_dict = calculate_pos(user_words)
            keywords_size, keywords_size_ratio, order_list = analyse_keywords(origin, user_words)
            time_taken = data[i]['Time']
            # print(time_taken)

def calculate_pos(s):
    """Get list of POS tags, count and % of each POS"""
    doc = nlp(s)
    tags = [token.pos_ for token in doc]
    pos_dict_copy = copy.deepcopy(pos_dict)
    for t in tags:
        if t in pos_dict_copy:
            pos_dict_copy[t] += 1  
    print(tags)
    # print(pos_dict_copy)
    tag_count = sum(pos_dict_copy.values())
    tag_ratio_dict = dict((k, v / tag_count) for k, v in pos_dict_copy.items())
    # print(tag_ratio_dict)
    return tags, pos_dict_copy, tag_ratio_dict

def analyse_keywords(gt, s):
    """Get # and % of keywords, order of keywords found"""
    keywords = s.split()
    keyword_processor = KeywordProcessor()
    keyword_processor.add_keywords_from_list(keywords)
    keywords_found = keyword_processor.extract_keywords(gt, span_info=True)
    order_dict = dict()
    for i in range(len(keywords_found)):
        if keywords_found[i][0] in order_dict:
            order_dict[keywords_found[i][0]] += i * 10**len(str(order_dict[keywords_found[i][0]]))
        else:
            order_dict[keywords_found[i][0]] = i
    order_list = list()
    for k in keywords:
        try:
            if order_dict[k] > 10:
                order_list.append(int(str(order_dict[k])[-1]))
                order_dict[k] =  order_dict[k] // 10
            else:
                order_list.append(order_dict[k])
        except KeyError as e:
            print('ERROR', e)
    print(order_list)
    # print(keywords_found)
    # print(len(keywords)/len(gt.split()))
    return len(keywords), len(keywords)/len(gt.split()), order_list

# Custom Yake
def kw_from_yake(text, max_ngram_size, deduplication_thresold, deduplication_algo, windowSize, numOfKeywords):
    """Get keywords from YAKE for comparison"""
    custom_kw_extractor = yake.KeywordExtractor(lan='en', n=max_ngram_size, dedupLim=deduplication_thresold, dedupFunc=deduplication_algo, windowsSize=windowSize, top=numOfKeywords, features=None)
    keywords = custom_kw_extractor.extract_keywords(text)
    return keywords

def has_oov(s):
    doc = nlp(s)
    tags = [token.pos_ for token in doc]
    intersect = set(tags) & pos_filters
    return intersect

def generate_sentences(n, min_size, max_size):
    """Randomly selects test sentences for study"""
    p = Path("amazon/test/sentences")
    sent_arr = list()
    for child in p.iterdir():
        sent_arr.append(child)
    selected_arr = np.random.choice(sent_arr, n, replace=False)
    test_sentences = list()
    for r in selected_arr:
        sent_dict = pickle.load(open(r, "rb"))
        # Filter by number of words
        sent_dict_sized = dict(filter(lambda elem: min_size <= len(elem[1].split()) < max_size, sent_dict.items()))
        idx = np.random.choice(len(sent_dict_sized), 1)[0]
        sent = list(sent_dict_sized.items())[idx][1]
        # Remove OOVs by POS
        while has_oov(sent):
            idx = np.random.choice(len(sent_dict_sized), 1)[0]
            sent = list(sent_dict_sized.items())[idx][1]
        test_sentences.append(sent.lower())
    return test_sentences    


if __name__ == "__main__":
    # Uncomment for test data generation
    test_sentences = generate_sentences(50, 6, 12)
    textfile = open("examples/amazon_test_1.txt", "w")
    for ts in test_sentences:
        textfile.write(ts + "\n")
        print(ts)
    textfile.close()
    # Uncomment for data analysis
    # p = Path("res/test.json")
    # read_json(p, n)
    pass
