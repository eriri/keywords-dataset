��K      }�(J�`
 �NContinuum is a marvelous excpetion to most "electronic-based" music out today.�J�`
 �HLong gone are the hissing, head-thumping, ear-gnawing drum synthesizers.�J�`
 ��Instead, Jenkins and Lackey have woven a tapestry of luxurious undertones and wonderfully spatious, soul-tugging rhythms that literally take one to the edge of reality.�J�`
 �0To that place just beyond our conscious reach...�J�`
 ��Indeed, this album has proven to be an indispensable engine of creativity because that of its artists shines through so brilliantly.�J�`
 �RA worthy addition to any "new age" fan, or any fan of good music, for that matter.�J�`
 �MThe views of several people are hashed out as to the alibis of all concerned.�J�`
 �AIt is a little slow to start but gets very interesting latter on.�J�`
 �I liked the book very much.�J�`
 �CInsult to the words "House Music"; Stu Allen would vomit instantly!�J�`
 �AThe Hippodrome and Hacienda would crumble if they played this cd!�J�`
 �Need I say more?�J�`
 �O.K...�J�`
 �@well, the cd is a compilation of the worst remixes known to man.�J�`
 ��They were put together by music interns at Columbia Records in a vague attempt at creativity...like putting a drumset in an Enya "song".�J�`
 �UThere is no scientific evidence that the Johanna Brandt's "Grape Cure" has any value.�J�`
 ��Even worse, her recommended diet is deficient in most essential nutrients and can cause constipation, diarrhea, cramps, and weight loss that is undesirable for cancer patients.�J�`
 �fThe only nutrients present in significant amounts in grapes are carbohydrates, potassium, and vitamins�J�`
 �A, B6, and C.�J�`
 �This book should be ignored.�J�`
 �,I used this for some research on Highwaymen.�J�`
 �/For a primer on the subject it's a decent book.�J�`
 �A little dry at times�J�`
 �Kbut it does get into great detail on Highwaymen and the legend behind them.�J�`
 �OJust to add to the warnings of many others here on this "Cassia Press" edition.�J�`
 ��Example: the book begins:"One summer evening in the year 1848, three Cardinals and amissionary Bishopfrom America were dining together in the gardensof a villa in the Sabine hills, overlook-ing Rome.�J�`
 �7The villa wasfamous for the fine view from its terrace.�J�`
 �"The whole book is like this.�J�`
 ��I'm guessing that this is a cheap OCR copy of an older edition of the book,and where there was a line break in the original, the OCR software jammed the two words together.�J�`
 �5A simplespellcheck run would've found these mistakes.�J�`
 �JAs it stands, the book goes in the trash (and I hate throwing books away).�J�`
 �This copy is unreadable.�J�`
 �-Cassia Press should be ashamed of themselves.�J�`
 �@Welcome to the age of on-demand printing with noquality control.�J�`
 �I thoroughly enjoyed this book.�J�`
 �JCather's descriptions are vivid and extensive, but to my taste not boring.�J�`
 �DI also liked the "freeze frame" style, because (this may be shallow)�J�`
 �PI have a 6 month old baby and that limits my time and attention span these days.�J�`
 �FI resisted this book at first, but got caught up in it pretty quickly.�J�`
 �FI did wish there was a chapter that fleshed out the Pike's Peak story.�J�`
 �cThis is a wonderful book detailing life in the Southwest around the time America acquired the land.�J�`
 �dCather has a nice writing style that keeps you interested throughout the life of the main character.�J�`
 � Expect to fly through this book.�J�`
 �rI attempted to read this book over the summer, but misplaced the copy not until this fall was I able to finish it.�J�`
 �=My overall opinion over the novel is a criticism on religion.�J�`
 �,If you are looking for action skip this one.�J�`
 ��What interested me is the impression I had of arrogance and ignorance of the religious Bishops to the well beings of others and especially the culture of the Indians and their beliefs.�J�`
 �FSeemingly they pay no emotional attention to others beyond themselves.�J�`
 �UWhat efforts/miracles/gifts they encounter they simply assume this is to be expected.�J�`
 �4I found it replusive to read about these Characters.�J a
 �\On a mission for God, but so much missing the point of morality and respect for one another.�Ja
 ��I felt like it was a lesson for us all to try and be more respectful of not only others but what they have to say and believe in.�Ja
 �xThe novel is not gripping like a Clancy, but it does have an elusive thought process and meaning underneath the surface.�Ja
 �2I was forced to read this book for summer reading.�Ja
 �VIf my grade had not depended on my reading this i would have burned it after 30 pages.�Ja
 �,The characters jump in and out of the story.�Ja
 �It drags on and on-�Ja
 �0i almost cried with joy when i was finally done.�Ja
 �7There is not an interesting plot or syquence of events.�J	a
 �FThis book qualifies as one of the most boreing books i have ever read.�J
a
 �FIt was a group of side stories that didn't tie in together in the end.�Ja
 �"I had to force myself to finish it�Ja
 �:so i had some idea of the plot for the test i had to take.�Ja
 �Bottom line-Waste of time.�Ja
 �MOne word in 4 is wrongly attached to another which makes the reading painful.�Ja
 �=A book like that should never have been out of the printshop.�Ja
 �(It is a shame because the story is good.�Ja
 �<It is also sadly evidence that Amazon do not check the books�Ja
 �%Purchased this book for a book study.�Ja
 �+Had been unable to find any copies locally.�Ja
 �!The book was very poorly printed.�Ja
 �QEach page had multiple words that were run together making it agravating to read.�Ja
 �%Lucked out by finding a copy locally.�Ja
 �fDid not send this one back because had thrown away all the paperwork and did not want to mess with it.�Ja
 �IVery disappointed that Amaazon would carry a product of such low quality.�Ja
 �[It is not my intention in this review to remark at any length on Cather's literary ability.�Ja
 �bInstead, I merely advise to anyone who plans on purchasing this book to purchase a different copy.�Ja
 �.This particular edition is poor in two ways.1.�Ja
 ��The only information the book offers other than the text itself is the publisher and date (Cassia Press, 2009) and that it was printed on 23 December 2009 in Lexington, KY.�Ja
 �;It fails even to give the year it was originally printed.2.�Ja
 �eMore significantly, almost every single sentence in this edition has a typo where a space is omitted.�Ja
 ��The first two sentences of the prologue, for example, read: "One summer evening in the year 1848, three Cardinals and amissionary Bishop from America were dining together in the gardensof a villa in the Sabine hills, overlooking Rome.�J a
 �7The villa wasfamous for the fine view from its terrace.�J!a
 � " It makes it difficult to read.�J"a
 �Horribly unprofessional.�J#a
 ��This is essentially a series of short stories dealing with the missionary efforts of French priests in the New Mexico territory in the mid to late 19th century.�J$a
 �PWilla Cather has pieced together a word portrait of life on the rugged frontier.�J%a
 �QIt is a mature piece of literature intended for a mature and reflective audience.�J&a
 �Book club assignment.�J'a
 �+Interesting about New Mexico in the 1920's.�J(a
 �Good caracter descriptions.�J)a
 �-Wonderful views of the high desert landscape.�J*a
 �RInteresting description of development of the religious life in a unsettled state.�J+a
 �SVery good writing, she makes the scenery of the Southwest come alive to the reader.�J,a
 �RThere really is no particular plot to this book, but rather a series of vignettes.�J-a
 �MI thought the best were at the end of the book, but I enjoyed reading it all.�J.a
 �>I never seemed to care what happened to any of the characters.�J/a
 �VIt seemed like a series of stories about persons whom I was not interested in meeting.�J0a
 �;Willa Cather is the very best at the descriptive narrative.�J1a
 �LThis description of pioneer New Mexico society is the best I have ever read.�J2a
 �HThis edition of Death Comes For the Archbishop was absolutely atrocious!�J3a
 �$It was littered with spacing errors.�J4a
 �HA sentence would look like this, "I lovedto listen to what hehad to say.�J5a
 �" It was horrifying!�J6a
 �aTo make it even more disappointing, my copy of the book was "printed" the day after I ordered it.�J7a
 �RThey are either stealing the material from some source or just being plain sloppy!�J8a
 �FCassia Press, you do a great disservice to literature and its readers.�J9a
 �What a waste of five dollars!�J:a
 �CCather's book is a wonderful book, but this edition is a nightmare.�J;a
 �It looks like print on demand.�J<a
 �2Take a look inside the first pages of the edition.�J=a
 �)You will see that the words run together.�J>a
 �It is very difficult to read.�J?a
 �Don't waste your money.�J@a
 �0Get an edition put out by a reputable publisher.�JAa
 �	Horrible!�JBa
 �HThe book, Death Comes for the Archbishop is a beautifully written novel.�JCa
 �1This particular edition has some printing issues.�JDa
 �[On every line there is at least one mistake, two words are joined together without a space.�JEa
 ��For example, on page 5, the words "a" and "missionary" are spelled "amissionary" Also on page 5, the words,"gardens" and "of" are also combined to look like, "gardensof".�JFa
 �7The weird thing is, it is always two words, never more.�JGa
 �:This mistake occurs many times, 11 in the first paragraph.�JHa
 �It makes for difficult reading!�JIa
 �OAnother thing you should know about this edition is the publishing information.�JJa
 �MThe only publishing and copyright information given is, "Cassia Press, 2009".�JKa
 �0After Googling Cassia Press, no results came up.�JLa
 �Hmmmmmmmmm.....�JMa
 �bPlease keep this in mind as you buy your copy of, Death Comes for the Archbishop, by Willa Cather.�JNa
 �LThis book has been recommended to me many times, and I have finally read it.�JOa
 �Well worth reading.�JPa
 �The language is beautiful.�JQa
 �&Willa Cather paints images with words.�JRa
 ��She has captured the splendor of the New Mexico landscape as well as the interactions of the various groups living there in the 1850s.�JSa
 �RI would love to hear this one read out loud... the imagery is so vivid and poetic.�JTa
 �VI recently purchased the Cassia Press 2009 printing of Death Comes for the Archbishop.�JUa
 �*Many sentences have words strung together.�JVa
 �Very careless typesetting.�JWa
 �EI bought the book to give as a gift, but I am going to throw it away.�JXa
 �\Willa Cather would be horrified to see what happened to her work in this particular edition.�JYa
 �?Amazon.com ought to be more careful in who does their printing.�JZa
 �sThe problem is, though, that customers cannot know in advance if the printing of the work has been carelessly done.�J[a
 �+Let's hope for higher quality next time....�J\a
 � Joseph R OrnigWaukegan, Illinois�J]a
 �X"Death Comes to the Archbishop" is one of the most moving books I have ever experienced.�J^a
 �^In many ways the American Southwest has never been protrayed better than in this Willa Cather.�J_a
 �pQuiet as the desert itself, this story evokes the all that is best about the people and the places of that area.�J`a
 �1Magical is the best word I can use for this book.�Jaa
 �0Magical in its evoking of the American Frontier.�Jba
 �9Magical in its representation of a humble servant of God.�Jca
 �(Magical in Cather's language and images.�Jda
 �Magical in its use of language.�Jea
 �One of my favorite novels.�Jfa
 �/My introduction to Cather and her unique style.�Jga
 �VWhatever happened to writers like this?Any Cather book is also a treat in audio forms.�Jha
 �\The beauty of the language is even more apparent when you can hear the words and the images.�Jia
 �Magic.�Jja
 ��I became interested in Willa Cather after watching a C-Span segment featuring Women Authors of the American West--a program sponsored by Laura Bush.�Jka
 �lI haven't read Willa Cather since high school and remember her writing as being beautiful but uninteresting.�Jla
 �GWhat a pleasant surprise to pick up this book and fall in love with it.�Jma
 ��I found the descriptions of the two French missionaries, their adventures, the people they met, and the Southwestern landscape indescribably tender and evocative.�Jna
 �RI loved this book so much that I have resolved to make it part of my home library.�Joa
 �_Cather's historical novel is as serious and as peaceful as an amber-into-rust-into-rose sunset.�Jpa
 �6The landscape itself has the richest characterization.�Jqa
 �ZShe has crafted her prose in such a way as to give the land its own vitality and presence.�Jra
 ��If you like landscape art; if you stop at Scenic Overlooks on the highway; if you ever like to walk in the woods, the hills--just because--read this novel.�Jsa
 �}Archbishop Latour, a Frenchman, has taken on the missionary work of creating a diocese in New Mexico Territory in the 1850's.�Jta
 ��Throughout his life's ministry, he encounters rogues, blackguards, ragmuffins, scalliwags, and ruthless soldiers, and then some simple saints, some lasting friends, and finally, death.�Jua
 �RWhen death finally does come, it brings his own peace, the restitution of justice.�Jva
 �HCather writes lugubriously, moving from one anecdotal story to the next.�Jwa
 �hBut it is all against the backdrop of the land; when she is finished, the tapestry of the whole appears.�Jxa
 �Nice.�Jya
 �xHaving read this book many years ago, I was anxious to read it again on my visit to Santa Fe and environs in October 06.�Jza
 �gIt was a pleasure to read Cathers prose and steep myself in the history of the places we were visiting.�J{a
 ��One became aware of the difficulties experienced by this fine French priest as he travelled the land by mule and his persistance in bringing Christianity to the people.�J|a
 �@Wonderful descriptions of the landscape and colorful characters.�J}a
 �A great re read�J~a
 �7and I would recommend it to anyone - travelling or not.�Ja
 �6It's cheap for a reason - this is a terrible printing.�J�a
 �YI don't particularly care for the size of the book, it's an awkward size for a paperback.�J�a
 �5But mostly its the typos - EVERY sentence has a typo.�J�a
 �VIt makes it very difficult to read as you are constantly jolted out of Cather's story.�J�a
 �.Spend a little bit more and get a decent copy.�J�a
 �cI read willa catcher's book because I was staying in 'her' room at a Bed and Breakfast in Santa Fe.�J�a
 �6I had read Cather's books in high school 55 years ago!�J�a
 �This was a delightful reunion!!�J�a
 �IWilla Cather speaks as though she were one of the characters in the book.�J�a
 �!Continuity of events flowed well.�J�a
 �NCharacters were developed so that one could sense how they looked and behaved.�J�a
 �$Her attention to detail was amazing.�J�a
 �KW�J�a
 �oGrowing up in New Mexico, I often took for granted the many things that make the American Southwest remarkable.�J�a
 ��Willa Cather vividly explored the vast landscape in a simple tale of two lonely missionary vicars filling the void in their lives.�J�a
 �As two unwelcomed priests, Fr.�J�a
 �Latour and Fr.�J�a
 �]Joseph, as different as night and day, tame the unbridled Catholicism of New Mexico for Rome.�J�a
 ��While merely serving out their simple roles, they begin a new page in New Mexico's history that still plays a role in daily life in 1998.�J�a
 �$I came to this book with enthusiasm.�J�a
 �IMy Antonia is one of my all time favorites and O Pioneers right up there.�J�a
 �This was a major disppointment.�J�a
 �_The characters (2) were not developed nearly as well as the characters in Cather's other books.�J�a
 �0The characters make the Ms Cather's other works.�J�a
 �0The Archbishop particularly is never filled out.�J�a
 �I found him very unsympathic.�J�a
 �5I enjoyed his Vicar and wished there was more of him.�J�a
 �aIn fact at one point I thought it would be a much better book had he been the primary charcacter.�J�a
 �iAs I neared the end of the book I came to this site to see if there was any historical basis to the book.�J�a
 �XAt first I was pleased to learn that it is based upon the lives of these two characters.�J�a
 �!Then I became disappointed again.�J�a
 �.As a history or biography it is truly lacking.�J�a
 �/I find it hard to classify this as a "classic".�J�a
 �6I find it many grades below My Antonia and O Pioneers.�J�a
 �`To read this book the mightiest feat the missionaries accomplished was traversing the geography.�J�a
 �BI was left with a feeling of "So what?" after finishing this book.�J�a
 �.Not unlike eating a bag of Doritos for dinner.�J�a
 �[To read a very very good book on very similar lines, check out "Black Robe" by Brian Moore.�J�a
 ��Others are "No Other Life" by the same author, "The Power and the Glory" bu Graham Greene; even "Monsignor Quixote" by Graham Greene exhibits more emotion than this flat tale.�J�a
 ��Perhaps, if you're not Catholic, want an overly scrubbed version of life on the frontier, and don't care about passion in the books you read; this'll do the trick.�J�a
 �,Willa Cather is a superios sytlist, however.�J�a
 �KRead the book for how she says something, and not what it is she is saying.�J�a
 �iLet me state right now, i'm a 15 year old that had to read this novel for her summer project for english.�J�a
 �;I couldn't read more than two pages without falling asleep.�J�a
 ��The story had some good parts, but Ms. Cather, in my opinion, goes into side stories that don't add anything and in the long run nothing is accomplished but the missionary dying, after making some friends.�J�a
 �4I thought maybe I am too you to appriciate this book�J�a
 �_and maybe I'll try to read it later on in life when i dont' have a paper hovering over my head.�J�a
 �\So I apologize for offending anyone, I just don't recommend it if you're an average teenager�J�a
 �kLook, there's nothing wrong with descriptive prose to create a sense of place and time but GIVE ME A BREAK!�J�a
 �fPage after page consisting of detailed descriptions of the native foliage had me bored out of my mind.�J�a
 �Save this one for bedtime.�J�a
 �WThis video presents little concrete information regarding the natives of North America.�J�a
 �_It is a shame that entire cultures are reduced to such mind numbing and unscholarly commentary.�J�a
 �hPlease review the numerous books available on this subject and enjoy something a bit more knowledgeable.�J�a
 �?There is so much more than the sound bites of this cheap video.�J�a
 �'The videos were in excellent condition.�J�a
 �_Thank you Ted Turner for not copy protecting them (1994) I Immediately copied them over to DVD.�J�a
 �&The cinematography transferred nicely.�J�a
 �,This second video of Mechelle's is awesome!.�J�a
 �I really liked the first one�J�a
 �but this one is great!..�J�a
 �oShe once again combines the important message of women's self defense techniques along with a terrific workout.�J�a
 �|The self defense scenerios in this video I found were the types of things you see women being victimized with most commonly.�J�a
 �2The techniques were deliberate and easy to follow.�J�a
 �cThe workout was very challenging, she kept a great pace and added some really cool stuff this time.�J�a
 �!Definitley not for a first timer.�J�a
 �-Get the first on first before doing this one.�J�a
 �But if you want a challenge...�J�a
 �This is it!�J�a
 �I love it..�J�a
 �keep em' coming Mechelle!.�J�a
 �HI have tried several at home tapes and this is by far the best one ever!�J�a
 �yMechelle provides useful information regarding self defense and provides a great workout to tone and strengthen the body.�J�a
 �bI'm fortunate to personally attend classes where Michelle teaches and she is truly an inspiration.�J�a
 �mI have learned the safe and correct techniques of cardio kickboxing and the workouts are challenging and fun!�J�a
 ��The introducton about women's self-defense was definitely helpful and interesting, and I found Mechelle to be very credible and able as an instructor.�J�a
 �bHowever, the video had a low-production feel to it, despite the relatively high cost of the video.�J�a
 �EAfter the promising beginning, this became just another workout tape.�J�a
 �<Wild Horses were a semi supergroup formed in the early 90's.�J�a
 �aThe band was comprised of members from several bands, including Dokken, Kingdome Come, and Shout.�J�a
 ��Though none of these bands were immensely popular, these guys would create a formidable record that should have shot them to stardom.�J�a
 �jBareback is a blues-rock oriented album, it follows the same vein as bands like Whitesnake and Cinderella.�J�a
 ��The album is solid from start to finish, songs like Burn It Up, Whiskey Train, The River Song, Tell Me Something Good and Cool Me Down are all standouts, and should have made Wild Horses a big blip on the radar screen.�J�a
 �@I recommend this album to anyone who likes 80's style hard rock.�J�a
 �XI can't see how you would be disappointed, and for the price, it's a pretty decent deal.�J�a
 �0They worked great for the first day I used them.�J�a
 �[I suppose if you are not planning on riding hard or far or often, these would work for you.�J�a
 �hFor me, I lost a nut and a bolt and broke the right clip within 30 miles of riding at about a 20mph avg.�J�a
 �)I am going to check out some steel clips.�J�a
 �!What a beautifully crafted story!�J�a
 �hIn this age of "relaxed morals"- "22 Friar Street" was like a breath of fresh air, no profanity, no sex?�J�a
 �AThis author created her characters so life-like and so relatable.�J�a
 �hShe made me want to laugh and cry and jump into the book and work the main characters' step-father over.�J�a
 �WRead how this author blends the races and unites them as one in the most loving manner.�J�a
 �\Once you read this you'll have both a renewed faith in mankind and a feeling of contentment.�J�a
 �GI've read many books by this author, and I have yet to be disappointed.�J�a
 �ZShe has mastered the craft of writing to hold her ground with the best of the bestsellers.�J�a
 ��This novel will rip your emotions apart and yet send you on a high that will never let you forget the characters and the life they lived, and how they loved.�J�a
 �zIt's amazing to think about how one person can impact another's life, but this great, great novel shows you how it's done.�J�a
 �Beautiful storyline.�J�a
 ��This is the kind of novel that surprises you with its great writing, fantastic plot, and wonderful characters with wit and passion.�J�a
 ��I cried when Peggy was burned by a molester, laughed when she was learning how to drive, and I loved all the characters...even ol' Aunt Audrey.�J�a
 �THere's a book that you ought to have on your shelves, and introduce to it to others.�J�a
 �^This writer knows how to write...and without obscenity, gratuitous sex, or anything off-color.�J�a
 �)I read this entire book in one afternoon.�J�a
 �	Go Peggy!�J�a
 �
Go author!�J�a
 �WThis is one of the best shamanic electronic music that I have heard in quite some time.�J�a
 �7It rivals the best work of Steve Roach and Robert Rich.�J�a
 �GThis CD creates a deep altered state in just a few minutes of listening�J�a
 �GBreathing in the Deep is one of the best space-ambient pieces there is.�J�a
 �DFor those times when you need to get immediate distance on a matter.�J�a
 �CRecommended particularly for difficult analytic issues of any kind.�J�a
 �
Brilliant.�J�a
 �HThis collaboration between Eugene Wilde and George Duke is simply magic.�J�a
 �VI'm not surprised this album is rare - you have to be a music connoissuer to enjoy it.�J�a
 �zListen to the quiet storm version of the title track, and the track Whenever Your're Ready, and you will know what I mean.�J�a
 �UIf you enjoy R&B from the early nineties, this belongs in your collection - no doubt.�J�a
 ��Perhaps because he was (more often than not) given truly mediocre material Eugene Wilde was unable to realize his full potential as a vocalist.�J�a
 ��While not in the same league as Peabo Bryson, Luther Vandross or Freddie Jackson Eugene was a fine and able soul balladeer who could sound quite convincing when given the right material.�J�a
 �}"Special Feelings", "You Are So Beautiful", "Whenever You're Ready" and the ultra-sexy title track were this CD's best songs.�J�a
 �This CD is overall pretty good.�J�a
 ��The music is definitely brutal and the instrumentals flow well with the vocals (which might I add are a lot better than Chris Barnes' vocals in Cannibal Corpse).�J�a
 �@I'd definitely suggest this CD to death metal or grindcore fans.�J�a
 ��This CD is just pure death metal, the production (especially on the drums) is aggresive as hell, and the vocals are as indeciferable as mortician but with a completely different style, theyre fit the music more.�J�a
 �"This CD is pure headbanging riffs.�J�a
 �AI can only picture it live, its perfect basic death metal, heavy.�J�a
 �heavy.�J�a
 �HEAVY.�J b
 �Only printed on one side.�Jb
 �But that one side is nice.�Jb
 �&How does one tie dye on only one side?�Jb
 �I'd like to see that happening.�Jb
 �7As you've probably read, the print is only on one side.�Jb
 �=But, the side that's printed on is really pretty and stylish.�Jb
 �jI bought this for my dog as a bandana around her neck and I get a lot of compliments on it when we go out.�Jb
 �5It's not super soft, but not completely rough either.�Jb
 �I would recommend it.�J	b
 �This bandana is terrible.�J
b
 �"It looks nothing like the picture.�Jb
 �ZInstead of being "tie dye" it is a blotchy mess of light pink, sky blue, and light yellow.�Jb
 �?It seems like it was made for girls, but that's just my opinion�Jb
 �II'm of average height (5'8") but this "tunic" doesn't have enough length.�Jb
 �?It doesn't reach the top of my legs and the arms are short too.�Jb
 �,I ended up giving them to my shorter friend.�Jb
 �"So if you're over 5'5", don't buy.�Jb
 �Material is nice though.�Jb
 �cSearched for this CD for a friend, looking for "The Girl from Ipenema" I hope the gift was enjoyed!�Jb
 �UThis was a good book until I started to think about what was happening in the "past."�Jb
 �hHe marries his ancestor and has a child with her who will grow up to be another of the hero's ancestors.�Jb
 ��Am I supposed to think how romantic it is that when he comes back, he is nicer to his wife, who appears to be a reincarnation of his g grandmother?�Jb
 �TI agree with all the negative comments about Nikki loving Emelia and maybe not Emma.�Jb
 �\Emma doesn't feel as if it's from a love of long ago reincarnated, even though Nicolas does.�Jb
 �How unfair to Emma.�Jb
 �uAnd if the vision of Nicolas is true, then did he sire a son who down the line will be him several generations later?�Jb
 �4Did any one think of that as really, really strange?�Jb
 �I just didn't enjoy this book.�Jb
 ��Emma seemed spoiled and childish and Nickolas was your typical wounded mysterious alpha male and they didn't seem well suited at all, other than both wanting their own way.�Jb
 �wThere really wasn't any character developement for either of them and it just became a painful, irritating read for me.�Jb
 �4I disliked both characters and hated the story line.�Jb
 �LWhy did he have to travel into the past to learn about love from a relative?�J b
 �1Too much mumble jumble cluttering up the romance.�J!b
 �.Try her other book "Someone To Watch over me."�J"b
 �"I was disappointed with this book.�J#b
 �dI couldn't stop thinking that the hero was involved in an incestuous relationship with his ancestor.�J$b
 �/I am also not a big time travel romance reader.�J%b
 �;Shouldread both Stokehurst books,this one being the second.�J&b
 �0The Russian parts were particularly fun reading.�J'b
 �3Would recommend to those who like European history.�J(b
 �BSo I am to the "dream" sequence, and I just got bored really fast.�J)b
 �.I don't know if I will finish the book or not.�J*b
 �dI did really like Nikolas and his dark side at first, a redemption storyline is one of my favorites.�J+b
 �-He really was heading for the big redemption.�J,b
 �He is a super bad boy.�J-b
 �VAll of the bad stuff he did I was okay with, until he cheated and that was a turn off.�J.b
 �HThat, in my opinion, crosses the line and true trust can never be again.�J/b
 �bI read Because You're Mine (I have read this book twice and I very rarely ever read a book twice.)�J0b
 �'and I fell in love with Lisa's writing.�J1b
 �UI have read a lot of her books since, but this one doesn't hold a candle to the rest.�J2b
 �=I am a big fan of Kleypas and have read almost all her books.�J3b
 � This one is not one of the best.�J4b
 �(The book was promising at the beginning.�J5b
 �&A tortured hero primed for redemption.�J6b
 �TBut *SPOIL ALERT* the back in time part of the book destroyed the flow of the story.�J7b
 ��You can pretty much skip those 3 chapters with the following take away "Nicholas remembered what it was like to love someone" and move on to the last part of the book.�J8b
 �cThe book would have been better had the "learning to love someone" part taken place in the present.�J9b
 �`Having said that, I still think this book is better and more engaging than your average romance.�J:b
 �CI was desparate for something to read on a trip and picked this up.�J;b
 �fI was under the impression that this was a time travel book and was not very excited about reading it.�J<b
 �But I was pleasantly surprised.�J=b
 �I really enjoyed this.�J>b
 �+Lisa Kleypas is one of my favorite authors.�J?b
 �NWhile this was a good book, my favorit book by her is still "Dreaming of You".�J@b
 �:I did not like this book at all, and I certainly tried to!�JAb
 ��An avid Kleypas fan, I was eagerly anticipating this novel, but found it hard to wade through, follow, and got completely confused during the flashback section.�JBb
 ��While Emma and Nikki are entertaining as the central characters, their relationship and dialogue was not enough to make this a positive experience for me.�JCb
 �~This novel certainly follows a different formula from Kleypas's other books and I would only recommend it to her devoted fans.�JDb
 �gI absolutely loved the first 2 parts of this book, it was on it's way to becoming a classic in my eyes.�JEb
 �GThen the author had to go and do the whole travelling back in time junk�JFb
 �and it just stunk after that.�JGb
 �@I was so sad that my wonderful book took a turn to the darkside.�JHb
 ��When he goes back in time it's almost like A Christmas Carol where he's shown his past and then wakes up reformed and wants to mend his ways.�JIb
 �dI didn't like that he slept with Emelia even though he thinks it is Emma several generations back...�JJb
 �it's still not her.�JKb
 ��This is the first time I was dissapointed reading a Lisa Kleypas novel but it won't keep me from reading all of her other books.�JLb
 �JI own all of Lisa Kleypas books and this one was the last one I purchased.�JMb
 �/I was actually looking forward to reading this.�JNb
 �#However,was extremely disappointed.�JOb
 �qI have to agree with some of the reviews that the dream and sudden turn-around in Nikki's behaviour were strange.�JPb
 �.Does he ever really love Emma or is it Emalia?�JQb
 �ZHis behaviour towards Emma and his son prior to the dream was disturbing to say the least.�JRb
 �_I had great expectations when I started reading the book but had great difficulty finishing it.�JSb
 �iThese two books with the Russian characters were without a doubt my least favorite ones of all her books.�JTb
 �TThat said I think Lisa Kleypas is a very talented writer and I truly enjoy her work.�JUb
 �EThe wallflower series is great and I really loved Mine till Midnight.�JVb
 �+I am looking forward to her upcoming books.�JWb
 �RI am a total beginner and find this book frustrating, even working with a teacher.�JXb
 �5I like the Hal Leonard 'Guitar Method Book 1' better.�JYb
 �RI sometimes teach guitar, and I am always interested in finding good method books.�JZb
 �IMany of the reviews here claim that this method is "the best" and "great.�J[b
 �(" I purchased it based on these reviews.�J\b
 �*I found this method to be average at best.�J]b
 �LThere is nothing remarkable about the songs or the way it teaches technique.�J^b
 �iAlso, be aware that this book does not include the accompanying CD; perhaps it can be ordered separately.�J_b
 �bIf this book included the CD for the same price, I might be willing to recommend it to a beginner.�J`b
 �"Okay, this is getting out of hand.�Jab
 �SMy girlfriend's daughter started lessons, and the teacher said to go buy this book.�Jbb
 �So I pick it up WITH THE CD.�Jcb
 �Oh no giant mistake.�Jdb
 �;Each track on the cd (gee, only 150 tracks) has no "count."�Jeb
 �Useless.�Jfb
 �Guess when the track starts.�Jgb
 �Now?�Jhb
 �How about now?�Jib
 �Oops, you missed it.�Jjb
 �What a hunk of junk.�Jkb
 �A waste of $20.�Jlb
 �""Corporate" America at its finest.�Jmb
 �6I especially like to use this scope for lunar viewing.�Jnb
 �FMy family and neighbors often join me for a look around our local sky.�Job
 �2Favorites are the Orion nebula and Saturn's rings.�Jpb
 ��Not the most spectacular device, but worth hours of fun (even without an automated "go to" system!)Short tube makes it easy to "toss" into the car for travel.�Jqb
 �MThis was a fun and well received "recognition" gift in our shop here at work.�Jrb
 �RIt works well and is beneficial here on the job and for use in our everyday lives.�Jsb
 �This is a product to pass up.�Jtb
 �0The light activation button is difficult to use.�Jub
 �<It slides out of position where it won't activate the light.�Jvb
 �>When you do get it aligned it doesn't illuminate consistently.�Jwb
 �1It flickers on and off making it annoying to use.�Jxb
 �9It is compact but the annoyances are not worth the money.�Jyb
 �ESpend you money on something more reliable like a keychain LED light.�Jzb
 �2I bought three of these for friends and NONE work.�J{b
 �-The light worked for about 10 seconds on one.�J|b
 �zThen when we pressed the light button again (gently I might add), it made a clicking noise and the light no longer worked.�J}b
 �1The other two were duds right out of the package.�J~b
 �Excess packaging too!�Jb
 �V(If the website would let me give this item NO STARS, I would have chosen that option)�J�b
 �Too tacky--not worth it.�J�b
 �^The led is too small to truely illuminate and it is not focused on the area being illuminated.�J�b
 �BI, too, thought I was going to recieve a pair of them but did not.�J�b
 �RThe magnifier works fine, but you can get a thinner card to do that for less cost.�J�b
 ��I also bought the "Fulcrum LED 2-Pack Card Magnifiers" which is a little over 1/4 inch thick but works, is lightweight, and contained 2 of them, as advertised.�J�b
 �!Bought one for my dad and myself.�J�b
 �WHis worked mine didn't (it was too tempermental - you had to have it aimed just right).�J�b
 �.I purchased the owl lite from ads I saw on TV.�J�b
 �>It is advertised as credit card sized, but that is misleading.�J�b
 �aIt is in fact the shape of a creidt card, but it has the thickness of three credit cards stacked.�J�b
 � That is too large for my wallet.�J�b
 �|It works as advertised so I may throw one in my gym or computer bag, but i can't put it in my wallet which is what I wanted.�J�b
 �JThe light was a little confusing at first, you have to push the button up.�J�b
 �2But it works well and does what it is supposed to.�J�b
 �4The shipping was a lot for such a tiny thing though.�J�b
 �\To me it seems like it is small enough that it could have been shipped in a normal envelope.�J�b
 �Overall very happy with it.�J�b
 �CThis is an outstanding product for its portability and ease of use.�J�b
 �yBut the reason it does not get five stars is based on the fact I was led to believe that I would get a second one 'free.'�J�b
 �.Yeah, except for $13.98 shipping and handling!�J�b
 �That is a heck of deal, huh?�J�b
 �(For a product that is about ten dollars?�J�b
 �RIGHT!�J�b
 �tToo much in this book is probably true, even tho I'd like to believe these stories of hellish roommates are made up.�J�b
 �}Every few pages I burst out loud laughing about a character or episode that totally echoes the reality some of us have lived.�J�b
 �(Only four stars because it is too short!�J�b
 �`Could have done a lot more with the personalities that drift into and out of the main narrative.�J�b
 �Worth finding...�J�b
 �#can't wait to check out the sequel!�J�b
 �Depends what your looking for.�J�b
 �^It's not a novel in the standard sense, each chapter is like a completely separate "incident".�J�b
 �#Sometimes they're thinly connected.�J�b
 �7The "incidents" are fairly random, some are quite funny�J�b
 �but mostly they're average.�J�b
 �NIf you're looking for some gross humor stories then you'll get a kick from it.�J�b
 �Otherwise, pass on by.�J�b
 �.There's something for everyone in this volume.�J�b
 ��Virtually every job search experience possible is found here -- everything from whether or not to establish a gender identity to whether a tweed skirt is appropriate attire.�J�b
 �ZA must-read for ANYONE involved with the Academy, whether searching for a position or not.�J�b
 �Do not read this book.�J�b
 �(It's the worst sort of academic writing.�J�b
 �lI have a PhD and I know that the market is tough, but this book contains no helpful advice, no useful ideas.�J�b
 �?It's simply a collection of laments by self-absorbed academics.�J�b
 �!This is the absolute worse album.�J�b
 ��Her voice, the music, the production values, and everthing else about this recording are terrible, except of course the songs themselves.�J�b
 ��Tom Waits is one of the all time best songwriters in the entire history of music and Holly Cole should be ashamed of herself for mutilating so many of his classics.�J�b
 �vI can't believe so many people on this site have praised this album - they must have never heard any of the originals.�J�b
 �XDo yourself a favor and buy absolutely any Tom Waits album you can find instead of this.�J�b
 �PI heard this album for the first time when reviewing some high end loudspeakers.�J�b
 �9I bought the speakers and i think it was Ms Cole's fault.�J�b
 �fFrom the first track, her - so laid back I take valium to get high - voice was almost trance-inducing.�J�b
 �mTrack 2 of Temptation was even more irresistible and the music did not release me until the end of the album.�J�b
 ��This is not fanboy talk; I hadn't even heard OF Holly Cole before this and I am not a huge fan of her other albums - purchased subsequently.�J�b
 �1But Temptation is a truly unique shade of mellow.�J�b
 �Magic.�J�b
 ��This album is 100% Tom Waits songs; however, you would never know it since you'd need an excellent pair of glasses to have known.�J�b
 �#Tom sings the songs so much better.�J�b
 �&Some might call Cole's rendition Jazz.�J�b
 �How poor, and how boring.�J�b
 �-I have owned Don't Smoke in Bed for 12 years.�J�b
 �5When I wore the cassette out I replaced it with a cd.�J�b
 �BI really love the simplicity and earthiness of Holly Cole's voice.�J�b
 ��Because I liked EVERY song on the other album, I bought this without first listening to it; as a result, I was sadly dissapointed.�J�b
 �EI would have rated it lower, but there are a couple good songs on it.�J�b
 �8No, it's not a 5-star recording, but it is a fine album.�J�b
 �qI was excited about hearing these Waits songs stand on their own, divorced from his singular style of performing.�J�b
 ��But in this album, some are dressed in ill-fitting clothes, and when things go flat it's not clear if the song is to blame, the arrangement, or Cole's performance.�J�b
 �eStill, the cover of "Jersey Girl" is over-the-top, 5 stars, worth the price of the album right there.�J�b
 �/And "I don't want to grow up" isn't far behind.�J�b
 �~I was very disappointed in the quality of this chain...basically it doesn't look at all like real gold, it looks totally fake!�J�b
 ��I returned it since I expected something better, only to then find out that the gift certificate I used on the order was not valid any more and could not be used for another purchase.�J�b
 �)And had to pay for the shipping back too!�J�b
 �,So definitely won't be buying jewelry again.�J�b
 �;I find the series of Goodman's Five Star Stories excellent.�J�b
 �1Most of the stories are inviting and interesting.�J�b
 �>This is my second book by this author and I do like her style.�J�b
 �UThis was a little more unpredictable then the first one I read, "Her Mother's Shadow.�J�b
 �N" I liked this story because you are actually reading two stories at one time.�J�b
 �4It's like reading two love stories at the same time.�J�b
 �iThe plot is a little far fetched but it still really held my interest and I found it hard to put it down.�J�b
 �I read it in one day.�J�b
 �CI recommend it to anyone who just wants a book you can get lost in.�J�b
 �<I always buy Diane Chamberlain books as soon as they're out.�J�b
 �She's a guaranteed good read.�J�b
 �RThis book has a fascinating premise and an intriguing past/present dual-narrative.�J�b
 �8I sort of guessed the ending, but it was still powerful.�J�b
 �I admit it, I cried.�J�b
 �:)�J�b
 �This was an excellent book.�J�b
 �7My first to read by Diane Chamberlain, but not my last.�J�b
 �There were so many twist in it.�J�b
 �%Surprises throughtout the whole book.�J�b
 �JAnyone who likes Mystery/Romance will not be dissappointed with this book.�J�b
 �5Enjoyed the book so much, just could not put it down!�J�b
 �4Such a good story line and factual and felt so real!�J�b
 �HI like romances -- and novels in general -- about the dark side of life.�J�b
 �;Unfortunately, this novel went overboard with the darkness.�J�b
 �RIt included suicide, Alheimer's, alcoholism, and dark medical secrets of the past.�J�b
 �5That said, the parts set in the past were compelling.�J�b
 �,The author made wonderful use of flashbacks.�J�b
 ��If the present-day segments had been as good, and if the hero and heroine had been more likeable, I would have liked this book a lot more.�J�b
 ��(Hint: He takes way too long to recognize his true feelings.)By the way, don't read the Author's Note at the beginning before reading the book.�J�b
 �JIt contains BIG-TIME SPOILERS.I gave this novel a D+ at All About Romance.�J�b
 �%An entertaining, multi-layered story.�J�b
 �PFive year old Emma has gone mute after witnessing her adoptive father's suicide.�J�b
 �`Her mother, Laura, enlists the help of Emma's biological father to help Emma through the trauma.�J�b
 ��Laura is also looking into a mystery: why did her own father make a deathbed request that Laura help an unknown elderly woman in an Alzheimer's care facililty?�J�b
 �yThere are lots of flashbacks as the older woman tells of her past, and it is slowly revealed how she knew Laura's father.�J�b
 �IThis novel includes a fairly predictable romance, but it's a sweet story.�J�b
 �$Diane Chamberlain never disappoints!�J�b
 �5I love all of her books and this one is no exception.�J�b
 �7I read this book in 3 days and did not want it to end!!�J�b
 ��This book was an easy read for me, a good mystery about a woman, Laura, who promises her dying father that she will visit with an older woman named Sarah, even though she doesn't know her at all.�J�b
 �UOnce things are revealed through her visits with Sarah, Laura's life changes forever.�J�b
 ��This book also tackles the story of Laura's daughter, Emma, who witnesses Laura's husband's suicide and then refuses to speak afterwards.�J�b
 ��It is an interesting story line with the addition of Emma's real father, Dylan, who becomes an important figure in all of their lives.�J�b
 �&I enjoyed the plot and the characters.�J�b
 �0This is the first Diana Chamberlain I have read.�J�b
 �7I was persuaded to try her after reading other reviews.�J�b
 �IShe writes well but the plot is ludicrously far fetched and unbelievable.�J�b
 �#An insult to a normal intelligence.�J�b
 �"I shall not read any further works�J�b
 �SPOILERS AHEAD!!Um.�J�b
 �Please.�J�b
 �KA drunken one-night stand just agrees after years and years to be a father?�J�b
 �7And tend to a bunch of people in need of major therapy?�J�b
 �$And they all magically fall in love?�J�b
 �Huh.�J c
 XI  The therapist spends all her time telling the mother how crappy her dead husband was, and encourages her to go off on wild-goose chases that have nothing to do with the reason for the therapy, and only addresses the serious traumas a child experienced by saying something to the effect that the child will eventually get over it?�Jc
 �Huh.�Jc
 �OAll the big SURPRISE SHOCKERS were way too predictable in this one, by the way.�Jc
 �"And just way too dang far fetched.�Jc
 �UBummer because I've read works by this author in the past and liked them pretty well.�Jc
 �
This book?�Jc
 �Not so much.�Jc
 �)I purchased the product and installed it.�Jc
 �'Ran an image backup and it seemed fine.�J	c
 �[Next I tried backing ups certain Quicken Data files and folders and could not restore them.�J
c
 �hCalls to Symantec technical support go half way around the world and they never get back with an answer.�Jc
 �bWhen I call daily to check on the status, they continue to say that they are "still working on it.�Jc
 �"Buy at your own risk.�Jc
 �qAfter installing Norton S&R;, I backed up 182GB of data from a 300GB internal C: drive to a 300GB external drive.�Jc
 �It went well.�Jc
 �EI checked the following day and found the auto backup went well, too.�Jc
 �9Confident that all was well, I ignored it for a few days.�Jc
 �mWhen I checked a few days later, I found out that S&R; thought my destination drive was full after 3 backups.�Jc
 �,But, Windows reports it had over 100GB free.�Jc
 �.That's not what ticks me off the most, though.�Jc
 �fWhat ticks me off the most is that S&R; DIDN'T TELL ME that every daily backup failed for over a week.�Jc
 �9It lulled me into thinking I was backed-up when I wasn't.�Jc
 �QI think that borders on irresponsible!I sent an e-mail to "Support" and got none.�Jc
 �2I don't think the support tech knows what a GB is.�Jc
 �dWhat a dunce!I tried to uninstall and it wouldn't give up the space it claimed on my external drive.�Jc
 �3I had to reboot in safe mode to reformat the drive.�Jc
 �;Not only shouldn't you buy this POS, it should be outlawed!�Jc
 �0I installed NSR on my new windows XP pro system.�Jc
 �SAfter installing all software I backed up my system using NSR and it went smoothly.�Jc
 �[After that I tried to create backups for select folders which turned out to be a nightmare.�Jc
 �@It seems you can not include same file in two different backups.�Jc
 �"Backing up on DVDs is problematic.�J c
 �#Error messages are not descriptive.�J!c
 �0There aren't any options to recover from errors.�J"c
 ��And whenever I restarted the job, it seemed to me, NSR just assumed that the files were protected by the previous run and finished without doing any work.�J#c
 �GI have spent a whole day and I haven't been successful to make it work.�J$c
 �(This program failed an elective restore.�J%c
 �6Crashed on first test and my hard drive is unbootable.�J&c
 �TSymantec no help but will answer the phone fpr $69.95.Get GoBack, at lease it works.�J'c
 �iNorton Save and Restore installed automatically without giving me the options of how I wanted it to work.�J(c
 �0Now it is on my desktop (where I don't want it).�J)c
 ��It slowed down my email, while it checks everything for virus, it would not install without demanding that I install another Microsoft program over the internet.�J*c
 �2It slows down everything and clutters my computer.�J+c
 �I erased it.�J,c
 �UPowerquest has a product called "drive image" that I have used in the past and liked.�J-c
 �/I'm going to check it out for a version for XP.�J.c
 ��While it is becoming a rule that in every new album that pop-divas are recording only two-three tracks are worthy some attention while the rest are a pure junk, this new album from Ornella Vanoni one could listen without skipping between tracks.�J/c
 �fAll songs are excellent, from rather nostalgic "Vendo a Casa" to "Casa Mia", inspired by African beat.�J0c
 ��Vanoni demonstrate her incredible vocal mastery in "Una Ragazza in Due" when she sings from low to high pitch in a single breath with poise that gives a goosebumps.�J1c
 �cEven people who do not understand Italian will appreciate this excellent album from Italian singer.�J2c
 ��I can't imagine any EBTG fan not getting this album, and tho there are no standout tracks, over 60 minutes of mixes make it well worth your dollar.�J3c
 �<I've become a fan of EBTG's music after first hearing Wrong.�J4c
 �nSince then I've found that their 12" remixes are usually just okay unless you're heavy-heavy into drum-n-bass.�J5c
 �nThe producers of this 12" seemed to steer away from DnB this time and go for more generic quasi-housey sounds.�J6c
 �"EXCEPT #6, the Ananda Project Mix!�J7c
 ��I love it cause it is totally different from the original form of Temperatmental, it has deep-deep pounding almost-calyso bass, southwestern acoustic guitar, what sounds like African chanting and calls and it's over 10 minutes long.�J8c
 �RI don't even bother with the other mixes though #8 Da Deepah Dub works for me too.�J9c
 ��If you liked the Dave Wallace remix of Walking Wounded or Kevin Yost's Five Fathoms remixes you'll absolutely LOVE the Ananda Project mix by Chris Brann.�J:c
 �,Be sure to turn your bass up for this track!�J;c
 �1Also worth mentioning is #5 Wamdue Project remix.�J<c
 �@At over 14 minutes, it's a an ETBG overture perfect for driving.�J=c
 �Thanks.�J>c
 �Very good track from EBTG.�J?c
 �@The most notable remix is the Hex Hector-Mac Quayle (HQ2) remix.�J@c
 �It's just so intense.�JAc
 �0The Ralphi Rosario remix is not that bad either.�JBc
 �7It's a good single to add to any club remix collection.�JCc
 �`Goodall's music on this CD is energizing while lyrically rolling you from one piece to the next.�JDc
 �cI found it was perfect as an accompianment to many tasks in my office -- it soothes, yet energizes.�JEc
 ��I first heard it in Sodona, Arizona, and it reminds me of soaring heights, majestic rock formations and the South West's diverse terrain.�JFc
 �+The flute, bells and percussion are superb.�JGc
 ��I was a big GP fan back in the early days, but like a lot of people, I started to lose interest around the time of Another Grey Area and The Real Macaw.�JHc
 �cI totally lost track of him for twenty years, until 2003, when I got a bug to hear Life Gets Better�JIc
 �
Yet again.�JJc
 �SI ordered the Master Hits CD from Amazon, and then found a Best of..at the library.�JKc
 �I was hooked all over again.�JLc
 �fI've obtained every GP album out there within the last year, and have obsessively listned to them all.�JMc
 �AI wonder what in the world I was thinking when I "lost interest."�JNc
 �+Your Country fits smoothly into his ouevre.�JOc
 ��Although there were a few songs on last year's Deepcut to Nowhere that I like better, this is overall the stronger collection, one of his best.�JPc
 �|(As an aside, one album that doesn't get any attention on Amazon is Loose Monkeys, which is only available from Razor & Tie.�JQc
 �@It's a collection of great songs that never made it onto albums.�JRc
 �Give it a try, too.)�JSc
 ��As someone who has heard virtually all of Graham's work over his whole wonderful career, I can tell you that, in my opinion, this is as poor a collection of songs that he has ever released.�JTc
 �CThe only comparable lowpoint is "Human Soul", which is just as bad.�JUc
 �PI listened to it over and over, hoping it would get better, but it never does. "�JVc
 �\Deepcut to Nowhere" is a fantastic collection, every cut being better than anything on here.�JWc
 �QBut, he'll be back with some better material next time, if he stays true to form.�JXc
 �YHe even ruins a great song, "Crawling from the Wreckage" which tells you how bad this is.�JYc
 �5There is a blunt side and a sharp side to these nips.�JZc
 ��On the first use I was pulling nails out of a timber frame, and sharp side of the pliers just shattered leaving a quarter inch chip in the sharp side.�J[c
 �qAfter one days use they no longer cut nails and when you are pulling nails it tracks directly back into the chip.�J\c
 �#Buy Knipex, costs more but worth it�J]c
 �VI have yet to understand why this product received such a good review from the editor.�J^c
 �It's not easy to use.�J_c
 �KThere is no natural flow and progression when you attempt to make projects.�J`c
 �^Not to mention the layout of the design screen is elementary and not very pleasing to look at.�Jac
 ��If you use this product make sure that your pictures are in folders that aren't more than one folder deep on your hard drive or you won't be inputting them easily into any projects.�Jbc
 ��This product has caused me nothing but irritiation it feels like it is more suited for Windows 98 than Windows XP, I refuse to believe this product was designed and produced since computers came in the 75Mhz variety.�Jcc
 �,Save your money and use it on something else�Jdc
 ��The editing portion of the software is completly separate from the CD portion of the software and does not store in the same file.�Jec
 �vCD's can hold upto approx 500 photos but the softwware will not accept more and allow you to discard down to burn 500.�Jfc
 ��I was told by Nova Development that the merging of photo shows was not being supported by the company since the CD writer was a free add on to the program.�Jgc
 �/The box and program info did not indicate that.�Jhc
 �HI offered to purchase the full program but it is not available anywhere.�Jic
 �RPhoto Explosion Deluxe is easy enough to use but won't print to printer correctly.�Jjc
 �zHave a canon s900 that prints beautiful photo prints but after editing with photo explosion My print quality is very poor.�Jkc
 �Very limited photo touch up.�Jlc
 �PWhen it comes time to burn you might as well have customer service on the phone.�Jmc
 �6Pretty much a total piece of crap!!!!!!!!!!!!!!!!!!!!!�Jnc
 �1This is the worst company I have ever dealt with.�Joc
 ��They offer 30 day trial on this product, I wasn't happy and mailed it back to them in 5 days of purchase and I'm still waiting for the refund.�Jpc
 �MI have called them 3 times, it has been almost 4 months now without a refund.�Jqc
 ��I would never buy a Nova product from this company again and I would recomend everyone going somewhere else to get good customer service.�Jrc
 �>I needed a stubby adjustable wrench that would open to 1 inch.�Jsc
 �"This is the only one I could find.�Jtc
 �>The quality is poor, but it's better than not having the tool.�Juc
 �KI guess that "you get what you pay for", and this wrench was not expensive.�Jvc
 �NIt's amazing how people will sob that "this doesn't sound like Giant Steps..."�Jwc
 �cso therefore it's bad?I think Steller Regions is quite different, but very lyrical and very pretty.�Jxc
 �1Just listen to Seraphic Light or Stellar Regions!�Jyc
 �,John Coltrane's playing is sweet, not angry.�Jzc
 �:And Alice provides the perfect, colorful backdrop for him.�J{c
 �8Rashied Ali too plays with remarkable restraint as well.�J|c
 ��There are a few moments (Tranesonic) where your eardrums will explode, but on a whole, Stellar Regions is quite calm and meditative.�J}c
 �rDon't be fooled by the closed minded fools who wrote in with 1 or 2 stars just because the album sounds different.�J~c
 ��Sure, you won't be snapping your fingers or tapping your feet... in fact, you'll have trouble sometimes identifying any sort of traditional rhythms.�Jc
 �]But music doesn't always have to adhere to the traditional, white, european rules to be good.�J�c
 �Be adventurous.�J�c
 �Get the album.�J�c
 �rThis recording session was one of the last for John Coltrane, an unparalled giant of jazz soloing and composition.�J�c
 �ZHis life ended (1967)during his experimentation with the "new thing" or the "avant-guarde.�J�c
 ��" At this point, he was usually playing with tenor man Pharoah Sanders, whose tonality was sometimes unendurable, painful--as hard as I've tried to appreciate it over the years.�J�c
 �'However, Sanders is absent on this gig.�J�c
 �"I must say I am thankful for that.�J�c
 �QThe pieces tend to be more mellow, even serene, than the usual fare with Sanders.�J�c
 �&There is freedom, but there is melody.�J�c
 ��There are some hauntingly beautiful tunes and some moments of utter intensity, as when Trane and Ali play a duet for about three amazing minutes--similar to what happened on "Interstellar Space.�J�c
 �O" The feeling and depth of Trane's playing here are immense and transfixing....�J�c
 �fMy rating to this CD is between 3/4.Stellar Regions brings back the old memories and leaves me cold...�J�c
 �LSeraphic Light reminds me of Spiritual,Ole, Om Kulu se Mama and many others.�J�c
 ��In spite of his short career, he had more to offer musically, most importantly he was at his best when he partnered with Miles Davis.�J�c
 �@His music touches many hearts and influenced many young artists.�J�c
 �nI will shortly be buying The Best of Mark-Almond, Blue Mitschell's Bantu Village & Cyrus Chestnut/Anita Baker.�J�c
 �~This music explores the harsher tones of Trane's horn and his views on this world as he died, and it is quite confrontational.�J�c
 ��The urgent vibe is bolstered by the mucho caliente poly-rhythms courtesy of drummer Rashied Ali, but fans of Coltrane's classic melodic and groundbreaking mid 60's work need to be cautious of this troubling music.�J�c
 �2I recently borrowed this CD from my local library.�J�c
 �HWhereas, Mr. Coltrane is a very talented saxaphone player, the songs(and�J�c
 �PI use that term extremely loosely here) are not very memorable to say the least.�J�c
 �lI can not understand for the life of me how one of the editorial comments can say that this work is focused.�J�c
 �So many of tracks just fly off on wild instrumental breaks that you really wonder the mental state of Mr. Coltrane at the time.�J�c
 �PIris and Stellar Regions are probably the most memorable tracks on this release.�J�c
 �eThe keyboard work on these particular tracks nicely compliments some of Coltrane's saxaphone soloing.�J�c
 �~However, there are times when the sounds from Mr. Coltrane are so disonant that it sounds like he is murdering the instrument.�J�c
 �=This is mostly for completists and die hard fans of this man.�J�c
 �<I would not recomend this album unless you are a completist.�J�c
 �5It will be a little bit too far "out there" for most.�J�c
 �Do Not Add To Shopping Cart�J�c
 �|Movie Mars advertised the movie as closed captioned so, since I am hearing impaired, I took their word forit and ordered it.�J�c
 �9It was NOt closed captioned so I feel I wasted the money.�J�c
 �oI was unable to understand thedialogue but was able to tell the movie wasnt the masterpiece I had heard it was.�J�c
 �Not bad though by any means.�J�c
 �oI probably would give it a higher rating if it had been closed captions as Movie Mars led me to believe it was.�J�c
 �UI really didn't think this movie was appropriate, especially under a christian label.�J�c
 �TI know-Adam and Eve and all, but I've seen porno's with less nudity than this movie.�J�c
 �PIt makes eating the apple seem like a SACRIFICE compared to the rest of the sin.�J�c
 �@Tracks 1 and 10 are great, the rest of the album is just boring.�J�c
 �!Betrayal was a much better album.�J�c
 �r"Double your Sales" is a quick read, with examples of verbage to use to draw out clients or prospects into buying.�J�c
 �MMr. Walker is convinced that people won't buy "unless it FEELS right to them.�J�c
 �" A good additon to your arsenal of sales books, but for 100 pages, it's hard to justify the high price tag for this paperback.�J�c
 �)The same material can be found elsewhere.�J�c
 ��Sid Walker's book hits the mark when it comes to how agents/producers can increase their sales by asking a few, RIGHT questions.�J�c
 X`  Having been in the insurance industry for over 20 years, I have seen too many agents walk past a sale that could be a lifelong policy for their clients, if only they would understand the true value of life insurance - helping their clients fulfill their dreams - whether around to fulfill them in person, or through the legacy funded by life insurance.�J�c
 X2  Aren't the client's dreams worth the agent's effort and taking the risk to uncover the client's feelings?Getting at the heart of why a client would want the fulfillment of their dreams should not be something to avoid - quite the contrary - it is something to be embraced and used to its fullest potential.�J�c
 �)It is a win-win for the agent and client.�J�c
 �Great job, Sidney!�J�c
 ��Well, I'll try not to make this a biased comment, only because I'm a huge HIM fan, and their music is probably the most down to earth stuff my ears get to catch.�J�c
 �`The acoustic version of "The Funeral of Hearts" is incredible, along with "Buried Alive by Love.�J�c
 ��" You can really hear Vallo giving his all to both of those, and not to mention the Apocalyptica hauntings in "Gone With the Sin."�J�c
 �dDefinitely worth buying, and a must-have for anyone who liked even one song of His Infernal Majesty.�J�c
 �This cd was pretty good.�J�c
 �[I have to disagree with the other review about the vocals not being good, because they are.�J�c
 �3I think its nice to hear a song in a different way.�J�c
 �9One of the best tracks is the "Salt in our Wounds" remix.�J�c
 ��It took forever for HIM to be released in the US, so now I think they are trying to get new fans to listen to their older tracks.�J�c
 �UIf you like the first couple of albums, then you will probably like this one as well.�J�c
 �cWell for starters I'm a Very big HIM fan, but not just of HIM, but of Gothic rock and Finish music.�J�c
 �5So I guess it's hard to say, but this was a let down.�J�c
 �#Not because of quality of tracks...�J�c
 �but the selection used.�J�c
 �=Most of these can be found on the Singles collection box set.�J�c
 �FWhich includes the vast majority of these and MANY other great tracks.�J�c
 �5So if you can drop the extra $$$, go for the box set.�J�c
 �7The Rock version of Beautiful is worth the price alone.�J�c
 ��Aunque el titulo del disco diga que es dificil de oir nada mas lejos de la realidad, versiones perfectas y suaves goticas de los grandes exitos de nuestra�J�c
 �His Infernal Majestic�J�c
 ��I really have to disagree with the first reviewer, the vocals were awesome, and if you know anything about playing guitar and enjoy listening to it without a hard beat in the background this is the cd for you. "�J�c
 �mBuried alive by love" was the one coolest acoustic tracks I have heard to date, and "its all tears" was epic.�J�c
 ��While the majority of the 15 tracks were solid,there were some disappointments, such as "Join Me", which sounded exactly the same as the original, but overall it was a good album, although a bit different from HIM's usual fair.�J�c
 �bI thought I knew what to expect with the condition of this book but was surprised when it arrived.�J�c
 �iIt was worse than imagined & I'm sure the friend I gave it to thought I had retrieved it from the 'dump'.�J�c
 �<We liked the contents of the book & would give it 5 stars!!i�J�c
 �bFor librarians looking for a good general overview of library acquisitions, I recommend this book.�J�c
 ��The authors have produced a book that can be useful for new acq folks and library generalists who are looking to gain a better understanding of acquisitions.�J�c
 �HThis product was shipped to me after it had been open and possibly used.�J�c
 �CThe quality of the product wasn't comprised but it wasn't sanitary.�J�c
 �2It wasn't listed as having been previously opened.�J�c
 ��This book really opened my eyes to the plight of the US Merchant Mariners and their value to the WW11 effort and how they were treated.�J�c
 �`I've enjoyed this album (oops I'm dating myself) CD since about 1970 and have never tired of it.�J�c
 �\Regular listening to "The Creator Has A Master Plan" can lift your spirts anytime, anyplace.�J�c
 ��I had the joy of seeing Sanders and Leon Thomas perform this song and the other track "Colors" a couple of times during a stint they did at a club in Cleveland and it remains one of the great musical moments of my life.�J�c
 �WTo those who trim a star from the rating because it's "hippy jazz," I say "get a grip."�J�c
 �VThe music of Sanders, Thomas and their excellent accompanyists is pure, unbridled joy.�J�c
 �sSanders takes Coltrane's overblowing technique to original heights; Thomas sings the best of his incredible career.�J�c
 ��Liston Smith's Piano and Reggie Workman's bass are tremendous throughout - in particular they are in terrific form in the parts where they group breaks away from the main theme of "Creator.�J�c
 �"Hey Amazon!�J�c
 �Can I make it 6 Stars?�J�c
 ��The best of Pharoah's music from the early 70's stands apart from that of his contemporaries as something unique, even while it sounds of its time and place.�J�c
 ��Pharoah was reaching for something dep and spiritual, but (unlike some of the music he had been involved with in the late 60's) he had both feet planted in the ground and made music free of elitism, music that anyone can appreciate.�J�c
 �QThis record consists mostly of the massive piece "The Creator Has A Master Plan".��&      J�c
 ��It's got a nice undertow (courtesy in part of master bass player Cecil McBee) and maintains a vibe not unlike 70's music that we've heard before - "quiet storm" music - music played by collaborator Lonnie Liston Smith, for example.�J�c
 �fBut here the music keeps striving for transcendence even while it relaxes - this is something special.�J�c
 �yKarma is a departure from many of the similar music of the era, in that it goes towards more pop/pop-culture genre music.�J�c
 �FAt this point in jazz, there was a split between fusion and free jazz.�J�c
 �7This album is right down the middle and I recommend it.�J�c
 �One of my all time favourites.�J�c
 XI  Truly shamanistic, much has been written of the visions of Africa Coltrane's music can produce in the listener, but this is the one that takes me there, some of Pharoah's playing will take you to the ritual of the ancestors, the spirit dance, the masks of the winged gods, and then bring you back to earth with just solid beauty.�J�c
 �@Great arrangements, from a much more spiritually optimistic era.�J�c
 ��A unique feel,yet I always associate this record with the Brainticket masterpiece "Celestial ocean" and Jodorowksy's "la montana sagrada", I think it's due to the cover photo, it has that Jodo alchemist feel to it.�J�c
 �.My favourite of his many brilliant recordings.�J�c
 X)  Mr.Sanders, among the other brilliant musicians on the album, delivers a piece of music (specifically "The Creator has A Master Plan") that seems an earnest desire to traverse through a musical movement that pays homage to life, happiness and transcendence which feels very warm and compassionate.�J�c
 ��The shift from various segments of the piece to the next is stunning; sometimes quite jarring, but in the sense that creates the thought: "How was that so seamless?".�J�c
 X_  Chaos, in the 'free jazz' vein, reigns towards the end, then merges back to the beautiful coherence of earlier and seems to say: Here we were; master plan still in effect!The last, much shorter track, to my ears, does not detract; it is like a closing statement of thanks and goodbye; let's love and triumph; why not?Beautiful, challenging and unique.�J�c
 ��Even with Colors threatening to leave a poor aftertaste, Sanders charts his own love supremely with an enveloping spiritual homage of whirling tribute.�J�c
 �8I have used DGL throughout the years with great success.�J�c
 �=DGL is a wonderful remedy for acid problems with the stomach.�J�c
 �EI purchased this brand of DGL thinking all DGL products are the same.�J�c
 �"So I did not read the ingredients.�J�c
 �FBut Planetary Herbals puts orange flavoring into their formula of DGL.�J�c
 �*Which makes the taste absolutely horrible.�J�c
 �3I chewed one tablet and just about lost my cookies.�J�c
 �$I've never tasted anything so awful.�J�c
 �4Licorice and orange flavors do not go well together.�J�c
 �*So this bottle of DGL went into the trash.�J�c
 �2Now I know to read the label of each brand of DGL.�J�c
 �GI read this book as a teenager and now I wonder how I ever finished it.�J�c
 �cThe Characters are thin, the world building is simplistic and would likely not get published today.�J�c
 ��I think the only reason it ever made it into print was due to the strong sale performance of fantasy books like The Sword of Shannara and The Chronicles of Thomas Covenant a few years before.�J�c
 �Don't waste your time.�J�c
 �pEven for it's day, I suspect the only way it made it into print was the author was the child of a senior editor.�J�c
 �kPurchased as a gift - but several of the movies were corrupted and we were not able to play them on the DVD�J�c
 �lToo faded colors and Old Movies the action was very poor in most of them and some I had seen long time a go.�J�c
 �,Chick is not playing guitar on this weak cd.�J�c
 �)Its from the early 90s and sounds dated .�J�c
 �BThere isnt much of a blues feel and doesnt have any standout cuts.�J d
 �OIt lacks the fire and feeling of his excellent 2001 cd From The Heart and Soul.�Jd
 �DI live in the Las Vegas Desert with some of the worst water quality.�Jd
 �~My Vicks Humidifier is a must but the water causes the filer to build up with what is probably a colorful mixture of minerals.�Jd
 �So far this is my analysis.�Jd
 �WIt does seem to slow down the filter calcification that I used to scrape almost weekly.�Jd
 �bBut that unnecessary pink dye makes this product to where you must handle it as if it were bleach.�Jd
 �7It stains everything including the machine its used in.�Jd
 ��I accidentally knocked the bottle over resulting in a hard time cleaning a difficult stain on my carpet and not much had spilled.�Jd
 �KIll use this up but most likely will never buy it again because of the dye.�J	d
 �\Instead I'll search for another product with the same active ingredients but clear in color.�J
d
 �The spill was my fault 100%�Jd
 �Jbut I can't imagine what's in there that made the color settle so quickly.�Jd
 �.This stuff did not work for my humidifier .All�Jd
 �it did was just stained it.�Jd
 �!Very disappointingwaste of money.�Jd
 �UI travel a lot and I usually pack my on snacks and drinks, and don't like a messy car�Jd
 �%so I needed a place to keep my trash.�Jd
 ��I usually use a plastic bag left over from grocery shopping, but sometimes those leak and the trash doesn't stay secured in the bag.�Jd
 ��Anyway, this is nice because it's weighted (it comes with a weighted bean bag you zip into the bottom), and there's velcro on the bottom of the bag for those of you who have carpeted mats in your car.�Jd
 �yThe bag is lined with a waterproof/plastic liner so you don't have to worry about nasty old stuff dripping into your car.�Jd
 �GThe cover for the bag is held shut with velcro, and it's pretty sturdy.�Jd
 ��I would suggest putting a little liner in the bag so you don't have to worry about sanitizing or cleaning the bag, depending on what type of waste you're putting in there.�Jd
 �lI would definitely recommend this product to others, I'm a clean person and I like my car to stay clean too.�Jd
 �:)�Jd
 �>I got this for the back seat of our truck for the kids to use.�Jd
 �dThe walls seem very sturdy but the velcro on the bottom is not sticking to the floor like it should.�Jd
 �UIt does come with a weight to put in the bottom so hopefully that'll keep it upright.�Jd
 �3Other than that, really nice product for the price.�Jd
 �?This would be great if it didn't sort of collapse between uses.�Jd
 ��It holds your trash but the top pushed thefront of the litterbag inward so that every time you want to put trash into it, you have to lift the top up,and straighten the front side out.�Jd
 �[I might buy it again because trashstands are hard to find as reasonably priced as this one.�Jd
 �YIt is inconvenient to use but it looks better than liter or a bag on the floor of my car.�J d
 �-All things considered, it is worth the money.�J!d
 �vThe Good:Super quick shippingSleek when emptySticks to the floor, even without a carpetThe ballast bag really worksDid�J"d
 ��I mention super quick shippingThe Bad:Its a big black bag, but you know that before you order itIt is no where near as rigid as the photos make it seem.�J#d
 �I like the product�J$d
 �Sbut it looks much bigger in the picture and after awhile the Velcro does not stick.�J%d
 �Overall it is a good purchase�J&d
 �4but I think the price should be reduced a little bit�J'd
 �Nice size and design.�J(d
 �nThe problem is that it wants to collapse upon itself, making it difficult sometimes to just drop in the trash.�J)d
 �5I've tried a few different trash bins for my new car.�J*d
 �This one is pretty good.�J+d
 Xa  Pros:- sand bag that can go in the bottom to anchor it down- pocket in the front- fairly spacious- the right size for grocery bags as linersCons- sides don't stand up as well as it says- lid/cover is heavy and can cause the sides to sag inward even more- sandbag is oddly shaped and when it's inside the bottom of the bin it causes the base to be uneven�J,d
 �vGood size, sits in the middle of the back seat on the floor and the kids can still walk past it to get to their seats.�J-d
 �Has trouble keeping it's shape.�J.d
 �SCame folded so it keeps wanting to collapse back to flat instead of staying square.�J/d
 �+Really nice that it stays put on the floor.�J0d
 �>the velcro on the bottom sticks to the floor carpet in my SUV.�J1d
 �i always keep the top open.�J2d
 �pI use it in my 2008 Mitsubishi Outlander .It sits behind the front seat where i can easily dump all my trash .It�J3d
 �:has Velcro that keep it on the floor and it doesn't leak .�J4d
 �WThis trash bag stands upright fairly consistently because of the beanbag in the bottom.�J5d
 �QIt is easy to clean and does not take up much space in the passenger's foot well.�J6d
 �@The bos does not stay standing up (as many reviews reported it).�J7d
 � It is not easy to keep it clean.�J8d
 �EWe came back to a simple plastic bag that we trash everytime we stop.�J9d
 �fI'm sure the product description had dimensions, but I apparently didn't really pay attention to them.�J:d
 �OIt is well made, weighted and will stand up for easy use, but it is very small.�J;d
 ��Ultimately, I ended up getting a small plastic trashcan with a spring-loaded flip lid at Target and stashing it behind the passenger seat instead of using this so I wouldn't have to empty the container so often..�J<d
 �II feel like it could use some support on its sides so it doesn't fold in.�J=d
 �Other tHan that...�J>d
 �Great trash bag for my SUV.�J?d
 ��This is an okay waste container, but I found that it was not worth it to me to pay this much for something which has similar competition for less money.�J@d
 �LI bought it for two daughters as well as myself, and they were underwhelmed.�JAd
 ��It is good if you tend to toss wet trash in a waste recepticle and don't want leakage, but beyond that it is not worth the price.�JBd
 ��It won't tip over, which is good, but I would hope that by the time someone is middle-aged like me, they would know how to handle garbage without making a huge mess in the car.�JCd
 �?I didn't think I would end up writing a review for a trash can.�JDd
 �But I really like this.�JEd
 �LIt does what it says, it stays put in the car and hold quite a bit of trash.�JFd
 �BIt's very attractive...even with a shopping plastic bag in it :-).�JGd
 �ZI would recommend it for 1-3 people...not quite sure how it would for a very large family.�JHd
 �$I found this at Marshall's for less.�JId
 �8Same product (hint: shop around if you have the time :-)�JJd
 �
Not a fan.�JKd
 �[...].�JLd
 �!I've already lost money as it is.�JMd
 �Too small -�JNd
 �Vbut if you're looking for something to put in your car that might collapse when using.�JOd
 ��Or you only have a few small items to throw out and don't mind having to empty this each time you use it, then you may like it more than I do.�JPd
 �7If you like the old time songs, you will love this one.�JQd
 �LJohn has a greatvoice and this makes a great CD to play while eating dinner.�JRd
 �Soft, goodmusic.�JSd
 �DI heard it while having dinner at a friend's house and had to buyit.�JTd
 �MI had never heard of the singer but so glad I heard him as he is really good.�JUd
 ��I am a huge fan of Johnny Hartman's and have several of his albums but I must disagree with the writer that claimed this album to be better than the Coltrane collaboration.�JVd
 ��There is no album better than the Coltrane collaboration and quite honestly several of Hartman's albums are better than this (i.e. Songs from the Heart, Coltrane/Hartman, and I Just Dropped By To Say Hello).�JWd
 �GOnce you have declared yourself a Hartman fanatic, purchase this album.�JXd
 ��If you are a recent convert or are still on the fence, select one of the other three, although you may have difficulty finding Songs from the Heart.�JYd
 �FJohnny Hartman's wonderful baritone voice is on display on this album.�JZd
 �EUnfortunately the songs are a little slow and not terribly memorable.�J[d
 �ySo it's a tossup, I'm not a fan of the album as a whole, but his voice is so great that you have to appreciate the album.�J\d
 �aI guess I could see why someone would rate this as a "5" (for his voice) or as a "1" (too slow) -�J]d
 �so I'll give it a 3!�J^d
 �dFor those of you who are interested in what the first Jewish revolt was like, this books if for you.�J_d
 �8It's an eyewitness account to the events as they happen.�J`d
 �3Consider it the "official history" of the conflict.�Jad
 �JHowever, like all official histories, it shows a definite political slant.�Jbd
 �CUnlike other histories, however, this was written by the conquered.�Jcd
 �XThe work is in the tone of an apology for Jewish actions and motivations during the war.�Jdd
 �QStill, there is a lot more information out there as Josephus wrote several books.�Jed
 �GIt is a better idea to go ahead and get the Complete Works of Josephus.�Jfd
 ��The reason being is that there is a lot of information in his other work Antiquities of the Jews that is very important to understanding the conflict.�Jgd
 �9The Complete Works will also have The Jewish War in them.�Jhd
 �JIf ever there was a history book that you couldn't put down , this is it .�Jid
 ��The plotting and behind the scenes action is blended with clash of metal , the recoil of catapults and the wailing of those caught in the tempest of ancient Middle East politics , religion and power struggles .�Jjd
 ��From such details as the consequences of one of the Roman soldiers decidng to "moon" the crowds gathered at the Temple , to the upheavals throughout the Roman Empire as Nero and his following Emperors fall one after the other in rapid succession .�Jkd
 �lEventually Rome being stabilised by the general in charge of the Judean war , Vespasian , becoming emperor .�Jld
 �9A classic of military history but also an exciting read .�Jmd
 �An excellent translation.�Jnd
 ��Though slow to start the story builds successively as Josephus relates to us the deep roots for the causes of the war until it reaches its bloody and inevitable end with fall of Jerusalem and the sacrifice at Masada.�Jod
 �+In my opinion, this book is poorly written.�Jpd
 ��It's author has fundamental misunderstandings regarding mind/body/spirit, and yet approaches the subject as though she has the truth in hand.�Jqd
 �eThe approach feels oddly like an elementary school teacher leading her students to the blessed truth.�Jrd
 ��I don't know; it's just...too dogmatic, and from time to time there are concepts presented which will make you (as an intelligent reader) WINCE!�Jsd
 �I mean, actually WINCE!�Jtd
 �vIt's soooo poor, that I THREW it away rather than leave it for another reader to stumble upon at my local coffeehouse.�Jud
 �Save your money.�Jvd
 �,(THAT'S a concept you can take to the bank!)�Jwd
 �3This is not an easy read for someone new to Zen....�Jxd
 �/Maybe better suited to a seasoned practitioner.�Jyd
 �.I'm looking for a more beginner friendly book.�Jzd
 �>Many people consider zen as some sort of high-brow philosophy.�J{d
 �.Undecipherble koans and austere living habits.�J|d
 �ZThis is a book that explains the essence of zen and how it is applicable to everyday life.�J}d
 �pIt's really nothing more than "mindfulness" and the conscientious performance of the most trivial of daily acts.�J~d
 ��Charlotte Beck was instrumental in bringing zen out of it's foreign and exotic context and explaining it's true meaning to the american novice.�Jd
 ��These are informal talks given during the sesshins (meditation weekends) she conducted in various retreats throughout the world.�J�d
 �BThey touch upon all aspects of life and are illuminating at times.�J�d
 �WI've read many of these books and I find myself buying this one for friends and family.�J�d
 �8It's clearly written for beginners and I really like it.�J�d
 �+This is not a sustained argument or thesis.�J�d
 �|The reader should know something of Buddhism and something of Zen practice in order to understand what she is talking about.�J�d
 ��This is a collection of talks that Joko Beck--who has impeccable credentials--gave to her students, followed by short question/answer sessions.�J�d
 �[As such, it covers a lot of ground, and not every talk will speak to your present concerns.�J�d
 ��Nonetheless, it is a gem which has now stood the test of time in a field littered with books on Zen, many of which do not bear reading.�J�d
 �EI believe that it will ultimately enter the American Buddhist cannon.�J�d
 �LI purchased "Everyday Zen: Love & Work" for my husband for Christmas (2006).�J�d
 �;He has enjoyed other Charlotte Joko Beck books in the past.�J�d
 �6So, I decided to buy another one of her books for him.�J�d
 ��This book is good, it has many interesting ideas and thoughts to convey but, my husband still favors "Nothing Special: Living Zen.�J�d
 ��" I believe he would recommend that book, if a reader is interested in introducing themselves to or wants to read another one of Charlotte Joko Beck writings.�J�d
 �IAlthough, "Everyday Zen: Love & Work" is readable and worth checking out.�J�d
 �mA notation: toreaders, you may just want to start with one of her other works first, before reading this one.�J�d
 �ZThe book is organized in a series of talks where Joko shares her wisdom with her students.�J�d
 �vUnfortunately, I found that the tone of the talks lacked compassion and understanding for the complexity of the world.�J�d
 �^Thicht Nacht Han tells us not to create us/them dualities, but this book is replete with them.�J�d
 ��Further, the book overwhelmingly emphasizes the importance of sitting, but *ignores* the importance of everyday ethical practice in providing the groundwork for a joyful life of awareness.�J�d
 �sI find that it did not provide much help for ordinary people trying to deepen the practice of their everyday lives.�J�d
 �7Perhaps this is a problem with Zen Buddhism in general.�J�d
 �9A much better book is Sharon Salzberg's "Lovingkindness".�J�d
 �7I have always been curious about Zen for a few reasons.�J�d
 �>But, it's mostly because many of the books I read refer to it.�J�d
 �AThis is a good book to read if you're just plain curious like me.�J�d
 ��I thought that some topics in this book were easy to comprehend, but then there were others where I had to go back and reread paragraphs two or three times until it sunk in.�J�d
 �EBeing a Christian, I enjoyed the parallel references to Christianity.�J�d
 �IAnd, it didn't seem like the author was trying to convert me to Buddhism.�J�d
 �3She writes in a tone that encourages understanding.�J�d
 �PI can't honestly say I completely understand all of Zen after reading this book.�J�d
 �ABut, I can say that I'm enthusiastic about wanting to learn more.�J�d
 ��That's not because I'm interested in converting to Buddhism but because I am fascinated by the similarities between Zen and living a Christlike life.�J�d
 ��And, on a less serious note, I really enjoy baffling my friends and coworkers with the Buddhist parables mentioned in this book.�J�d
 �wI was looking forward to reading this book but after the first 60 pages my patience wore thin and I had to put it down.�J�d
 �uThe writing is unnecessarily convoluted, the examples given are at best trite (at worst condescending to the reader).�J�d
 �gThis book is a fine collection of supposed nuggets of wisdom and truisms endlessly rinsed and repeated.�J�d
 ��Example statement:"What we have to join together these seemingly separate divisions [sic] of life is to walk the razor's edge; then they come together.�J�d
 ��But *what* is the razor's edge?"There are some interesting thoughts in this book but they vanish in a sea of poorly written blah.�J�d
 �This is a good book.�J�d
 �AIt covers several bird species and explains their idiosyncracies.�J�d
 �PI do not care much for Mr. Tarrants writing style and find it a bit too flowery.�J�d
 �A good read.�J�d
 �"Great laundry sink for our garage.�J�d
 �$Includes OK faucet and easy install.�J�d
 ��Sink shape should have included better drain angle toward rear to allow for faster drainage, but install with shims under front legs to compensate.�J�d
 �iThe author is a Maryland librarian; though the book is set in England, the lead character is an American.�J�d
 �vThe result is that everything you need to know about England to understand what is going on is gently set out for you.�J�d
 �IAmong other things, you pick up the British equivalent of "library card".�J�d
 ��There's plenty of mystery, plenty of suspects, plenty of sleuthing, and at least the lead character is one you'll look forward to meeting again in a subsequent novel.�J�d
 �:I love a mystery that has a kind of sleuthing twist to it.�J�d
 � This one was a pleasure to read.�J�d
 �?Let me tell you I do alot of reading and mysteries top my list.�J�d
 �3If you like a great mystery don't miss out on this.�u(J�d
 �Off to find another mystery.�J�d
 �Gina�J�d
 �5The central mystery in this book is an average story.�J�d
 �\It is a good book for a lazy weekend, but I would not waste any precious reading time on it.�J�d
 �pHowever, it truly bothered me that the main character could not stop harping on her daughter's parenting skills.�J�d
 �@As a new mother myself, I found this to be extremely irritating.�J�d
 ��It seems that Ms. Harwin had to go out of her way to make the daughter seem crazy (expecting ANYONE to stay INDOORS all day with a very active two year-old without turning on the TV)just so that her older, "wiser" character could criticize.�J�d
 �Some older readers may feel a camaraderie with the main character, but I find enough advice from busy-body relatives on my own.�J�d
 � I don't need it in a light read!�J�d
 �A disappointment.�J�d
 �A weak and unappealing heroine.�J�d
 �+Lots of good books out there, keep looking.�J�d
 �WIt's readable, and I'll give the next one a look, but basically it ain't all that good.�J�d
 �9The protaganist isn't particularly likeable or realistic.�J�d
 �ZHarwin has the talent, but she is going to have to develop it to keep a lot of us reading.�J�d
 �.I gave it three stars on the strength of that.�J�d
 �>Very difficult to read the type, especially the Dutch section.�J�d
 �KSeems to be geared toward Dutch speakers wanting to translate into English.�J�d
 �.Pronunciation is only given for English words.�J�d
 �TComprehensive and reasonably priced, but I'll be looking for a different dictionary.�J�d
 �QSince i do research in Flemish-Belgian historical sources, this is highly useful.�J�d
 �0I recommend it to any with these language needs.�J�d
 �XWhen you're selecting a translation dictionary you want one that has a large vocabulary.�J�d
 �pThis one seems to, but disappoints big time since it doesn't have many, many commonly-used verb- and noun-forms.�J�d
 ��If you are a beginner in raising chickens this is a book for you, it talks about coop building to how to expand your hobby into something bigger.�J�d
 ��This book talks about most of the things you need to know about raising chickens but it does not go into to much detail on hatching your own eggs in a incubator.�J�d
 �#I would really recommend this book!�J�d
 �thanks�J�d
 ��Informative, inexpensive book for the beginner, however, be aware that it was written in 1944, with the last revised edition printed in 1977, so there are some out of date suggestions like using asbestos tiles for your chicken coop.�J�d
 �]Other than that, the book is helpful, a quick read, and provides some plans for simple coops.�J�d
 �I disagree with other reviews.�J�d
 �NUp until recently, I was using a computer with, believe it or not, Windows 95.�J�d
 �With only 32MB ram.�J�d
 �lI had a need to be able to do image editing, and this program has much, much less of an intimidation factor.�J�d
 ��Once a person learns the "SmartSelect" function, they can cut out very clean, very neat images, transform them into "objects", and merge elements from other photos.�J�d
 ��Once a person gets the "hang" of using the various effects and selection tools, no one will even know you're not using Photoshop Elements.�J�d
 ��The best thing, the absolute best quality to this program is adding text, it makes it one of the easiest things to do, move it around, add shadows, color gradients, textures, etc, etc.�J�d
 ��It isn't for very, very serious artists, but for the average person or even a person who merely likes to play with their photos and share them, this is a absolute blast.�J�d
 �8Every picture I do is great fun, and never looks sloppy.�J�d
 ��could not finish installcalled Nova tech supportafter asking for codes from the package guy concluded:it's an old version,WILL NOT WORK WITH XP HOME SR�J�d
 �2Returning it back.�J�d
 �What a scam!!�J�d
 ��It is really disappointing to learn that the domestic release from this live masterpiece has only twelve songs instead of eighteen.�J�d
 �:Anyway, if you can't find that other release buy this one.�J�d
 ��Once you listen to it you will simply not believe your ears; if it not were for the screams of a live audience, you will guess that this isn't a live CD.�J�d
 �But it is a live recording.�J�d
 ��Like Soda Stereo, Lucybell pays special attention to how they sound live, so, this recording have the same quality as a studio version, but with the tinge of a live audience.�J�d
 �zBelieve me when I tell you, this CD is a unmerciful masterpiece, and is destined to prevail in your Discman or car stereo.�J�d
 �kWhy then waste your time listening to another bunch of wackos playing the same ska when you can have this??�J�d
 �Buy it or lose.�J�d
 �Lucybell has come to stay.�J�d
 �So, check this out.�J�d
 �Enjoy.�J�d
 �RThe chileno version of this is one of the absolute best CDs I have ever purchased.�J�d
 �GI was disappointed to see that only 12 tracks appear on the US release.�J�d
 �GThis is an incredibly-accurate representation of how they are, en vivo.�J�d
 �hSo, I'd recommend trying to find it as an 18 track import first, but if you can't find it, this will do.�J�d
 �VI had seen McCoy Tyner in concert the night I ordered the CD and I am glad that i did.�J�d
 �Jazz fans will love it.�J�d
 ��Balakirev's 1st Symphony is less well known that it ought to be, and I was hoping this newish recording (1998) would add a valuable new voice to the repertoire.�J�d
 �Unfortunately, not so.�J�d
 ��The BBC Philharmonic has one of the best wind sections of any orchestra; but in this recording, despite beautifully accurate playing, the whole orchestra seems strangely detached from the music.�J�d
 �RSinaisky sets fast tempi throughout, and there is no feeling of grandeur or space.�J�d
 ��Compare this with the 1950's recording of Karajan and the Philharmonia - Karajan conveys awe, wonder, discovery; with Sinaisky speedy tempi substitute for drama.�J�d
 �^A shame, because the engineering is good; Chandos have done a fine job of capturing the sound.�J�d
 �4But nothing can quite make up for loss in the music.�J�d
 �lAs a medical school applicant I thought this book could help me get my bearings and select schools to apply.�J�d
 �9First, the statistics are inaccurate and not well edited.�J�d
 �SClass size statistics between out of state and in state, for example, don't add up.�J�d
 �QThe information about each school sounds like marketing directly from the school.�J�d
 �kIt's almost impossible to differentiate between the schools by reading about their curriculum in this book.�J�d
 �.The schools websites are far more informative.�J�d
 �dAlso, the MSAR book published by the AAMC has much more detailed statistics that seem more accurate.�J�d
 �cordered a book that has been out of print for years and was able to locate it through this company.�J e
 �PVery quick shipping, and book was in very good condition like it was advertised.�Je
 �!Would buy from this seller again.�Je
 �JThis turns the Senseo into a plain coffeemaker, so why bother to have one?�Je
 �There's no froth on top.�Je
 �7The 2-cup option came out watery, weak and undrinkable.�Je
 �KThis will be short and sweet: Bought it at WalMart, returned it at WalMart.�Je
 �,This looked too good to be true, and it was.�Je
 �CThe coffee is weak, the process is messy, and the headache extreme.�Je
 �DThis is an excellent product and works very well when used properly.�J	e
 �RIf you want to use non pod coffee in your senseo this is the easiest way to do it.�J
e
 �bThe key to using this product is to compress the grounds as much as possible in the coffee holder.�Je
 �KYou need to compress them in the same way you would in an expresso machine.�Je
 �wIf you do not compress them you will get watery coffee, you would have the same experience with some expresso machines.�Je
 ��What would make this 5 stars would be if the manufacturer provided a better tool to compress the grounds, I use the bottom of a Tylenol bottom which fits perfectly into the coffee holder.�Je
 �Highly recommended!!!!!�Je
 �%The product delivers on it's promise.�Je
 �$It's great for using regular coffee.�Je
 �"I do recommend wetting the filter.�Je
 �0Definitely more economical than buying the pods.�Je
 �`I recommend Eight O'Clock coffee recently rated the best supermarket coffee by consumer reports.�Je
 �DIt's sold in whole bean, but I use the in store grinder to grind it.�Je
 �&I use it with the Senseo coffee maker.�Je
 �pLet me preface my review by saying that after reading the reviews of the MyPod I was a little wary of buying it.�Je
 �4However, it turned out that there was no need to be.�Je
 �<I'm not sure how people are having so many problems with it.�Je
 �It seems fairly simple to me.�Je
 �pI put in a paper filter, scoop out 1.5 scoops of pre-ground Dunkin Donuts coffee, put on the lid, and then brew.�Je
 �I use the 2 cup setting�Je
 �Eand I don't think it makes any more than the original Senseo pods do.�Je
 �fWhen it's finished, I take off the lid and immediately rinse it down the sink (with garbage disposal).�Je
 �
That's it.�Je
 �So far it's worked flawlessly.�J e
 �_I love the fact that I can drink Dunkin coffees without using an old fashioned coffeemaker now.�J!e
 �Thanks Presto!�J"e
 �EI bought a Senseo Coffee maker which only uses expensive Senseo pods.�J#e
 �(I like Eight O'clock coffee fresh ground�J$e
 �+so I purchased this replacement pod system.�J%e
 �
Good price�J&e
 �and it works perfectly for me.�J'e
 �!I am able to use whichever coffee�J(e
 ��I choose (I like to mix Eight O'Clock Columbian with hazelnut or vanilla flavored coffees) and I am not locked in to buying $4 plus bags of coffee pods over which I have no creative control.�J)e
 �?I purchased my presto refillable pod/coffee holder at Wal-Mart.�J*e
 �EIt does work, but there are two things that you must know about this.�J+e
 �XThis will make a large cup of coffee when you push the single cup button on your Senseo.�J,e
 �`This will not froth the coffee the way other products such as the 'Coffee Duck' and 'Ecopad' do.�J-e
 �OThis will do the job, but I recommend the other products that I just mentioned.�J.e
 �5I bought a Senseo to enjoy a good bold cup of coffee.�J/e
 �NThe unfortunate side effect is that one has to buy 18 pods a week at $5 a pop.�J0e
 �rSo I thought I would try the Presto MyPod to help save a few dollars, and be able to use the machine that I liked.�J1e
 �The concept is a great idea.�J2e
 � Presto just didn't get it right.�J3e
 ��I'm not sure what the answer is, but the bottom line is that this product produces weak coffee with a lot more hassle than the Senseo should have.�J4e
 ��The instructions are full of disclaimers about the type of coffee, how you have to stand by the machine to make sure it doesn't overflow, etc.�J5e
 �`It's like Presto knew this wasn't going to be a great product, but put it on the shelves anyway.�J6e
 �@The construction is the best of the similar items on the market.�J7e
 �I bought them all.�J8e
 �+Nonetheless, I am just too lazy to do this.�J9e
 �$I prefer to buy the pods ready made.�J:e
 ��If you really love your own brand of coffee and do not mind dealing with coffee grounds, this is the best of the similar items you find on the market.�J;e
 �WHard to load, messy, and most of all DOES NOT FIT properly when used with Senseo Delux.�J<e
 �Don't waist your money.�J=e
 �7Most all of the criticisms of this product are correct.�J>e
 �GIt will produce undrinkable watery coffee if you try to make a big cup.�J?e
 �%Using it is time-consuming and messy.�J@e
 �No "crema."�JAe
 ��(Not a huge issue with me, but the crema is a nice touch.)However, if you're willing to put up with the hassle, you can produce decent coffee using 1 1/2 scoops and using the small cup button.�JBe
 �AYou don't get much coffee that way, but you get drinkable coffee.�JCe
 �`I haven't done the math so I don't know if this is cheaper than just buying the ready-made pods.�JDe
 �\It seems like you are using an awful lot of ground coffee to make very little brewed coffee.�JEe
 �5You can use the ground coffee of your choice, though.�JFe
 �OI'll use it on occasion, but will probably mostly stick to the ready-made pods.�JGe
 �This product is a little fussy.�JHe
 �WYou must have just the right coffee grain consistency for a light or dark cup of coffe.�JIe
 �@We purchased this item and thought it would be a wonderful idea.�JJe
 �(Unfortunately it did not work very well.�JKe
 �vIt did not provide a 'foamy' cup of coffee like the pods did, thus the coffee lacks a quality that the foaming offers.�JLe
 �jAlso the item lodged in the Senseo Machine and the machine had to be opened with a screwdriver and pliers.�JMe
 �/Fortunately it did not ruin the Senseo machine.�JNe
 �BThe Senseo did not even seem to recognize the MyPod in the holder.�JOe
 ��When I brewed the coffee, the unit did not shut off and what was supposed to be a great morning wakeup, turned into a huge mess of boiling hot water and coffee grounds.�JPe
 ��Thinking I did something wrong, I cleaned up and tried again (when my blood pressure dropped!) and had just about the same result.�JQe
 �XI'm just glad I only bought this on clearance at WMart, and didnt pay full price for it.�JRe
 �Junk!�JSe
 �%Should never bought this silly thing.�JTe
 �Very messy.�JUe
 �Hard to clean up after.�JVe
 �$I have allready put it in the trash.�JWe
 �Weak, watery coffee.�JXe
 �3I can't believe this thing made it out of the labs.�JYe
 �0Maybe they didn't care whether it worked or not.�JZe
 �H(Note that all the positive reviews of this thing are very conditional.)�J[e
 �<I'm not sure if my Senseo model may have been the wrong one.�J\e
 �PWhen I try to use the product by the directions, I get water all over the place.�J]e
 �=Was there supposed to be another part of help seal it better.�J^e
 �I don't know.�J_e
 �-I am just writing it off as a bad investment.�J`e
 ��Presto 09402 Mypod Refillable Personal Pod/coffee Holder +100 Extra Filters I used this and it is NOT the same as using a real pod of coffee.�Jae
 �)It does not foam up like a Senseo at all.�Jbe
 �,Coffee is weaker no matter how much you use.�Jce
 �I give it 1 to 2 stars.�Jde
 �&I have a Juan Valdez Pod Single Serve.�Jee
 �6Can this product also make Pods for the JVPM2 product?�Jfe
 �"Bought this for use in our office.�Jge
 �Very disappointing.�Jhe
 �KWe've tried it with three different types of coffee and none of us like it.�Jie
 �3Too difficult to get a full strength cup of coffee.�Jje
 �$Not enough resistance for the water.�Jke
 �^Surprisingly the most effective grind so far was the espresso which is not recommended for it.�Jle
 �RGave up after two days, countless machine overflows and thrown out cups of coffee.�Jme
 �,Buying a Perfect Pod and hoping it's better.�Jne
 �Expensive experiment so far.�Joe
 ��I got the Presto 09402 MyPod Refillable Replacement Pod (Senseo) it does works but it doesnt make the coffee like the senseo pods do.�Jpe
 �Its ok for the money�Jqe
 �fbut I'll be willing to pay more if the coffee will come out like it does when you used the senseo pod.�Jre
 �!Bottom line its ok for the money!�Jse
 �2We have a pod maker at work & someone bought this.�Jte
 �GIt's not the best, but it's not as bad as people are complaining about.�Jue
 �lFor starters, it does allow more water to flow into your cup, so hit the one cup button or use a bigger mug.�Jve
 �Problem solved.�Jwe
 �1Cleanup is a fuss, but when isn't cleanup a fuss?�Jxe
 �Get over it.�Jye
 �IIf you are a real coffee drinker, then you know about cleaning up grinds.�Jze
 �?Overall, this is a good buy if you want to use your own coffee.�J{e
 �@If you are happy w/the pods available, then pass on buying this.�J|e
 �I purchased this awhile back.�J}e
 �0I must say that I'm really disappointed with it.�J~e
 �QI was hoping for something to use to avoid using the expensive pods all the time.�Je
 �NThe main problem with this is that the Senseo will not shut off automatically.�J�e
 �.It makes a mess each time I attempt to use it.�J�e
 �;The coffee is very weak and usually has some grounds in it.�J�e
 �_Also there is no crema, which isn't a big deal to me, but others might be disappointed in that.�J�e
 �"For now, I'll stick with the pods.�J�e
 �[Target has started selling its house brand coffee in pods so I'll probably give that a try.�J�e
 �TI purchased this as a backkup for the no pod occasions, but it is totally worthless!�J�e
 �yThe reason we all have pod coffee makers is because they are quicky, easy and make a great cup of joe without the hassle.�J�e
 �6So why in the world would we want to use this device!!�J�e
 �eIt makes weak flavorless coffee, requires clean up, and doesn't shut off automatically like the pods.�J�e
 �Also, there is no crema.�J�e
 �I returned mine!�J�e
 �kAmazon sells pods by Gala that are really good, great price, and now there is no reason to run out of pods!�J�e
 �Skip this one.�J�e
 �EThe presto my pod makes, to me, a very weak and watery cup of coffee.�J�e
 �XIt could be just me, I prefer using the medium or dark roast pods, I like strong coffee.�J�e
 �lI tried using a few different types of coffee in the presto my pod, but it didn't seem to make a difference.�J�e
 ��I'm not convinced that the pressure is strong enough to brew a decent cup.don't buy this... heck I'll sell you mine for a dollar :-)�J�e
 �$I'll never use the dang thing again.�J�e
 �rI followed up by buying the brown eco pod, and THAT worked decently and made a nice cup of coffee by my standards.�J�e
 �vIt's easier to make drip coffee in a cheap machine for better flavor than using this as even an emergency replacement.�J�e
 �fIt was worse than awful I'd rather drink gas station coffee for a better flavor than what this can do.�J�e
 �Please don't waste your money.�J�e
 �RI bought this gadget on amazon a week ago to try it with my Senseo coffee machine.�J�e
 �[I was very seduced by the idea of using my favorite coffee with my favorite coffee machine.�J�e
 �\But with Mypod from Presto, I only get very weak cups of coffee with a lot of water in them.�J�e
 �_As if it wasn't enough, there is no foam on the coffee I get, which is very frustrating for me.�J�e
 �/I will never recommend this product to anybody.�J�e
 �2Save your money for real pods from Senseo or Kona.�J�e
 �It works great�J�e
 �Kbut I will not give up my pods completely because they are more convenient.�J�e
 �@And when I need coffee, manual dexterity is not my strong point.�J�e
 �$I usually use it for the second cup.�J�e
 �)This pod does not fit the Senseo Supreme.�J�e
 �It leaks the water out.�J�e
 �)The top does not close tight with pod in.�J�e
 �4I find the Eco pads work well in the Senseo Supreme.�J�e
 �Bad execution.�J�e
 �TI picked one of these up at a local store and had returned it by the end of the day.�J�e
 �
What junk.�J�e
 ��No grounds on the rim, I tried drip and espresso grinds of varying sizes and still it leaked and gave me nothing but brown water.�J�e
 �II got one of these thankful to be lowering my per-cup cost on my machine.�J�e
 �I didn't even use it once.�J�e
 �First, MESSY MESSY MESSY.�J�e
 �BI end up with grounds all over the place trying to get this right.�J�e
 �-Incidently, that's one reason I got a Senseo.�J�e
 �:Second, it made the weakest coffee without the great foam.�J�e
 �I say weak, I mean watery.�J�e
 �#I was even using an expresso blend.�J�e
 �Third, the clean up stinks.�J�e
 �*Overall, hit Target when pods are on sale.�J�e
 �I buy them buy the case!�J�e
 �Love my Senseo!!!!�J�e
 �5I have had the blue ecopods for well over a year now.�J�e
 �WIlove the taste of the coffee and it does give a fair amount of foam but hard to clean.�J�e
 �,I saw the Presto MyPod and leaped to try it.�J�e
 �HThe coffee overfills the cup making it too weak and no room for creamer.�J�e
 �There is absolutely no foam.�J�e
 � I got hooked on the Senseo Foam.�J�e
 �JWe will use it in a pinch but only on the 1 cup setting with 1 1/2 scoops.�J�e
 �8The product description doesn't provide full disclosure.�J�e
 �1This product is not a substitute for coffee pods.�J�e
 ��It does not make the characteristic crema of standard pods and it won't regulate the amount of water that goes through the coffee maker because of pressure variations.�J�e
 �@As a result, cups overflow and brewing is difficult to regulate.�J�e
 �HIt will save you money over pods, but the coffee is not as satisfactory.�J�e
 �/Final judgement: waste of money and not useful.�J�e
 �jThis product does allow you to use regular coffee if you are out of pods; however, it is messy to cleanup.�J�e
 � Pods are the better alternative.�J�e
 �jThis was advertised as fitting my pod machine, but doesn't - I'm not sure that it would function that well�J�e
 �even if it did�J�e
 �BI folowed the instructions and found that the coffee was very weak�J�e
 �=so I experimented with differint grinds and filter no filter.�J�e
 ��I found not useing the paper filter and grinding the coffee expresso and stoping the water when it starts to look pale makes a med.�J�e
 �strong cup.�J�e
 �A lot of work for a decent cup.�J�e
 �7I still think that my old krups drip makes a great cup.�J�e
 �KI will keep trying other alternets to the pods becous there very expensive.�J�e
 �BI am waiting for some one to start manufacturing a home pod maker.�J�e
 �!that you can fill & seel at home.�J�e
 ��Concept is great, however it does allow small amounts of coffee ground residue to leak past the pod holder into the coffee maker and the cup.�J�e
 �JI suspect continued use may result in eventual damage to the coffee maker.�J�e
 �>I was using the Senseo coffee pod coffee maker with this item.�J�e
 �SFace it, the Senso pods are expensive and one cup a day just doesn't cut it for me.�J�e
 ��I use the Presto MyPod Refillable all the time without the little pod filters (which are a pain and unnecessary) with regular off the shelf maxwell house columbian coffee.�J�e
 �EYes you have to place enough coffee in so the pod top packs it tight.�J�e
 �DYes you have to be sure there are no grounds outside of the pod top.�J�e
 �TYes you have to stand at the machine and shut the pot off once your cup is 3/4 full.�J�e
 �x(but i normally stand there waiting for it anyway)No you don't get fancy foam - but fancy foam isn't why i drink coffee.�J�e
 �/No the coffee isn't as strong, but i prefer it.�J�e
 �WI also love the fact that i can use regular coffee off the shelf in my favorite flavor.�J�e
 �RI save tons of money and I save the environment from all those spent pods as well.�J�e
 ��I'd have given the Presto MyPod 5 stars but the handle is a bit flimsy for the cost so be careful when you're tapping out the grounds and cleaning it.�J�e
 �BThis system seemed like a great idea when I bought it in February.�J�e
 �9A cheap alternative to overpriced pods and all that jazz.�J�e
 �Then I had to clean it.�J�e
 �FAnd decided my time was, in fact, worth the cost of pre-packaged pods.�J�e
 �rThe reviewer who mentioned that this item eliminates the fast convenience of a pod coffeemaker hit it on the nose.�J�e
 ��Prep is inconvenient, clean-up is awful (my senseo is in a room without a sink) and it was just a nastier mess than I wanted to face on a regular basis.�J�e
 �xI kept the kit for absolute coffee emergencies, but make an effort to ensure I don't run out of pods rather than use it!�J�e
 �MI had the same experience about the coffee maker not stopping the water flow.�J�e
 �I had quite a mess.�J�e
 ��Even ignoring the water flow issue though, because I could at least shut it off manually since I'd only use this occasionally...�J�e
 ��The real problem is the lid that covers the coffee grinds: It's a design flaw in that there's room for the lid to float once water is let into it.�J�e
 ��Once it floats the water flushes the grinds out from under the lid and up into the coffee maker itself (where coffee would never go normally).�J�e
 ��No grinds made it into my coffee cup but I had to clean the counter and spent the next half-hour flushing and re-flushing grinds out of the upper mechanism.�J�e
 �No�J�e
 �Thanks�J�e
 �{I have tried different grinds of coffee, different coffees, different amounts of loading, and still just get colored water.�J�e
 �|Doesn't come close to the Ecopod in getting a decent cup of coffee out of the machine, or for that matter the standard pods.�J�e
 �My coffee came out watery�J�e
 �4and I missed the trade mark foam from my usual pods.�J�e
 �/I was underwhelmed and now it's gathering dust.�J�e
 �Kit's to messy, does not hold enough coffee to make a decent tasting coffee.�J�e
 �cthe way this works sounds easy, however in reality it does not provide the quality coffee promised.�J�e
 �This was not worth the hassle.�J�e
 �<I gave it and the coffee maker away for free in a yard sale!�J�e
 �WThe Presto MyPod Refillable Replacement Pod set for the Senseo is just okay, not great.�J�e
 �It is easy to fill and empty.�J�e
 �oHowever, the amount of water it passes with each brew is significantly more than it does with the premade pods.�J�e
 �rThe resulting coffee is weakly flavored because the machine is using more water than usual in a single brew cycle.�J�e
 �sI will try to grind coffee a little finer to see if I can slow the brewing and extract more flavor from the coffee.�J�e
 �=A nice fit with the Senseo coffe maker that uses coffee pods.�J�e
 �/This product allows you to use your own coffee.�J�e
 �RWorks well except does not produce the frothy top that comes with use of the pods.�J�e
 �KOne of the things I like about the Senseo is the lack of grounds and stuff.�J�e
 �>This thing turns my nice clean pod-life into grounds and mess.�J�e
 �Makes great coffee though.�J�e
 �9So I give it a middle minus one because I hate messiness.�J�e
 �7I have had the same problems many other reviewers have.�J f
 �*Too much water makes a weak cup of coffee.�Jf
 �4Using 1 1/2 scoops and the 1 cup button on my Senseo�Jf
 �I get an okay cup of coffee.�Jf
 �WI like the fact that it gives me the option of brewing a lot of flavors with my Senseo.�Jf
 ��I thought I would save a lot of money, not having to buy the expensive pods, but I end up using so much ground coffee to get a decent flavored cup it pretty much equals out.�Jf
 �AOverall this is an okay product, but I will still be buying pods.�Jf
 �YI loved this book- it had beautiful photographs and good descriptions of the plants used.�Jf
 �fIt was helpful to have the seasons and preferred exposure of the plants included in their description.�Jf
 �0There are also several good sample garden plans.�J	f
 ��My only disappointment (if it can be so considered) was that many of the featured plants are not hardy in my zone 4 garden (Ms. Raven's garden is in Britain).For anyone who loves contrast in their gardens, this is the book to inspire you!�J
f
 �VUnlike many who have "reviewed" this book prior - this is NOT a Sherlock Holmes novel.�Jf
 �gIt is, however, a great piece of work, painstakingly documenting period England and quite entertaining.�Jf
 �}Shame so many think because Mycroft appears so stodgy and sedentary in the canon that there can't be more than meets the eye.�Jf
 �rI daresay someone described as Mycroft is by Holmes and Watson wouldn't remain in a high government position long!�Jf
 �#It's also clear they weren't close.�Jf
 �So let it go.�Jf
 �Enjoy the read!�Jf
 �-The story line is Fast paced and interesting.�Jf
 �3Though having Mycroft Holmes ( Sherlock's brother).�Jf
 �/Traveling about .Does not agree with his image.�Jf
 �Of being " exact in his ways.�Jf
 �gAnd not one to exert himself ....other than crossing the street from his dwelling to the Diogenes Club.�Jf
 �5And Sherlock is not a active party in this adventure.�Jf
 �_But for us Sherlock Holmes " Followers " anything related to him or those connect to him .Makes�Jf
 �for a must reading.�Jf
 �A good story.�Jf
 �So read and enjoy.�Jf
 �/Elegant whodunit against a colorful background.�Jf
 �~I deducted one star because the character of Micah Holcomb and Poirot-style confrontation in the lounge car were over the top.�Jf
 �8Now I regret sleeping through a similar journey in 1975.�Jf
 ��I ran into a girl the other day (literally) who was wearing these cool skates and I was quite impressed by their remarkable style and grace as she swayed across the deserted parking lot.�Jf
 ��It was a good day, I bent down to help her back up when I did I took a look at her Freeriders and noticed she had her phone number written on the side.�J f
 �JSo I gotta get going and find out how my wounded freerider bunny is doing.�J!f
 �PI found too many errors/typos/inconsistancys between the textand the appendices.�J"f
 �4Almost too much information smashed into smallspace.�J#f
 �-Not up to the quality of other Putnam titles.�J$f
 �END�J%f
 �=This is one of the later Putnam efforts in the larger format.�J&f
 ��Whether its because of the different format, a reduction in 3-view drawings, or simply the writing, I didn't like this book as much as the 20+ other Putnam aviation books in my library.�J'f
 ��It lacks the detail and (dare I say) "charm" of the early books written by true marque experts, who could make the history of esoteric UK firms (a specialty of the early Putnam books) interesting.�J(f
 �cI'm a huge fan of Bell helicopters, and I found the information hurridly and confusingly presented.�J)f
 �mIn the Bell 47 section, it presents all the the various productions models without any background or context.�J*f
 ��It comes across more like one of the souless "aviation encylopedias" filled with lots of numbers, than a well-researched history of a noteworthy firm.�J+f
 �)Let's hope they return to the old format.�J,f
 �.This is absolutely the best one you could get!�J-f
 �1It sits easily on your neck,and doesn't fall off.�J.f
 �'It holds the heat for a very long time.�J/f
 �The fragrance I love.�J0f
 �oI have even been taking it to bed to use as a heating pad for different areas, and fall asleep with its warmth.�J1f
 �[I have been telling others about it, and would not hesitate to give it as a gift to others.�J2f
 �
I LOVE it!�J3f
 �Great value for your money.�J4f
 �I dont get it.�J5f
 �DI read the reviews and and was really looking foward to trying this.�J6f
 �uI have alot of trouble with stress-related headaches and neck soreness from tension and I really wanted to love this.�J7f
 �It was..usless.�J8f
 �(I threw it away after the third attempt.�J9f
 �#The buckwheat does not heat evenly.�J:f
 �5The heat doesnt get held for more than a few minutes.�J;f
 �GThe scent of lavender..well...it smells really off and I dont know why.�J<f
 �I really was disapointed.�J=f
 �hThe product is easy to use but when heated it smells like overheated oatmeal (it must be the buckwheat).�J>f
 �AThe smell is not too strong but it bothers me so I do not use it.�J?f
 ��I ordered this as a gift for someone, but decided not to give it to her because a fine gritty dust sifts out through the wrap (heated or not).�J@f
 �HThis dust gets on any surface you place the wrap on, including yourself.�JAf
 �nIt was also kind of bunched up and too firm/almost crunchy especially in the part that curves in front of you.�JBf
 ��I have purchased other similar products (Bed Buddy)and was very satisfied--just wanted to try something that would wrap around the neck more.�JCf
 �FI wonder if all of the Happy Company products are like this one?--ugh!�JDf
 �#It stays warm for maybe 10 minutes.�JEf
 �VI reheated it for maybe 3 minutes and now it stinks like a burning smell all the time.�JFf
 ��The Cherry stone neck pillow is much better, it stays hot longer and the heat is stronger as the weight of the wrap really helps the heat sink into sore neck muscles.�JGf
 �'Buy the cherry stone neck wrap instead.�JHf
 � I use it everyday and its great.�JIf
 �fI love the countryside that Marsh describes in this book and Overture to Death (set in the same town).�JJf
 �^However, I found the bizarre nature of the host almost unbelievable and somewhat antagonizing.�JKf
 �LThe character interaction is excellent and the whodunnit portion satisfying.�JLf
 �AFYI, the host is not murdered, as is implied in the first review.�JMf
 �XWho is murdered is part of the suspense, so you are better left not knowing this detail.�JNf
 �@Really not her best novel: kind of slow and somewhat artificial.�JOf
 �hOf Human Bondage (Signet Classics)is about a fellow with a club foot,so the playwright here isn't alone.�JPf
 �vIn 1940 club feet were all the rage asBoris Karloff as a fictitious club-footed executioner Mord in "Tower of London".�JQf
 �WThe botched plastic surgery that forms the main plot device is stilltaking place today.�JRf
 ��The estate entail is still active in England and Wales,so that first sons get the land which in many cases is the basis ofthe family fortunes.�JSf
 ��I thought that this mystery was kind of long and somewhat boringcompared to an earlier novel by this authorthat took place in her home New Zealand.�JTf
 ��The upper class snob effect is very much in evidence here,and I think the early novel was better becauseit was more realistic and less contrived.�JUf
 X
  As my mixed playlist drove through five ace songs from this CD once again, and as I looked up for the fiftieth time to see who was playing such nice crunchy guitars, I realized that I hadn't given much thought to where this music came from and what happened to them.�JVf
 �YSure, as others point out, they stole everything, an entire band a song, but who doesn't?�JWf
 �|Shoot, other groups like Coldplay are pure derivative, and I have more songs off "Million" than I do "X&Y;" on my playlists.�JXf
 �So, where did they go?�JYf
 �Zero Wiki entry�JZf
 �E, the website is some shopping spam and Google comes up nearly empty.�J[f
 ��Mike Thrasher is apparently still in the biz as a promoter in Oregon, but still, in terms of profile vs. quality, this has to rank way up there for lack of notice.�J\f
 �GHeck, it was only 7 years ago, how could you be this solid and be gone?�J]f
 �=Ah well, past time for a reminder of how good this album was.�J^f
 �Check it out.�J_f
 ��I bought this CD because I heard "Come Here" as the background music for some fashion show, and I was totally hooked on that song.�J`f
 �VThen I bought the album and was really surprised how good ALL of the songs really are.�Jaf
 �PIt's a sunny mix of bright, smart, melodic, Califonia and Brit pop-rock-electro.�Jbf
 �gHere in Japan, a lot in life is kept behind closed sliding doors and emotions rarely surface in public.�Jcf
 �*Private and public are clearly delineated.�Jdf
 �WThe narrator courageously allows us into his complex private world of tangled emotions.�Jef
 ��Complicated sexual desire, an artistic sensibility, wit and intelligence create a picture of a precocious teenager that will remind you of Salinger's and Joyce's jaded teens.�Jff
 ��The narrator is intensely introspective, sympathetic, and has an active imagination fixated on death, sex, and workingclass muscular male bodies.�Jgf
 �pGay and straight readers alike will find this novel engaging and full of meaning about growing up behind a mask.�Jhf
 �zAmazon product description talks about diamond certification but the fine print says that no certificate will be provided.�Jif
 �4I ordered this item and NO certificate was provided.�Jjf
 �<Also, Amazon charges 15% restocking fee for jewelry returns.�Jkf
 �#And all returns need to be insured.�Jlf
 �#So, return becomes super expensive.�Jmf
 �-THINK before you buy jewelry from Amazon.com.�Jnf
 �6You are probably better off buying from a local store.�Jof
 �@Like many reviewers here, I also read this book a LONG time ago.�Jpf
 �LYet there is one idea from this book that has stuck with me all these years.�Jqf
 ��As a creative person, it has sustained me, encouraged me, and got me going again whenever doubt crept in: "If you are a maker, you will know that somewhere the thing you would do has already been done, and you will set about quietly to do it."�Jrf
 ��That said, file my latest effort,Shake That Brain: How to Create Winning Solutions and Have Fun While You're At It,under "books on creativity."�Jsf
 ��What I read of this book looked good, but the author had the great idea of formatting it like a court reporter's transcript, complete with numbered lines and page numbers at the top of the page.�Jtf
 � The heroine is a court reporter.�Juf
 �Get it?�Jvf
 ��The transcript format works fine on the printed page, but if you're reading on a Kindle and change the typeface size, you can wind up with a jumble of lines that break at weird places and numbers that don't serve any useful purpose.�Jwf
 �#I read 10% of the book and gave up.�Jxf
 �PIt was good, but not good enough that I wanted to struggle deciphering the text.�Jyf
 �fThe format in which the book was written made it difficult to read so therefore I never read the book.�Jzf
 �?I wanted to because of the summary I read before I purchased it�J{f
 �Pbut when it came over every line was numbered and I cannot read books like that.�J|f
 �JMaybe it was just my Kindle that this happened to, I am not sure, a fluke.�J}f
 ��Who knows but I cannot read numbered lines unless it is a reference book and is supposed to be like that for I was not expecting that kind of format.�J~f
 �{I have had a headache for over 15 yrs and difficult formats or tiny prints worsen the pain, so I avoid things such as this.�Jf
 �In a nutshell, this one sucked.�J�f
 �9Not even sexy like past ones of girl/girl or solo action.�J�f
 �ZBasically they just showed facial closeups of the girls doing their thing or them moaning.�J�f
 �Boring!�J�f
 �I HATED it.�J�f
 �UI have about 15 other GGW videos that I love, but this one completely disapointed me.�J�f
 �JThe "solo" scenes aren't at all what you would expect from this franchise.�J�f
 �Three thumbs down!�J�f
 �I really like these canisters.�J�f
 �1The bright red color really livens up my kitchen.�J�f
 �LThey are very sturdy and have not yet chipped (even with 4 kids using them).�J�f
 ��The only reason I did not give it 5 stars is because the rooster handles on the lids are a little too small to grab when taking the lid off.�J�f
 �Other than that, I love them.�J�f
 �ZPsychedelic/hip-hop/jazz-funk approximates what you will find on this groovy two-disc set.�J�f
 ��With instrumental flashes of the Zap Band and a voice akin to Barry White's (though higher-pitched), Trible and friends produce dynamic tracks that will challenge assumptions.�J�f
 �wHippy and hipster sets will accept this record immediately, but it might take the hardcore hip-hoppers a little longer.�J�f
 �qThey, like all people, need to take Trible's message to heart: "It's all about love, peace, unity and havin' fun.�J�f
 �"�J�f
 ��Perhaps that's why the trio included the second disc of instrumentals; to draw in rappers thirsting for innovative beats who might not have picked it up otherwise.- Ky JunkinsSynthesis.net�J�f
 ��The realist in me feels like it is all a little too far fetched, and way beyond what most people can relate to however for those who enjoy a rollicking good ride, well, why not?�J�f
 ��Make believe can produce all sorts of wild and wonderful experiences, so I would have to recommend highly for those who like to step outside the straight and narrow.�J�f
 �A real page turner!!�J�f
 �2This is the second entry in the Elvis Cole series.�J�f
 �VIt is not quite as good as "The Monkey's Raincoat", but it is still very entertaining.�J�f
 ��Elvis is deeply involved in martial arts and yoga and Mr. Crais has him mixing it up with the Japanese mafia, whose members also follow the discipline.�J�f
 �xElvis is hired by the assistant to a high profile hotel magnate who has aligned himself with the Asian community in L.A.�J�f
 �_An ancient manuscript in the magnate's care has disappeared and Elvis is brought on to find it.�J�f
 �NMr. Crais suffers a little bit of a sophomore slump with "Stalking The Angel".�J�f
 �THe uses some tired mystery novels cliches, many of the charcters are stock profiles.�J�f
 �_Despite some of these shortcomings, this is still an above average story and an enjoyable read.�J�f
 �|I'm a big fan of Crais' work, including the early novels, but Stalking the Angel lacks the energy and sparkle of the others.�J�f
 �<The plot holds few surprises and the wisecracks seem forced.�J�f
 ��There's a flatness to this work, despite some good moments, and the key character, a 16 year old girl, never really comes to life.�J�f
 ��If you haven't read any of the early Elvis Coles, don't start here -- I'd suggest beginning with the first one, The Monkey's Raincoat, then moving on to Lullaby Town.�J�f
 �TI have read several Elvis Cole novels and really enjoyed them all... until this one.�J�f
 �$The plot was just SO unbelievable!!!�J�f
 ��He goes out on a limb (not unusual for Elvis) but for such a teeny, tiny reason, then, there's a ton of violence and posturing (OK, so that's not unusual for him, either).�J�f
 �iIt was the assumptions he made and the unwelcome "Knight in Shining Armor" schtick that really got to me.�J�f
 �7Maybe I've just read one too many of his, or something.�J�f
 ��Also, I must say, that I rely heavily on Publisher's Weekly Reviews to help me determine which books to read (so many books, so little time...), and I feel the review for this book was way off base.�J�f
 �?And, honestly, how does Elvis pay his mortgage and power bills?�J�f
 �aI've yet to see him take money for a case, isn't he supposed to be a 'professional' private eye??�J�f
 �;I just could NOT suspend disbelief while reading this book.�J�f
 �Shades of Mikky Spillane.�J�f
 �Very cliche.�J�f
 ��The wife of the rich man supposedly works very hard and makes lots of sacificies to become the wife of a rich man and she has no other marketable skills.�J�f
 �oYet when she learns her daughter had been molested by the father she supposedly attacks and condems the father.�J�f
 �HaHa.�J�f
 �yAfter so-o-o-o much work and sacificie to win the position and with no markable skills it is not likely this would occur.�J�f
 �kMost likely she would declare the daughter a lier, kick her out of the home, defend the husband and father.�J�f
 �>IS SHE REALLY GOING TO DESTROY HER ONLY MEANS OF LIVELY HOOD??�J�f
 �VGet real, do you think she wants to be homeless and starve over alittle bit of incest?�J�f
 �You don't know women.�J�f
 �&Not half the book its predecessor was.�J�f
 �mFriends tell me the next book is not very good, either, but that the more recent books are as return to form.�J�f
 �CSure hope so: "The Monkey's Raincoat" whetted my appetite for more.�J�f
 �fThis novel has all the gritty realism of a late 70s network cop show: TJ Hooker meets Starsky & Hutch.�J�f
 �rThe plot was painfully predictable, the characters laughably cliched and the dialog stupid, forced and irritating.�J�f
 X  Toward the end of the book, after heroes Elvis and Pike, easily blow away a half dozen oriental villans, they proceed to voluntarily and inexplicably put down their weaponry in order to engage in hand to hand combat with one final bad guy...just like on tv.�J�f
 �bI, for one, expect more from a detective novel and won't waste my time with the other Crais books.�J�f
 �=When Cole walks into the room you say, "What will happen now?�J�f
 �;" This is the best thing you can say about a popular novel.�J�f
 �So I like to read Robert Crais.�J�f
 �RUN FOR IT!�J�f
 X  This is my first Robert Crais book, after I had seen all of his work rate so highly on Amazon.com, and I had to say that it is brilliant!Crais introduces us to ex-cop, now PI, Elvis Cole - who is very much into martial arts (with baddies) and witty one-liners with potential clients.�J�f
 ��Along with his psycho friend, they out to the streets and fight crime (along probably making & breaking their own rules as they go along).Stalking the Angel brings us into the world of the Japanese Mafia and kidnapping.�J�f
 ��Although this can be a heavy subject, it is dealt with in an extremely amusing & entertaining storyline and you will be laughing along with Cole's antics and razorsharp dialogue.�J�f
 ��If you are looking for a good crime/thriller novel with alot of humour and entertainment, then don't pass up Stalking the Angel!�J�f
 �0I recently came upon this author and I love him.�J�f
 �GHe has very humorous dialogue in his books that make me laugh out loud.�J�f
 �8He is on my list now and I intend to read all his books.���      J�f
 �I liked this book .�J�f
 �It was a fun read.�J�f
 �UCraig's is a very entertaining author and can make me laugh right out loud sometimes.�J�f
 �(This is just a nice quick read to enjoy.�J�f
 �Good drama and good humor.�J�f
 �Very entertaining read.�J�f
 �GElvis cole and Joe pike are a good team and they compliment eack other.�J�f
 �$I've read this book out of sequence.�J�f
 �,Possibly that's why I wasn't crazy about it.�J�f
 �>I like the characters of Elvis Cole and Joe Pike a great deal.�J�f
 �$I felt they could not save the plot.�J�f
 �pUndoubtedly, the later novels containing this pair's exploits have been honed more finely than the earlier ones.�J�f
 �!This suffered from a so-so story.�J�f
 �#The later novels I find much better�J�f
 �.This is the third Robert Crais book I've read.�J�f
 �cI'm late on the scene so I started at the beginning of his bibliography with the Monkey's Raincoat.�J�f
 �0I must say I enjoy the characters and the humor.�J�f
 �KHowever, in this particular book I had to work hard at believing the story.�J�f
 X+  With so many clever defense lawyers out there, and so many cases thrown out because the information was somehow tainted because a detective or policeman didn't follow the proper protocol, would a police detective so freely divulge the facts of a case to a PI just because they know him and like him?�J�f
 �|Also, how is it that Elvis and Pike can break into a house and rummage through people's things without leaving fingerprints?�J�f
 ��How do they get away with using their guns to intimidate people, and more importantly kill that many people without more hassle from the police or the legal system?�J�f
 �Really?�J�f
 �-This item is definitely of very poor quality.�J�f
 �RTo use this thing you will need to sign up at your local gym for stength training.�J�f
 �=Bought this because I saw one in use at an Italian resturant.�J�f
 �xNext time I am there I will check and see who makes it(what I should have done prior to ordering this one)and order one.�J�f
 �GThis one is in the trash and should be in the land fill by next Monday.�J�f
 �It grates cheese well.�J�f
 ��My biggest problem is that when the metal parts rub together while cranking, it tends to get metal particles in the grated cheese.�J�f
 �%I would never buy this product again.�J�f
 �NThis grater works fine and it comes apart for easy cleaning in the dishwasher.�J�f
 �>The problem is that the finish comes off and discolors cheese.�J�f
 ��I was using the grater to grade parmesan into lasagna and some of the cheese strands were a grey, slate blue color from the finish.�J�f
 �GThis happened several times and with several different kinds of cheese.�J�f
 �XI bought this to replace a workhorse stainless rotary grater that I had given to my son.�J�f
 �QThe HIC looked the same as its predecessor, but has been a bitter disappointment.�J�f
 ��There is no lock to hold the drum in place and it requires maximum strength on the handle to hold press down on the cheese to grate it effectively.�J�f
 �"(I use it only to grate parmesan.)�J�f
 �#The cutting edge is not very sharp.�J�f
 �]Most disturbingly, every time I use this grater, a faint bluish powder appears on the cheese.�J�f
 ��At first I thought the cheese was moldy, but on closer inspection, it appears that the metal-on-metal pressure is producing a small layer of metal filings.�J�f
 �Ick.�J�f
 �)We have had this cheese grater for years.�J�f
 �It has had problems for years.�J�f
 �+It leaves black marks on the grated cheese.�J�f
 �9I don't think it is metal flakes, but it is unappetizing.�J�f
 �$Will be finally replacing it today!!�J�f
 �7But otherwise, there are many better recordings of his.�J�f
 �[Start with the Coltrane collaberation and then the soundtrack to Bridges of Madison County.�J�f
 �2After that, if you want more, jump to the box set.�J�f
 �'He is truly one of the great vocalists.�J�f
 X  Another fine example of Hartman's lush singing (just likeI Just Dropped by to Say Hello) doesn't have the proper instrumental backing, apparently necessary to propell the singer to the greatness he is more than capable of achieving (seeJohn Coltrane & Johnny Hartman)...�J�f
 �-Still, nice music, very romantic and musical.�J�f
 �mThis masterpiece came at a time when Stevie was at his best and generally speaking popular music was amazing.�J�f
 �%Includes my favourite song "Visions".�J�f
 �I love Stevie.�J�f
 �I love funky "Sir Duke" stevie.�J�f
 �CThis album was so highly billed, but it was a real let down for me.�J g
 �(I wanted more funk, and did not find it.�Jg
 �4you will find that many sounds take your mind away!.�Jg
 �A magic moment with Stevie.�Jg
 �cWatching BET's Walk Of Fame tribute to Stevie the other day inspired me to write some reviews here.�Jg
 �II don't have the "perfect" view of this album that most Stevie fans have.�Jg
 �wAside from Living For The City, Higher Ground, and Jesus Children Of America there isn't anything I like on this album.�Jg
 ��There is just something about Stevie's languid, rather sappy, elevator-music ballads that I can't stand, and this cd has a bunch of them.�Jg
 �zHaving said that, I absolutely love the great stuff on this cd, with Jesus Children Of America being the highlight for me.�Jg
 �/That alone makes it a disc worth owning, to me.�J	g
 ��Overall though, I think I am going to look into getting that multi-disc Stevie box-set that came out a few years ago or whenever it was.�J
g
 �cThere just aren't enough Stevie tunes I love per-disc to make buying all his albums worth it to me.�Jg
 �~I don't own any of those "greatest hits" types of box sets but depending on whats on that multi-disc Stevie set, I may buy it.�Jg
 �Stevie at his best !�Jg
 �1Album & cover are in great shape, what a treat. "�Jg
 �9Living for the City", "Higher Ground" familiar favorites.�Jg
 �LDidn't realize how well Stevie could sing until heard "All in Love is Fair".�Jg
 ��I have loved this album since the first time I heard on vinyl (part of my dad's collection)--there isn't a single cut not worth listenintg to over and over.�Jg
 �4I simply can't believe it took me so long to own it.�Jg
 �AI am now passing the music on to a third generation of listeners.�Jg
 X�  This album is still just as incredible and compelling to meas it was when I first heard it as a 9-yr old kid in 1973!Those were the days when artistic giants still walked the earth!Just put it on...you don't have to skip over any tracks!Now how many albums can you say that about today?"Too High", "Visions", "Living For The City", "Golden Lady","Higher Ground", "Jesus Children Of America", "All In Love Is Fair","Don't You Worry 'Bout A Thing" and "He's Misstra Know-It-�Jg
 �All"...�Jg
 �YOne complete thematic vision from start to finish!!'Nuff Said!Incidentally, 1976's "Songs�Jg
 ��In The Of Life" is number 2and 1972's "Talking Book" is number 3!Only but a handful of artists of the last 30 yrscan claim even one masterwork in their career!Stevie has three!�Jg
 ��He was definitely tuned in to thecreative force of the universe during his hey-day!His music is just timeless and seamless and will alwaysinvoke love, stimulate positive thought & emotion!�Jg
 ��Stevie Wonder's meditations on inner city life and the African-American push towards middle-class stability in the wake of the '60s civil rights movement made for some pretty great music...�Jg
 ��This disc had its share of radio hits and anthemic ballads, such as "Living For The City," "Golden Lady," and "Don't You Worry 'Bout A Thing," but it also has some curious sleeper tunes as well, such as "Misstra Know�Jg
 �9It All," which is one of my favorite Stevie Wonder songs.�Jg
 �<Oh, yeah, "Higher Ground" is also an amazing funk classic...�Jg
 �As is this whole album!�Jg
 ��I have been using this product and not only have my stretch marks gotten worst, I now have a very itchy red rash all over my stomach.�Jg
 �}I started researching rashes during pregnancy and found that a lot of women are getting rashes after using palmer's products.�Jg
 �4I will not be recommending their products to anyone.�J g
 �SThis book is well oganized and easy to use, but the information is often incorrect.�J!g
 �3We have found it to cause more problems than solve.�J"g
 �6Paul Clayton's book come much more highly recommended.�J#g
 ��I don't mean to quibble or diminish what Grier has served up, but it should be said for potential buyers: many tracks on this album are not just a single guitar (I didn't realize it myself until I listened with headphones).�J$g
 �|Some tunes include subtle rhythm chords and occasional unison and octave doublings that create an almost twelve-string feel.�J%g
 �vI don't think there's any intent to deceive here, the music is wonderful and ultimately who cares how its constructed.�J&g
 ��In my younger "guitar freak" phase I was more into the idealistic "one person, one guitar" purity thing and this record would have bugged me a bit (which shows where I was at).�J'g
 �9Just in case there's anyone out there who feels that way.�J(g
 �}Even though Grier's coming out of the White/Rice school in terms of touch and phrasing, this record put me in mind of Blake's�J)g
 �A"Whiskey Before Breakfast", another great "solo guitar" recording�J*g
 �that's a got more going on.�J+g
 �+How can one guy and a guitar sound so good?�J,g
 �@A total mystery to those of us who can't even strum a G chord...�J-g
 �0but all I can say is I'm glad someone can do it.�J.g
 �JThe songs on this cd are mostly familiar tunes, along with some originals.�J/g
 �My favorite?�J0g
 �VHard to decide--either the title track (written by Grier) or his take on Bill Cheatum.�J1g
 �.You won't hear flatpicking like this anywhere.�J2g
 �MThis is music of the highest quality--can't seem to take this one off repeat.�J3g
 � I -never- get tired of this one.�J4g
 �3Sadly, "Too Good To Be Forgotten" has been deleted.�J5g
 �JWhen used "marketplace" copies become available, they fetch a hefty price.�J6g
 ��Good news, though - the same label (Demon/Edsel) has issued an even better deal - two sets totalling four CD's compiling every track from the Chi-Lites' eight LP's for Brunswick - plus bonus tracks!�J7g
 �,Search for "Complete Chi-Lites On Brunswick.�J8g
 �" You can thank me later....�J9g
 �TRACKLISTING:1.�J:g
 �MC Mario Radio Mix 3:422.�J;g
 �Radio Premire 3:353.�J<g
 �MC Mario Mastermind Mix 6:214.�J=g
 �1Neo Disco Premier Mix 6:215. 1-800 Dis-N-Dat 7:20�J>g
 �I like the format very much-�J?g
 ��I am disappointed that it appears to be limited to beginning level Spanish which was not indicated on the purchase information and there is no way to access differnet levels.�J@g
 �all in all, I like it however.�JAg
 �\I recently took a Spanish emersion program in Costa Rica and this is a good segue from that.�JBg
 ��I also tried to use this to boost the very low audio from the AVIC-F500BT and it introduces a significant amount of engine noise from the power line without any direct ground except (I assume) through the RCA's (who wants that??).�JCg
 ��I'm surprised it wasn't designed to eliminate this as other inline boosters do - very poor design and one that isn't very functional without adding a filter.�JDg
 �nI do not recommend this product, but if you do use it, be prepared to get a filter to remove the engine noise.�JEg
 �NI normally don't read detective novels, but this wasn't a typical who-done-it.�JFg
 �jThe murder mystery was a mere backdrop for a compelling portrait of a man living with Tourette's Syndrome.�JGg
 ��I personally live with two people who have obsessive-compulsive disorder, and it was helpful to be reminded how ever-present these types of conditions are, and how hard one must work just to stand still.�JHg
 ��I enjoyed a positive portrayal of a man who found a place in the world where he can function and even thrive, despite the epithet of "freakshow."�JIg
 �fThe only part that seemed unrealistic is the lack of embarrassment Lionel Essrog seemed to experience.�JJg
 ��Even the most stoic of individuals with Tourette's go through moments of exteme embarrassment and feelings of shame, and frustration at their lack of control over their ticcing behaviors.�JKg
 ��While I'll concede that there is an original premise to this novel (specifically the condition of the narrator) overall it didn't seem all that remarkable.�JLg
 �xThe writing was serviceable, nothing spectacular, and the story moved along and kept me interested--but that's about it.�JMg
 �oI didn't care much about the characters, and I read the entire book just wondering what the big deal was about.�JNg
 ��Maybe if it hadn't been so talked up I would have had lower expectations and appreciated this fairly entertaining novel for what it is.�JOg
 ��I have read some of Lethem's other works and found them to be unsatisfying, but Motherless Brooklyn is a one-of-a-kind novel, with a main character that turned out to be even more sympathetic that I expected.�JPg
 ��The whodunit detective story that is the background of this work is only OK, but Lethem's writing and his protagonist Lionel Essrog leave a major impression.�JQg
 ��Touching without being sentimental, this is the story of a physically flawed yet three-dimensional human being, and I only wish that the author would write a sequel so I could be assured that Lionel gets the happy ending he deserves.�JRg
 �FAn unusual literary treat that had a remarkable effect on this reader!�JSg
 �bAs a mother of a gentleman with Tourretts, I found this book very informative and extremely funny!�JTg
 �A good read.�JUg
 �XI generally prefer books with more "heft" in terms of meaning, this was a little frothy.�JVg
 �:This said, the writing was good and I chuckled throughout.�JWg
 �9A very interesting main character - who would of guessed!�JXg
 ��How about a thriller set in Brooklyn featuring a protagonist with Tourette syndrome, a couple of gangster brothers, some Zen intrigue and a subplot involving the Japanese sea urchin egg industry?�JYg
 �yIf this sounds intriguing, you will enjoy this highly contrived, yet entertaining tale featuring cartoon-like characters.�JZg
 �HThe plot involves two brothers in over their heads in crime in Brooklyn.�J[g
 �lOne brother goes to a home for boys ("motherless" boys) and selects a few to help out with his crime capers.�J\g
 �?One of the boys, Loinel Essrog, suffers from Tourette Syndrome.�J]g
 �LHe narrates the story starting with a murder and ending with him solving it.�J^g
 �I won't say more than that.�J_g
 �hIt is interesting to get some insight into Tourettes (assuming this novel is accurate in its portrayal).�J`g
 �7I appreciate that the author tried something different.�Jag
 �`If you want something a little different from the average detective story, give this book a try.�Jbg
 �....hated the book.�Jcg
 �I guess it's a genre thing.�Jdg
 �qI don't care for detective stories, but decided to give it a try after I read a few of positive customer reviews.�Jeg
 �hThe author did raise the stakes by the "added touch" of having the protagonist have Tourette's Syndrome.�Jfg
 ��I found this very interesting and was drawn to these scenes but the detective stuff bored me mostly and really couldn't wait for it to end.�Jgg
 �xIf you have not read any of Jonathan Lethem's other books, this one might turn you off to his Science Fiction abilities.�Jhg
 �UAmnesia Moon is what essentially dragged me into his other books, including this one.�Jig
 ��This book has less of a twisted plotline than some of his others (for instance, Girl in Landscape); however, the Tourettes is a beautiful touch and I applaud him for it.�Jjg
 �sI gave Motherless Brooklyn three stars for two reasons: One: I much prefer the Sci-Fi touch of his other books, and�Jkg
 �Two:�Jlg
 �MI have not finished this yet and thus far feel the need for only three stars.�Jmg
 �zIt's a good book if your not expecting him to be in outer space or anything, and it's wonderful if you're into psychology.�Jng
 �lI wouldn't recommend it if you haven't loved his other books, for the talent would go unnoticed in this one.�Jog
 �Hi dont really like it because i dont ccare for crime type mystrey books.�Jpg
 �.however i was intrsted for the tourettes part.�Jqg
 �1i have ts myself and found the protraly accurate.�Jrg
 �$i felt the book was aliitle to long.�Jsg
 �3I am sorry, but this isnt a "serious" piece of lit.�Jtg
 �KThis is another one of those stories that my grandfather gets into so much.�Jug
 �HThere is no excitement, no story line that is different then the others.�Jvg
 �UWhen you all find a book that "Oh my God" is different than the rest, give me a ring.�Jwg
 �6I am not sure why this book has received so much hype.�Jxg
 �&The plot was confusing and convoluted.�Jyg
 �_It was difficult to understand what compelled the characters to take the actions that they did.�Jzg
 �rThe main character's struggle to keep his Touretts under control was entertaining, but was difficult to relate to.�J{g
 �dAll in all, this book resembles more a work of detective fiction than it does a great literary work.�J|g
 �>The hero and portrait of the Tourette's world are captivating.�J}g
 �GThe plot is sub-standard, the secondary characters all one dimensional.�J~g
 �NThis story gives a great insight into people who suffer from turrets syndrome.�Jg
 �kI wasn't thrilled with the story line but thought the main character troubled in trying so hard not to tic.�J�g
 �I liked the book.�J�g
 �2Great concept but a bit too gritty/gangsta for me.�J�g
 �[I felt the interruption of the sentences with his tics and syndrome blurts were a bit much.�J�g
 �Sorry to all the Lethem fans.�J�g
 �pThis book was very hard for me to keep in my hands and I only finished it because it was recomended by a friend.�J�g
 ��It may be the fact that I am not from NY and don't understand the neighborhood references or just I am too compasionate for the main character.�J�g
 �'I thought this was a really a bad book.�J�g
 �#I would not reccomend it to anyone.�J�g
 ��Sorry to disagree with all that hyper-praise, but this is just another run-of-the-mill detective story parroting Raymond Chandler, James Cain, Mickey Spillane and God knows who else.�J�g
 �DThe Tourette business is just a gimmick, adding nothing to the plot.�J�g
 �>Hard to believe that this is classified as serious literature.�J�g
 ��If you liked the boring mystery novel that was the Da Vinci Code and if you thought writing a novel from the perspective of an autistic child was clever, then this is the book for you.�J�g
 ��On the first page Mr. Lethem compares subvocalizing to tickling reality like piano keys (whatever that means), an army, and replacing divots.�J�g
 �KHis similes are too diverse to create any sort of impression on the reader.�J�g
 �Read Lolita instead.�J�g
 ��A friend gave me this book when I moved to Brooklyn (the area of Bklyn where the book takes place) and as he handed it over said "It's not very good, but you will appreciate it.�J�g
 ��"Later that week, I mentioned that I had gotten Motherless Brooklyn to another friend (both of which have great literary taste in my opinion) and she said essentially the same thing.�J�g
 �[Now that I have read it I can safely say that the plot was weak and the end was even worse.�J�g
 �WI decided to give him another chance with "Fortress of Solitude" and som far so good...�J�g
 �but this one is a stinker.�J�g
 �This is a great book.�J�g
 ��Lionel tells his story, complete with Tourette's interruptions, of how he tracks down the killers of his mentor and father-figure.�J�g
 ��The Tourette's alone makes the story fascinating, as you watch Lionel struggle with his syndrome during conversations with people who do not understand why he does what he does (tics).�J�g
 �#The murder and intrigue top it off.�J�g
 � Great read, highly recommend it.�J�g
 �BI read this on a recommendation because of my love for Fight Club.�J�g
 �SI don't think the comparison is fair and I found myself bored from start to finish.�J�g
 ��The prose is dense and colorful, which is why I gave it two stars, but it is too cumbersome for a story and a lead character such as this.�J�g
 �|The plot never twisted, I was never surprised, and I didn't relate to any characters nor learn anything from the whole book.�J�g
 �yThe Tourette's theme, in my opinion, seemed wedged into the story at times, and would only amuse the most simple-humored.�J�g
 �IMaybe Edward Norton will make something out of this, but I sure couldn't.�J�g
 �)Why can't more authors write like Lethem?�J�g
 �^He's that rare combination of storyteller and prose turner that keeps us coming back for more.�J�g
 �We laugh and cry.�J�g
 �kThis is one of my desert island books, along with Klim's Jesus Lives in Trenton and Palahniuk's Fight Club.�J�g
 �Take them if you will.�J�g
 �XAn amazing tour-de-force; Tourette's as a wildly successful literary and literal device.�J�g
 �^A wonderful tour of the seamy side of Brooklyn as well; sort of Paul Auster meets James Joyce.�J�g
 �A great read.�J�g
 �*Read it slowly to savor the language play.�J�g
 �ZThis book had fabulous characters, a suspenseful, complicated plot, and wonderful writing.�J�g
 �<I often laughed aloud and read sections aloud to my husband.�J�g
 �)So imaginative, creative, and compelling.�J�g
 �$I'd give it another star if I could!�J�g
 �VThe writing is excellent and original, but the story and plot isn't all that inspired.�J�g
 �=Sweet main character with few chances in life who makes good.�J�g
 �FIt's hard not to like Lionel Essrog, but Lethem didn't go deep enough.�J�g
 �We're teased.�J�g
 �I was a little disappointed.�J�g
 �ALethem's "Motherless Brooklyn" is a classic mystery with a twist.�J�g
 �zIts narrator has Tourette's syndrome, and his condition emerges on the page in the form of a jerking, twitching narrative.�J�g
 ��While the story itself is undistinguished - it's a P.I. book, no more, no less - the main character, Lionel Essrog, has one of the most distinct and memorable voices in contemporary literature.�J�g
 �SHis brain is like another character, a comic genius who always does the unexpected.�J�g
 �QAnd it's this character, the Tourettic madman, that makes the story so enjoyable.�J�g
 �yThe fool, the jester, the clown acting up in a mob-run Brooklyn neighborhood, tracking down criminals, and meeting girls.�J�g
 �A great summer read.�J�g
 �qLethem has created a sympathetic, intriguing, and wonderfully whimsical alternative to the hard-boiled detective.�J�g
 X  Lionel Essrog, the tourettic narrator of this short but rich caper is immesenly human, and though most of us will have little in common with his experience or his illness, let alone his predicament in this story, I found a little of myself in all of his foibles and follies.�J�g
 �Wonderful, witty, well-crafted.�J�g
 �Wobbly, weegly, wear witchess.�J�g
 �,Total gibberish, literally and figureativly.�J�g
 �7How can this book be rated so highly by so many people.�J�g
 ��the author is so involved in what he thinks is his own genius that half the time I don't understand what the hell he is talking about.�J�g
 �5The only thing that makes sense is Tourettes Syndrome�J�g
 ��As I'm sure you've heard or read, this book's protagnonist and narrator suffers from Tourette's Syndrome, and his verbal tics seem to require the spontaneous creation of Joycean wordplay.�J�g
 ��This *sounds* like a recipe for a psuedointellectual disaster of a mystery novel, complete with unnecessary MFA-level artsy flourishes, but Lethem is a very fine writer and he not only makes it work, it works very well indeed.�J�g
 ��While the plot didn't quite have me on the edge of my seat, the loving, finely drawn characterizations of Lionel, the narrator, and his struggle with the crossed wires in his head, kept me captivated to the very end.�J�g
 �)Yet, Jonathan Lethem makes us understand.�J�g
 ��This is a brilliant work, filled with completely fleshed out characters, a crazy, creative plot, and a murder mystery wrapped in a sense of the absurd.�J�g
 ��Lionel Essrog is the central character with Tourette's syndrome, and I cannot say enough superlatives about his characterization and dialogue.�J�g
 ��I found myself thinking about Lionel long after I finished the book; the tics, the words, the creativity, I can't say enough good things about this work.�J�g
 �IWhat a great book that balances prose, plot, characters, and originality.�J�g
 �This book is unforgettable.�J�g
 �}Kudos to Lethem for creating Lionel, one of the most captivating and provocative narrators to hit the shelves in a long time.�J�g
 X  Part of Lethem's genius is to allow Lionel to be more than a one-trick pony--Lionel's struggle with his Tourettic self is less a punchline than an insightful exploration of a debilitating condition that informs both Lionel's view of the world, and his unique position in it.�J�g
 ��One part mystery and two parts memoir, Motherless Brooklyn is filled with memorable one-liners and anecdotes conveyed with the frenetic energy that radiates from its narrator.�J�g
 �SWith his unlikely hero, Lethem puts a new spin on the hardboiled detectives of old.�J�g
 �BI felt a little sorry for one of the main characters in this book.�J�g
 �XIt's a clear reminder that the friendships we choose are the foundation of our progress.�J�g
 �LIt is also an interesting NYC area (Brooklyn) psychological character study.�J�g
 �ZThe plot did not really capture me like other books I have read that have won Book Awards.�J�g
 �SIt did make for some good reading and some very well-written & unforgettable lines.�J�g
 �EA recent college graduate raved about the book which made me curious.�J�g
 �hHis excitement was a bit overdone as far as I was concerned because the story sort of fell flat with me.�J�g
 �@The book did succeed in keeping me awake (through a plane ride).�J�g
 �.One line I took away was, "Don't tug my boat!"�J�g
 �EAs a psychiatrist I thoroughly enjoyed reading 'Motherless Brooklyn'.�J�g
 �nThe plot was somewhat mundane but the portrayal of Lionel's Tourette syndrome was more than just a case study.�J�g
 �/Lionel was a well drawn and loveable character.�J�g
 �#His Tourette symptoms became alive.�J�g
 �XHaving friends who live in Brooklyn Heights also added to the familiarity of this novel.�J�g
 �7I have to pick a selection for my book club next month.�J�g
 ��Having read a number of excellent books over the summer (I'm mostly retired), it was a tough choice, but Lethem's crisp and easily read writing won out.�J�g
 �[I think most readers would really enjoy the humor and the unusual state of the protagonist.�J�g
 �qThis book is about a bunch of low-life losers who follow another low-life loser and, by the way, solve a mystery.�J�g
 ��The main character is Lionel Essrog, an orphan who has never had a family (motherless) and never been out of his own little world (Brooklyn), yet is a likeable character who spits out profanities at inappropriate times due to his Tourette's Sydrome.�J�g
 �+A delightful book with engaging characters.�J�g
 �XThe mystery is more of a subplot to the character study of Essrog and his loser friends.�J�g
 ��By creating a narrator/ protagonist who suffers from Tourette's Lethem certainly took a bold and unique approach in this story of a group of Brooklyn orphans who are recruited by a local shady character to do a variety of 'jobs".�J�g
 ��Lionel Essrog narrates the story and his inner dialogue as he struggles with his condition allows the author to introduce some remarkable word play that is really the best this book has to offer.�J�g
 �=The novel's greatest strength is also it's greatest weakness.�J�g
 ��The plot involving a group of local Brooklyn street hoods playing at being detectives engages the reader at the start but slows to a crawl about halfway through.�J�g
 ��What kept me reading was the continued musings of the narration but ultimately the other characters are not truly fleshed out , the plot is somewhat thin and Motherless Brooklyn becomes a one man show.�J�g
 �`While that man is entertaining at times, its not enough to sustain the book for the full length.�J�g
 �+I really like Jonathan Lethem as an author.�J�g
 �>His books are usually quite interesting, clever, and humorous.�J�g
 �$Motherless Brooklyn disappointed me.�J�g
 �CThe humor was not there, I seriously think Lethem tried to be funny�J�g
 �but it simply failed.�J�g
 �sThe characters were especially bland, and it was hard to become "attached" to Lionel the main character/narrarator.�J�g
 �nHe spent too much time informing us that Lionel has Tourette's Syndrome, not enough time developing the story.�J�g
 �{The Tourette's was seriously (as someone previously stated in their review) the crutch that this book propped itself up on.�J�g
 ��Had it spent more time on the story, the book would have earned at least 4 stars because the story was interesting when time was spent explaining it.�J�g
 �KGun, With Occassional Music is a much better detective story in my opinion.�J�g
 �fAfter a while, while I enjoyed parts of the book, I found myself reading it so that I could finish it.�J�g
 �For serious Lethem fans only.�J�g
 �bThe first half of MB is excellent - solid on plot and character development and very well written.�J�g
 ��The second half is also well written, but has little in the way of interesting further character development and a plot that roams randomly towards a rather fizzled ending.�J�g
 �7MB could have been a first-rate short story or novella.�J�g
 �$It does not succeed at novel length.�J�g
 �0Unfortunately this really wan't my kind of book.�J�g
 �WTo me the plot just seemed to limp along without going anywhere interested or engaging.�J�g
 �nI didn't bond with any of the characters, which probably made the book less enjoyable that it could have been.�J�g
 �'I don't feel the book is poorly written�J�g
 �Lbut I think that it is just simply not the style of book that appeals to me.�J�g
 �xSo unfortunately I can't recommend this book to others but that shouldn't deter those who are interesting having a read.�J�g
 �Iif i was not a Edward Norton fan i would have never know about this book.�J�g
 �kI heard that he will star in this, he will be awsome, can't wait to see how he portrays a guy with torrets.�J h
 �kBut even if you are not a fan, read this book anyways it is a good way to pass some time, and enjoy it too.�Jh
 �;Some parts are dry, a tad slow, but most are very enjoable.�Jh
 �4the language is what gives this book a unique flare.�Jh
 �i love books in first person.�Jh
 �2this is a really good book, i highly reccomend it.�Jh
 �Not very cool.......�Jh
 �I love Yamamoto�Jh
 �1but.. this one just didn't do it for me.. sorry..�Jh
 �GThe recording seems to be not its normal class as well... disappointed.�J	h
 �elenny bertoldo from boston/nyc's xmix scores big on michael fredo's new single "love all over again".�J
h
 �Whe takes a light pop ballad and pumps it way up, turning this into a slammin' club hit.�Jh
 �Mthis is exactly what this otherwise lackluster song needed for it to make it.�Jh
 �)pablo larosa's remix is also pretty good.�Jh
 �oas a bonus, there's a video for fredo's previous single 'this time around' in the enhanced section of the disc.�Jh
 ��I got this toy for my child but after opening it when the block was pressed down we found that the sound of the animal didn't match the picture no matter what I tried it never corrisponded.�Jh
 �UI looked in the manual and it said that if this happens try changing the batteries...�Jh
 �I did and same thing.�Jh
 �I was going to send this back�Jh
 �9but I lost the return info in all the christmas wrapping.�Jh
 �DI took the thing apart and found that two of the wires were crossed.�Jh
 �HSo I took my soldering iron and swapped them and the toy works fine now.�Jh
 �BIts too bad that companies can't do a little better quality check.�Jh
 �?how hard would it be to check to see if the monkey goes "moo"??�Jh
 ��Ordered this item because my son wanted it for his 3rd birthday, we placed our order on 1/3/2013 and we were gaurenteed the item would be here by 8pm on 1/14/2013, my son's birthday was the 19th so we should have been all set.�Jh
 �MWell at 4pm on 1/14/2013 I recieved an email that the item is out of stock...�Jh
 �<so now I will have a disapointed 3 yr old on his birthday...�Jh
 ��the kicker is I could have ordered this from another company if they notified me sooner than the day it was supposed to be here!!!�Jh
 �LThe toy is great (our speech therapist has one) but this company BLOWS!!!!!!�Jh
 ��I got this toy for my child but after opening it when I pressed down on the block I found that the sound of the animal didn't match the picture no matter what I tried it never corrisponded.�Jh
 �UI looked in the manual and it said that if this happens try changing the batteries...�Jh
 �I did and same thing.�Jh
 �I was going to send this back�J h
 �9but I lost the return info in all the Christmas wrapping.�J!h
 �DI took the thing apart and found that two of the wires were crossed.�J"h
 �HSo I took my soldering iron and swapped them and the toy works fine now.�J#h
 �BIts too bad that companies can't do a little better quality check.�J$h
 �?how hard would it be to check to see if the monkey goes "moo"??�J%h
 �`"The Big Book of Scandal" from Paradox Press digs up all the hottest dirt from the 20th century!�J&h
 �>Find out that Clinton wasn't the first philandering president!�J'h
 �@And royal scandals didn't begin under Queen Elizabeth II either!�J(h
 �(Learn just how Hollywood became Babylon!�J)h
 �You won't believe your eyes!�J*h
 �*I was TOTALLY disappointed with this book.�J+h
 �,When I received it, it was a big comic book!�J,h
 �BNothing but comic strips throughout, which is not what I expected.�J-h
 �qIf you feel like reading the comics, pick up your local newspaper, otherwise don't waste your time with this one!�J.h
 �Are you kidding me?�J/h
 �XSure they're great songs, but only when they're sung by the people who made them famous.�J0h
 � This thing works off a satelite?�J1h
 �%How come it can't get the time right?�J2h
 ��If you don't mind getting detoured off a main road then back onto the same road after going out of your way; I reckon its alright,lol.�J3h
 �HIt also freezes up now and again out of the blue; why? ain't no tellin'.�J4h
 �LI would recommend someone looking for a gps to find one that actually works.�J5h
 �NMolloy has something different to say than most professional apparel advisors.�J6h
 �AHis theme is to stick with power projection as based on research.�J7h
 �UDecision points involve color, form and cut, pattern, matching, material and texture.�J8h
 ��This is useful for becoming familiar in an intuitive way with the historical basis for contemporary professional fashion, from which trends revolve.�J9h
 �hWhat is dated only requires modification, whereas the principles seem to have been identified by Molloy.�J:h
 �KIt's hard to believe there is no photos at all for a book on women's dress!�J;h
 �disappointed.�J<h
 �:No comarison can be found for what is a preferrable style.�J=h
 �+More like a narrative telling type of work.�J>h
 �*The advice in this book is rather painful.�J?h
 �ESome paragraphs feel elitist, sexist, ageist, and racist all at once.�J@h
 �%This is just like the business world.�JAh
 �&The data is old now, twenty years old.�JBh
 �$The illustrations are old-fashioned.�JCh
 �GHowever, the underlining theme and directions are still relevant today.�JDh
 �:When I began reading, the book seemed very dry and boring.�JEh
 �>Molloy certainly does not write to be lyrical or entertaining.�JFh
 �8However, I actually found some great advice in his book.�JGh
 ��I plan to go into law, so all the advice about suits and colors to make people listen to you and cooperate with you were very interesting.�JHh
 �I never knew most of it.�JIh
 �fI especially like how he "disproved" the idea that looking beautiful or sexy makes a woman successful.�JJh
 ��I would recommend this book to people who are concerned with getting good jobs and promotions, and are looking for specific advice on how to improve their image.�JKh
 �0There is a lot of negative reviews of this book.�JLh
 �MApparently women don't want to accept the cold truth about business dressing.�JMh
 �5Business people make judgements on first impressions.�JNh
 �IMolloy has studied the habits of executives and tells us what they think.�JOh
 �lIf you look around at women CEOs and and the like, they are dressed just like Molloy describes in this book.�JPh
 �(They played the game and got to the top.�JQh
 �NThey didn't complain that they couldn't wear certain things--they just did it.�JRh
 �CIn his book he tells you if you don't believe him test it yourself.�JSh
 �ZDress your way and make notes of how you are treated and the level of respect you receive.�JTh
 �+Then dress his way and compare the results.�JUh
 �"Try it--it could be an eye-opener.�JVh
 �SThe only thing this book lacks is photos, otherwise it is a very informative guide.�JWh
 �JI read this book and, to my dismay, found the author to be quite arrogant.�JXh
 �_As a recent college grad (96), I have used my fashion flair to pick out suits on many occasion.�JYh
 �9Of the five interviews I went to, I received four offers.�JZh
 �bI believe I would have looked stiff and out of touch had I followed the "directions" in this book!�J[h
 �<This probably is one of my worst purchases on Amazon.com ...�J\h
 �ait is all text, a few illustrations and just about the most impractical book on women's style ...�J]h
 �>God help anyone wanting to learn from this book.==============�J^h
 �rThis book is reasonable in its assumptions, and I believe that it really would help some women with their careers.�J_h
 �<But who really wants to read something all about appearance?�J`h
 �wOur society is superficial enough, and there is no reason to make it worse by devoting one's time to reading this book.�Jah
 �gMolloy should write something about helping people to find themselves rather than the appropriate shoe.�Jbh
 ��And I cannot believe that someone wrote a novel entitled "How to Date Young Women : For Men over 35," and that one of Molloy's books is being sold with it as a special deal.�Jch
 �dIf I was Molloy I would hate to know that someone laughed at me the way that I did when I read that.�Jdh
 �5My boss considers this book to be the absolute truth.�Jeh
 ��I especially hated the part that says, "if you're not thin, you're not upper class, you'll never be successful no matter how well you dress.�Jfh
 �" That's so disheartening!�Jgh
 ��I'm losing, but it will be a long time before I'm "thin" -- someone please write to me and tell me you know successful career women who are overweight--please!�Jhh
 �nGood overall message about dressing professional but, the clothing styles he writes about are from the 1980's.�Jih
 ��Let me sum this book up in a few sentences: Don't wear jeans to work, wear a suit that fits you, don't wear obtrusive shoes, wear boring colors like black, gray (but not too dark or too light), beige with a tinge of gray, or blue with a tinge of gray.�Jjh
 �Cut your hair short.�Jkh
 �Don't show your cleavage.�Jlh
 �Dress kind or like a man.�Jmh
 �DSounds pretty basic, but many women don't wear such outfits to work.�Jnh
 �sPerhaps it's because while this is a quick and easy read, there should be more images to show exactly what to wear.�Joh
 �'Also it's a bit dated by a few decades.�Jph
 �8And it doesn't consider different types of corporations.�Jqh
 �1What about creative companies (web, ad agencies)?�Jrh
 �kWearing the outfits described in this book to those companies might make you look too corporate and stuffy.�Jsh
 �OI found this book excellent for preparing for my job search and for interviews.�Jth
 �\In the process of my job search, I attended five interviews, and was offered five positions.�Juh
 �eGenerally, I hate interviews, but felt that I had excellent preparation through the use of this book.�Jvh
 �mI have lent it to several friends, who have reported their success using the mehtods suggested by the author.�Jwh
 �$Easy to read, practical, and USEFUL.�Jxh
 �IFirst off, I am a BIG Harvey Mandel fan and have been for 30 years or so.�Jyh
 �HI like the fact you can't label or pigeonhole this guy's style of music.�Jzh
 �ELike most gifted musicians, he wants to explore new areas and genres.�J{h
 �%That said, I can't listen to this CD.�J|h
 �Electronic beats?�J}h
 �Distorted guitar?�J~h
 �Where's the melody?�Jh
 �=This is by far and away the worst CD I have bought this year.�J�h
 �XMandel has every right to record anything he wants, but we have the right to not buy it.�J�h
 �,I'll listen to my other Mandel Cd's instead.�J�h
 �=Not your typical guitar style, this record just blew my mind.�J�h
 �bI can't stop playing this thing, it was great on the first spin, and it just keeps getting better.�J�h
 �II'm glad this is a cd cause if it was vinyl I'd have worn it out already.�J�h
 �8I wonder if I'm the only one that truly likes this here?�J�h
 �!But this thing works for my ears.�J�h
 �GGreat job Snake, looking forward to the next one keep up the fine work.�J�h
 �GThis is a low budget slasher movie that is light on slashing and blood.�J�h
 �fA dangerous man escapes from a mental institution with about as much security as an elementary school.�J�h
 �We don't see his face.�J�h
 �SThe security man at the institute nicknamed "ski mask" goes out to find the psycho.�J�h
 �(We don't know what he looks like either.�J�h
 �>Meanwhile there is a subplot of a woman going on a blind date.�J�h
 �ENote the diploma on the doctor's wall: Northwestern Bluff University.�J�h
 �[Just when you think you have the movie pegged, there is a flashback which gives it a twist.�J�h
 ��There are numerous sudden flashbacks, but they are not too hard to figure out because it generally contains a person who is dead.�J�h
 �BKudos to Steve Hudgins who starred, directed and wrote this movie.�J�h
 �'Normally that is a recipe for disaster.�J�h
 �,Some good dialouge, with many quirky twists.�J�h
 �>I enjoyed the film more as a quirky indie than a slasher film.�J�h
 �Adult dialouge and groping.�J�h
 �No sex, no nudity.�J�h
 X-  A piece of the casting was broken off when the starter arrived, but it did not affect the mounting or performance, but upon using the starter for the first time, the "C" clip that holds the gear in place came off, causing the gear, spring, spring washer and retainer cap to fly off, damaging the gear.�J�h
 �+Two issues with a new part is unacceptable.�J�h
 �rThe motis operendi of Rotery Connection is to take popular songs of their era, the late 1960s, and transform them.�J�h
 �POn this twofer,they do so with material from Aretha Franklin, Cream and Hendrix.�J�h
 �5This music is altered so much as to be unrecogniable.�J�h
 �MOn "Respect," the band uses sensual strings and plays the song at half tempo.�J�h
 �s"Sunshine of Your Love" has the famous bass/guitar line removed and is also turned into a erotic, orchestral dirge.�u(J�h
 �AAnd orchestration is the name of the game with Rotery Connection.�J�h
 �3But this is not rock or R&B; turned easy listening.�J�h
 �jThe arrangements feature silky, ensamble vocals, and the string charts take on a dark, psychadelic flavor.�J�h
 �3This sounds more like David Axlerod than Montavoni.�J�h
 �AConceptually and in practice, this is gripping psychadelic music.�J�h
 ��Take an utterly beautiful, soulful band like the Fifth Dimension and add a heavy, professionally-crafted theatrical sheen, and you've got Rotary Connection.�J�h
 �xOne of my amazingly astute and lively college companions/girlfriends was heavily into theatre - and was wild about them.�J�h
 ��If you like listening to music that's reminiscent of 'Hair' and/or 'Jesus Christ Superstar', RC will be right up your alley - they're definitely a very artistic/dramatic/appealing element of the late 60's counter-cultural ethos.�J�h
 �vThis product was bought to transport to other country and I've not been able so far, because it's a flammable product.�J�h
 �=I can't return it to Amazon either because of the same cause.�J�h
 �So, I've not proven it yet.�J�h
 �=I thought this book started slow but then it got interesting.�J�h
 �QThe characters were first introduced in childhood and then met again years later.�J�h
 �MTwo of the characters became involved and their troubles related to the past.�J�h
 �4I was happy with the ending which was very climatic!�J�h
 �.This was a good book and I would recommend it.�J�h
 �{I did not notice any difference with or without this product, but perhaps it was not the right device for my noise problem.�J�h
 �8However, I highly recommend the PAC SNI1 Noise Isolator.�J�h
 �/It fixed my alternator noise problem on my amp!�J�h
 �A lot of nice and easy reggae.�J�h
 �PThe "Strictly The Best" volumes appear to do a great job in compiling good hits.�J�h
 �;Even though this is made out of plastic, it is very sturdy.�J�h
 �It attaches to the wall easily.�J�h
 �Very good product�J�h
 �	05-25-00:�J�h
 �4This is an amazing album, out of print for 30 years.�J�h
 �OIt represents the peak of Bola's playing of the Brazilian/American Jazz milieu.�J�h
 �It is a classic recording.�J�h
 �%/It even has Bola speaking on the CD!�J�h
 �Buy it while it's available.�J�h
 �<This book has lots of exercises, but there is no answer key.�J�h
 �rSo, unless you have a teacher's edition, or you are in a class with someone who does, it's got limited usefulness.�J�h
 �This book is the best!�J�h
 �>I had to buy it for school, but I would have bought it anyway.�J�h
 �It's the greatest.�J�h
 �DIt teaches you all about logic and arguments and other useful stuff.�J�h
 �4Some words I didn't understand; but I skipped those.�J�h
 �&Now, I can beat anyone in an argument.�J�h
 �Try me!�J�h
 �	Go ahead.�J�h
 �Buy this book!�J�h
 �8My favorite Pearl Buck book is of course The Good Earth.�J�h
 �This book is a close second.�J�h
 �2It was totally absorbing and seemed true to life..�J�h
 �`I could hardly put it down and highly recommend it to anybody who likes good historical fiction.�J�h
 ��If you plan to read this book and do not wish to have it spoiled for you, do not read the review by "Manuela Bonfanti from Geneva".�J�h
 �\Pavilion of Women is one of the novels that Ms. Buck wrote about the struggle of old vs new.�J�h
 ��Within the Wu household are bought concubines along with revolutionary wives who chose their husband and did not have their parents involved.�J�h
 �SMadame Wu is an interesting character who sparks a riveting tale with her decision.�J�h
 �iIt might not be for everyone, but whether or not you're a Pearl S. Buck fan, you should enjoy this story.�J�h
 �lOne of Fat Jon's earlier albums, still great for listening, if you are a fat jon fan i definitely recommend.�J�h
 �Nthere aren't too many prints of this cd unfortunately, good luck on acquiring.�J�h
 �bMusic of my mind is for me the first album of the new musical career of Stevie Wonder in the 70's.�J�h
 �2He played all instruments and produced this album.�J�h
 ��In my opinion this not a very good album, I suggest one that is beginning to collect Stevie Wonder to go to the next album which is "Talking Book", "Songs in the key of life", or "Innervisions".�J�h
 �These are masterpieces.�J�h
 �yIn music of my mind you can listen to some ideas, rythms and sounds that Stevie would develop in his next albums, though.�J�h
 �DThe song Superwoman(Where were you when I neeeded you) is very nice.�J�h
 �3 stars for this album.�J�h
 �@This album is supposed to be the first of Wonder's 70s classics.�J�h
 �YWith nary a melody or groove to sink your teeth into, it's the perfect cure for insomnia.�J�h
 �$Don't drink coffee when you play it.�J�h
 �9All that pent up caffeine energy will just infuriate you.�J�h
 �GFor a good pick-me-up, get "Rags to Rufus" by Rufus/Chaka Khan instead.�J�h
 �:I was weighing up whether to give this five stars or four.�J�h
 ��I had reviewed all of Stevie albums from the 70s (except Secret Life of Plants because I haven't heard it and Songs in the Key of Life because that review will take some time since that album is so great) and given them 5 stars each.�J�h
 �;Is Music of My Mind a contender of Stevie's greatest album.�J�h
 �	Possibly.�J�h
 �0But with the opposition, Music seems to be lost.�J�h
 �fTalking Book, Fulfilliness First Finale and Songs In The Key of Life are better than Music of My Mind.�J�h
 �HHowever raw Music of My Mind is, I felt it was better than Innervisions.�J�h
 �ATo be fair to Innervisions, it is a classic so I gave it 5 stars.�J�h
 �9To be fair to his other albums, I had to give it 4 stars.�J�h
 �&Favourite cuts would have to be I Love�J�h
 �*Every Little Thing You Do, Love Having You�J�h
 �Around and Evil�J�h
 �bOkay, you sorta root for the guy, and defer judgement on his corruption, but he's a Bad Detective.�J�h
 �$Not just neglecting his job, either.�J�h
 �`When he applies himself to solving a problem, he NEVER has a plan, options, or an exit strategy.�J�h
 �(Sound familiar?)�J�h
 �This continually suprises him.�J�h
 �=He's always a hair-trigger away from violence - will he blow?�J�h
 �-�J�h
 �.no, the only violence he commits is off-stage.�J�h
 �7Nor does he ever learn from his mistakes and improve. "�J�h
 �RVictories" gained by luck are claimed; inevitable "defeats" are rationalized away.�J�h
 �xThe guy's a loser, and what little care he has for his fellow man always loses out to his own short-sighted selfishness.�J�h
 �4The writing, on the other hand, is often delightful.�J�h
 �Clipped phrasing.�J�h
 �Idiom-tastic.�J�h
 �Veddy, veddy British.�J�h
 �}I have not read this author before and won't again... unless his loyal fans can convince me of a different title I might try.�J�h
 �The earrings are very pretty.�J�h
 �_My only negative comment is that the backings that hold on the earrings are entirely too small.�J�h
 �2We will have to replace them with larger backings.�J�h
 �pIt is ashame to buy a product; then to have to go back to a jewlery store to to replace parts that are inferior.�J�h
 �7Picked this book up in a bargain bin several years ago.�J�h
 �\Hadn't heard of the author, in fact haven't heard much since, but thought I'd give it a try.�J i
 �"Unfortunately, I was disappointed.�Ji
 ��While I did dedicate the time to finish the book, mainly because I really wanted to know how it would all end up; I just wasn't that impressed with the overall story.�Ji
 �wJudging by the description on the back I thought it would be much more exciting and worthwhile than I felt it ended up.�Ji
 �{I certainly can appreciate the point the story was trying to get across, about how life used to be in the south for blacks.�Ji
 ��And while I did find the two main characters pretty entertaining at times- I just felt the entire story and cast fell short for me.�Ji
 �LIt was not very suspenseful or exciting or even what I felt "all that deep".�Ji
 �.I would not recommend this book to a friend...�Ji
 �RTerry Kay has had a successful career as a purveyor of popular "Southern" fiction.�Ji
 �QBut those of you who love his novels, please, there are better writers out there.�J	i
 �Try some of them.�J
i
 �DLee Smith, Fred Chappell, Charles Frazier, Doris Betts....and so on.�Ji
 �YThe author of this book uses his self-described "bag of tricks" to manipulate the reader.�Ji
 �'Go elsewhere for your reading pleasure.�Ji
 �vI first heard laurie Berkner on a today show childrens concert and could not get her music out of my 46 year old head.�Ji
 �_I searched local stores, could not find it, and was thrilled to order all of her cds on Amazon.�Ji
 �|We play the songs constantly in the car on short and long car trips, and they are both my 3 year old son's and my favorites.�Ji
 �iThe lyrics are wonderful, melodies great and songs are just the right length, with a variety of subjects.�Ji
 �`Laurie's music raises the bar for all children's music in creativity, originality, and pure fun.�Ji
 � I hope she keeps producing more.�Ji
 �cSting and Madonna certainly were not wrong in finding the best new talent in childrens music today.�Ji
 ��It is great to have someone else with the energy of Jessica Harper's Music and even more variety and originality in song content.�Ji
 �9My children love this cd and I love listening to it also.�Ji
 �QIt's refreshing to hear some different songs other than the regular kiddie songs.�Ji
 �aIt took a few playings, but now both of my kids (nearly 4 and nearly 2) actually request this CD.�Ji
 ��The songs are catchy, easy to sing, and will run through your head 1000s of times each day (one drawback, if you can call it that!).�Ji
 ��Laurie and her compatriots have very easy-to-listen-to voices and the variety of styles of songs satisfies everyone in the group.�Ji
 �It's great in the car too!�Ji
 �One great song after the other.�Ji
 �YMusic that the kids adore and you find yourself listening to even when the kids are gone.�Ji
 �Buy 2 and keep one in the car.�Ji
 �YI've just recently discovered Laurie, but I'm loving her music as much as my little kids.�Ji
 �,They are kids songs that are adult friendly.�J i
 �BWe just love dancing and singing along to this music will cooking.�J!i
 �3She asks to it all the time in the car and at home.�J"i
 �AShe especially likes "Victor Vito", "Oleanna" , and "Oh Susannah.�J#i
 �Y" I ordered another Laurie Berkner CD because I was getting to tired of hearing this one!�J$i
 �dMost songs are bearable to adults, a couple are annoying but on the whole this was a great purchase.�J%i
 �This is a wonderful CD.�J&i
 �5Both my children dance and sing from start to finish.�J'i
 �;We are looking forward to seeing this band live in concert.�J(i
 �LThis is classic music that children will enjoy for many, many years to come.�J)i
 �LWe still listen to this all of the time, and we've had it for quite a while!�J*i
 �9The songs are great and encourage listener participation.�J+i
 �=I don't understand why this CD is getting such great reviews.�J,i
 �rI listened to the samples on Amazon and liked the first song, Victor Vito, but didn't like any of the other songs.�J-i
 �I decided to give the CD a try.�J.i
 �1The first song is the only good one on the album.�J/i
 �-Some of the other songs are mediocre at best.�J0i
 ��If I have to hear "the cow jumped over the mooooooooooooooooooooooon" or the toy museum song one more time, I may just have to get rid of the CD.�J1i
 �QThere are other annoying pieces and bad singing on the CD and the songs are dumb.�J2i
 �ZAlso, the recording is not good and the words are hard to understand in some of the songs.�J3i
 �xIf you listened to the samples and you're not sure if you should purchase this CD, I'd recommend that you do not buy it.�J4i
 �9Take a look at my other reviews for some recommendations.�J5i
 �bWe got this CD after seeing Lori in "video's" between cartoon's on TV really expecting to love it.�J6i
 ��We had loved the three or four songs we saw on TV and thought that, even though we didn't know any of the songs on the CD except Victor Vito, we would give it a try.�J7i
 �$I'm really pretty sorry that we did.�J8i
 �The songs are very random.�J9i
 �YIf you like that kind of thing, sort of a kid's version of jazz, then this CD is for you.�J:i
 �VSome of the lyrics are cute but the music is annoyingly random and the singing is too.�J;i
 �And it's not danceable.�J<i
 ��My kids danced to Victor Vito (though it too was a different version that the video on TV) but very few of the other songs had any sort of a beat to allow for dancing.�J=i
 �NAll in all, if you like jazz and think your kids will too, this CD is perfect.�J>i
 ��If you are looking for upbeat songs that the kids will like and that you can learn to see along with, take my advice and buy something else.�J?i
 �!This one was quite disappointing.�J@i
 �Okay, okay.�JAi
 �rI admit that I get these songs stuck in my head from time to time, but that doesn't mean they're of great quality.�JBi
 �^My five-year-old loves a few of the selections on this album and absolutely despises the rest.�JCi
 �\My biggest complaints with this, and Berkner's other recordings, are quality and intonation.�JDi
 �nDespite all the hype, Laurie Berkner and her backup troupe have a bit of trouble keeping their vocals in tune.�JEi
 ��If you want your child to appreciate good music, play selections that put an emphasis on compelling and full arrangements, fun lyrics and great singing.�JFi
 ��While Laurie Berkner may have some catchy riffs and clever lyrics, her voice is mediocre at best...and sometimes, downright amateur.�JGi
 �$Choose They Might Be Giants instead!�JHi
 �oI bought this CD because we had bought another of her CDs, "What Do You Think of That," and absolutely love it.�JIi
 �*But this one is a bit of a disappointment.�JJi
 �8The songs just aren't as fun and catchy as I would like.�JKi
 �BWe like the first song, but after that, nothing really stands out.�JLi
 �NThese tracks are just slower and less creative that the ones on her other CDs.�JMi
 �:It's not a bad thing to listen to; my two year likes it OK�JNi
 ��and I can tolerate it, but it just doesn't bring about the same sort of excitement that he has when he listens to some of his other CDs.�JOi
 �Laurie Berkner is fantastic!�JPi
 �Victor Vito is a great CD!�JQi
 �[As parents, we're pretty selective about the music and tv our 2 year old son is exposed to.�JRi
 �/So,we were thrilled to discover Laurie Berkner.�JSi
 �AWe think music is very important in early childhood (and beyond).�JTi
 �*There are some good messages in the songs.�JUi
 �6The title track is what actually got me to buy the cd.�JVi
 �:The CD is fun and goofy and I (an adult) totally enjoy it!�JWi
 �Oh, and my son loves it, too!�JXi
 �:Some of his favorites are Moon, moon, moon and Good Night.�JYi
 �I highly recommend this CD!�JZi
 �eOur family started off with Laurie Berkner's album, "Under a Shady Tree" which is absolutely amazing.�J[i
 �KI can literally listen to that album all day long and never get sick of it.�J\i
 ��I was soooo excited to get "Victor Vito" after reading reviews, but once I popped it into the CD player in my car, it was "eh..." from the beginning.�J]i
 �SAfter having had it for about a month now, I have yet to listen to the whole album.�J^i
 ��It lacks the polished, finished, complex sound I've come to expect from the LBB, and while I realize that it was released before "Under a Shady Tree," there wasn't THAT much time between albums.�J_i
 �yI think Inside/Out showed people the real stories and views behind songs, that they're not just words that someone sings.�J`i
 �Vrelied on this map heavily while in Amsterdam and didn't have a single gripe about it.�Jai
 �UIt's compact and sturdy and fairly easy to pinpoint the area you are wanting to find.�Jbi
 �?Some of this kit is really nice and some of it is really cheap.�Jci
 �&I guess over all I would buy it again.�Jdi
 �I like the case a lot.�Jei
 �,Most of the tools work well for hobby stuff.�Jfi
 �OA few of them like the plastic clamps cutting knives and poking tool are cheap.�Jgi
 �0I have not used ever tool in the kit yet either.�Jhi
 �A disappointment.�Jii
 ��Although the author deservedly credits Dorothy Lamour as an important part of the star combo for this series, he seems to have very little knowledge about her own illustrious non-Road career.�Jji
 ��Indeed, his bio chapter on her seems little more than paraphrasing James Robert Parish's chapter on her in the old 70s book The Paramount Pretties.�Jki
 �vAnd there is regrettably little fresh information for fans of the movies, who no doubt know everything mentioned here.�Jli
 �DAt least he could have included some rare reviews to these pictures.�Jmi
 ��I did enjoy this book, I must admit, yet the 47.99 price tag seems a bit hefty for a book that is a pretty superficial overview of the Road Films.�Jni
 ��The fimographies at the end do not list the cameo and unbilled performances of the three stars in each others' films (which is easy to research).�Joi
 �^All in all, the result was disappointing for this truly sparkling era of Hollywood production.�Jpi
 �|A critical study of the great Hope-Crosby-Lamour Road Movies is long overdue - but this does not even begin to fit the bill.�Jqi
 �nLow on facts and thin on substance, the book seems more like a student's essay than a real study of the films.�Jri
 X  The author seems content that a quick overview of scenes from each film is enough to comprise an in-depth discussion, and constantly repeats himself, often putting the exact same paragraph within a couple of pages (Who was the proof reader for this book? ....)�Jsi
 �Y.When the book attempts to be critical (which it rarely does) it falls flat on it's face.�Jti
 ��The idea that The Road To Hong Kong is the least of the series purely due to it's Space Travel plot, - meaning it is less based in reality than the other films - is totally flawed.�Jui
 ��To sum up - a potentially great book is scuppered by repetitious prose, little new information or research and very weak arguments.�Jvi
 �....�Jwi
 �KOh well, nice try - hopefully someone will do the series justice one day...�Jxi
 �]I bought this CD because I fell in love with the Bumblebee Tuna Song after hearing it on NPR.�Jyi
 �NImagine my diappointment when the rest of the CD turned out to be pretty lame.�Jzi
 �^As far as the ska aspect goes, I've heard much better - and the lyrics can be downright scary.�J{i
 �JI'd recommend against it - get the Tuna song as an MP3 and screw the rest.�J|i
 ��After a number of years touring the indie club circuit and gaining a great following, Wildside landed a major label contract in 1991, and in 1992, released UNDER THE INFLUENCE.�J}i
 �UWe all know what happened to rock bands/albums that got released in that time period.�J~i
 �6Nevertheless, Wildside did rock, and they did it well.�Ji
 ��Singer Drew Hannah doesn't sound all that unique compared to other metal singers but he still has his own style and thats the main key, that and some great solos by a very talented guitarist.�J�i
 �-These guys have the talent no doubt about it.�J�i
 �Now let's focus on the songs.�J�i
 �9I have owned the vinyl, cassette, and now the CD of this.�J�i
 ��It has about 5 very good songs that have a taste of power groove along with a couple of ballads, which all songs have excellent vocals.�J�i
 XT  I have also purchased their self-titled CD that was released several years after this one, which was recorded with a different guitarist and is a totally different sound than "Under the Influence" and is almost impossible to find anywhere, which is also true about any info about the band, I have yet to find any whatsoever on the internet.�J�i
 �DI saw Wildside perform in the early 90's, and they had a great show.�J�i
 �iTheir music did, as other reviewers mentioned, come a little too late in the game for commercial success.�J�i
 �3If you still love 80's/90's metal, give them a try.�J�i
 �You won't be disappointed.�J�i
 �My son liked this toy.�J�i
 �SI'm not sure the price justifies its fun value, but he enjoys the motorized trains.�J�i
 �VThis particular one is cute, as it is so different than the rest of the Thomas trains.�J�i
 �I never recieved my my product.�J�i
 � I recieved 1 follow-up response.�J�i
 �)My account was billed and never refunded.�J�i
 �%I will not repeat business with them.�J�i
 �NI was advised to wait and see if the product turned up and nothing more since.�J�i
 �TI have both of Joyce's first two albums and found Third Wish to be a dissapointment.�J�i
 �There is no growth here at all.�J�i
 �=Her songs like South Of Market and Callie are rich and fresh.�J�i
 �%She needs to try something different.�J�i
 �<She might have been better served with her previous company.�J�i
 �AThese big companies seem to always get in the way of a new artist�J�i
 �CThis toaster oven seemed to work fine for the first 6 months or so.�J�i
 �`The toasting element is now really weak and it takes a LONG LONG time to toast a slice of bread.�J�i
 �_I can see from the window that the back element is redder than the front element on the bottom.�J�i
 ��I also have to flip the bread 1/2 way through if I want the bread evenly toasted because the top doesn't seem to be toasting at all.�J�i
 �)The "oven" component seems to work great.�J�i
 �@It makes wonderful nachos so I gave it two stars instead of one.�J�i
 �7I had a smaller B&D;, mounted under the kitchen cabnet.�J�i
 �It lasted 8 years.�J�i
 �&It cooked fast and was very dependable�J�i
 �5so I was happy to look for another B&D; toaster oven.�J�i
 �NI bought the TRO910W. It took a long time to toast bread: about eight minutes.�J�i
 �%Now for the bad news it lasted 8 mos.�J�i
 �#I could not recommend this product.�J�i
 �HI like the printer, it would be perfect if it wasn't for the paper jams.�J�i
 �PIt's Complicated is my favorite song on the entire album which came out in 2000.�J�i
 �sIt sounds like they lost some focus on this album and it is not as fun as their earlier recordings back in the 80s.�J�i
 �{They are changing with the times and that is undrstood, but still, their sound was so unique and pop-rock alternative cool.�J�i
 X2  If XTC hadn't gone on to produce some of the best music of the latter part of this century, I think we'd all be pulling this one out of our LP collections, dusting it off, and throwing 2 or 3 tunes on a comp. cassette of cool 80s stuff, along with bands like China Chrisis, UB 40, and Echo & the Bunneymen.��      J�i
 �XTC was better.�J�i
 ��This release had a few signposts of things to come: Life Begins at the Hop, Making Plans for Nigel, and (I'll have to disagree with the other reviewers)�J�i
 �Complicated Game.�J�i
 ��Complicated Game is one of their most powerful recordings ever, rivaled only by Mummer's "Deliver Us From The Elements" IMHO.I bought this after buying English Settlement.�J�i
 �'Naturally, I was slightly disappointed.�J�i
 �But�J�i
 �+when Mummer came out, I new they were gods.�J�i
 �PThe book is filled with informative scientific hypothesis' about the man eaters.�J�i
 ��I found it to be very good reading until the the chapter when the author started bashing the hunters he had quoted through out the book.�J�i
 �Throughout the world hunters are usually amoung the first to call for conservation of a species, not the enemy of conservation.�J�i
 �gOver all I would say the book is educational and worth reading just skip chapter 9 if you are a hunter.�J�i
 �JIf this book is the writing for a doctoral thesis it is a pretty good one.�J�i
 ��The book (aka thesis) is not the writing reminiscing of Earnest Hemmingway, Earnest K. Gann or Peter Hathaway Capstick and is in no way attempting to do so.�J�i
 �qWhat I have derived from reading this book (theses) is to what great extremes someone will go to proving a point.�J�i
 �eYes...it is true, Lions do eat people for varied and speculative reasons and lions are unpredictable.�J�i
 �Thanks.�J�i
 �I think I got it.�J�i
 �TWhile it is more like a text book, I found it was well written and very informative.�J�i
 �}I have read several books by Jim Corbett and Ken Anderson and find that Patterson goes into greater depth of man-eating cats.�J�i
 �If you enjoy Corbett, Anderson, and other man-eater stories this book is a must for you as it answers a lot of "why" questions!�J�i
 �VThe author does an excellent job of making the subject matter readable for the layman.�J�i
 ��This is based on a series of scientific studies which are often laborius reading for most but it is presented in an easily understood form.�J�i
 �~No definite conclusions are drawn but anyone with an interest in the big cats will find this a valuable source of information.�J�i
 �dI have to agree with the other reviewers that the sound quality is abhorrent, but Judy is stellar!!!�J�i
 �wThis is great to compare against the Carnegie concert (for vocal styling, not sound - Carnegie is far better on sound).�J�i
 �5Some of her interpretations are better, some are not.�J�i
 �DA true Judy fan has to get this!Buy this if you are a true Judy fan.�J�i
 �uIf you are looking for the ultimate Judy for your collection get the 2001 release of "Judy Garland at Carnegie Hall".�J�i
 �PI won't rehash too many things already written, but I will say I enjoyed the cd.�J�i
 �KI had already owned the Carnegie Hall concert and the Paris concert cd too.�J�i
 �MI had this concert on record album too, but I wanted to update it in cd form.�J�i
 �dIt is close to those concerts, but if you are a real Garland fan, you want each different recording.�J�i
 �yOne thing I liked about this recording is Judy talking to the band and to the audience in her own self deprecating style.�J�i
 �.She is warm and funny and just plain adorable.�J�i
 �\Some of the stories (Mrs. Neanderthal) were told here, but some new ones were added as well.�J�i
 �#Judy was in good voice at the time.�J�i
 �AAnd as for the sound of the cd itself, I didn't think it was bad.�J�i
 �hIt's not as crisp and clean as some remastered ones, but I didn't have a problem with the sound quality.�J�i
 �1This disc falls in the "what a tragedy" category.�J�i
 �mJudy comes across in excellent voice --- alive, fresh, full of engergy, and in complete control of her voice.�J�i
 �*But the quality of the sound is atrocious.�J�i
 �+I cannot recommend a purchase of this disc.�J�i
 �%I'm afraid this book disappointed me.�J�i
 �cFar from being a comprehensive work on Japanese weapons, the weapons covered are extremely limited.�J�i
 �^Cunningham seems to have only covered weapons contained in 'the author's personal collection'.�J�i
 �MThere's a few lines about this and that, but no detail at all about anything.�J�i
 �~I found this book very much an 'outsider's' look at Japan, giving small pieces of information on Japanese history and culture.�J�i
 ��As a long-time resident of Japan and martial arts practitioner, however, I found the book rather shallow and lacking insider's insight.�J�i
 ��I had trouble understanding why some of the sections, such as the recounting of the well-known story of the 47 Ronin had anything to do with 'secret weapons of jujutsu'.�J�i
 �LSome of the illustrations also puzzled me, not seeming relevant to the text.�J�i
 �gMy conclusion; If you know nothing about Japan and its martial history, maybe it's an interesting read.�J�i
 �7If you know a little already, there's nothing new here.�J�i
 �BExpected something special and ended up with a picture collection.�J�i
 �Mostly Jutte.�J�i
 �?Nothing on any of the other myriad of secret weapons out there.�J�i
 �[...]�J�i
 �)If you like Tony Hawk games get this one.�J�i
 ��If you haven't played a game like this before, it's fairly simple and easy, but it can be hard to perform long combos, even those long combos aren't necessary to advance throughout the game.�J�i
 X	  I cant be to trusted, because i just got this today and have never previously played a tony hawk game, but from what i have [layed today i can say this much, the gameplay is absolutely wonderful, the graphics are AMAZING for a DS game (the screenshots DONT do it justice, you have to see it in motion.)the soundtrack isnt just blips and bloops but real songs by real bands..wow..its an amazing peice of technologie with huge areas to scate through..i would buy this is you have ot havent played the other tony hawk games.�J�i
 �iI called a bunch of times they said it comes out nov15 but still nothi anyway this game looks really cool�J�i
 �`and im gettin it u should to because u can play with anyone in the world using the wifi feature.�J�i
 ��i dont know if the game is good or not my little brother lost the game he was playing it in the front yare by the sewer and when he came back in it was gone.�J�i
 �@Talking Book definitely will have you hooked in as a Stevie fan.�J�i
 �IIt has a rock/R&B type sound that will just having you yearning for more.�J�i
 �iThis album contains the smash hits Superstition and You Are The Sunshine of My Life as well as I Believe,�J�i
 �9You And I, Tuesday Heartbreak and You've Got It Bad Girl.�J�i
 �QIf you want some of that 70's type rock then Maybe Your Baby is the song for you.�J�i
 �9Any hardcore Stevie fan should not be without this album.�J�i
 �this isn't really a review.�J�i
 ��i just wanted to point out that in Amazon's review Steve Knopper says that 'superstition' features funky wah-pedal guitar or something like that.�J�i
 �but it doesn't.�J�i
 �BStevie Wonder's playing the clavinet in that song...check ya facts�J�i
 ��When you think about the fact that Stevie was so young when he wrote this music, that he was taking huge artistic risks, that he was talking about serious things in serious ways while fluff abounded around him makes this album that much more significant.�J�i
 �SHe didn't just write love songs; he infused deep, soul-wrenching emotion into them.�J�i
 �lI dare you to say you don't get goose pimples listening to "You and I" or "You are the sunshine of my life."�J�i
 �aOn the flip side, songs like "Superstition" and "Maybe Your Baby" shows how he can jam hard-core.�J�i
 �The brother is bad!�J�i
 ��And let's not forget the fabulous players on this record - Ray Parker, Jr. on "Maybe Your Baby" or Jeff Beck on "Looking for another Love," to name a couple.�J�i
 �@Stevie's music is incomporable, and he is a rare musical genius.�J�i
 �MThis CD is a fabulous representation of what a master songwriter can produce.�J�i
 �You should check it out.�J�i
 �JAfter listening to this CD, I now appreciate Stevie Wonder more than ever.�J�i
 �"This is a funky, beautiful record.�J�i
 �MThere are classics here -"Superstition" and "You are the Sunshine of My Life"�J�i
 �?but I think the best are "I Believe" and "Blame it on the Sun".�J�i
 �%I could listen to them over and over.�J�i
 �I recommend this record!�J j
 ��I think it's a big mistake for Audio Fidelity to no longer indicate on the cover that the original master tape was used to produce the product.�Jj
 ��People may find it harder to justify the extra cost if they don't see that the CD was taken from the Master Tape and may pass it up.�Jj
 �2I hope Mobil Fidelity doesn't start this practice.�Jj
 �OI bought the Stevie Wonder CD and it is an improvement over the standard issue.�Jj
 �MI had hoped for more but this book, in my opinion, is strictly for the young.�Jj
 ��If you like "Harry Potter" then you may like this book, but I would not reccomend it for the mature reader looking for something to sink their teeth in.�Jj
 �$A good book of stories for children.�Jj
 �KThey all had happy endings for the principle characters and a moral ending.�Jj
 �PI just started to download some free ebooks on my tablet and this caught my eye.�J	j
 �-Everything seems to work fine and reads fine.�J
j
 �2Have not read much to comment on the actual story.�Jj
 �VThis was an excellent book that I would encourage preteens, teens, and adults to read.�Jj
 �$I also liked the layout of the book.�Jj
 �=i bought this book because i love dragonns,and this was free.�Jj
 �3WARNING this book is not ilustrtated as advertised.�Jj
 �=This would bore evn the most ecstatic of children.do not buy.�Jj
 �CNeeded to find more stories of dragons for some of my art projects.�Jj
 �WNot all books on dragons have good discriptions that you can use to form art work from.�Jj
 �!This book has helped some not bad�Jj
 �!and it's a gives a strting point.�Jj
 �pIt helps when you read this book to have the skill to turn word to images in your head makes the reading better.�Jj
 �\I have been working on a teaching unit involving dragons and looking for background reading.�Jj
 �'The Years of the Dragon is not far off.�Jj
 ��It's a fact: every fantasy geek loves dragons, and even though E. Nesbit's time was prior to the modern fantasy lore era, these stories still pack an awesome punch of good dragon stories.�Jj
 ��Through her nostalgic style of writing, she gives us a variety of dragons, good and evil, a variety of wondrous settings, and all types of personalities of the fabled beasts.�Jj
 �5This is a fairy-tale necessity for all dragon lovers.�Jj
 �yThe standard edition of this book appears to have illustrations, but the economy publication dispenses with that expense.�Jj
 �1(Maybe I missed that fact in the description....)�Jj
 ��In a book that's designed for children, seems like it was a poor choice to 1) publish it in the economy version and 2) buy this version.�Jj
 �=So, buyer beware: there are no illustrations in this version.�Jj
 �,My son and I enjoyed this book tremendously.�Jj
 �*He really enjoys all of E. Nesbit's books.�J j
 �This is a great read aloud.�J!j
 �My son is 10.Enjoy!�J"j
 ��'The book of dragons' is a collection of fairy tales for young readers, spanning many different time periods (even modern) but with dragons as the essential theme.�J#j
 ��I figured that, even though these are stories for kids, I would still enjoy them, just like I enjoy many fairy tales and folklore books.�J$j
 �iI was wrong however, because after the first two stories, which were decent, the book went downhill fast.�J%j
 �/The stories were bland, quite honestly, boring.�J&j
 ��I don't see how these stories could be enjoyable for any age, they are too long and boring for young kids to read, they are too kiddish for young adults, and they have no flavor for adults at all.�J'j
 �<I know many people like this book, but I am not one of them.�J(j
 �Sorry...�J)j
 �2 stars�J*j
 �HTo whoever looks, they will notice that there is nothing about the book.�J+j
 �8That made me mad because I want to know what I'm buying.�J,j
 �
Don't you?�J-j
 �This book is a sleep feast.�J.j
 �gThe storys are cute but will never waste my time ever again if this is how the writer chooses to write.�J/j
 �8Some storys are just total nonsense and done make sense.�J0j
 �'I was very disappointed with this book.�J1j
 �+I bought it to read to my 5 year old twins.�J2j
 �JIt is not a picture book which disappointed them but I knew that going in.�J3j
 �fFor me, it was the awful old style wording that was so far off anything a 5 year old would appreciate.�J4j
 �They lost interest very fast.�J5j
 �^I had to keep reading ahead and change the wording to try and make it understandable for them.�J6j
 �TI wish I hadn't wasted my money on this book and certainly would never recommend it.�J7j
 �I PURCHASED THIS ITEM�J8j
 �ONLINE WAS WORTH THE PRICE.�J9j
 �*AT PETCO THE SAME ITEM WAS 1O DOLLARS MORE�J:j
 �ALSO IT CAME VERY FAST .....�J;j
 ��Not sure what lovely choral singing is loaded for playback in the preview, but it is most definitely not Ms. Parker's Melodious Accord.�J<j
 �fThis is a decent album, but in my opinion no where near as good as the album done with Johnny Hartman.�J=j
 �eI know a lot of people who really love the warmth and beauty of this cd, but to me I never felt that.�J>j
 �RColtrane and the Quarter play very well, but they always seem a little restricted.�J?j
 �$Not as steamy as I'd like 'em to be.�J@j
 �&But it is good, and I do recommend it.�JAj
 �DI think though, that Lush Life is actually better for straight mood.�JBj
 �LIt's a little known album by Coltrane recorded in 1957, but it is beautiful.�JCj
 �DPianoless trio on the first three numbers... very dark, very bluesy.�JDj
 �xThen a 13 minute version of Lush Life that is so slow and melanchololy that it sounds right out of some back alley cafe.�JEj
 �
Very good.�JFj
 �'Check that one out if you like this one�JGj
 �Jof all the John Coltrane cd's that i own, i seem to play Ballads the most.�JHj
 �why?�JIj
 �>because it is pure beauty stated simply with purpose and ease.�JJj
 �lJohn was a true master of music and he seemed to have an exceptionally special sensitive touch with ballads.�JKj
 �?a signature Coltrane album and a landmark recording. essential.�JLj
 �FI'm new to jazz but have grown to almost exclusively listen to it now.�JMj
 �eI'm fond of the old jazz masters, but Coltrane was hard for me including his best-selling Blue Train.�JNj
 ��Then I heard Duke Ellington with John Coltrane and the sounds he wrung out of his sax on that one were so beautiful, I was tempted to try this album out and was I rewarded.�JOj
 �[Coltrane plays beautifully throughout this album and with a gentleness that feels poignant.�JPj
 �*He is ably assisted by his rhythm section.�JQj
 �vLike one of the other reviewers said, I listen to the entire album usually, but the first cut is the catchiest for me.�JRj
 ��If you liked this one, My Favorite Things is another very accessible Coltrane that has much longer tracks, but still very melodious.�JSj
 ��For the truly hard-core John Coltrane fans who are also hard-core audiophiles, this gold CD is going to be an automatic purchase.�JTj
 ��Recorded by Rudy Van Gelder in 1962, Ballads presents the softer side of John Coltrane, which for most audiophiles is much more interesting than the softer side of Sears.�JUj
 ��What is interesting to me is the way that Coltrane could play lyrically without ever sounding sentimental--the man seems to have been simply incapable of insincerity in his playing.�JVj
 X(  Pianist McCoy Tyner also demonstrates a nicely lyrical touch, weaving little countermelodies around Coltrane's tenor lines, while Jimmy Garrison provides fluid accompaniment on bass (with one cut featuring Reggie Workman instead) and Elvin Jones is his usual busy but effective presence on drums.�JWj
 ��Those not willing to invest in gold can be well-served by the nice SBM-remastered version of this album on Impulse, but for the ultimate listening experience, the MFSL version is the way to go.�JXj
 �KThis is something Coltrane had a great gift for - bluesy, romantic ballads.�JYj
 �V30+ minutes of him and his classic quartet sticking close to the tune is nice to hear.�JZj
 �3Fits in nicely with some of Trane's earlier output.�J[j
 ��Lacking the classic lyricism and stylistic improvisation of Kenny G, one can only wonder what was running through the mind of this evidently derailed Trane when he recorded this middlin' effort.�J\j
 �Does the emperor wear clothes?�J]j
 �3Me thinks not, but instead lots of sheets of music.�J^j
 �_the only reason I gave this a low mark, was because the album was just too short in my opinion.�J_j
 �gI was wanting to add some older jazz selections to my collection and thought this might be a nice pick.�J`j
 �Well, I really picked a winner!�Jaj
 �2I could sit and listen to this CD for a LONG time.�Jbj
 �Very nice, a super selection!�Jcj
 �The Playing On Here is Good but�Jdj
 �9Compared TO Lush Life or at the Vanguard it's Not on Par.�Jej
 �Still The Tones Here are Good.�Jfj
 �_The Disc Keeps a Equal Pace.it's Worth Hearing.it's very Straight-Foreward all the way through.�Jgj
 �Good Solid Tones.�Jhj
 �*This album was not Coltrane and cos. idea.�Jij
 �dImpulse wanted a hit and asked them to do it even though it was in no way where their heads were at.�Jjj
 �The results show it.�Jkj
 �<The band plays without lustre and Coltrane is on auto-pilot.�Jlj
 ��On an absolute standard, it is still better than 90% of the stuff that routinely gets dumped out there, so I give it 3 stars anyway.�Jmj
 ��But compared to the best of Coltrane's classic quartet recordings (Love Supreme, First Meditations, Crescent), this one is definitely an also-ran.�Jnj
 �[By the way, the earlier reviewer's comment about Kenny G was clearly meant to be satirical.�Joj
 �)It guess some people just don't get that.�Jpj
 �PRecently, I am playing this CD almost everyday as background music at the house.�Jqj
 �.Two discs and lots of electronic compositions.�Jrj
 ��I have not grown to recognize distinctions between the various tracks; I imagine that some would find the variety of melodies limited and no lyrics.�Jsj
 ��I will continue to play though, as the music is intellectually interesting while not distracting for conversational-type get togethers.�Jtj
 �ei'm not a big PVD fan, i only have Reflections and this new mix plus some loose songs here and there.�Juj
 ��but knowing he is a highly ranked superstar dj i thought lets give this mix a try (though i only liked about 6 songs from Refrlections- the 2 dics edition)POD cd1 i just cant understand it.�Jvj
 �Nthe track selection is bad and then the good tracks are mixed in wierd places.�Jwj
 �)it really doesnt make me want to dance...�Jxj
 X�  POD cd2 the track seclection is better here and so is the track "placement" but still it feels like thats not the order to arrange the songs.also on both cds (though a lot of these songs i didnt know) the mixing seemed very apparent that is to say it was everything but seamless.over all there are good songs here that i'm glad i have, now i can hear then just in whatever order i want cause it is for sure that the order they come in is not for me.�Jyj
 �1This CD ladies & gents is a masterpiece of crap..�Jzj
 �)Paul Van Dyk manages to pull of a junk CD�J{j
 �
YET AGAIN.�J|j
 Xt  full of bad remixes and horrible songs, the few highlights of this ablum include Purple Haze - Adrenaline, Calmec - Tangerine, Kyau vs. Albert - Falling Anywhere.. that is all, it also includes a crappy remix of Marco v's 'more than a life away', Perasma's 'swing to harmony' and Paul van Dyk even managed to rape my favorite song of all time by spinning it way too fast..�J}j
 �@this track is Holden & Thompson - Nothing ('93 Returning Mix)...�J~j
 �yAll i all, this album is a waste of money, which could be used to buy one of the following "infinitly better" albums : 1.�Jj
 �MAbove & Beyond Present Anjunabeats Vol.1, 2 or 3 all are really great albums.�J�j
 �2.�J�j
 �4Ferry Corsten - Passport Kingdom of the Netherlands.�J�j
 �3.�J�j
 �Garmin van Buuren - A State of Trance 2005 or A State of Trance Yearmix.�J�j
 �4.�J�j
 �Vadim Zhukov - Evolution 5.�J�j
 �"Solarstone - Destinations vol.1 6.�J�j
 �Deep Dish - George is On 7.�J�j
 �Blank & Jones -�J�j
 �
In the mix�J�j
 �3�J�j
 �2Van Dyk has really picked up his beat in this one.�J�j
 �NGiving you two straight discs on pumping dance beats with his flawless mixing.�J�j
 ��The track selection across the two discs is flawless and lets you know that in no way, shape or form did Paul draw this effort out.�J�j
 �<He just put the necessities, ranging from 02 to the present.�J�j
 �pHe really put alot of energy into this album, and I would consider this the best dance album of the year so far.�J�j
 �DIf Luke Fair would of made a double disc he would be a close second.�J�j
 �bBut many fans doubt that djs can put together double disc sets without taking away from the theme.�J�j
 �	Not true.�J�j
 �The good ones can.�J�j
 �lThis album definately beats out some of the greats like Tiesto's In Search of Sunrise, and Ferry's Passport.�J�j
 �#Great comeback cd Paul, This rocks.�J�j
 �kTHIS CD IS NOT TRANCE OR EVEN WHAT I CONSIDER QUALITY MUSIC TO BE, I'M TRYING TO BE NICE, BUT LET ME SPEAK-�J�j
 �sTHIS CD IS AWFUL UNLESS YOU LIKE SLOW REPETITIOUS MUSIC, PERSONALLY I WOULD RATHER LISTEN TO A MOUSE FART THAN HEAR�J�j
 �0THIS TRASH, I HAVE OVER FIFTEEN YEARS EXPERIENCE�J�j
 �/AND MY REVIEW IS THE ONLY ONE YOU NEED TO READ.�J�j
 �Oorder armin van burren state of trance 2006 if you want some spectacular music.�J�j
 �that cd is an instrant classic.�J�j
 �,The Politics of Dancing (1) was spectacular!�J�j
 �6But it, own it, love it, if you don't already have it.�J�j
 �DHowever, sadly, The Politics of Dancing 2 was a Great, Big Let-down.�J�j
 �cI can't believe Paul van Dyk put his name on it; of course, the Reflections Cd largely sucked, too.�J�j
 ��So, I guess that, and TPoD2, just proves that we're not going to love everything done by our most favourite dance-trance-DJ artists.�J�j
 �II just picked this bad boy up yesterday and let me say Im most impressed.�J�j
 �qThis 2 cd album is full of energy and motivation and will keep ya goin nonstop no matter what atmosphere your in.�J�j
 ��The first cd like vol.1's first cd is a little bit mellower while the second tends to pick up and provide more dance floor energy.�J�j
 ��P.O.D. was good enough but this a great follow up and even better than vol.1.This is a must have for any dance fan,and easily one of the best releases of 2005!�J�j
 �!Good music put at weird places...�J�j
 �oh well, i still liked it.�J�j
 �Btw.�J�j
 �gany trance DJ's interested in getting "out there" Email me:lunarshtaliar@yahoo.com(im looking for some)�J�j
 ��Paul Van Dyk has once again turned the trance genre around with and absolutely astonishing 2 CD set release known as The Politics Of Dancing Volume 2.�J�j
 �sNever before have I been able to listen to not one, but TWO CDs all the way through without skipping a single song!�J�j
 �OThis compilation is the only set that's in my 6 CD changer in my car right now.�J�j
 �OOut of a possible 6 CDs that I could put in there, only 2 stand out the most...�J�j
 �cPaul Van Dyk's 2 CD set, The Politics Of Dancing Volume 2.Wicked awesome, Paul... wicked awesome...�J�j
 �KI picked up Politics 2 this morning and have listened to it non-stop since.�J�j
 �NThere is not a single song on this album that doesn't completely blow me away.�J�j
 �nI was a huge fan of Armin Van Buuren's ASOT 2005 as well as Tiesto's ISOS 4, but this one certainly tops them.�J�j
 �8The mixing is phenomenal as well as the track selection.�J�j
 �rThere is everything you can want on this album, euphoric melodies, hard floor-pounding, and everything in between.�J�j
 �uDefinately would recommend this album for anyone who wants to get into the electronic scene or is a seasoned veteran.�J�j
 �HAn absolutely awesome mix from (in my opinion) the best DJ in the world.�J�j
 �(Paul van Dyk - The Politics of Dancing 2�J�j
 �NYou think that the passing of four years would have a world-class DJ evolving?�J�j
 �Even a little?�J�j
 �Well, think again.�J�j
 �The Politics of Dancing, Vol.�J�j
 �i1 was released in 2001, and this second volume demonstrates no clear progression in Paul Van Dyk as a DJ.�J�j
 ��He still choses to feature as many short anthem tracks as possible on each disc with the ever present "bubblegum" trance overtones.�J�j
 �ZConjures up images of a time when it was cool to wave glowsticks around at a club or rave.�J�j
 �oPaul Van Dyk is great spinning live, but the majority of his commercial compilations leave a lot to be desired.�J�j
 �Both discs get 2/5 stars.�J�j
 �$I recently purchased this 2-disc CD.�J�j
 �`I had enjoyed other work by this artist, so I thought I'd see what this latest CD would be like.�J�j
 �)Disc 1 was just too "calm" for my tastes.�J�j
 �MThere is a huge difference between "trance" music and "sleep-inducing" music.�J�j
 �zActually, I only cared for one mix on CD 1; thus, the lower rating for the entire CD.Disc 2, however, was an absolute joy!�J�j
 �lI liked every song on here (except one--I thought it broke the atmospheric tone the rest of the CD had set).�J�j
 �FAs stated in another review, this was truly the "Politics of Dancing".�J�j
 ��Each song (save the one mentioned above) flowed magnificently into the next and kept a fever pitch for shaking the old junk in the trunk!Again, I wish Amazon would review its ratings and assign half-stars.�J�j
 �7I would have given this CD 3-1/2 stars if I could have.�J�j
 �>Disc 1 was not my idea of trance; i.e. it was slow and boring.�J�j
 �BDisc 2 managed to save the day and garner a decent overall review.�J�j
 �;The 2-disc set is quite a great collection of trance music.�J�j
 �HHowever there are some fillers, meaning tracks which are decent quality.�J�j
 �>The mixing from one song to the other is awesome and flawless.�J�j
 ��In case you didn't know each cd is like a trance set (meaning non-stop music and you wont realize when track 13 becomes track 14).The music is a bit soft, not as hard and energetic as I expected it to be.�J�j
 ��Overall it is a good value, for $16-$18 you get over 20 trance songs.my favorite songs: Forbidden Love, Summerdream and The Unknown.�J�j
 �yOnly 1 of the songs is actually produced by Paul Van Dyk himself...the others are different artists with remixing by PVD.�J�j
 �BPaul van dyk shows in this album that he has no signs of weakness.�J�j
 ��This album is not only mind blowing but PVD definitely gave trance fans what they deserve after hearing a bunch of trance albums floating out there that are just not good enough.�J�j
 �DPaul van dyk shows the heights of how he can take his mixing skills.�J�j
 ��On most of the tracks you have to listen very very closely to tell if the song is changing he mixed them so good as you can tell he spend a lot of time working on this gem.�J�j
 �*This album gives off party vibes all over.�J�j
 ��There has to be something wrong if not even one track on this cd doesn't make you want to get up and dance as it has so much energy from start to finish.�J�j
 �QThis album is worth every penny and is by far one of the top trance cd's of 2005.�J�j
 �dIt's hard to get a compilation where you get 2 good cd's in one, this album is an exception to that.�J�j
 �Hands down to PVD!�J�j
 �..unless you order it here.�J�j
 �FSomeday Swiss pop will be big news, but it may take hundreds of years.�J�j
 �BUntil then you'll have to trust me that this stuff is just golden!�J�j
 �xIt's hard to know what to compare it to: I'd have to say this album sounds a lot like the bands Big Geraniums, or Oasis.�J�j
 �aExcellent singing, catchy songs and hooks, cool accordion playing (but don't let that scare you.)�J�j
 �YAlso, the Swiss language is just beautiful to listen to, even if you don't understand it.�J�j
 �1It's a little like German, but sings much better.�J�j
 �6I found this book well written and easy to understand.�J�j
 �4Unfortunately it is quite outdated in it's material.�J�j
 ��It has subjects as Magnetic Amplifiers, Rotary Exciters like G.E.'s Amplidyne, it refers to the SCR as a new device and ac motors becoming of age in ships.�J�j
 �oThis book was probably excellent material for the shipboard engineer back in the 1940's but ceretainly not now.�J�j
 �;I've been in the marine/shipboard business now for 50 years�J�j
 �Land I loved this book as I enjoy reading all technical material new and old.�J�j
 ��I wouldn't recommend this book though for those who want to advance their careers but for those who want to know what the old days were like.�J�j
 �3I love all her books and this one was no exception.�J�j
 �)Very well written with a great storyline.�J�j
 �A great read.�J�j
 ��CD #l was outstanding but in this second concert, one gets to hear Muggsy Spanier, Sidney Bechet (shades of their Big Four), George Brunies, and on some sides, Jack Teagarden together.�J�j
 ��Brunies basically achieves the award of the best ensemble tailgate trombone in the business, to which he jokingly alludes in Concert #1.�J�j
 ��Again, he tries to outshine Muggsy and Sidney; and probably has never sounded better (reminiscent of when I heard him in Chicago in the late l950's).�J�j
 �LThe Two Gospel Keys are a plus in their recreation of shouts and spirituals.�J�j
 �`This is the first time I've heard Cyrus St. Clair on tuba (string bass is his usual instrument).�J�j
 ��The selections are good, showcasing Brunies, and "Dippermouth Blues" as a finale with Muggsy and Johnny Windhurst on trumpets is a piece de resistance(sp?).�J�j
 �*Live concerts like these are a real treat!�J�j
 �This set is quite powerful.�J�j
 ��Sun Ship and Amen show the classic quartet in all their blazing power while Dearly Beloved and Attaining show just how beautiful creative jazz can be.�J�j
 �PI give this album a lower rating for Garrison's solo for a large part of Ascent.�J�j
 �GColtrane always gave Garrison space and should be admired for doing so.�J�j
 ��Unfortunately, Garrison would create slow building, very structured solos that tend to disrupt the flow of some of Trane's work.�J�j
 X
  I think its interesting that when Garrison's solos work, the album is a classic (see ALS and Meditations).This is the last album Coltrane intended to release by the classic quartet (First Meditation was recorded a few days later but not released until the late 70s).�J�j
 � All in all, a very valuable set.�J�j
 ��Long after the critical praise of "Wall Street" and "Platoon", and well before the rebirth of "Money Talks" and "Spin City", Charlie Sheen teamed up with Kirsty Swanson for what basically amounts to a mediocre road movie.�J�j
 �But it does have its moments.�J�j
 �vThere are a number of great cameos in the film, including Chili Peppers Anthony Kiedis and Flea, along with Cary Ewes.�J�j
 ��But the best thing about this movie are the two bonehead cops, played brilliantly by Josh Mostel and Henry Rollins, who's got the best lines in the filmAs a comedy its alternately funny and silly, but definitely worth a rental.�J�j
 �mAt just about 90 minutes in length, you won't be rubbing your eyes or your ribs, but you could do much worse.�J�j
 �PFaster than a speeding bullet..... that describes the service from East to West.�J�j
 �IAm not just saying that because they used to carry the same name as mine.�J�j
 �&DVD arrived quickly and well packaged.�J k
 �8Will use East to West again when I'm looking for a film.�Jk
 �Thanks.�Jk
 �<Every time I see this movie, it just gets better and better.�Jk
 �+Whenever it plays on television I watch it.�Jk
 �vWhenever I rent it, I watch it at least three times all the way through and I watch the Charlie : "What are you doing?�Jk
 �J" Kristy : "Falling in love with you." scene over and over and over again.�Jk
 ��I don't know what their off-screen chemistry is like but on screen I can't get enough of Kristy and Charlie together...even if it's the few words they exchange in Hot Shots.�Jk
 �XI'm trying to find information on a soundtrack for the movie...but not having much luck.�Jk
 �More like 3.5 stars.�J	k
 �JEven though this movie was more or less universally reviled, I enjoyed it.�J
k
 ��Lots to quibble about, but it's basically a satirical take on both the movies and especially the news media - lots of right-on jabs at news anchormen, etc.�Jk
 ��Also, there are plot points that are ludicrous/cheesy, but people who complain about them are basically missing the point: IT'S SATIRE!�Jk
 �$THEY'RE MAKING FUN OF MOVIE CLICHES!�Jk
 �5This movie started my love affair with Charlie Sheen.�Jk
 �I love this movie.�Jk
 �It's funny.�Jk
 �It's action-packed.�Jk
 �HI am always on the edge of my seat every single time I watch this movie.�Jk
 �
I love it.�Jk
 �I even own it on laserdisc.�Jk
 �Pick up this dvd.�Jk
 �You will not be sorry!�Jk
 �!Fave song: Breakdown by One Dove.�Jk
 �lNot the best, could be a little longer... kinda lacks in total action thrillers, but its good, worth buying.�Jk
 �!This is your regular action film.�Jk
 �Nothing special.�Jk
 �@A nice dvd for 10 dollars so if you enjoyed the film pick it up!�Jk
 �BThe Magellan Roadmate Universal floor mount was a snap to install.�Jk
 ��All I had to do was loosen one seat bolt at the front of the passengers seat rail, slide the bracket under the bolt, tighten it down again and done!�Jk
 ��The flexible gooseneck allows me to position the unit so I can easily access it while not interfering with the passenger nor obstructing my view as would be the case if mounted on the windshield.�Jk
 �This was a pretty good song.�Jk
 �3I don't think this was ever commercially available.�J k
 �^Apparently Mr. Tydlewave and Jerry from New York have odds with each other, as reviewed below.�J!k
 �Jerry?�J"k
 �Why are you so angry?�J#k
 �DYou gotta put down those Lynyrd Skynyrd albums and getting out more.�J$k
 ��But then you wouldn't know about writing and producing, would you?Maybe you were just looking for the Love Boat theme song and got confused.�J%k
 �This is a great textbook!�J&k
 ��This book is very easy to follow, as it has provided step-by-step instructions on how to do each of the chapters in this book and of Office XP.�J'k
 �9It also has a chapter review section with more exercises.�J(k
 ��I work in a world where critical thinking skills are a must, so for things like this you should have more exercises that are "at your own pace" and "figure it out yourself" type of deal.�J)k
 �vThis book is also horribly lacking the ability to check how close your Office document that YOU made to their example.�J*k
 �/It's no use if you don't know how well you did.�J+k
 �EThings can look similar, but there are probably errors here-or-there.�J,k
 ��It's fairly comprehensive, but like I said, since you don't really use your brain much, you don't really gain the ability to do these type of things on your own.�J-k
 �_I've had numerous experiences with William Stallings networking books, and all of them are bad.�J.k
 ��My personal opinion is that he is that though it seems like he knows a lot (though I doubt it), he's not able to get that knowledge across to his readers.�J/k
 �tThere are few examples and the post topic questions have little or no bearing on the subject matter of the chapters.�J0k
 �kEvery one I knew in my networking classes and computer science classes felt the same way about this author.�J1k
 �LI'm sure there are other better books out there - don't know why lousy prof.�J2k
 �seem torequire this for classes�J3k
 �.Very suitable for beginners and intermediates.�J4k
 �0Easy to understand the sentences and main points�J5k
 �nbut I think it would be much better if there is a solution guide for selected question at the end of the book.�J6k
 �IChapter one dives right into signal theory and does a terrible job of it.�J7k
 �[Waveforms and formula's abound with absolutly no description of what the formula's signify.�J8k
 ��The text references figure's of waveforms and formulas stating that at F3 blah blah blah and there is no F3 depicted on the diagram.�J9k
 ��I haven't read beyond that chapter because I'm only using the book for a class and need the end of chapter questions, which I then google for the information.�J:k
 �This book is total garbage.�J;k
 ��I do not like it for the following reasons:-It touches the subjects and does not go deep into it.-It does not have examples.-The very few examples in the chapters are not clear.�J<k
 �xYou have to decipher it.-Some of the end of the chapter problems talks about things not covered.-It is not an easy read.�J=k
 ��You have to read a paragraph several time to figure it out.-I do not recommend it.-On second thought, I should have given it 1 star�J>k
 �#but it seems I can not change that.�J?k
 �1I ordered two of these as a space saving measure.�J@k
 �6Each one holds 36 discs safely and keeps the dust out.�JAk
 �IThey're plastic and kinda cheap looking but functional for their purpose.�JBk
 �1Better than 72 jewel cases or an enormous binder!�JCk
 �CThis is a cute little wind up music box with a carousel and horses.�JDk
 �>For the price it will make a great up-coming stocking stuffer.�JEk
 �Rvery old fashioned looking and the matchbox design is reminiscent of the old days.�JFk
 �Great buy for what I got.�JGk
 �HLiving at the Summit is immediately engaging and compels you to read on.�JHk
 �gI was sure that the good guys would win and even then, I needed to follow the ominous twists and turns.�JIk
 �KThe Five Life Priorities are appropriately depicted by the main characters.�JJk
 �aThey gave me a new perspective of the changes that I am now able to seriously look at in my life.�JKk
 �bThanks for the insight on how to forge past our greatest challenges and live life to the fullest .�JLk
 �fI enjoyed this fast-paced novel that takes the reader on a trip around the world and through intrigue.�JMk
 ��The life lessons are easily palatable in this fictionalized account of people facing their own mortality and, when given another chance at life, decide for themselves what is actually important.�JNk
 �FTemp readings are pretty accurate when compared to other thermometers.�JOk
 �Not so much with the humidity.�JPk
 �%The best part is the wireless hookup.�JQk
 �&Very easy and reaches a good distance.�JRk
 �IWhile I was around during the "hippie" era I was not a part of the genre.�JSk
 �gI do remember the constant friction between the war protestors and the military or establishment folks.�JTk
 �KThe characters are very believable and generally react as you would expect.�JUk
 �^I guess the disturbing part of the story is how everyone expected the eventual trial decision.�JVk
 �YEven more how Sergeant Morales wanted the reward for the almost staged attack on himself.�JWk
 �4The story got me thinking a lot about the early 70s.�JXk
 �tI suppose being a young adult at the time gave me more appreciation for the twists and turns developed by Mr. Kazan.�JYk
 ��the product looks a bit different in person, but im satisfied.the only thing i really dnt like is that the fingers fit me too tight and igot an XL.�JZk
 �i wish they had a sizing chart.�J[k
 �6The reviewer below seems to me to be a hired marketer.�J\k
 �'I owned this drill for about 1.5 years.�J]k
 �vIn that time, I used it to drill somewhere less than 10 holes - all less than 1/2" in diameter into ordinary concrete.�J^k
 ��After the warranty expired (No Bosch standing behind it), the drill just quit going in forward - it will run all day in reverse, which isn't much use...�J_k
 ��Since the cost to repair it, while out of warranty, is darn close to the cost to buy a new one, I have decided not to repair it.�J`k
 ��Since I started having problems, I have found a number of reviews and web pages of people complaining about the electrical problems of this drill.�Jak
 �}I purchased this drill instead of a Milwaukee to save a few dollars and because I read that Bosch hammer drills are superior.�Jbk
 �]I drilled less than 50 holes ranging in size from 5/16" to 1/2" before it died abruptly......�Jck
 �)no smoke, no smell; it just quit running.�Jdk
 �'It acted like the switch was defective.�Jek
 �LBecause the drill was out of warranty, I decided to open the case and check.�Jfk
 �?The switch checked OK and the brushes and commutator were fine.�Jgk
 �0As other reviews have stated, the motor was bad.�Jhk
 �-Nearly every power tool I own is a Milwaukee.�Jik
 �-Milwaukees have proven to be reliable for me.�Jjk
 �5Trying to save a few dollars cost me in the long run.�Jkk
 �Lesson learned.�Jlk
 ��Looking for a replacement for my Bosch drill that just died, I ran across this 1199VSRK unit here and must note that the Bosch predecessor to this drill (the 1194VSR) just experienced the same "death by use" problem.�Jmk
 �qDrilled maybe 10 holes with it, when all of a sudden it lost variable speed control and would only run full tilt.�Jnk
 ��I changed to a smaller bit, as it was running too fast for the larger one and when I next pulled the trigger, the drill ran for about 0.2 seconds and is now dead.�Jok
 �lMaybe Bosch needs to go back to the electrical design lab and create a better variable speed control module!�Jpk
 �#i purchased this drill in december.�Jqk
 �Dafter using it for a week i noticed that the chuck wobbles slightly.�Jrk
 � i returned it for a replacement.�Jsk
 �it also wobbles.�Jtk
 �6if you use a long bit in it the wobble is significant.�Juk
 �gonce the bit drills into your workpiece, the wobble transfers from the bit to the drill and your hands.�Jvk
 �Si've e-mailed bosch twice about the problem without ever getting a competent reply.�Jwk
 �i'll get the milwakee.�Jxk
 �BNeeded to make 3, 3/4 inch holes 4 inches deep in a concrete slab.�Jyk
 �A 60 year old concrete slab.�Jzk
 �BI mention the age because as concrete ages it continues to harden.�J{k
 �=This drill worked fine using Bosch Blue Granite carbide bits.�J|k
 � I first drilled a 1/4 inch hole.�J}k
 �1That hole actually was the slowest hole to drill.�J~k
 �KThen drilled it out to 3/4 inch in steps using 1/8 inch larger drill sizes.�Jk
 �zFor the last 3/4 inch hole I used a bit made NOT by Bosch and the carbide tip chipped badly on the second and third holes.�J�k
 �2Caused by the hard aggregate in the slab concrete.�J�k
 �)The Bosch bits took it all in good shape.�J�k
 �IProbably took me 10 minutes each hole because of all of the bit swapping.�J�k
 �RSure a Hilti would have done a hole in 30 seconds and it should for what it costs.�J�k
 �GOther choice was to rent a Hilti for about half of what the Bosch cost.�J�k
 �@I own a lot of Bosch power tools but this one is a real clunker.�J�k
 �}I burned out two of them on one day while drilling about 12 lag bolt anchor holes in concerete with a 3/4" Bosch masonry bit.�J�k
 �6Total time on each drill was probably under two hours.�J�k
 ��After returning the first one and burning out the second, I disassembled it and the wire clip to the field coil was completely burned out.�J�k
 �pClearly this is a design flaw with much too small a connector to handle the current this thing draws under load.�J�k
 �APowerful, but useless for more than a few minutes work at a time.�J�k
 �iI was drilling 3/4" dia holes 2 inches deep in concrete to install a pool cover when the drill just died.�J�k
 �.First thought my extension cord was unplugged.�J�k
 �7I have 14 of the 22 hoes complete and had to shut down.�J�k
 �GI had drilled about ten 3/8" dia by 3" deep holes about two months ago.�J�k
 �,In addition, the chuck kept loosening on me.�J�k
 � My first Bosch tool and my last.�J�k
 �mUnderrideRockHorsepower KillsThe first track, Suffocate, on the album Horsepower Kills was a great selection.�J�k
 �*It's hard hitting, heavy and in your face.�J�k
 �1The guitar screams right along side the vocalist.�J�k
 �UThe CD revolves around that hard hitting sound that calls out any hard rock listener.�J�k
 �7The track entitled Motive further elaborates that idea.�J�k
 �BEnding the album with Big Easy couldn't have been a better option.�J�k
 �It finalizes the CD nicely.�J�k
 �This CD receives a rating of 4.�J�k
 �;One thing missing from the album was a mix of easier songs.�J�k
 �!They all some what sounded alike.�J�k
 ��If all you've read of Wells is The Invisible Man, The Time Machine, The War of the Worlds, and The Island of Dr. Moreau, you're not getting the full story.�J�k
 ��This novel--writen some twenty-five years after the aforementioned works--shows the author's more philosophical side; there's less of the straight SF romance aspect to it; it's also, in my estimation, a considerably more gripping read.�J�k
 ��In short, it's about a group of people who are inadvertantly drawn into an alternate dimension, which turns out to be a paradisiacal version of Earth thousands of years in ahead of contemporary society.�J�k
 ��There's some action involved, but it's more a book of moral philosophy than anything else, as it explores issues of what humanity should be and be striving for.�J�k
 �@I liked it a lot, and you should try to find a copy and read it.�J�k
 �)Awaiting my Toshiba SDP-1000 in the mail.�J�k
 �rI looked at and compared about 10 models in various stores and the Toshiba stood out--by far--as the best picture.�J�k
 �8I also use a test that many others use--the credit test.�J�k
 �OGo to the movie credits at the end and compare the print between portable DVDs.�J�k
 �cOn some, you cannot read the credits...on the Toshiba almost every line is clear and readable. ....�J�k
 ��Took my new Toshiba portable DVD to work today to see if I was the only one totally amazed by the spectacular color display and and sound system.�J�k
 �jCouldn't get it away from my co-workers, many of whom are very fussy about their audio and video products.�J�k
 �3All agreed this is one terrific performing product.�J�k
 �HWhen I first heard about the product, I was more than anxious to try it.�J�k
 �=Every review that I read was good, so I had to give it a try.�J�k
 �KI must be the only one in America that was not satisfied with this product.�J�k
 �'It actually did me more harm than good.�J�k
 �FThe only good thing was the warming sensation which was very soothing.�J�k
 �*However, after that, it itched like crazy.�J�k
 �'It's possible that I am allergic to it.�J�k
 �I returned it.�J�k
 �>Yes, Sucker, is how I felt the first time I used this product.�J�k
 �=Waste of money, use Abreva, or get a prescription if you can.�J�k
 �qI cannot think of a better rock album from the previous decade that matches this one in intensity and creativity.�J�k
 �aWhen I was in high school this was the album that brought together the punks and the metal heads.�J�k
 �PIt compleley reinvigorated hardcore in a way that only they could have imagined.�J�k
 �KRefused took what they learned from Nation of Ulysses and made it anthemic.�J�k
 �GThis album is literaly exploding with ideas and excellent song writing.�J�k
 �%For me, this record set the standard.�J�k
 �EI had been listening to hardcore for a few years, you know, Converge,�J�k
 �RCave In, Turmoil, etc and had heard Refused, but never really given them a chance.�J�k
 �RThough most everyone I knew who owned this record would not stop talking about it.�J�k
 �So I went out and bought it.�J�k
 �YEver since then, my entire presepctive on hardcore and music in general has been changed.�J�k
 �]I have been playing in a hardcore band for a few years now and writing music for even longer.�J�k
 �)The arangement of the songs is brilliant.�J�k
 �iPerfect dynamics...each song stands alone, though most are linked together with various clips and segues.�J�k
 �_Even better, as different as each song is, the album possesses a unity that is so hard to find.�J�k
 �qTo top it off, the layout includes provocative manifestos and ideas regarding the bands' politics and viewpoints.�J�k
 �MAs sorry as I am that they have called it quits, it is such a perfect ending.�J�k
 �Why not go out on top?�J�k
 �BUY THIS RECORD.�J�k
 ��2 1/2A few standout moments do not justify all the artistic praise in what is essentially consciously experimental, hardcore punk-politics.�J�k
 ��I bought the dvd version of this album a year and a half ago and loved it like I like an album only a few times a year - listened to it ten times in a row and to the exclusion of doing things loved it.�J�k
 �]Well loved the music portion of it anyways, the dvd features aren't really worth it honestly.�J�k
 �dYou can get the gist of what this album is from the other reviews, I merely offer a vote of support.�J�k
 �XTo put it in perspective, after I listened to it over and over again for months I moved.�J�k
 �And lost the dvd.�J�k
 �And dropped and broke my ipod.�J�k
 �cAfter not being able to find it on emusic I am willing to yet again drop over $12 to have it again.�J�k
 �\If you like hardcore but INNOVATIVE punk pay for it, if you don't have money then pirate it.�J�k
 �But listen to this, seriously.�J�k
 �KI was walking through a big chain cd/video store and heard this cd playing.�J�k
 �I was blown to bits!�J�k
 ��It takes a VERY VERY impressive bit of music for me to be taken back at a first listen, but this was definately doing the trick.�J�k
 ��I went even one step further to go right up to the counter and ask who this was they were playing and where in the store was it stocked.�J�k
 �I wanted it RIGHT NOW !�J�k
 �>Of course they were out of stock but I put in a special order.�J�k
 �That was three days ago.�J�k
 �DI have a few days to go before it arrives and i'm itchin to get it!!�J�k
 �I ALMOST NEVER BUY A CD UPON�J�k
 �GFIRST LISTENING TO IT, ESPECIALLY WHILE STILL IN THE STORE I HEARD IT !�J�k
 �This is the exception.�J�k
 �These guys kick the royal ... !�J�k
 �@I swear, and I'm from the old school metal / punk era , BUY THIS�J�k
 �and you will not be sorry.�J�k
 �
I'm Done .�J�k
 �vIt does look better on the picture as the color is a bit off and it appears a bit more neat and accurate on the photo.�J�k
 �I still like it�J�k
 �#and it's good enough for the price.�J�k
 �yLe Groove Eclectique, "mixed" by the Director of Music at the Au Bar in New York City, seemed like a wonderful CD to buy.�J�k
 �dIt seemed like a nice CD that would leave me relaxed and possibly impressed by the mixing abilities.�J�k
 �0None of the above happened when i heard this CD.�J�k
 �|i was expecting a mix, when what i got was songs that overlapped each other, with no effort to match beats or sounds at all.�J�k
 ��Not only that, but there was a situation where the beats of the two different songs created a kind of mass confusion due to very poor mixing.�J�k
 �RI was expecting something much different(as the description told me it was MIXED).�J�k
 ��The songs were not terrible, don't get me wrong, but the absolute lack of effort in making this a smooth CD was enough for me to toss it.�J�k
 �aThis was an impulse buy, I bought both CD's, tried to appreciate them but they are just horrible.�J�k
 �4Very cheezy selection, badly mixed (if mixed at all)�J�k
 �7and I just wish I spend my money on something different�J�k
 ��(yes, even a best of the 80's collection would have been a better purchase)Seems these guys are trying to make a quick buck on the lounge/world music wave, which is fine, but please........not with this compilation.�J�k
 �sYou might like this if you are really into Arabian/Middle eastern music, Kolor is a nice track and that's about it!�J�k
 �-you don't need a magnifying glass to read it.�J�k
 �i'm glad to have it.�J�k
 �helpful.�J�k
 �(has a new feature i haven't seen before.�J�k
 �Clooking north is on one side and looking south is on the flip side.�J�k
 �1it's a sky chart that is closest to our latitude.�J�k
 �(Using with our astronomy class students.�J�k
 �hThis is helping them seek to know the skies at night on their own without needing our help all the time.�J�k
 �II recently received this piece after waiting a long time to commit to it.�J�k
 �:It's gorgeous - solid wood (very heavy), beautiful finish.�J�k
 �JI would highly recommend it as an addition to any suite of fine furniture.�J�k
 �<The musical content on this recording is smokin' throughout.�J�k
 �:However, the sound quality on the majority of it is lousy.�J�k
 �6The cd is taken from two different Boston gigs in '69.�J�k
 �XOn the first five tracks the music is distorted and the vocals sound especially muffled.�J�k
 ��The quality improves considerably for the second part with great versions of all four songs, but unfortunately these only comprise fourteen minutes of an already-too-short forty minute recording.�J�k
 ��If the quality were better, obviously I'd rate this much higher, but you really have to be a die-hard fan to enjoy the first two thirds, given the distortion.�J�k
 �Que lastima!�J�k
 �PThe works of E.A. Wallis Budge were out of date before they were even published.�J l
 �zHis sole gift to Egyptology was the popularisation of the field, leading at the least to extended availability of funding.�Jl
 ��While interesting as a piece of scholarly history, no beginner should work from this book alone; Budge's understanding of the language is fallible to a degree that would forever mar the education of the casual student in the Egyptian language.�Jl
 �LMy suggestion would be James Allen's new "Middle Egyptian", if not Gardiner.�Jl
 �zThis book may have some remaining marginal utility for people who just want to learn to write their name in hieroglyphics.�Jl
 �lHowever, if your interest in the Egyptian language runs deeper than that, you will want to pass this one by.�Jl
 ��Budge uses a system of transliteration that is no longer the standard, and now only found in reprints of books by Budge himself.�Jl
 �6If you learn it, you will have to unlearn it later on.�Jl
 �@His presentation of the grammar is also obsolete and incomplete.�Jl
 ��More serious students would be better served by Gardiner's -Egyptian Grammar-, or Allen's -Introduction to Middle Egyptian-, or even Mercer's similarly priced paperbacks.�J	l
 �The best part of this book is the beginning, where Budge traces the history of the Egyptian language and then its decipherment.�J
l
 �;All of these chapters are very interesting and informative.�Jl
 ��If you want to get a good picture of what the state of Egyptology was in the 1930's, then this is not a bad book for that, although the historical information is rather abbreviated compared to an actual history book.�Jl
 �qWhich isn't surprising, given that this book's main purpose was allegedly to teach the ancient Egyptian language.�Jl
 ��Unfortunately, the field wasn't thoroughly developed when Budge wrote this book, so any new student would be better off reading a newer work.�Jl
 �kAnd, while I can't speak for others, I found the information poorly organized and impossible to learn from.�Jl
 �This is an interesting introduction to the ancient Egyptian language, but the information in this book is from the early 1900s.�Jl
 �WAlso, Budge has a heavy-handed style of writing that is hard to wade through sometimes.�Jl
 ��The best thing about this book is that Budge provides lots of extended samples of ancient Egyptian writing, which makes for good reading practice.�Jl
 ��FOR THOSE WHO DID NOT KNOW OF EITHER ARTIST, THANKS SHOULD BE GIVEN TO CLINT EASTWOOD FOR HIS SELECTIONS ON HIS 'BRIDGES' SOUNDTRACK.�Jl
 �dTHE COMBINATION OF THESE TWO ENORMOUS TALENTS IS A CLASSIC FOR MUSIC LOVERS, REGARDLESS OF CATEGORY.�Jl
 X�  the johnny hartman john coltrane band collabration from the first note creates an atmosphere of a smokey bar with ice clinking in glasses as patrons talk in hushed tones as hartman waxes smooth as glass on vocals and the band is restrained but wonderful......not trying to overshadow hartman but letting you know that they are special to..and special they are as a who's who with elvin jones on drums johhy griffith on bass and mccoy tyner on piano...�Jl
 ��I'm not a reviewer I'm a just lover of Jazz, Old and New, and I am trying to turn my Cassettes into CD's, I wish there was a trade-in for the old cassettes so I could afford to get more CD's.�Jl
 �zThis CD is one of the Best!With two of the Greatest Jazz-Men of all time, If you love SMOOTH JAZZ this is for you....eb48.�Jl
 �TThere is neither one now nor will there ever be another voice like Johnny Hartman's.�Jl
 �$Nor a sax player like John Coltrane.�Jl
 �@This CD is a Jazz classic that belongs in your library of music.�Jl
 �GIt's smooth, gentle, romantic and a great example of true jazz ballads.�Jl
 �6Not to mention the great musicians playing on this cd.�Jl
 �VThere is magic in their music and in Hartman's voice, that comes across in each track.�Jl
 �YIf you're looking for true jazz musicians at their best, doing ballads, then buy this cd.�Jl
 �lThey all deserve appreciation for their gifts and talents to which you will feel when you listen to this cd.�Jl
 �Enjoy!�J l
 �MWords don't easily describe the smooth, low, velvet sound of Hartman's voice.�J!l
 �\With Coltrane, Hartman recorded the definitive ablum of love, but this CD is more than that.�J"l
 �UIt is a beautiful melding of instruments and voice wrapped around sentimental lyrics.�J#l
 �'This is the finest album ever recorded.�J$l
 �SYou know you've played a CD too often when you're neighbours are humming the tunes!�J%l
 �I can't help it.�J&l
 �TThis disc with a glass of wine is as close to nirvana as this DC chick may ever get.�J'l
 ��I first discovered Hartman after reading Billy Collins' poem, "Nightclub" which describes the voice as curling through the air like smoke from a cigarette someone left burning on a baby grand at 2am.�J(l
 �TListen to "Beautiful" and just thank your lucky stars for life and love and Hartman.�J)l
 �dJohnny Hartman's voice will make you melt, and the collaboration with John Coltrane works perfectly.�J*l
 �,This will be one of your favorite Jazz cd's.�J+l
 ��The hard-bop of 'blue train' served as my introduction to the talents of John Coltrane, and later I 'progressed' to listening to both A Love Supreme and Interstellar Space.�J,l
 X1  When my dad bought this album home a little over two weeks ago, it was a revelation to me that the man who is generally considered to be an 'avant' jazzman could play such heart achingly gentle and tender ballads with such feeling and taste (not that his other work lacks feeling, taste, or heart either).�J-l
 �KThis album is the proof that Coltrane could do it all and do it better too.�J.l
 �<All the non-believers need to hush the fuss and dig on this.�J/l
 �9Oh Yeah, why have I never heard of Johnny Hartman before?�J0l
 �NThis man has a voice like an angel and should be a superstar in his own right.�J1l
 �%These two brothers click on this set.�J2l
 �Buy this album, buy it now.�J3l
 �rThere have been few albums that I can listen to at any time, over and over again; night and day, year in year out.�J4l
 �This is one of them.�J5l
 �GColtrane and Hartman merge into a musical ghost of haunting perfection.�J6l
 �UThere are times that I cannot tell the difference between the two- it is that smooth.�J7l
 �MThe arrangements are wonderful, the selection classy, the packaging gorgeous.�J8l
 �hThere is nothing that I would not rate as 5 stars about this album- for that matter 11 stars and higher.�J9l
 �XColtrane and Hartman is the best realized duet between a voice and soloist ever: period!�J:l
 �A rare gem.�J;l
 �A gift.�J<l
 �Truly a gift.�J=l
 ��The smooth sensuous sounds of Coltrane's sax combined with the mellifluous tones of Johnny's voice will woo women into a profound state of arousal.�J>l
 �`This CD is very mellow and is the perfect accompaniment to a full bodied wine and a few candles.�J?l
 �gThe songs are standards and easy to listen to, but the musicianship of Coltrane and Hartman is amazing.�J@l
 �fHartman shines in "Autumn Serenade" and gives the listener a true sense of heart break in "Lush Life".�JAl
 ��Coltrane is impressive throughout the CD, particularly because his phrasing and lyricism is deeper than on some of his previous releases.�JBl
 �OPerhaps that is the result of the interplay between his sax and Johnny's voice.�JCl
 �Men, buy this CD.�JDl
 �!And do it before Valentine's Day.�JEl
 �KOf all the CDs in my jazz collection, this is one of those I treasure most.�JFl
 ��John Coltrane's tender and sensitive accompaniments are a refreshing contrast to the "sheets of sound" reputation for which he is better known.�JGl
 �gHowever, for all of Coltrane's greatness, make no mistake:Johnny Hartman is THE star of this recording.�JHl
 �=His warm baritone is enthralling and irresistible throughout.�JIl
 �nThe highlight of this outing is Lush Life, and Hartman gives THE definitive version of that timeless standard.�JJl
 �It is absolute perfection.�JKl
 �>Hartman's version never has been, and never will be surpassed.�JLl
 �OWe can only speculate why Hartman did not enjoy greater acclaim while he lived.�JMl
 �cI only know that since I first heard him on the radio in 1993, my life has been richer as a result.�JNl
 �yAs an aspiring singer myself, this CD is a must for anyone who wants to hear it done at the highest, most intimate level.�JOl
 ��I love "A love supreme", but I can understand how some people find that one and even perhaps "My Favourite Things" kinda hard to get and artsy-fartsy.�JPl
 �@This one here, on the other hand, serves it straight and smooth.�JQl
 �'Standards, great playing, great vocals.�JRl
 �That's that.�JSl
 �HJohn Coltrane is overwhelmed by the dated, drippy ballads on this album.�JTl
 �jIn the 60's, every jazz musician had to do a ballad album just the same as everyone did a bossanova album.�JUl
 ��The music audience who had either given up jazz for rock or could not deal with jazz that was more cerebral were putting pressure on declining jazz sales.�JVl
 ��John Coltrane watered down his sound for a few ballad albums and then went right back to his real "sheets of sound" force of nature.�JWl
 �HEven "My Favorite Things" was transformed into a transcental experience.�JXl
 �(This album though clearly sounds forced.�JYl
 �ZIf you dislike Coltrane because you don't find him accessible, you might enjoy this album.�JZl
 �.However, you are missing out on the real deal.�J[l
 �:quite by accident i happened to hear MY ONE AND ONLY LOVE.�J\l
 �"to put it mildly i was blown away.�J]l
 �Wi cant imagine how i have lived nearly a half a centurywith out hearing johnny hartman.�J^l
 �1all i can say is buy this cd, you wont regret it.�J_l
 �UI never heard of this band before reading a review which drove me to search this out.�J`l
 ��As a fan of their influences (Stereolab, the Fall, early P.I.L.), and a lover of droning and cryptic rock, this album combines an homage to these groups into a peanut-butter-meets-chocolate (remember the Reese's commercials way back?)�Jal
 X  that collides the misleadingly-billed-as-fragile English female lilt-litany and the gruff, over-the-top Scots anarchist bloke vocal styles into a sweet-and-sour concoction that's heavy on the ears but somehow uplifting even as it batters you into cringing before its aural assault.�Jbl
 ��It is, upon repeated listenings, layered and complex beneath its initially monolitihic surface sound, and I highly recommend it.�Jcl
 ��Not an album I would listen to everyday, but when I'm in the mood for a good bash of the old eardrums which is smarter than most any metal and more innovative than what passes for alternative these days, this is just the album the shrink prescribed.�Jdl
 �@i really liked his last effort, mostly cause of its naive charm.�Jel
 ��it sounded like he was trying to make a serious album, only he had the worst materials to put it together with, whereas on this one its the other way round.�Jfl
 ��he ups the production on it considerably, but now its the songwriting thats cheap and tacky.i dont mean to be harsh, but hes trying really hard to sound all quaint and charming, and theres only maybe three places on the album where it comes off.�Jgl
 �~MBGATE, as mentioned in the other reviews, is one of them cause it sounds like hes trying to make a semi-straight dance track.�Jhl
 ��the rest of the album sounds just way too wink-wink nudge-nudge ironic in its amateurness.compare Cake off the first one to anything off this, and you'll get an idea.�Jil
 ��instead of a complex collection of songs made with bad source sounds (which works on the first one), this one turns out like an IDM version of mr scruff.�Jjl
 �9thats not a good thing.im sorry, but it gets old quickly.�Jkl
 �NThis movie has great historical value, especially for the State of California.�Jll
 �.The story of Ishi is one everyone should know.�Jml
 �KHe was the LAst of His Tribe and just came walking out of the bush one day.�Jnl
 ��No one even thought there was any of his tribe left and he taught us all a great deal about a people that had been wiped of the face of the earth.�Jol
 �pThe acting in this movie was great and if you are a fan of Ishi then this is definately a movie you want to see.�Jpl
 �,If you are a fan of History, see it as well.�Jql
 �This movie was really good.�Jrl
 �
Different.�Jsl
 �I really like Grahm Green�Jtl
 �and this movie was really good.��;      Jul
 �I loved it.�Jvl
 �ZWASDISAPOINTED IN THIS MOVIE FRIST IT MADE THE INDIAN LOOK LIKE HE WAS STUPID.WHAT A SHAM.�Jwl
 �TTHEN TRYING TO CONVERT HIM INTO A NEW KIND OF LIFE.JUST WASNT INTERESTING AT ALL....�Jxl
 �`it was great and in good condition was real to life really enjoyed am native so was great to see�Jyl
 ��While I'm sure some poetic license has been taken, the movie has a factual base about a young Indian man, greatly probed and studied, who was basically the last person left from an already obscure tribe.�Jzl
 �bIt lists Jon Voight as star, but I believe he was outshone by the always believable Graham Greene.�J{l
 �WDuke and Trane play it sweet this time, no big orchestras, no avant garde explorations.�J|l
 �cTwo of the greatest ever playing together, perhaps there is too much to expect from this meeting. '�J}l
 ��In a sentimental mood' and 'my little brown book' definitely live up to anyone expectations, to me, the best versions of these 2 classic Duke songs.�J~l
 �NBut there is also a fair amount of jams that do not quite live up to the hype.�Jl
 �SNot among Trane or Duke greatest recordings or essential, but still, a nice listen.�J�l
 �3this pairing of jazz greats is such a strange idea.�J�l
 ��two very different musical thinkers who you might think would clash in the studio, join together in a nice little collaboration.�J�l
 �'the problem is, it's a little too nice.�J�l
 ��there is no real spark here and the players seem to be genuinely delighted to be playing together, but there is no overt displays of passion in these songs.�J�l
 j�  J�l
 �qIn A Sentimental Mood" starts off very warm and nice and it's great to hear Coltrane's playing over Duke's piano.�u(J�l
 �8but once you get the general idea, it's all of the same.�J�l
 ��overall, it's a nice little cd, but i cannot recommend it highly, there are many many more worthwhile albums that were made seperately by these two artists.�J�l
 �ustill, if the mood hits you, you can slip on your cardigan and your pipe and slippers and listen to this by the fire.�J�l
 �cI love Duke Elllington, and I usually don't love John Coltrane, but this album together is amazing.�J�l
 �I can't get enough of it!�J�l
 �0My only complaint is: why is/was there not more?�J�l
 �[Just set this going and put it on endless repeat and maybe it will seem/feel more complete.�J�l
 �+Sublime playing from both Picassos of jazz.�J�l
 �-This session/collaboration is simply amazing.�J�l
 �.The first song especially is really wonderful.�J�l
 �JI can't say how many times I have given this as a gift and it never fails.�J�l
 �+Wonderful, relaxing, and inspiring music...�J�l
 �kI've just received my copy of Ellington and Coltrane in the "Originals" collection, and feel disappointed..�J�l
 �"Of course, the music is top shelf.�J�l
 ��BUT: The sound has only been slightly improved (I guess you have to be a wizard to improve Rudy Van Gelder's mediocre engineering), and moreover, the new digipack is really a cheap affair compared to the previous reissue from the 90's..�J�l
 �EBad cardboard, ridiculously small liner notes, low quality printing..�J�l
 �iIf you already own the 1995 reissue, just keep it: you really can live without this "brand new" version..�J�l
 �ZIt's not that there's anything wrong with this music, but it's neither artist's best work.�J�l
 ��The opening "In A Sentimental Mood" is the definitive take of that tune, and "Big Nick" is nice, but to me this recording drifts for the most part.�J�l
 �^Big Ellington fans will want this to hear his poano playing in a different context than usual.�J�l
 �GI had heard some of Coltrane's later work and was not very moved by it.�J�l
 �But this is an excellent album.�J�l
 �I'd recommend it to anyone.�J�l
 �8The sound that JC wrings out of the sax is just awesome.�J�l
 �tI heard other renditions of "In A Sentimental Mood", but none come close to this one in terms of the saxophone work.�J�l
 �KThe rest of the songs are also superb including the very catchy "Angelina".�J�l
 �vThis was also the first work by Duke Ellington and I am impressed with his compositions; less so by his piano playing.�J�l
 �aThis along with Ballads and My Favorite Things are among the most accessible of Coltrane's works.�J�l
 �XHow can I say anything negative about two of the greatest jazz musicians who ever lived.�J�l
 �The music was subperb.�J�l
 �0Amazon sent me this CD in a most timely fashion.�J�l
 �All went smoothly.�J�l
 �8Almost a good tool but falls short due to its cheapness.�J�l
 �)But what do you expect for a few dollars?�J�l
 �7Got exactly what I paid for with no astounding results.�J�l
 �_This book is simply an extensive list of artist biographies with a brief summary of their work.�J�l
 �UIt is a "Who's who" reference of artists working with science/technology based media.�J�l
 �IIt does not have good examples (photos) or explorations of the art works.�J�l
 �pThis book covers an extremely wide range of artists, but with disappointingly little theoretical rigor or depth.�J�l
 �^The strength of this CD is its purity - there are no other instruments used on this recording.�J�l
 �!Only the sounds of Singing Bowls.�J�l
 ��However, as much as Thea Surasu allows the vibration of the Singing Bowls to stretch out to a complete fade away, there are large gaps of silence between bowl sounds.�J�l
 ��These gaps often exceed about 30 seconds or longer, and they are frequent - Just when I get to the point that I am used to the singing bowl vibrations, they stop.�J�l
 �.Just when I get used to the silence, it stops.�J�l
 �JThe back-and-forth keeps me from being able to meditate with these sounds.�J�l
 ��The vibrations of the bowls is stretched out to such an extent that only about 10% of the sound comes through the 6-speaker audio in my car!�J�l
 �KThus, don't expect much from this CD unless you have a good-quality stereo.�J�l
 �FSo, it's no good in the car, and doesn't work for me in mediation. ...�J�l
 �-Frankly, I expected more from Steven Halpern!�J�l
 �tThe authors' logistics in this book is as confusing as their language used to try to teach this simple math subject.�J�l
 ��Often, they forget to introduce essential formulas and concepts, or relegate them to parts of the book long after they should have been mentioned.�J�l
 �/The main example of this is the empirical rule.�J�l
 ��They substitute lengthy processes in lieu of essential formulas, such as the confidence interval formula, and the formulas used in hypothesis testing.�J�l
 X<  Examples of problems are sparse, and the authors' work is generally not shown, but rather they imply that the student already knows what they are doing (kind of like moving across four lanes with no turn signal, "Everyone knew what I was doing!")This book got two stars because, since it is a paperback, it is cheap.�J�l
 �tThe authors' logistics in this book is as confusing as their language used to try to teach this simple math subject.�J�l
 ��Often, they forget to introduce essential formulas and concepts, or relegate them to parts of the book long after they should have been mentioned.�J�l
 �/The main example of this is the empirical rule.�J�l
 ��They substitute lengthy processes in lieu of essential formulas, such as the confidence interval formula, and the formulas used in hypothesis testing.�J�l
 X<  Examples of problems are sparse, and the authors' work is generally not shown, but rather they imply that the student already knows what they are doing (kind of like moving across four lanes with no turn signal, "Everyone knew what I was doing!")This book got two stars because, since it is a paperback, it is cheap.�J�l
 �5I'm a student and have never taken Statistics before.�J�l
 �gI found it to be too confusing, the instructions to be too vague and the examples to be hard to follow.�J�l
 ��They automatically assume that you know immediately what they're talking about; some new concepts they explain in one sentence, which I found to be seriously lacking for someone who's trying to learn.�J�l
 ��Another thing is that you have to keep flipping back and forth through pages to get to formulas, charts, etc., including the references in the back of the book.�J�l
 �0There is nothing "basic" about this book at all.�J�l
 ��I bought this book (used)because in the advertisement said "like mew", it was all riped, front and bzack page, it was not used, it was abused.�J�l
 ��I waite about one month but i can't get my book.it disappear through transportation.if you are order this stuff outside America?think twice please.�J�l
 �+This was a great book for Intro Statistics.�J�l
 �%It was for a Distance learning class.�J�l
 �]This is actually a book that is used as textbook at Gaston College in Dallas, North Carolina.�J�l
 �^I had been looking for a vacume cleaner for a while, trying to replace a old and noisy Hoover.�J�l
 �FLiked Dysons but just couldn't pull the trigger on its $400 price tag.�J�l
 �PGot this unit with an unbeatable deal, and result was much more than I expected.�J�l
 �BIt's extremely light to push around, and it has a very wide track.�J�l
 �&Emptying the can is a snap, literally.�J�l
 �Best of all, it is QUIET!!!!�J�l
 �>Now my dog no longer runs for his life when I start to vacume.�J�l
 �:)�J�l
 �gWe have 3 (actually 4, but i don't consider the 'shark' a real vacuum of any use) vacuums in the house.�J�l
 �One for each floor.�J�l
 �Miele..hoover, and this one.�J�l
 �/The sharp is lightweight, easy to put together.�J�l
 �Tthe one screw to put the handle on is kind of ghetto, but you get what you pay for..�J�l
 ��it's no multi hundred dollar miele!as for performance, it's pretty powerful. easy to empty and it's easy to see when it gets full.�J�l
 �3I find the on/off annoying as a foot activate item.�J�l
 �i like on handle on/off.�J�l
 ��I also do not like the lever for putting the tilt function so the vacuum can be used for the floor.plasticky unit, but it makes it really light. seems to hold up to moderate use just fine.�J�l
 �-(...) Needed to replace my 7 year old Hoover.�J�l
 �#My first bagless and I'm loving it.�J�l
 �Really powerful and quiet.�J�l
 �PDon't have to scream and shout anymore while vacuuming and talking on the phone.�J�l
 �&Light weight, really easy to manuever.�J�l
 �=Cleans easily with washable filters except the carbon filter.�J�l
 �Looks good too!�J�l
 �(...)�J�l
 ��I 've been using this machine for over 4 years and even today i took it out and said to myself that i would be wrong not to put up a review. excellent machine.�J�l
 ��i used to use a $700 upright bag less vacuum cleaner and i like this better over the $700 because it is much lighter and does a superb job of what it is meant for.i paid $150 when i bought it.�J�l
 �money well spent.�J�l
 �Si don't even care if it went bad tomorrow after all these years of faithful work..!�J�l
 �9[...] Used it a couple times with generally good results.�J�l
 �It's not THAT quiet.�J�l
 �:No more quiet than the 7 year old eureka it was replacing.�J�l
 �DI came home from work one day, and my wife was using the old vacuum.�J�l
 �%She said the sharp would not turn on.�J�l
 ��I plugged it in and it worked ok, although suction seemed way down ( yes, the bin was empty and clean, and there was no obstruction in any hoses. )�J�l
 �MI vacuumed for around 10 minutes, then it just shut off and wouldn't turn on.�J�l
 �]I should have just bought a good vacuum to begin with, because I'll have to do it now anyway.�J�l
 �I love this vacuum�J�l
 �Dbut I broke the belt and can't find a replacement locally or online.�J�l
 �ACan somebody point me to a source for parts for Sharp vacuums????�J�l
 ��Michael Allen Sings is a delightfully happy collection of some of the greatest songs ever written, remastered with a crisp freshness that make the songs sound new again.�J�l
 �$There is an infectious feeling here.�J�l
 �7I dare anybody to sit still while listening to this CD.�J�l
 �I LOVE this album.�J�l
 �]It took me awhile to track down this song/artist after I heard it on a friend's old cassette.�J�l
 �VUnfortunately info is still hard to locate and I can't find a picture of her anywhere.�J�l
 �WBut this woman has a great voice in my opinion and "Killer Blues" is a "Killer Song!!!"�J�l
 �QThis case does what it should, and that is cover your Sandisk e200 series player.�J�l
 �qThe problem, is that I was trying to get it on my newly-bought e280, and it just tore on the bottom-right corner!�J�l
 ��It works okay, but I would've liked a more durable material, and/or an indicator for the power/menu button likeSandisk Sansa E200 Series Sillicone Case (Black)has.�J�l
 �HAlso, it is quite difficult to get on and off, so when it's on, it's on.�J�l
 �cheap�J�l
 �%and sounds good,but sometimes it dead�J m
 �+and you have no idea to solve that,not good�Jm
 X�  Stephan Likosky's anthology of international gay and lesbian writing, Coming Out (New York: Pantheon, 1992) includes much interesting stuff culled mostly from the gay press (including an interview Jared Braiterman conducted with Brazilian AIDS activist Herbert Daniel), ILGA (a nuanced historical chapter on Cuba), and a few pieces reprinted from journals (including Ruan and Tsai on China and Joseph Carrier on Mexico).�Jm
 ��Likoksy endeavored to include material on Asia and Africa, though it tends to be by/about Asians and Africans in Britain and the US.�Jm
 �]Why he included the samplings of Theory he did (Fernbach, Hocquenghem, Mieli) I have no idea.�Jm
 �@Nonetheless, he produced an interesting collection of materials.�Jm
 �GI was a little worried to order this because I thought it wouldn't fit.�Jm
 �OIt's adjustable and not as flimsy as I imagined from reading the other reviews.�Jm
 �AI thought the bra was a nude bra though, but it's just a cut out.�Jm
 �Which is even sexier!�J	m
 �GIt comes with two extra hooks for the cuffs in case they break I guess?�J
m
 �-Or if you want to change the length yourself.�Jm
 �I am on the thinner side�Jm
 �@but I tried it on and looked just like the woman in the picture.�Jm
 �It's great for the price!�Jm
 �Buy it!�Jm
 �It seems cheap and poorly made.�Jm
 �It rips too easily.�Jm
 �<If I had the chance, I would go back and buy something else.�Jm
 �4This outfit is great as long as you treat it gently.�Jm
 �0It will rip fairly easy if you are playing hard.�Jm
 �But it does look stunning.�Jm
 �aProduct is hot like the picture, but a little overpriced considering the quality of the material.�Jm
 �dThe chain connecting the neck piece to the hands is a cool idea, but it acts more like a decoration.�Jm
 �SYou actually need to be careful about snapping the fabric where the chain connects.�Jm
 ��A good buy if you're worried about getting something that won't fit- my girlfriend has a large bust so it's hard to find stuff that fits.�Jm
 �This did the trick!�Jm
 �SThis bra and panty set broke the fisrt time I wore it,the metal broke on the cuffs.�Jm
 �DIt is very poorly made, and I would advise anyone against buying it.�Jm
 �UYou have a better chance of quality buying bras out of a van on the side of the road.�Jm
 �This book is horrible.�Jm
 �/It is not true to the original classic stories!�Jm
 �DMost of the endings, of the stories I've read so far, are different.�J m
 �*I don't care for the illustrations either.�J!m
 �I was very dissapointed.�J"m
 ��I bought this for my neices, and didn't read the book until they came to visit me, and by then it was too late to return this book.�J#m
 �"It's basically going in the trash.�J$m
 �^After an exhausting hunt though Amazon, the library and the bookstore I found this 'treasure'.�J%m
 ��I thought I would end up with three or four separate books to get what I wanted - fairy tales, mother goose rhymes and Aesop fables.�J&m
 �Whew!�J'm
 �They are in here.�J(m
 �DThe style is kept in fairy traditional and only updated when needed.�J)m
 �QThere are graphics on every page making enjoyable for young readers or listeners.�J*m
 �%We love Thomas the Tank EngineTrains!�J+m
 �Henry is definitely a favorite.�J,m
 �6Recommend for any child who loves to play with trains.�J-m
 �Good sturdy plastic.�J.m
 �&Battery operated, usually a C battery.�J/m
 �QMy boys have the Ultimate Tomy Thomas the Train set with a collection of engines.�J0m
 �4They love this Train set and play with it for hours.�J1m
 �:And as rough as my boys are, these toys manage to survive!�J2m
 �But parents beware!�J3m
 �=You'll be making new track configurations on a regular basis!�J4m
 �AI recently met the author at a conference and purchased her book.�J5m
 �QThe 1459 plan she describes in Self Creation will change your life if you let it!�J6m
 �AIncorporate her tips and be ready for wonderful things to happen.�J7m
 �#The disc skips and is poor quality.�J8m
 �&I would be careful if you plan to buy.�J9m
 �,There are only a couple of really good cuts.�J:m
 �JI bought this book expecting a very in-depth well written book on equines.�J;m
 �.What I got is something quite diffrent though.�J<m
 �!It was vauge, and poorly written.�J=m
 �AIf you a beginer however, it is a fine book, I will give it that.�J>m
 �;I do not think this is a good thing for kids to be reading.�J?m
 �YHow people can find this a great book... unless they have a likng for revolting things...�J@m
 �I do not understand.�JAm
 ��Example from The Red Pony- Steinbeck describes how a boy mushes a vultures head 'til it's a red pulp... and cutting the horses throat 'til the mucus comes out in a stream.�JBm
 �6Do you really want your kids or students reading this?�JCm
 �MI warn you... if you don't like revolting things, don't read this nasty book.�JDm
 �FPLEASE don't force kids to read this!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!�JEm
 �YIf Red Pony is suppose to be about Steinbeck as a young child, I feel very sorry for him.�JFm
 �1I felt very uncomfortable while I read this book.�JGm
 �hThe family in this short novel isn't right; they don't talk to each other or are able to share emotions.�JHm
 �]The theme which is about how tough learning lives lessons and maturing can be is very boring.�JIm
 �DO NOT READ!�JJm
 ��Book Review OutlineBook title and author: The Red Pony by: John SteinbeckTitle of review: ITS OK!!!Number of stars (1 to 5):***Introduction: I thought I was an ok book.�JKm
 �+I don't like reading, but this book was ok.�JLm
 �PI I don't like however, how the horse dies at the end that wasn't a good ending.�JMm
 �YDescription and summary of main points: I did however, like the place where they were at.�JNm
 �MA dude ranch out in the west I think that is very cool and a fun place to be.�JOm
 �gEvaluation: I like the theme of the story and all the characters in the story I think it's a good book.�JPm
 �?I like when Billy say "Now he'll be all right in the morning ?"�JQm
 � (pg.24)Conclusion: I didn't like�JRm
 �but I didn't hate it ether.�JSm
 �Like I say I don't like reading�JTm
 �%but this book wasn't all that boring.�JUm
 �All in all it was ok�JVm
 �I'd give it three stars.�JWm
 �;Your final review: Reading is boring but The Red Pony is ok�JXm
 �I don't hate it�JYm
 �+but my hate for reading makes me dislike it�JZm
 �so I'm kind of iffy about it.�J[m
 � So I'm going to give it 3 stars.�J\m
 �PSeriously, I don't know how this can be considerd an appropriate book for a kid.�J]m
 �oIt was assigned reading when I was 12, and I was utterly traumatized to this day (20 years later) by "The Gift.�J^m
 X  " I remember graphic descriptions of pus and the sadistic delight Steinbeck took in detailed accounts of the home surgeries on the horse, a bloody tracheotomy and especially the image of the buzzard eating the pony's eye, his beak dripping with thick, dark blood.�J_m
 �.What kind of crazy person gives this to a kid?�J`m
 ��I cried for hours upon hours and I have never, never forgiven Steinbeck, nor have I been able to make myself read Steinbeck again.�Jam
 �Too bad.�Jbm
 �I hear he's a decent wrier.�Jcm
 �This book has no sense.�Jdm
 �JIt stupidity goes beyond what would be the real meaning of life and death.�Jem
 �+No one is this kind of realistic with life.�Jfm
 �-This is the worst book that I have ever read.�Jgm
 �$What kind of name is Jody for a boy?�Jhm
 �Sounds like a woman to me!�Jim
 �And Billy Buck?�Jjm
 � Welcome to Hicktown, California!�Jkm
 �3I gave it 1 star because I can't give it any lower.�Jlm
 �This book is really good.�Jmm
 �;If I could recommend a book, it would be this book because.�Jnm
 �It is a really a good book.�Jom
 ��I especially liked it when the birds began to peck the horse,and when Billy Buck had to cut the mare open so that he could get the colt for Jody.�Jpm
 �+So why don't they make another one like it?�Jqm
 �cI liked this story because it is about the lessons Jody Tiflin learns and the hardships he endures.�Jrm
 �+In every chapter somthing terrible happens.�Jsm
 �"As a result, Jody learns a lesson.�Jtm
 �PI especially liked the way the author made mini stories instead of a whole book.�Jum
 �>I recomend this book because it might help you learn a lesson.�Jvm
 �6I also think this book will live on for quite a while.�Jwm
 �IThe Red Pony is an interesting book because there are four short stories.�Jxm
 �balthough the book can be sad it gives you an idea of life on the ranch for a teenage boy maturing.�Jym
 �RIt is not my favorite book but it is definitely worth checking out at the library.�Jzm
 �#I think The Pearl is a better book.�J{m
 �Please don't buy this book .�J|m
 �It is a waste of your life.�J}m
 �There is no point to this book.�J~m
 �>The boy gets a pony the pony dies and then he gets a new one .�Jm
 �The End!!!!!!!!!!!!!!!!!!!�J�m
 �&Ahhhhhhhhhh PLEASE DONT READ!!!!!!!!!!�J�m
 �I think that John Steinbacks�J�m
 �(The Red Pony was a I guess descent book.�J�m
 �3But it skipped around alot and had not much detail.�J�m
 �*I liked itmyself because Ilike the author.�J�m
 �BI also like to read books like this in my 8th grade reading class.�J�m
 �]These four stories are well written but their is absolutely no flow between the four stories.�J�m
 �WIf this was submitted as four short stories I think that it would be easier to swallow.�J�m
 �$The first three stories interconnect�J�m
 �Ibut then you are left hanging as to whatever happens to the newborn colt.�J�m
 �>The fourth story doesn't really fit as a final chapter either.�J�m
 �\In the end I would have to say that this is a sorry waste of time to read for entertainment.�J�m
 �WOW.�J�m
 �THIS BOOK IS PRETTY BORING.�J�m
 �9BUT THERE ARE SOME INTERESTING THEMES DEALING WITH DEATH.�J�m
 �GAND AS USUAL, STEINBECK SUCCESSFULLY DESCRIBES THE SETTING BEAUTIFULLY.�J�m
 �#BUT IT'S NOT ONE OF HIS BEST BOOKS.�J�m
 ��I'M GUESSING THE BOOK IS CALLED THE RED PONY BECAUSE THATS JODY'S FIRST EXPERIENCE WITH DEATH, AND FROM THEN ON, HE STARTS TO MATURE AND LEARN.�J�m
 �KI read the red pony recently, and I thought that it was a pretty good book.�J�m
 �#The book also had a deeper meaning.�J�m
 �&It is about a maturity of a young boy.�J�m
 �VBecause at the end Jody begins to think of other people, for example his grand father.�J�m
 �And that is a sign of maturity.�J�m
 �_This collection of stories showcases Steinbeck's writing at its descriptive and evocative best.�J�m
 �bIt's a tender and moving depiction of the cycle of life and death, and a boy's journey to manhood.�J�m
 �_I am puzzled that this book appears so frequently on high school and junior high reading lists.�J�m
 ��There are several scenes that are graphic, violent, and disturbing, and the book is perhaps best read with the perspective of an adult.�J�m
 �6I read this book aloud to my children; ages 10 and 12.�J�m
 �!It was fascinating in its detail.�J�m
 �:The language and style are observant, forceful, unadorned.�J�m
 �lCalifornia ranch life in the depression is drawn as spare - a tenuous, unforgiving existence without frills.�J�m
 �1The characters are pithy, pragmatic, responsible.�J�m
 �~They stand in contrast to the people of the late 90's like the book's black cypress differs from an artificial Christmas tree.�J�m
 �dThe end of the book leaves you wondering; trying to sort out what Steinbeck wanted us to understand.�J�m
 �I enjoyed it.�J�m
 �lEnough that at midnight, before the week begins on Monday, I am trying to answer those questions for myself.�J�m
 ��While there are other, greater works by Steinbeck, this one combines his disarmingly simple prose style with a number of concepts from his work in the 30s and early 40s.�J�m
 �qMainly, the strange and ultimately destructive drive of humans in industrial society (as shown in the 4th story).�J�m
 �[The rest is a fascinating portrait of a boy's coming-of-age and alienation from his father.�J�m
 �0maybe people are tired of coming-of-age stories.�J�m
 �:but that doesn't mean Steinbeck's version isn't well-done.�J�m
 � This book was a really sad book.�J�m
 �WI hate the fact that poor innocent animals have to die because of us, humans of course!�J�m
 ��In the story it really wasn't Billy Buck's fault that Gabilian died but, then again he sould have gotten him out of the rain when it started!�J�m
 �0Poor Jody going through two deaths in one novel!�J�m
 �<But in the end when Nellie died his maturity really showed!!�J�m
 �I'm proud of him!!�J�m
 �HWe read this book in Mrs. Mimbs 5th perid class this book was all right.�J�m
 �DThe Red Pony by John SteinbeckA boy is given a pony to take care of.�J�m
 �5He still continues to do his chores and go to school.�J�m
 �`One day the ranchhand says it will not rain to leave the pony outside as the boy goes to school.�J�m
 �_Problem is it does rain and the boy hurries home to cover him up and bring him inside the barn.�J�m
 �aThe horse gets very sick and the ranchhand tries many things to get the horse better to no avail.�J�m
 �:He has to deal with the death of the pony as time goes on.�J�m
 �jRanch life continues as an old man comes to stay as the house he was born is no longer there on the ranch.�J�m
 �IA new pregnant animal has arrivedat the ranch and a very difficult birth.�J�m
 �zThe hardships of ranch life are forefront but there are good times, like when Jody's grandfather comes to stay for a week.�J�m
 �YThe Red Pony is a touching story about a boy who learns to care for a pony then loses it.�J�m
 ��At first I had thought it was a kids story because I had gotten the book from my grandmothers house and it was written inside the cover that it was given to my aunt when she was only ten.�J�m
 �pBut I was obviously mistaken because there are some parts of the book that would be too much for young children.�J�m
 �I really didn't like this book.�J�m
 �*It was too detailed and gory for my taste.�J�m
 �_But I did think the author did a good job describing things that you might have not understood.�J�m
 �AThe first 3 chapters had absolutely nothing to do with chapter 4.�J�m
 �;I would never read this book for fun or thorough enjoyment.�J�m
 �"Over all, I didn't like this book.�J�m
 �THIS BOOK IS AWFUL.�J�m
 �5IF YOU READ IT YOU'LL HAVE A 99% CHANCE OF HATING IT!�J�m
 �HIT IS UNINTERESTING, BAD PLOT, AND IT IS JUST A SIMPLE "BAD LITERATURE".�J�m
 �ANOTHER THING.....�J�m
 �IT'S DIGUSTING....�J�m
 �BAD LANGUAGE...�J�m
 �
IF YOUR AT�J�m
 �%A YOUNG AGE I SUGGEST NOT TO READ IT.�J�m
 �*THIS BOOK CAN BE SUMMED UP IN THREE WORDS:�J�m
 �THIS BOOK STINKS!�J�m
 �.WE HAD TO READ IT FOR READING, AND I HATED IT.�J�m
 �MY FRIENDS HATED IT, TOO!�J�m
 �+I DON'T EVEN FEEL RIGHT GIVING IT ONE STAR!�J�m
 �?IF THERE WAS A 0 STAR RATING, THAT IS WHAT THIS BOOK WOULD GET!�J�m
 � i hated this book with a paaion.�J�m
 �ki had to read it in english class last year ansd it was the worst book i have every read in my entire life.�J�m
 �dont read it!!!�J�m
 �MThe Red Pony was kind of interesting, but some parts were better than others.�J�m
 �>I didn't like the way the pony died so close to the beginning.�J�m
 �It was sad.�J�m
 �%I like books when good things happen.�J�m
 �This story was all about death.�J�m
 �lThe pony died, Nellie died, Old Easter and Gitano went to the mountains to die, and Jody killed the buzzard.�J�m
 � But, all in all the book was ok.�J�m
 �The Red pony is a good book�J�m
 �mbut I didn't understand the second part when Gitano comes and their was a whole chapter about that one thing.�J�m
 �!But over all it was not the best.�J�m
 �The Red pony is a good book�J�m
 �mbut I didn't understand the second part when Gitano comes and their was a whole chapter about that one thing.�J�m
 �I liked reading this book.�J�m
 �.Parts of it was blurry, but the rest was good.�J�m
 �%If you like horses, read The Red Pony�J�m
 �This book was very boring.�J�m
 �mIt's hard to understand the plot of life and death and doesn't go into much detail or develop the characters.�J�m
 �=The book skips around and does not make the chapters connect.�J�m
 �5Some readers say that it has a lot of symbolism in it�J�m
 �and yes, there is some.�J�m
 ��But if you're looking for something that grips you and actually makes want to read farther than chapter one, then read something else like Richard Wright's Native Son.�J�m
 �.If you ask me, this book was a waste of trees.�J�m
 �Definetly not Steinback's best.�J�m
 �It might have been a good book.�J�m
 �Might have.�J�m
 �I found it slightly boring�J�m
 �but I can tolerate that.�J�m
 �	But Jody!�J�m
 �He is so mean!�J�m
 �jTaking salamanders and horned toads and dumping them in his lunch pail and letting them die without water!�J�m
 �That's fun???�J�m
 �(I notice little things)�J�m
 �And the family!�J�m
 �)Carl Tiflin is a pretty mean person, too.�J�m
 �BThe only interesting story out of all four chapters is 'The Gift.'�J�m
 �KJody is reminiscient of the boy in My Friend Flicka, just a meaner version.�J�m
 �4In fact, 'The Gift' reminded me of My Friend Flicka.�J�m
 �.The boy there grows up with a sick horse, too.�J�m
 �0Two of the other stories didn't make much sense.�J�m
 �(Jody was just a tiny bit nicer in those.�J�m
 �eI know this book is supposed to be sixty feet deep, but the theme was very covered up and unoriginal.�J�m
 �EHow many stories out there are talking about young people growing up?�J�m
 �Ah, hundreds.�J�m
 �>We don't need another book, even a classic, to tell this tale.�J�m
 �$I suppose you should read this book.�J n
 �But don't expect much.�Jn
 �?I believe The Red Pony was not one of Steinbeck's better books.�Jn
 �*It jumped around between subjects to much.�Jn
 �-There was no real action in the four stories.�Jn
 ��This book you should definitely not waste your tme reading because it is very boring and depressing and all the story lines are hard to keep track of.�Jn
 �rLike so many of Steinbeck's other novels, The Red Pony is full of vivid imagery, raw emotion, and touching themes.�Jn
 �fHowever, unlike many of Steinbeck's other works, I found the Red pony to be a very unsatisfying novel.�Jn
 �PIt has wonderful character and plot development, but it dosn't take us anywhere.�Jn
 �+We are introduced to the Pony, but it dies.�J	n
 �%Nothing is accomplished by its death.�J
n
 �-Each chapter of the story finishes like this.�Jn
 �fThe reader is given very little completion at the end of eachc hapter and less at the end of the book.�Jn
 ��over all, the style was good, the plot development was good, the themes were powerful, but there was nothing to bring it together in the end.�Jn
 �OOf Mice & Men and The Pearl are both much more enjoyable novellas by Steinbeck.�Jn
 �A good book.�Jn
 �7This book shows what it's like growing up and maturing.�Jn
 �,If you do not like horses, read this anyway.�Jn
 �It is not about the pony.�Jn
 �It is about a boy growing up.�Jn
 �OMy favorite part is The Leader of the People because it has a lot of symbolism.�Jn
 ��Jody's grandfather, after being chewed out by Jody's father, is sitting on the porch when he notices a hawk sitting on a dead branch.�Jn
 �8He realizes that this is what his life is like: stopped.�Jn
 �Sorry, there is no money.�Jn
 �$I just wanted to get your attention.�Jn
 �Ha-ha.�Jn
 �8The TITULAR CHARACTER, the red pony DIES 40 pages in!!!!�Jn
 �oIt's basically the same depressing Steinbeck style of writing, but this is the worst I have ever read from him.�Jn
 �WI first picked up this book as a child, thinking it would be another great horse story.�Jn
 �hAlthough it does have merit as a novel, this is not a book for a child, especially one who loves horses.�Jn
 ��The death of the pony is horrific and traumatic and thinking about the book still gives me a very unpleasant feeling, even 20 years later.�Jn
 ��This book is not in the same category as other 'sad' children's books - The Yearling, Old Yeller, Black Gold - which made me cry (a lot!)�Jn
 �but I still enjoyed.�J n
 �5Please, don't give this book to your horse-mad child.�J!n
 �6I LIKED THIS BOOK BECAUSE THE BOOK BRINGS YOU INTO IT.�J"n
 �DMY FAVORITE CHARACTER IS JODY, BECAUSE HE IS JUST LIKE A NORMAL KID.�J#n
 �$I LIKED IN THE BOOK THAT THAT IT WAS�J$n
 �LIKE YOU WERE IN THE BOOK.�J%n
 � ALSO, IT WAS LIKE JODY WAS REAL.�J&n
 �:I THINK THIS BOOK IS ONE OF THE SADDEST BOOKS I HAVE READ.�J'n
 �.I STILL THINK IT WAS A REAL FINE BOOK TO READ.�J(n
 �IF I HAD TO READ THIS BOOK�J)n
 �AGAIN I WOULD READ IT.�J*n
 �THE THINGS THAT WERE COOL�J+n
 �(IN THE BOOK WERE WHEN THINGS GOT KILLED.�J,n
 �What did I think of the book?�J-n
 �cI thought it was pretty much o.k in the first chapter, but other than that I thought it was boring.�J.n
 �There was hardly no action.�J/n
 �"I think he could have done better.�J0n
 �:I liked the book The Red Pony because it was exhilarating.�J1n
 �The plot was awesome.�J2n
 �?I liked it because it was on a ranch, and I really like horses.�J3n
 �1I also liked the book because it was about death.�J4n
 �First Gabilan dies.�J5n
 �Then Nellie terminates.�J6n
 �Then Grandfather perishes.�J7n
 �&In every chapter Jody learns a lesson.�J8n
 �#This book was really extraordinary.�J9n
 �]Parts like when Jody sees the buzzard poke out Gabilan's eye and that part was really superb.�J:n
 �That is why I enjoyed the book.�J;n
 �`I did not choose to read this book... my great-grandmother gave it to me for christmas years ago�J<n
 �,and I knew I should read it sooner or later.�J=n
 �So I read it.�J>n
 �And didn't like it.�J?n
 �xIt was boring, and to me there was no point and each different part starts off like the part before last never happened.�J@n
 ��These stories don't seem to connect to each other, and I was confused and I wish that the whole story could've been about "The Red Pony" like the title.�JAn
 �MBecause I actually liked the first part, but the rest was a waste of my time.�JBn
 �WThe only good thing I could say about it was that I finally got that one out of the way�JCn
 �'so now I can read more appealing books.�JDn
 �Generally, I like Steinbeck.�JEn
 �I liked Of Mice & Men.�JFn
 �I liked The Pearl.�JGn
 �UThis book, however, remains at the top of my list as the worst book I have ever read.�JHn
 �&It moves slowly and bored me to tears.�JIn
 �?The only redeeming value of this book was that it was so short.�JJn
 X  The book includes four short stories about a boy growing up on a ranch in the Salinas Valley during the 1920s and is truly representative of Steinbeck's early writing, with his coarse, sometimes remorseful and uncmoplicated style clearly on display for the reader's pleasure.�JKn
 ��The physical condition of the book was somewhat disappointing because it was delivered without the the illustarated cover shown in the adverisement picture and the promised collector's case.�JLn
 ��If the purchase price had been higher I would have returned the book, but since it was intended as a gift, I gave my original copy away, and kept this copy for my library.�JMn
 �LIn the future if the book is not sold by Amazon directly, I will not buy it!�JNn
 �/I really didn't care too much for The Red Pony.�JOn
 �LIt might be interesting for some people, but it was by far my favorite book.�JPn
 �+I like books with action, unlike this book.�JQn
 �It was very slow paced.�JRn
 �dTo have such a slow plot, I not for once thought that it would have had as much profanity as it did.�JSn
 �LEven though you can't see any pictures, a lot of the book was quite graphic.�JTn
 �@I wouldn't recommend this book to you if you had a weak stomach.�JUn
 �lFurthermore, I wouldn't recommend this book to anyone, unless there is nothing left to check out in library.�JVn
 �YI JUST COMPLETED THIS BOOK FOR REQUIRED READING THIS SUMMER.I USUALLY LIKE LOVE ALL BOOKS�JWn
 �'BUT THIS IS ONE OF THE RARE EXCEPTIONS.�JXn
 �5THIS IS ABSOLUTELY THE WORST BOOK I HAVE EVER READ!!!�JYn
 �~THIS BOOK HAS NO PLOT AND THE RED PONY (THE TITLE OF THE NOVEL)DIES IN THE FIRST CHAPTER ( A VERY GORY DEATH) AND IS FORGOTEN.�JZn
 �THIS STORY WAS PAINFUL TO READ.�J[n
 �YWARNING: IF BOOK IS COMPLETED CONSULT A PSYCHIATRIST AT ONCE.I HAVE BEEN SCARED FOR LIFE.�J\n
 �lTHIS BOOK IS SO GORY, John Steinbeck GIVES TERRIFYING DESCRIPTIONS OF THE DEATH OF TWO HORSES IN THE STORY.O�J]n
 �LPLEASE FOR YOUR OWN SAKE SPARE YOUR SELF THE PAIN AND DO NOT READ THIS BOOK.�J^n
 �AI really didn't like this book and do not recommend it to others.�J_n
 ��It is far too short has a terrible misleading confusing plot no moral except don't read this book and it's writing style was too old fashioned I thought that this book would be about something�J`n
 ��but it had no main character that the plot can you call it that revolved around and no challenge or excitement at all unless you count learning about horses dying and doing nothing at a farm�Jan
 �6If I could I would give it negative one million!!!!!!!�Jbn
 �5I understand that this story is about life and death.�Jcn
 �cBut I had to read it for school the cussing and vivid metaphores are too much for kids 15 or under.�Jdn
 �CExample The smell of intestines mingled with the fresh morning air.�Jen
 �9He beat the buzzerds head untill it was a red mushy pulp.�Jfn
 �cAll the kids reading it with me said that it scarred them for life especialy ones that love horses.�Jgn
 ��As they discribe the sound of metal against bone as Bill Buck kills Nellie cuts her open takes out the foal dropes it at Jodys feet and says theres your foal just because he promised him he'd get it.�Jhn
 �OIf you have children or are not parchale to bad language do not read this book.�Jin
 ��This book is a crime against humanity and people that want to grow up in a safe nonviolent world were you don't have to be horrible people to keep a silly little promise.�Jjn
 �<I had an extremely difficult time trying to close the clasp.�Jkn
 �'Just too small, practically impossible.�Jln
 �CI refuse to spend 5 or more minutes trying to put this necklace on.�Jmn
 �)It is hard to go wrong with Ansel Adams!!�Jnn
 �}I gave this book as a present and the person that I gave it to was extremely impressed by the beautiful collection of photos.�Jon
 �<After Balkan Ghosts, expectations where high for Mr. Kaplan.�Jpn
 ��Unfortunately, he fell victim to the debunked theories of Malthus and depends on as best very questionable and at worse totally false "scientific facts" from sources such as Cheryl Simon Silver.�Jqn
 �UThis work cries out that the human race is too ignorant to learn, adapt, and survive.�Jrn
 �8History has proven otherwise and will continue to do so.�Jsn
 �DI hooked the unit up easily in my 2007 Accord in the late afternoon.�Jtn
 �<It worked seamlessly, read connecting and then it connected.�Jun
 �AI then left my car and didn't return to it till the next morning.�Jvn
 �ANeedless to say, the battery was dead and the car wouldn't start.�Jwn
 �6I disconnected the scangauge and got a boost from AAA.�Jxn
 �I returned the scangauge.�Jyn
 �_This was about a month ago and my car has been starting up religiously without a problem since.�Jzn
 �EI don't know if the scangauge is incompatible with my particular car.�J{n
 �(I do know that I hooked it up correctly.�J|n
 �{This item's description does not specify what vehicles it will and will not work with,it will not work with many vehicles .�J}n
 �CI happen to have 4 vehicles and it will only work with two of them.�J~n
 �7It does work fine with the ones that it will work with.�Jn
 �/seems to be a nice product haven't used it yet.�J�n
 �)will do so in near future.quick shipping.�J�n
 �=update 11-28-2012 great item used many time to find problems.�J�n
 ��works great used for a while to see how I was driving and to save gas (lead foot).Probably helped 10 people by resetting there ck engine light.�J�n
 �Love it.�J�n
 �Very helpful tool.�J�n
 �wI found the air intake temps very helpful in building and monitoring my new aftermarket intake/airfilter for autocross.�J�n
 � Would definitely purchase again.�J�n
 �EOther nice features include the ignition timing and open/closed loop.�J�n
 �Install was a breeze.�J�n
 �EIf you have a vehicle without a trip computer, this is a good option.�J�n
 �ZIt's not very fancy and not very intuitive, but it does give technical data and trip data.�J�n
 �+Calibration of the fuel economy is awkward.�J�n
 �wIt's easy to install and provides valuable info for operators interested in knowing details of your vehicles operation.�J�n
 �$works great with my aging GMC Envoy.�J�n
 �]Gauges are accurate and the fuel estimator is also great, since i hate going to gas stations.�J�n
 �lAfter helping a family member instal their gift for x-mas, I thought that this gauge looked a little cheap..�J�n
 �My thought was dead on.�J�n
 ��I've been trying to get this thing to operate correctly, has a million useless features and the only one that we want to use does not function properly..�J�n
 �Here's my take.�J�n
 �Don't waster your money!�J�n
 �7I've had broken calculators that work better than this.�J�n
 �[In fact, you would be better off counting rocks in your back yard to determine gas mileage.�J�n
 ��Check out the ultra gauge before buying this, I was enlightened to a much cheaper and better alternative only after sharing my horror story of the SG II.�J�n
 �GEasy to hook up and gives many more readings than standard OBII gauges.�J�n
 �gProvides a gas monitor as you drive and lets you see your bad driving habits and your vehicles economy.�J�n
 �Great little unit.�J�n
 �nGood product, works as it should (with some minor inconsistencies and issues) but so expensive for what it is.�J�n
 �yIt will save you gas and make you a more conscious driver but this is a hefty price tag for such a basic electronic item.�J�n
 �(paid one sixty in 2010)�J�n
 �dHad some issues with the first one over time and beyond the warranty period, they fixed it for free.�J�n
 �ECannot complain about that, but they also did so in a timely fashion.�J�n
 ��I have been able to increase the mileage on my '98 high mileage Ranger pickup from mid 17s all the time to mid 20s all the time now.�J�n
 �No small deal.�J�n
 ��Other people I know who got one of these or something similar all seem to get about 10% better by some changes in driving behavior.�J�n
 �-It's human nature to see how well you can do.�J�n
 �oBy watching some more engine parameters than just gas mileage, I am able to optimize shift points, speeds, etc.�J�n
 ��If you have a car that already has a capable trip computer, you probably won't need this, although this will give you far more information.�J�n
 �SThe added benefit of reading and RESETTING check engine codes is a really big deal.�J�n
 �-I used several times, saving plenty of money.�J�n
 �fI agree the manual is ummmm, not so good, but like another reviewer said, you can download the manual.�J�n
 �MThere are lots of good reviews here to give you a decent feel for the device.�J�n
 �All I add is, "GO FOR IT!"�J�n
 �I liked the idea of having a code scanner AND a realtime diagnostics tool that looked good mounted in the interior of my truck.�J�n
 �hHowever, it took me a few times filling up the gas tank before mine would list 'realistic' HP and MPG's.�J�n
 �\It started out telling me I had around 400hp and was only getting 8-10mpg on my 2001 Tacoma.�J�n
 �\After the extra fillups, it's starting to read 20-24mpg and a realistic hp of about 150-185.�J�n
 �The code scanner is great.�J�n
 �XI like being able to know in an instant that I get a code what it is, and then clear it.�J�n
 ��The instruction manual is a little vague, and detailed instructions on filling the tank get a little confusing, which meant a few extra minutes sitting in the gas station parking lot to get it set right.�J�n
 �sI have allready recommended it to others as a good digital diagnostics tool that looks good mounted in the vehicle.�J�n
 � I'm glad I found this on Amazon.�J�n
 �lI had been thinking about trading vehicles because my 06 Honda Van did not have gas mileage instrumentation.�J�n
 � This thing is so easy to set up.�J�n
 �LIf you can plug in a lamp, once you find the OBD plug, you can install this.�J�n
 �,It's not glamorous but extremely functional.�J�n
 �:I added some of the X-guages and could be no more pleased.�J�n
 �Charlie Herring�J�n
 �5Well this thing is as easy as tying your shoes laces.�J�n
 �IPlug in two wires, let it sit there for a few seconds and BOOM, it works.�J�n
 �I still have to wait to fill up�J�n
 �Bso I can calibrate that portion but everything else is good to go.�J�n
 �XThis thing tells you everything you want to know about your vehicle components and more.�J�n
 �?The X-gauge is an awesome addition to an already great product.�J�n
 �sYou can go the their website and review how to program the X-guages that are compatible with your specific vehicle.�J�n
 �Don't wait like I did.�J�n
 �Order this NOW!!!�J�n
 �/As usual Amazon came through with the shipping.�J�n
 �`If you don't want take my word for it, just look at the 300+ people that purchased it before me.�J�n
 �JNow I know exactly what my actual and average gas mileage is for my truck.�J�n
 �ZThis thing is great, I get more info than I expected and it is very easy to use and setup.�J�n
 �{The scangauge II is easy to use, probably need some speed adjustment and mpg adjustment to get the most accuracy out of it.�J�n
 �2Like mine i need to +2mph to match my speedometer.�J�n
 �+Now my speedometer tally with my scangauge.�J�n
 ��The only cons for this equipment is it can only refresh every second so it not real time updating and not much info for xgauge coding except for horsepower.�J�n
 ��I purchased the scangaugeshortly after purchasing my car so I could follow mileage and other engine stats - I has worked as advertised.�J�n
 �\After calibrating the unit thru three tanks of gasolinr the unit gives acturate MPG figures.�J�n
 �*Other engine stats are very acturate also.�J�n
 �dI bought this one for my daughter's boy friend because he was always finding reasons to borrow mine.�J�n
 �NI think this is all I need to say about its usefulness and what I think of it.�J�n
 �?Scangauge II is a very versatile tool for monitoring a vehicle.�J�n
 �0I only have 2 complaints about the Scangauge II.�J�n
 �DThe adhesive on the velcro attachment material is very poor quality.�J�n
 �AI had to find other adhesive material to hold the gauge in place.�J�n
 �GAlso I feel that the display numbers and words should be more detailed.�J�n
 �:It is very hard to see the information in bright sunlight.�J�n
 �7My 2012 Jetta came without a coolant temperature gauge.�J�n
 �3I know now many high price cars no longer have them�J�n
 �?and I'm just about sold on the idea that they're not necessary.�J�n
 �DNevertheless I'm too old and been relying on one too long to change.�J�n
 �With this device I now have one�J�n
 �Dand it's WAY more accurate than the ones that are factory installed.�J�n
 �AIn addition I like seeing the voltage the battery is putting out.�J�n
 �RI just installed it today and the other gauges I have displayed are RPM and speed.�J�n
 �,Don't need the last two as the car has these�J�n
 �Ibut it's nice knowing that the ScanGauge agrees with the car instruments.�J�n
 �/I'll use the MPG stuff if I ever figure it out.�J�n
 �"The cable was really easy to hide.�J�n
 �-I placed the device atop the steering column.�J�n
 �It sits below the instruments.�J�n
 �^The light is not really bright enough to see well in the daytime especially with dark glasses.�J�n
 �BWay cheaper then my first idea which was to put in a coolant Temp.�J�n
 �gauge from under the hood.�J�n
 �7This little data logger was great while it was working.�J�n
 �=Everything you read from all the other reviewers is relevant.�J�n
 �dHowever, in my unfortunate case, the device died just outside of the 1 year manufacturer's warranty.�J�n
 �+The screen lights up, but no data displays.�J�n
 �4I am not ready to stomach the price of another unit.�J�n
 ��best thing I buy see how many MPG I getting and read out code like the check engine light help me save money and time I would tell everybody for this�J�n
 �ZOnly partial information is displayed and just does not function with my 2001 Duramax LB7.�J�n
 ��I tried customer support and they were very responsive but just could not make it work and all of a sudden they stopped communicating with me.�J�n
 �RI have seen this work on a Ford truck and it looked great, that's why I bought it.�J�n
 �Too bad.�J�n
 ��I've used this product with some solid success, yet I've actually found that this gas crisis is so out of hand, I've resorted to drastic measures.�J�n
 ��Recently I converted my 04 Cadillac to utilize water as fuel from an easy to install kit, I obtained online from a company called [..]�J�n
 �This product is great.�J�n
 � I didn't have to tune it at all.�J�n
 �ZI was already driving pretty efficiently but this device made me tweak that a little more.�J�n
 �eI was more interested in seeing all the information that obd-2 supplies and that my car doesn't show.�J�n
 �6The trip numbers are also very interesting to monitor.�J�n
 �Great buy !�J�n
 �9Excellent design, easy to use, and no batteries required.�J�n
 �:They've thought of about everything you may want with this�J�n
 �and it's included.�J�n
 �QYou can even create your own gauges to customize the output to fit what you wish.�J�n
 ��Scangauge II is an excellent dashboard addition for drivers interested in fuel economy, performance or tracking down that intermittent "something" without paying major $ to the dealer repair shop.�J�n
 ��Out of the box, plug it in to the vehicle OBDII port, tell it a little about your vehicle (vehicle type, engine size, fuel type, fuel tank size) and begin to explore what goes on inside the computers that manage the engine of your vehicle.�J�n
 �The user manual is spartan.�J�n
 �)It presumes a somewhat motorhead mindset.�J o
 �7Online documents provide good background info and help.�Jo
 �Perfect for hybrid vehicles!�Jo
 �CAt first this gage worked great but more and more it quits working.�Jo
 ��It either fails to communicate with the vehicle and then times out and turns off, or it stops working and refuses to start again correctly when you try to start it while the vehicle is running.�Jo
 �mThere are troubleshooting sections discussing these very problems but the manufacturer solutions do not work.�Jo
 �zI am ready to return this item or contact the manufacturer to see if they have updated firmware gages that I can exchange.�Jo
 �;Features are great if the device would just work correctly.�Jo
 �[I've had this for almost 3 months, and it has helped me improve my gas mileage when towing.�Jo
 �DI've found out that not using the cruise control can add to the MPG.�J	o
 ��Throttle postion plays a big part in MPG-I can get higher MPG traveling at a higher speed sometimes if I stay light on the gas pedal.�J
o
 �XI also like the temp functions (water temp) as I can monitor how it is doing under load.�Jo
 �8Still playing with it, checking out different functions.�Jo
 ��You will have to experiment with it as many functions on the X-gauge are not supported by my truck (06 F-150), but you won't know till you do all of the programing and see if anything happens.�Jo
 �'Easy hook-up, took less than 5 minutes.�Jo
 ��It has helped me increase my MPG at least 1-2 MPG by learning new techniques and I average 10-12MPG towing my 5,000lb travel trailer, so I'm a happy camper�Jo
 �oI recently bought a ScangaugeII, but probably would have bought an Ultragauge if I'd done more recent research.�Jo
 �@Last I'd looked the Ultragauge was selling for $200...now ~ $60.�Jo
 �EFor most people the Ultragauge would do most of what's wanted/needed.�Jo
 X  The only advantages for the SGII for me is that it can be placed above the steering column right below the rest of the instruments...making it easier to see and use (you DON'T need extra distractions?)...and it's probably best not to have it too visible for security reasons.�Jo
 �GPlus the SGII has been around a while and might be more reliable?Either�Jo
 �the SGII...�Jo
 ��SG-e...or the UG allow you to monitor your real time and avg mpg...giving you feedback that helps you improve your driving habits and thus your mpg.�Jo
 �uNothing will help your mpg results more than seeing what effect your actual driving has on your evolving mpg average.�Jo
 �nPlus you can monitor other data and read/reset codes....very helpful if you do your own diagnosis and repairs.�Jo
 �simple, logic and easy to use.�Jo
 �~a useful tool to read data from the computer on your car, read codes and diagnose by yourself with a little search on the web.�Jo
 ��you can take note on the different indicators to know the "normal parameters" of your car, and notice when any change occurs and an error code lights your CEL, that way it will be easier to tell what went wrong.�Jo
 �uThe only con is that there isnt any cable or software to connect to a PC or laptop- that would make it a better tool.�Jo
 �,Easy to use!For the Geek, easy to customize.�Jo
 �/So you bought the car without the fancy gauges.�Jo
 �7Can't tell the temperature outside or your gas mileage.�Jo
 ��(or a zillion other things)Solution: try the ScanGauge!PS: driving an older car, it is great to push a few buttons while driving and have a check engine code cleared!�J o
 ��I bought this device about 2 months ago and I am still trying to understand how to use simple functions like total trip mileage and the total amount of fuel used on a trip.... pretty basic stuff.�J!o
 � My only complaint is the manual.�J"o
 �EThe instructions leave out details that would be helpful to the user.�J#o
 ��I have complained to the manufacturer about this and they agreed that it needs to be improved and promised that a better manual is on the way to me at some future date.�J$o
 �OI mainly use the device for gas mileage monitoring, both long term and instant.�J%o
 �NCalibration takes going through a full tank of gas, but is easy to accomplish.�J&o
 �BI was able to accurately set odometer (against gps) and gas usage.�J'o
 �;Out of the box mileage was within 3% and mpg was within 4%.�J(o
 �6After adjustment, both readings were easily within 1%.�J)o
 �VIt is interesting to see how bad mpg can be when starting out cold on winter mornings.�J*o
 �+I have it installed in a 2010 Kia Forte EX.�J+o
 �=This product does what it said it would but not consistently.�J,o
 �@You can read up on all the reviews about how features and stuff.�J-o
 �"I will instead focus on usability.�J.o
 �*I have this hooked up to a 03 towncar Sig.�J/o
 �2It will function ok for reading and resetting ECL.�J0o
 �mBut for everyday use (displaying the gauges, such as rpm, voltage, and other item etc.) the unit will freeze.�J1o
 �?Gets stuck in "connecting!" message when you try to re-connect.�J2o
 �|When it is connected and showing the gauges, display will turn itself off and you have to keep turning it back on to see it.�J3o
 �I guess its still good enough.�J4o
 �/But the extra $ it cost is not worth it for me.�J5o
 �_I'm ebaying it since return period is over :-(.Wish you all good luck if you decided to get it.�J6o
 �~I don't have board computer in my Nissan Almera 2000 model, and my primary goal about this tool is to monitor fuel consuption.�J7o
 ��I can watch at the same time 4 sensors or trip calculations, like how much fuel I have in tank, what is current fuel usage, temperature of cooling liquid, approx. fuel usage since tank refill, ...�J8o
 � It has diagnostic features also.�J9o
 �HIt's size is not so big, it can be attached on many places on dashboard.�J:o
 �Very good and usefull gadget�J;o
 �Nice and usefull unit.�J<o
 �JIt tracks my MPG and gives me my coputer codes I need to isolate problems.�J=o
 �VThe Manual coul use a little more work, it is hard to understand the higher functions.�J>o
 �I've purchased two of these.�J?o
 ��While they have a geek factor when you first get them, you soon learn they are not suitable for a warm car nor a cold one - the displays freeze up requiring them to be unplugged and replugged.�J@o
 �Gets annoying after a few days.�JAo
 X�  Also, the company does not respond to email about this or any other problems so if you wish to throw $160 out the window, by all means do so, but recognize you will get a quirky gadget that is almost never accurate (requires constant adjustment), whose display and internals are not well suited for the automobile environment and where there is no customer support if something goes wrong.�JBo
 ��I bought this item to display, coolant temp, throttle position, spark advance and manifold pressure for my 2005 subaru Impreza RS.�JCo
 �ZThe unit did not connect to the car so I tried it on my 2004 volvo and it worked, sort of.�JDo
 �PThe readings were slow to respond, I was looking for something a little quicker.�JEo
 ��PRO- Alot of data can be transmitted to the unitThe Scan gauge 2 is very easy to useIt has a great screen and very easy to readCon-�JFo
 �~It does not compute some readings fast enough as compared to a conventional gaugeIt did not work with my carFor a great price,�JGo
 �great functions�JHo
 �it's worth all the money.�JIo
 �But�JJo
 �8if you want accurate readings check out Autometer gauges�JKo
 �OThis item is very complicated to use and we found it was not worth the trouble.�JLo
 �zWe were able to get our dealer to change the settings on the items we wanted with their own computer so returned this one.�JMo
 ��I picked up a ScanGuage for my new car pretty shortly after we purchased it to get a readout of my MPG in real time (the car only has a per tank MPG readout).�JNo
 ��Not only did it provide me with the readout I wanted, but useful other information regarding my remaining fuel and how far I can still go on my tank.�JOo
 ��Pros:• Provided the readouts I wanted to improve my data• Using the data I collected I've improved my fuel usage to get more for my gas moneyCons:•�JPo
 �NThe gauges I wanted didn't come preprogrammed so I had to program them myself.�JQo
 �dIt wasn't difficult but it did take some reading through of the manual and a little experimentation.�JRo
 �9Took a little work to get working exactly how I wanted it�JSo
 �$but once I did it's been very handy.�JTo
 �KI enjoyed it so much that I purchased a second one when I got a second car.�JUo
 X  i had been researching different brands of products similar to this but this "scan Gauge 2" seemed like the best buy.it worked great for the first 2 week it DID NOT TELL ME MY FUEL LEVEL like i had wanted only miles per trip meaning it restarts after you turn off your car.�JVo
 �Bmaybe i miss read somewhere thinking it would tell me differently.�JWo
 X(  i read both good and bad reviews but the only one that i did not take to heart was the guy saying "it turned on in the middle of the night and drained my battery" this product is supposed to shut off when the car reaches 0 RPMS and most the time i believe it does shut off when it is supposed too�JXo
 �7but the few times it wont is not worth the risk to me..�JYo
 �gi will continue to use it for long straight drives so i can have my miles per trip plus heat and speed.�JZo
 �:It's a good product but i find it difficult to re-program.�J[o
 �cI cannot just change one information, I have to start to zero when I want to change an information.�J\o
 �Sie: If I fill the tank with less than full capacity the ScanGuage reads it as full.�J]o
 �_I have to re-program from zero if I want an accurate reading, I cannot add x number of gallons.�J^o
 Xh  works well with my Honda Jazz,easy hook updoes everything the maker claimsthe reason it gets a 4 star,is the web site shouldcontain more info on commands and have more programmablegauges.it is difficult to come by this info on the webbut still think the product is great,for the tech geekswho wants to know all those useless data that comes fromtheir ECU HA HA�J_o
 �2Took About 10 Days to get it, but it was worth It!�J`o
 �9If You're wanting to save a little Money On this Product.�Jao
 �fWhen You're Checking Out, just sign up for Amazon's Credit card and Get and Extra $30.00 buck's Off...�Jbo
 �PLUS�Jco
 �ZYou'll get super Saver's Shipping---(FREE)------ Can beat that Deal Anywhere!!!!!!!!!!!!!!�Jdo
 �WI purchased this product as a gift for my husband because he enjoys automotive gadgets.�Jeo
 �qAfter pricing some other similar scan tools, it appeared that the Scan Gauge II gave you the most for your money.�Jfo
 �USo far my husband is very happy with it and finds its multiple functions quite handy.�Jgo
 �UI bought to use in a 2010 Dodge Grand Caravan C\V mainly for the tachometer function.�Jho
 �=I also use the instant mpg and coolant temperature functions.�Jio
 �6It works better than expected and is simple to set up.�Jjo
 �nI wanted one of these for a long time, and when I got my economic stimulus check I couldn't resist any longer.�Jko
 �This is a great tool.�Jlo
 �nI was worried that it wasn't going to come with the latest software flash from L-L (manufacturer), but it did.�Jmo
 �%Great device and simple installation.�Jno
 �]I modified the trim piece on my dash so I could do a semi-permanent mount and it looks great.�Joo
 �PThis is helping me to save fuel by showing me my MPG's drop when I floor my car.�Jpo
 �HPlus it keeps track of just about anything your car's computer puts out.�Jqo
 �Very highly customizable.�Jro
 �5I recommend this to anyone that has a car with OBDII!�Jso
 �hAfter receiving my Scangauge, I installed it and was initially pleased with the information it provided.�Jto
 ��However, within days it began showing corrupted characters on the display and wouldn't store past data such as average fuel economy.�Juo
 �MData started over each time I started my vehicle, if it could be read at all.�Jvo
 �7None of the troubleshooting steps resolved the problem.�Jwo
 �HPerhaps I received one of the "bad" batch described by another reviewer.�Jxo
 ��The first scan gauge I received was defective, I contacted scan gauge by Email and they sent me a new unit by US mail, and it included a prepaid return shipping label for my broken scan gauge.�Jyo
 �ZI am very happy with the new scan gauge II, I am using it to monitor my transmission temp.�Jzo
 �5and other gauges while towing with my 2005 Ford F150.�J{o
 X  This is a great alternative to the cork bed for railroads layouts, it is more flexible and easier to use, the only problem was the packaging, even it came on a sturdy package and well packed some of the bedding came bend, other than that it was a good price.�J|o
 �#Give the seller a five star rating.�J}o
 �-The product is exactly how they described it.�J~o
 �;I received it item on time, and in the brand new condition.�Jo
 �,As for the product, I can't give it 5 stars.�J�o
 �bWhen I don't use the holder, meaning I hold the EZ-Pass up to the windshield, the Pass works fine.�J�o
 �AWhen I put it into the holder, I have a 50/50 shot of it working.�J�o
 �?So I've decided to use the adhesive strips and be done with it.�J�o
 ��This 'book' was quite boring, But it was easy to get through so meh?It can be read with any commentary you could want, perfect school assignment.�J�o
 �0Niel Simon is by the greatest pla write ever!!!!�J�o
 �YIt is full of laughter but still has a plot with a meaning full value to tell in the end.�J�o
 �8I myself played Sophia and enjoyed ever moment of it!!!!�J�o
 �#Be prepeared to laugh ur self silly�J�o
 �?When I first was told of "Fools," I didn't know what to expect.�J�o
 �7I was skeptical of the plot, but was happily surprised.�J�o
 �=The play's witty humor makes me enjoy Simon's work even more.�J�o
 �AIf you are sarcastic or just like to laugh, this play is for you.�J�o
 �8I am amazed by the amount of information in this volume.�J�o
 �-I am excited each time I read from this book.�J�o
 ��There are so many different parts of our habits, traditions, religions and customs that have been researched and compiled into this book that it is indespensible as a resource.�J�o
 �8This review needn't be long, for the title tells it all.�J�o
 �_Whether you want to know the origin of blue jeans, dental floss or a comb, you'll find it here.�J�o
 ��Because for some reason or another, I am on an unfulfillable quest to always know more and more, this is the type of book I love, and one I'd recommend more than any of the others I've read.�J�o
 �bThe book is broken up very nicely into setions so the thoughts are coherent and the content flows.�J�o
 ��This is actually a book you can read before bed or on the train - rather than others written in similar style, but that are better for reference.��      J�o
 �Enjoy.�J�o
 ��Informative, amusing, and easy to read, this book turns you into one of those people who can recite interesting random facts in almost any conversation.�J�o
 �I love this book.�J�o
 �eThis is the third time I've bought it because I keep lending it to people who lend it to people, etc.�J�o
 �3Everybody likes to find out where things came from.�J�o
 �Very interesting and great fun.�J�o
 �<I love this book and now have to find the rest of his books.�J�o
 �kThis book alone explains tidbits of things like the origin of the comb, or the napkin, holidays... so much.�J�o
 �People @�J�o
 �Mmy work couldn't wait to read it since I'd give out a history here and there.�J�o
 �7This book contains much that is fascinating and useful.�J�o
 �YUnfortunately some misinformation has slipped past the editors (of my edition, at least).�J�o
 ��For example, it repeats the false rumour that the rhyme "Ring Around the Rosie" is about the Bubonic plague; actually the song originated in 19th century America and describes a ring dance.�J�o
 �vThere are enough of these errors to cast doubt on the other "origins" in this book, entertaining though they might be.�J�o
 �%I don't trust very much trivia books.�J�o
 �;There always seems to be another version for the same fact.�J�o
 ��Even one of the reviewers of this book take the time to explain HIS version about one of the facts of the book and I'm pretty sure some specialists could do the same for other facts.�J�o
 �}BUT the plain fact is THIS IS A VERY FUNNY READING, full of details and stories about the time of the fact under examination.�J�o
 �BSo, even if the trivia itself may be wrong, still you learn a lot.�J�o
 �And ignorance is blissful.�J�o
 XJ  I don't know anybody whose profesional career depepends upon knowing the difference between Asyrian empire and Accadian empire, as long as they are treated with some respect (not like some movies, which flips around the pages of some book and take the first name they come along, even if the choice made is absolutely impossible).�J�o
 �UHistory, at last, is not a question of accuracy, but one of good-faith and enjoyment.�J�o
 �This book fills that purpose�J�o
 ��Clear and flat as promised, but it moves VERY easily and I'm hesitant to try to stick it down lest I damage the surface of my desk.�J�o
 �HKept it because I can anchor a corner with my monitor, minimizing shift.�J�o
 �,Otherwise I probably would have returned it.�J�o
 �Was just delivered today.�J�o
 �jIt's the right size, it's clear, and it seems like it's going to protect the surface of my desk just fine.�J�o
 �.Meets my expectations and the price was right.�J�o
 �@Looking for a good way to let a teacher know she is appreciated?�J�o
 �Look no further.�J�o
 X  It is not that big, but it is NICE.Tasteful and classy without being too expensive, but still don't show this off - it looks more expensive than it really is and you don't want to get the teacher in trouble or have people think you are trying to buy grades.�J�o
 �5Just a nice thing to do for someone that deserves it.�J�o
 �(This is Diana Krall's best work to date.�J�o
 �KShe's a good singer but a brilliant piano player in the Nat Cole tradition.�J�o
 �ZI'd love to see her in a dark, smoky (not in California) piano bar performing these tunes.�J�o
 �vFrom "Errand Girl for Rhythm" to "Frim Fram Sauce" and all the rest of these Cole classics, this album reeks of class.�J�o
 �3If you need one Diana Krall album, this is the one.�J�o
 �QHow is possible that a young blond lady could play so nicely this obviuos music ?�J�o
 �WIt's easy, for Her: true love for it and a perfect balance between heart and technique.�J�o
 �A trio (piano, guitar and bass)�J�o
 ��A perfect blend of rythm and music and, this is the fourth instrument, a full voice, with sensual flavour or vocalese speed, it's easy, for Her.�J�o
 �My favourite song ?�J�o
 �Hit that jive Jack, maybe.�J�o
 �AThis record is recommended by all my family (man, woman and cat).�J�o
 �Hit that song, Diana !�J�o
 �<A very high four-star for Diana,but just not her top effort.�J�o
 �aI prefer her ballads which,even more than the nifty swing numbers,are a fine tribute to Mr. Cole.�J�o
 �HShe does the best "Boulevard of Broken Dream" I've heard,slow and moody.�J�o
 �"You're�J�o
 �VLooking at Me" is also unbeatable,though truthfully I've never heard the Cole version.�J�o
 �"A�J�o
 ��Blossom Fell",and "If I Had You" are very nicely done here as ballads,the latter even better than the ballad version by Mr. Sinatra himself!�J�o
 �Make no mistake!�J�o
 �[Diana's building up a terrific career,and we can hope she'll keep at it for a long while...�J�o
 ��PS-If you have a chance,check up on yesterday's New York Times Arts section,with an article about Diana and others who are keeping the great song tradition alive!!�J�o
 �Wow!�J�o
 �$What a great voice, and piano style.�J�o
 �&I have been a jazz fan for many years.�J�o
 �I also play piano.�J�o
 �5I have Clavinova model 98, that is fantastic to play.�J�o
 �bDiana Krall is probably( in my humble opinion) one of the finest to come along in several decades.�J�o
 �*If you don't believe me, ask Dave Mckenna.�J�o
 �He is another winner.�J�o
 �&She will be with us for quite a spell.�J�o
 �<I first saw her at the Newport Jazz festival his year(onTV).�J�o
 �%I am definitely a fan of Diana Krall.�J�o
 �BWould love to see her come to the Jacksonville Jazz festival soon.�J�o
 ��All For You is one of five Diana Krall CD's in my collection, I must admit that I enjoy the other four equally as much as this one.�J�o
 �OMy two favorite songs on this disc are "I'm Thru With Love" and "If I Had You".�J�o
 �I find Diana's voice very soothing and comforting, even when she is singing an uptempo tune, not many artists can achieve that.�J�o
 ��This was exactly the style of artistry I was searching for and when I heard Diana for the first time I knew right then and there my search was over.�J�o
 �HI bought this as a download after being disappointed by "Glad Rag Doll".�J�o
 �HDiana is at her best with the standards and jazz that she knows so well.�J�o
 �3Her playing and vocals are excellent on this album.�J�o
 ��I have some of these tunes on other albums so it's a little like a "best of" album, but hey, you can't have too much Diana Krall!�J�o
 ��Not as unified an album as WHEN I LOOK INTO YOUR EYES, which I consider her masterpiece, Krall still manages some wonderful performances on this tribute to the Nat King Cole Trio.�J�o
 �OI highly recommend it for after dinner music and just over all lounging around.�J�o
 �gKrall's sultry delivery is one of a kind and she is perhaps the most gifted contemporary jazz vocalist.�J�o
 �Hlooks great and holds a lot of media but to put together is a real pain.�J�o
 �Valso some pre-drilled holes do not line up on the doors, tried adjusting but won't go.�J�o
 �bi have purchased many pressed board furniture in the past and i have nothing against pressed board�J�o
 �1but this is the lowest quality i have ever owned.�J�o
 �#I would not recommend this cabinet.�J�o
 �3It is difficult to put together and it falls apart.�J�o
 �?The shelves are flimsy and the doors are difficult to match up.�J�o
 �>I guess you get what you pay for, this was not that expensive.�J�o
 �;While I enjoyed this book, I think its title is misleading.�J�o
 ��The connections between the Egyptian Mysteries and anything resembling Freemasonry, at least that of the last several centuries, is very tenuous.�J�o
 �sFurthermore, it is not clear to me that the details of the Egyptian ceremonies described are more than speculation.�J�o
 �;If you would enjoy such speculation, as I did, that's fine.�J�o
 �Otherwise, beware.�J�o
 �This is an adorable stacker.�J�o
 �5Seams seem well sewn, though the fabric is very thin.�J�o
 �RMy wife wanted to use this as a no-footprint hamper, but it's way too lightweight.�J�o
 �OIt should hold our prefold diapers nicely, which is a far more appropriate use.�J�o
 �?The cardboard "floor" arrived broken/sliced and not cut square.�J�o
 �pAt first I thought it was just packing material, but the stacker cannot hold shape without a form for the floor.�J�o
 �BI'm sure we can cut a piece to fit, but this was a dissapointment.�J�o
 �;The stacker comes with a plastic clotheshanger for hanging.�J�o
 �hThis would be great if the hook swivelled, allowing one to actually hang the stacker in a useful manner.�J�o
 �@In every cd store I go in, I can't find any three sounds albums.�J�o
 �mif i really wanted to hear them, i could order a disk or two on line, but i just want to hear how they sound.�J�o
 �5are they straight ahead, or are they a chamber group?�J�o
 �Forget symphony.�J�o
 �Forget jazz trio.�J�o
 �Forget Burt Bacharach.�J�o
 �EPut the CD on and wallow in the luxury of this lush, beautiful MUSIC.�J p
 �4Try to resist if you can, but why would you want to?�Jp
 �>I've had this CD on and people have said it sounds like muzak.�Jp
 �WIt's a shame people can't hear romantic music without believing that it can be GENUINE.�Jp
 �tBacharach and Tyner we all know, but I never heard of this guy John Clayton who did the arrangements: he's a genius.�Jp
 �EI am a huge fan of McCoy Tyner and adore almost everything he's done.�Jp
 �)This disc, however, was a disappointment.�Jp
 �2Too much instrumentation has led to a sappy sound.�Jp
 �This is a MUST READ NOVEL!�Jp
 �GWHY HER BOOKS HAVE BEEN OUT OF PRINT SO LONG IS BEYOND HUMAN KNOWLEDGE.�J	p
 �=THEY ARE SINCERELY UNPUTDOWNABLE, ADDICTIVE, AND MAGNIFICANT.�J
p
 �RSHE IS WITH MAURICE DRUON A MUST FOR ANYONE WANTING TO UNDERSTAND THE MIDDLE AGES.�Jp
 �1This novel is exactly what you think it might be.�Jp
 �Nothing more, nothing less.�Jp
 ��I decided to pick up Madame Bovary at a library sale after having read Nabokov (in Speak, Memory) gush about Flaubert's linguistic mastery.�Jp
 ��Unfortunately, I don't speak French and, of course, quickly found that linguistic mastery does not necessarily transcend language (sometimes, though - Eco, for instance).It might be an important work of literature - historically.�Jp
 �oAnd Flaubert seemed to do an excellent job of inventively portraying the many revolting traits of his denizens.�Jp
 �?Regardless, or possibly because of this, the book left me cold.�Jp
 �JAfter I finished, I wasn't exactly furious with myself for having read it.�Jp
 � But I was a little disappointed.�Jp
 �2And relieved at having only paid a quarter for it.�Jp
 �4When I read this book I felt as if time had stopped.�Jp
 �It was so, so boring.�Jp
 �Emma Bovary was a psycho.�Jp
 �I hate this book so much.�Jp
 �4I never been to hell ,and I don't planon ever going.�Jp
 �GHowever, I think I know what it must feel like after reading that book.�Jp
 �Oh the horrors!!!!�Jp
 �The memories are coming back.�Jp
 �9I must stop now before I experience that hell once again.�Jp
 �Please stop the madness.�Jp
 �No more Madame Bovary!!!�Jp
 �uFlaubert objectively portrays his characters as fallible human beings in an environment which is both light and dark.�J p
 ��As a Realist, Flaubert's aim might have been to show human weaknesses and lack of communication through a realistic environment.�J!p
 �%He succeeds if this was his endeavor.�J"p
 ��What makes this book fascinating to read (or to listen to) is that all of the bothersome traits of the characters usually reflect those of the reader as well!�J#p
 �NFlaubert is excellent in painting images so that one can almost feel as if he/�J$p
 �she is in the moment.�J%p
 �"This book is very broodish though.�J&p
 �rIn fact, it is slightly over done in expressing melancholy and the main character, Emma, is somewhat hard to like.�J'p
 �%At times, you may want to skip ahead.�J(p
 �KOne can only stand so much darkness, in his own life, as well as in a book!�J)p
 �!MADAME BOVARY is one of it's own.�J*p
 �>No other writer has been able to recreate or mimic this woman.�J+p
 ��Chopin has tried it, and so had Hardy, but the power and weakness of Madame Bovary contrasted with each other somehow, creating a delightful and mystic woman for all centuries to read and enjoy.�J,p
 ��Madame Bovary is perhaps the finest French novel of the 19th century, and that is really saying something; consider that this was the century that produced Balzac, Zola, Maupassant, Hugo, Dumas, and Stendhal.�J-p
 ��Madame Bovary is one of the greatest anti-heroes in all of Western literature, as she leads the reader through a tragedy that explores the extremes of ambivalence.�J.p
 �Masterful and compelling.�J/p
 �An absolute must-read.�J0p
 �ZHOWEVER, this particular edition (published by General Books LLC) is absolutely atrocious.�J1p
 �{I have never seen a book so rife with typographical errors -- it's like reading a Kindle transcription gone horribly wrong.�J2p
 �|Several times, Charles is referred to as "Charlea", and many of the chapters are divided improperly and begin nonsensically.�J3p
 �^Spend a little more $$$ and get the Penguin edition, or one that is translated by Lydia Davis.�J4p
 ��This translation was too often seemingly a transliteration which resulted in many virtually incomprehensible sentences, grammatically incorrect and incomplete sentences.�J5p
 ��It read as if completed by a translator who was actually not fluent in spoken English and who was using an out of date French-English dictionary.�J6p
 ��I was left with the impression sometimes that perhaps this was a voice to text publication and that the computer had misheard the voice and produced a garbeled result.�J7p
 �>This is not the only book which has left me with this feeling.�J8p
 ��It would help to always be able to preview such things as publishing dates, the name and information about the translator, whether the book is an abridged version and other information provided on the pages before the commencement of the book proper.�J9p
 X0  Because there were Few Detals about the Plot of this Book or even what kind of Book this is Someone needs to Point out just what this BOOK ISFirst of All THIS IS A 1981 CHILDREN'S BOOK about a little boy who thinks he is having Bad luck because it is Friday the 13thIT HAS NOTHING TO WITH THE FRIDAY 13TH�J:p
 ��HORROR MOVIES OF THE SAME NAME WHAT SO EVERI am Posting this so there will be No more Confuseion with the Novelization of the Horror Movie Friday the 13th by Simon Hawke.�J;p
 ��i played wheel of fortune on line before, but i wish that i could remember from what game site,etc. that i had used to play the game.�J<p
 �Kthis game doesn't have the sound effects that i thought that it might have.�J=p
 �>the sound effects seem to make the game more pleasure to play.�J>p
 �'Hello,I was very happy with this order.�J?p
 �"It arrived sooner than I expected.�J@p
 �1It also came neatly packaged and is working well.�JAp
 �AOverall, I am very pleased and would do business with them again.�JBp
 �0This game was purchased for my mother as a gift.�JCp
 �She absolutely loves it!�JDp
 �TIt was shipped promptly and arrived in time for the event the gift was intended for.�JEp
 �Great purchase experience.�JFp
 �-I was somewhat disappointed with the product.�JGp
 �eI was expecting Vanna White and Pat Sajak to be included in the video as with the earlier versions...�JHp
 �Don�JIp
 ��this game is good overall, but it takes a ridiculously long time to play because the wheel spins for what seems like an exaggeratedly long time...�JJp
 �Cyou do get to create your character and play 3, 5, or 7 city tours.�JKp
 ��also, you can play a puzzle of the day and earn trophies for your accomplishments...really fun if you love wheel of fortune and have the patience to wait for the wheel :)�JLp
 ��Doesn't have Pat or Vanna or the theme song (I think?) but still not a bad game, especially since you can have a few people play it at the same time.�JMp
 �YI thought this deluxe version would be a lot better than the older version, but it isn't.�JNp
 �OIt doesn't even have different, interesting backgrounds like the first version.�JOp
 �:It's just very basic and the wheel takes too long to spin.�JPp
 �9It also seems like the "bankrupt" comes up way too often.�JQp
 �TEnjoyed playing this the first game when the software created the other two players.�JRp
 �TCreating my own players was fun the first time, but now they're a little ridiculous.�JSp
 �7For some reason, they all seem to look amazingly alike!�JTp
 �NI'm really playing all three parts, so it's easy to cheat so the real me wins.�JUp
 �"The video is jerky and off rhythm.�JVp
 �TI would have preferred the two other players be "played" by the computer, not by me.�JWp
 �JNot a challenge except for finding the words, but as I said, I always win!�JXp
 �8I purchased this at my local Wal-Mart for nearly $20.00.�JYp
 �|I had thought it would be ready to play within several minutes after loading the CD in the CD drive and installing the game.�JZp
 ��But after the icon was added to the desktop after download, and I would double click on the icon to start the game, I would get the famous audience chant, and the new version of the show's theme...with a black screen!!!�J[p
 ��I tried everything to get the game running properly: defragmenting the computer, turning off the pop-up blockers, etc., but a black screen remained.�J\p
 �:My mother and I could not figure out what the problem was.�J]p
 �What I got was a dud.�J^p
 �-I had followed the installation instructions.�J_p
 �`Could anyone please tell me what I had forgotten to do, and how to eliminate the black screen???�J`p
 ��I will just have to resort to getting a back version of the game for my computer or download free trials from the Wheel of Fortune's web�Jap
 �
page.-Matt�Jbp
 �RNot a high-tech game, a simple computer game alternative to solitaire and mahjong.�Jcp
 �aLove the simple yet colorful graphics, sometimes the little ones will just sit quietly and watch.�Jdp
 �dAnd my avatar is so accurate my grandson (almost 4) recognized me the first time we played together.�Jep
 �"The puzzle of the day is cool too!�Jfp
 �,I bought this product as a gift for someone.�Jgp
 �QThey said they really enjoyed it, and that it worked well with Vista on their PC.�Jhp
 �9I recently purchased this version and absolutely hate it.�Jip
 �{I owned a previous version containing video of Vanna White and allowing me to play using different locations as a backdrop.�Jjp
 �0The graphics were much better than this version.�Jkp
 �VI wish the other version worked on my new computer, as I doubt I'll ever use this one.�Jlp
 �I've had nothing but problems.�Jmp
 �XHalf the time when I hit a letter it doesn't work and when I hit solve, it doesn't work.�u(Jnp
 �>I noticed that other reviewers have had the the same problems.�Jop
 �II'm just kicking myself for not reading them before I purchased the game.�Jpp
 �)This is a great version of the real game.�Jqp
 �It's kept me busy for hours!�Jrp
 �*It's also working well with Windows Vista.�Jsp
 �Pokemon Play It! is a pc demo version of the Pokemon trading card came, circa 1998 when the card game exploded into popularity.�Jtp
 �}It's a short demo that shows you the basics of playing the game, and lets you do a few practice matches against the computer.�Jup
 �CIf this had been expanded to a full game, it would have been great.�Jvp
 �hPros:*Cheaper than any real cards.*Nice graphics.*Good tutorial for beginning playersCons:* Just a demo.�Jwp
 �* Can only play the computer.�Jxp
 �* No real challenge.�Jyp
 �K* Shows the game as it was back when Wizards of the Coast started the game.�Jzp
 �EThe basics of the game are still the same, but much else has changed.�J{p
 �I lost my father to cancer.�J|p
 �YAfter reading what really happens at that moment, I was joyful beyond belief for everyone�J}p
 �sHow can you copyright words from God, have all rights reserved for it, and require permission to show it elsewhere?�J~p
 �hA thinly veiled attempt by Satan to sound like God, misquoting Scripture and twisting everything around.�Jp
 �Typical of Satan.�J�p
 �0OK, so God actually writes messages to this guy.�J�p
 �7How come he doesn't give us that chance to talk to him?�J�p
 ��What in the world actually proves that the words are coming from God?Stupid, unbased, proofless book made by a guy who wants to suck money from the weak and stupid.�J�p
 �Seriously.....�J�p
 �git's pretty obvious that this guy found a money machine by writing stuff that people just want to hear.�J�p
 �UI commend you, Neale Walsch, on explioting money out of people who dont deserve it!!!�J�p
 �{To say I expected much from this book would be a lie; I cannot expect much thought in a book so inherently devout in topic.�J�p
 �hI did, however, expect more than some self-riotous sack conversing with god and assuring me of a heaven.�J�p
 �We don't need these books.�J�p
 �LAll they succeeds in doing is agreeing with the bible in every way possible.�J�p
 �)Way to use faith to blind your consumers.�J�p
 �NNeale Walsch is a manipulative hack, go out of your way to avoid this garbage.�J�p
 �?I have read all of Neale Donald Wasch's Conversations with God.�J�p
 �YI enjoyed each of them and found them especially helpful regarding death, as a new widow.�J�p
 ��It gives you hope to know that there is more to us and more to life than just what we experience in the here and now of a sometimes ordinary, mundane life.�J�p
 ��I've always thought that life is about lessons and "getting it," growing as spiritual beings, and Conversations with God reaffirms those beliefs.�J�p
 �GNeale has written many great books, but this one has to be my favorite.�J�p
 �FI enjoyed it so much right after finishing it I read it a second time.�J�p
 �DI feel his descriptions of the other side of life are very accurate.�J�p
 �UThe reasons are 1) I do have some out of body experiences myself, limited as they be.�J�p
 �2)�J�p
 ��I have read the accounts of many great masters who are quite adept with out of body experiences and this book echos what they have reported, but in its own unique way helps to clarify the experiences many others have reported.�J�p
 �bBravo, great job Neale!Michael SkowronskiAuthor ofUnforgettable: A Love and Spiritual Growth Story�J�p
 �RI have read almost all of the CWG books and they have always seemed true and real.�J�p
 �{It was a relief to read about the everlasting nature of our spirit and that our death is a transition and not to be feared.�J�p
 �*I believe and hightly recommend this book.�J�p
 � "I believe those are holy words.�J�p
 �&I believe they came directly from God.�J�p
 �IThose words have been floating around in my mind for the past four years.�J�p
 �'I see now that they were my invitation.�J�p
 �1An invitation from God for a larger conversation.�J�p
 �."Seriously people, how dumb do you have to be?�J�p
 �;If this guy thinks he's talking to god he needs locking up.�J�p
 �Simple.�J�p
 �5When I read the above quote all I could hear was "...�J�p
 �xI see now that there are many gullible people in the world and that was my invitation to rip them off with this rubbish.�J�p
 ��"Here's a little insight for ya - We all die and what a sad waste it would be if we all spent even 1 day of our lives reading idiotic books such as this.�J�p
 �What a great book.�J�p
 �xI believe that some people are lead to this book to experience the freedom that alot of us are looking for in this life.�J�p
 �^It has given me exactly that, freedom, and a sense of knowing that really, all is truely well.�J�p
 �The book takes my breath away.�J�p
 ��It's profoundly stunning and reaches beyond "The tibetan book of the dead" in its depth, scope, implication and application for Life.�J�p
 ��Listening in on the conversation between the author, Neale and God provides a clear, easy to understand discussion which reshapes the very foundation of conventional reality for the reader.�J�p
 �TThese discussions are well structured by building a tree of connected rememberances.�J�p
 �DThis book has the power to change the world as we currently know it!�J�p
 ��There may be other books on the afterlife - and the current life - as awesome as this one, but if there are, I've never heard of them.�J�p
 �)Best book in the series since Friendship.�J�p
 �Just feels right.�J�p
 �;Totally mindblowing in parts, yet weirdly never irrational.�J�p
 �Walsch's God makes sense.�J�p
 ��Neale Walsh has written several New Age style books on his conversations with God, all of them bestsellers and great in popularity.�J�p
 �YHowever, I always get the feeling on reading his works his God is a very wishy-washy one.�J�p
 ��The conversations seem to reveal a God who is basically what Walsch would like God to be; God is sort of like a friendly grandpa, all we have to do is take some time off from our day and talk to him.�J�p
 �|While I would certainly agree God's love is important, his version of God is very hard to believe, as are his 'revelations.'�J�p
 �?How can we know what Walsch claims to experience really is God?�J�p
 �hGod seems very vague and changeable in Walsch's book, and strangely conducive to our wishes and shallow.�J�p
 ��While Walsch may stumble on truths, like much New Age material it is very vague, to the point where virtually anything can be said about the realm of spirit, even if they are mutually contradictory.�J�p
 �4Such a faith is a bit too soft-headed for my liking.�J�p
 �DIt is practically impossible to read this book with a straight face.�J�p
 ��The images and the dialogue are so sophmoric that one begins to wonder how the author could write this "dialogue" without blushing.�J�p
 �jTo wrap genuine esoteric thought and principles up in such drivle is not worthy of the topic he addresses.�J�p
 X  Like most beginner's texts, it will prove to be either a stepping stone as an introduction to real life and death issues, or it will serve as a stumbling block to those who think that in reading this book they have found the "Truth" and will stop searching for real enlightment.�J�p
 �7This is a fantasy trip into the real reality of spirit.�J�p
 �VAs such, it provides answers to the questions man has posed throughout all of history.�J�p
 �XGod's responses to those questions are easy to understand, practical, and inspirational.�J�p
 ��They not only make sense, they give hope and understanding for all of life's challenges and without judgment, condemnation, or threats.�J�p
 �WWhat's truly amazing is the feeling of love that is promoted throughout all chapters...�J�p
 �Isn't that Godlike?�J�p
 �{Hi, I bought a book almost 2 months ago , and haven't receive it yet , I checked in the your web page and no news about it!�J�p
 �please help !!!�J�p
 �Antonio Amaya�J�p
 �NIn home with God Neale takes head on the question of what happens after death.�J�p
 ��Certainly, the book makes a significant contribution in helping us to overcome the fear of death regarding it as no more than a transition similar to the one we undergo everyday when we go off to sleep and wake up again in a conscious state.�J�p
 �jHowever, some may not be comfortable with the conversational methodology he has adopted to make his point.�J�p
 �How firm a foundation?�J�p
 �8The answer in regard to this pancake makeup is too firm.�J�p
 �rIt reminds me of the makeup, although I can't remember the brand, that we used in our local little theaater group.�J�p
 �&But, these people were going on stage.�J�p
 �XHowever, if it's really thorough coverage that you want this may be the product for you.�J�p
 �IJust be sure you don't leave any "embarkation lines" around chin or ears.�J�p
 �DI must have hit wrong button, but ordered the wrong makeup for wife.�J�p
 �QE-mailed the company and sent it back, they replace it immediately, no questions.�J�p
 �Thanks, John�J�p
 �NThe quality of these plates is great--They're nice and heavy and seem durable.�J�p
 �=However compared to the picture, they're quite dull in color.�J�p
 �TI was expecting them to stand out and be very vibrant, but they're all a bit ho hum.�J�p
 ��They're not really orange or red but more a burnt umber and they're all roughly the same shade unlike the picture--making them, well, boring.�J�p
 �OI paid $13 for these plates and am glad b/c for this price I won't return them.�J�p
 �>If I had paid a penny more though, they would have to go back.�J�p
 �HI just bought three sets of these plates and I'm really happy with them.�J�p
 �QThey aren't as high end as some other Spode dishes, but they are worth the price.�J�p
 �PThese are dishwasher and microwave safe, and a perfect size for serving dessert.�J�p
 �@The only caution is that they are more orange in color than red.�J�p
 ��As a long time fan of Bob Dylan, and as one who has dutifully bought every album that he has ever produced, I have to repectfully disagree with all the reviews which I have read.�J�p
 �QWith one exception, I find the songs on Love and Theft to be almost unlistenable.�J�p
 ��As I look back on the last couple of decades of Dylan albums, I find an interesting trend: Each record contains one gem amidst a lot of filler.�J�p
 �Songs like Not Dark�J�p
 ��Yet, Every Grain of Sand, Born in time, Brownsville Girl, Series of Dreams, and the recent Things Have Changed (which did not appear on an album) all sparkle like diamonds discovered in a rock pile.�J�p
 ��Perhaps Bob only has one great song in him per year, and maybe that's enough, maybe that's as much as any great poet has ever produced.�J�p
 �YThe gem on this album is Missisippi, and it is, by itself, worth the price of the record.�J�p
 �Bob Dylan was a watershed.�J�p
 ��Before him, the great songwriters like Cole Porter and the Gershwins actually wrote things that made sense, and this continued even into the early sixties.�J�p
 ��But when Dylan proved to all the rock and rollers that you could write meaningless drivel and "sing" it in an ugly croak and impress rock critics (who are not geniuses) without even trying, pop music deteriorated beyond repair.�J�p
 �tSoon we had punk, and now we have guys talking into a microphone while a drum machine plays ("rap"--it's not music).�J�p
 �Thanks, Bob.�J�p
 �:You're the guy most responsible for ruining popular music.�J�p
 �*I have been listening to Dylan since 1965.�J�p
 �"He has been a big part of my life.�J�p
 �I am not a casual listener.�J�p
 �I hate this record.�J�p
 �!It's worse than Time Out of Mind.�J�p
 �QBob should give it up while he still has some sense of respect for his listeners.�J�p
 �ZIf he was unknown, no one would pay a dollar to see him live or buy his recent recordings.�J�p
 �?He is a living legend and he should retire as soon as possible.�J�p
 �8What passes for singing is just a bit of a joke, really.�J�p
 �iBefore long you won't be able to tell him from Tom Waits...and I never thought things would get THAT bad.�J�p
 �`Bob, leave us with our memories of your greatness and stop parading your decline for all to see.�J�p
 �bOnly the most toadying and undiscrimibating of sycophants (or the deaf) could call this "singing".�J�p
 �SProbably my second fav Dylan to Blood on the Tracks, and his best work in a decade.�J�p
 �YDefintely a stronger, more eclectic and satisfying than the commendable Time out of Mind.�J�p
 �[Bob delves into rock, blues, country, r&b, swing, 1930s-40s standards, and everything else.�J�p
 �iA true folk album, Bob delivers his slice of americana, an amazing amalgam only a great artist can offer.�J�p
 �5Thanks for continueing to seek out new frontiers Bob!�J�p
 ��The biggest difference, for me, between this album and his last (Time), is that notwithstanding the fact that Dylan has been touring been with the band that plays on this CD for quite a while, the CD features Dylan the singer in song after song.�J�p
 �OThe CD covers a lot of territory, from blues to rockabilly, country to ragtime.�J�p
 �KYet the unifying feature is Dylan's voice, which at 60, is simply terrific.�J�p
 �LThe singing and words are the stars in this album, which suits me just fine.�J�p
 �%Here's the Truth: Bob Dylan is a man.�J q
 �)Bob Dylan (born Zimmerman) is a musician.�Jq
 �He plays music.�Jq
 �-You want to hear someone play from the heart?�Jq
 �Listen to this.�Jq
 �%And connect with him - i.e. hear him.�Jq
 �You want spiritual guidance?�Jq
 �Read your Bible fools.�Jq
 �]That's what this guy says sometimes, along with the many other realities of where we all are.�Jq
 �8The last track, Sugar Baby... good, from the heart good.�J	q
 �1"High Water" belongs with some of Dylan's best. "�J
q
 �Sugar Baby" is also wonderful.�Jq
 �[The rest of the CD is fun and sounds closer to Tom Waits than the Dylan of the mid-sixties.�Jq
 �XGreat to hear a legend back with the same edge that we've come to expect from his songs.�Jq
 �Bob Dylan is back.�Jq
 �&And after a five year wait; so are we.�Jq
 �fHis new album entitled "Love and Theft" has been recieving high acclaim from all around the continent.�Jq
 �8His astoundnig lyrics and vibrant melodies are stunning.�Jq
 �pThis is the perfect follow up to his previous album, "Time Out of Mind"- the darker, less laidback studio album.�Jq
 �nWhen we listen to the music on the new remastered SACD's, one can only realize that the time to listen is now.�Jq
 �4It's way more rewarding than one would ever imagine.�Jq
 �4Surround Sound is featured on this particular album.�Jq
 �;(Alike five others in the fifteen album re-release series).�Jq
 �_Pick it up for sure if you're into Bob, or if you think you may like to hear some of his stuff.�Jq
 �It's a great place to start.�Jq
 �So why not?...�Jq
 ��I purchased This before hearing samples or reading reveiws, then before it came I read some reviews, I got nervous, wondering what I had gotten myself into.�Jq
 �"Don't listen to those bad reviews.�Jq
 �9Sure he sounds different, but that does'nt mean it's bad.�Jq
 �]I also ordered Modern times, Dylan 3 discs deluxe, Highway 61 revisited and blonde on blonde.�Jq
 �I am so glad I did.�Jq
 �}You can never go wrong with Dylan he just went back to where he started, loving blusey,laid back music, He is a great Talent!�Jq
 �*I can't wait untill another one comes out.�J q
 �lI have been a fan for years but this is the first music I have bought and I don't know what took me so long.�J!q
 ��There is not a bad song on this album, but Po' Boy is my personal favorite; Bob should've been nominated for a grammy for best vocal for this song.�J"q
 �rThe bonus tracks (which have been available on various boots for years) also add to the greatness of this package.�J#q
 �`To answer another reviewer's question, Yes, I would buy this music no matter who was singing it.�J$q
 �,I happen to love Dylan's aching raspy voice.�J%q
 �(The raspier he gets, the more i love it.�J&q
 �$Beautiful, witty songs of course....�J'q
 �2Dylan is making better music now-a-days than ever.�J(q
 �EI had favorites before this, but this is great, absolutely wonderful.�J)q
 �Rock on, Bob!�J*q
 �Frock'n roll's poet laureate has put together another extraordinary cd!�J+q
 �*This cd is better than "Time Out of Mind".�J,q
 �VHis band is in top form and these songs are a collection of all types of music genres.�J-q
 ��You probably won;t hear any of these songs on the radio because the radio formats are too slow to realize that the sing songwriter is back!�J.q
 �And Dylan is back in a big way.�J/q
 �'Buy this cd, you won't be disappointed!�J0q
 �BI'm a fan of Dylan lyrically, but I have to get this off my chest.�J1q
 �The man CANNOT sing.�J2q
 �QWhew, please don't crucify me now, he writes the most wonderful pieces of poetry.�J3q
 �He should just recite it!�J4q
 �\My whole family saw him live last year, he had NO audience interaction and a terrible voice.�J5q
 �But he's a legend of lyrics.�J6q
 �LAlso, Dixie Chicks and Sheryl Crow both do fantastic versions of Mississippi�J7q
 �,And this guy is still putting out pure junk!�J8q
 �_This sounds even worse then the nonsense that was issued 30-40 years ago,not a easy feat to do.�J9q
 �Ear torture,pure and simple!�J:q
 �NThe voice is nearly gone, the wail is nonexistant, and he has no anguish left.�J;q
 �yThe only track I liked was on the freebie that came with, it was "The Times, They are A'changin'", one of Dylan's oldies.�J<q
 �From back when Dylan was Dylan.�J=q
 �CI agree with the reviewers who wrote that they can't stand this CD.�J>q
 �UI'm one of those who like some of Dylan's music, and who can't stand some of it, too.�J?q
 �6I borrowed it from a library, and am now returning it.�J@q
 �Glad I didn't buy it.�JAq
 ��i can't believe bob dylan is still trying to sing, well hey aerosmith, madonna, sting, and cher are still trying to cash in why not dylan.�JBq
 �aWith "Love and Theft," Bob Dylan continues his exploration of the blues and American roots music.�JCq
 �YThis disc includes some of Dylan's finest blues recordings (and that's saying something).�JDq
 �`"Lonesome Day Blues" and "High Water (for Charley Patton)" are low-down, gutbucket masterpieces.�JEq
 �4And "Mississippi" is achingly beautiful and wistful.�JFq
 �_There's some straight-ahead rock and roll, in "Tweedle Dee & Tweedle Dum" and "Honest With Me."�JGq
 �;And Bob even croons a little romantic ditty, in "Moonlight.�JHq
 �O" Just a lovely variety of styles here, played wonderfully by Bob and his band.�JIq
 �nBy all means, buy the two-disc limited edition, for the unreleased version of "The Times They Are A-Changin'."�JJq
 ��Dylan's elegiac reading of the song makes it more poignant and visionary than ever, especially in the wake of the tragedy of Sept. 11.�JKq
 �Bquite good - after so many bad records during the 1980s and 1990s.�JLq
 � I like to listen to Dylan again.�JMq
 �YLike so many others from the 1960s, he somehow lost his creativity (after "New Morning").�JNq
 �How to interpret the title?�JOq
 ��Seems quite obvious to me: he loved so much Woody Guthry - and has stolen so much from him that he should transfer the profit from this record to a "Woody Guthry Foundation for Unsuccessful Songwriters"!�JPq
 �5I think fans are getting a LITTLE carried away here!!�JQq
 �uI like this cd...very much...but "5 stars" should mean every track is at LEAST listenable every time you play the cd!�JRq
 �4I find myself passing a few tracks when I play it...�JSq
 ��I think it's a disservice to Dylan to say that this is as good as "Blood on the Tracks", or "Time Out of Mind"...both 5 stars in my book.�JTq
 �*Let's say it's 4 stars...a very good cd...�JUq
 �I'm glad I bought it...�JVq
 �but it's not perfect!!�JWq
 �#I'd give it 3 1/2 stars if I could.�JXq
 �:This album is good, but it's a far cry from a masterpiece.�JYq
 �IPerhaps most people are just happy to hear him still singing solid music.�JZq
 �nBut theres nothing new or innovative going on in the melodies, nothing major to be worked out in the lyrics...�J[q
 ��What we do have are enjoyable lyrics, tight musicianship and some fine songs - most notably "Mississippi", "Floater" & "Po' Boy".�J\q
 �BNot a masterpiece, but a nice piece of work from the mature Dylan.�J]q
 �3It is difficult to add to these other fine reviews.�J^q
 �zI hope Mr. Dylan comes from good stock and continues to capture and distill down into verse all that is good and American.�J_q
 �5I have been loving Dylan since I've got ears to hear.�J`q
 �$I bought my first LP, it was Desire.�Jaq
 �3I couldn't help but listen to it hundreds of times.�Jbq
 �RNow we have Love & Theft and I feel the same urge to listen to it again and again.�Jcq
 ��As everybody knows there are as many Dylans as listeners, or maybe more, there is a Dylan for each and every state of your mind.�Jdq
 �iBut this time Dylan has done something more, he has written a concise encyclopedia of the American music.�Jeq
 ��It's like a textbook on history plus a poetical and ironical synthesis of an artist, who knows where he can get, what he can master, what he can express.�Jfq
 �3And he does it his way, like no one else can do it.�Jgq
 �5As a Dylan fan from the start, this is his best ever.�Jhq
 �7There is little else to say about this excellent album.�Jiq
 ��While Dylan's early albums were little more than him, a guitar, a microphone, and a tape deck -- the recent Dylan offerings have actually been produced.�Jjq
 ��(Imagine if Phil Spector had gotten his wish and actually produced a Dylan album.)Even if you have been away from Dylan for a number of years or were turned off by the return to the folk/blues tradition, come back -- this album's worth the effort.�Jkq
 �Wonderful listening pleasure.�Jlq
 �pI agree with Mr. Chickeneater (see review)--Love & Theft was overhyped by the critics (as was Time Out of Mind).�Jmq
 �bIt sounds a lot like 1990's Under the Red Sky, which, a few cuts excepted, isn't very good either.�Jnq
 �\And I know it's lame to complain about Dylan's voice, but it's more grating than usual here.�Joq
 ��In an interview, the poet David Berman (also of the great band Silver Jews) talked about how he had only recently started listening to Dylan's catalog and was pretty disappointed, saying that "there should be someone better.�Jpq
 �#" I think Berman's right on target.�Jqq
 �}For all the acclaim Dylan's garnered over the years, even his best records are marred by clunkers or rewrites of older songs.�Jrq
 �uThe critical pendulum may have swung back in Dylan's favor, but that doesn't mean his records have gotten any better.�Jsq
 �RMississippi and Sugar Baby are great songs, up there with the best Dylan has done.�Jtq
 �KHowever, the rest of the album, while listenable, isn't all that memorable.�Juq
 ��Still, I suppose it's amazing that he can still produce a couple of great songs still, after over 40 years performing and recording.�Jvq
 �IThe same cannot be said for '60s peers like Paul McCartney or the Stones.�Jwq
 �bOverall, Time Out of Mind was better, although that too was a little overrated, with only Not Dark�Jxq
 �iYet, Standing in the Doorway and Trying to Get to Heaven truly belonging to the canon of great Bob songs.�Jyq
 �FSeems all the "new" songs were pulled from baselines of his old songs.�Jzq
 �9There is not a song I can't identify from an older album.�J{q
 �Chintz.�J|q
 �First off, I love Bob Dylan.�J}q
 �ZHowever, has anyone stopped stroking his ego long enough to realize how bad this album is?�J~q
 �OIt has one good song on it, "Mississippi" and that was written for Sheryl Crow.�Jq
 �kI wish she had sung this version as well because it would be much better than hearing Leon Redbone sing it.�J�q
 �2What is Dylan doing with his vocals on this album?�J�q
 �-Give me Nashville Skyline anyday of the week.�J�q
 �@Also, Tweedle-Dee is about the most ridiculous song ever penned.�J�q
 �kI know that it's probably really deep and about the 2000 election or something, but its called Tweedle Dee.�J�q
 �nI think I'll write a song about Humpty Dumpty and see if Columbia will market it as a topical song about 9-11.�J�q
 �AIt's interesting to see how forgiving we are of aging rock stars.�J�q
 �ZThe longer one stays in the music industry, the more we are willing to tolerate bad music.�J�q
 �"Neil Young and Mick Jagger anyone?�J�q
 �iThe music, lyrics, and arrangements are wonderful, but Dylan's voice is like fingernails on a blackboard.�J�q
 �{His voice has become a tortured croak that could not be less appropriate for the witty, airy songs that dominate the album.�J�q
 �8I still play Bob Dylan's first album bought 40 years ago�J�q
 �Land I hope he keeps writing forever, but someone else should do the singing.�J�q
 �-I have several reasons for hating this [...].�J�q
 �THE GUY CAN'T SING-�J�q
 �Wand yeah of course he never could sing, but here, it just sounds so bad that its WEIRD.�J�q
 �It's like Zimmerman from Mars.�J�q
 ��I'm baffled by this sort of aural pollution, but I know that there is no way anyone would get signed on the basis of this rubbish if they didn't have a 40 year career behind him.�J�q
 ��I am 19 years old, and I like some of his 60's albums along with "DESIRE"; maybe if I were a burnt out ex-rock star I would have sympathy for this album, too.�J�q
 �-Correctly titled because we all LOVE the guy.�J�q
 �MHe will forever be remembered as the greatest poet / writer / folk balladeer.�J�q
 �HTHEFT, because now he is using that well earned fame to steal our money.�J�q
 �This album is just plain awful.�J�q
 �=Very difficult to listen to his voice that really has failed.�J�q
 ��Other reviewers call as good as "Blood On The Tracks", not quite sure how any music lover or audiophile can make such as statement.�J�q
 � DO NOT WASTE YOUR CASH ON THIS!!�J�q
 �FHave tickets to his 11/23 show, hope he doesn't promote the album!!!!!�J�q
 �jthe backing band is tight, the music is good, with the feel of 50s Hank Williams, Carl Perkins and others.�J�q
 �ZAnd most of the songs, especially "Mississippi" are among his best, which is saying a lot.�J�q
 ��But the '60s Dylan voice needed maturity, the '70s voice was just right, the '80s voice was evolving, but the '90s-2001 voice has lost me.�J�q
 �oKind of like Tom Waits with a hangover and a head cold, after smoking his second pack of Marlboros for the day.�J�q
 �=That's really the only reason I can't listen to Bob nowadays.�J�q
 ��Too bad, because the "sound" on his last two records is even better than that "wild mercury sound" he loved so much back on 1966's world tour.�J�q
 �well, it wasn't bad.�J�q
 �not by a long shot.�J�q
 �,i am a little dissapointed in the new sound.�J�q
 �Wi guess i am just more of a fan of dylan's old folk songs, more than his rock and roll.�J�q
 �1But i have always enjoyed him, up until the 80's.�J�q
 �'then hee seemed to go sour for a while.�J�q
 �{but in 88, when he made "Down in the Groove" and in the earliy 90's, i was happy to see his return to hard core folk music.�J�q
 ��i thought "Time Out Of Mind" was a fine album, deservning of its praise, but again i am saddened by the switch back to a lot of electric.�J�q
 ��Love and theft had its share of good tracks, High Water was fabulous, and Mississippi sounded suspiciously like the type from "Time out of Mind".�J�q
 �0As for the others... i could take or leave them.�J�q
 �not what its cracked up to be.�J�q
 �+But if you like Dylan, might aswell buy it.�J�q
 �KAfter all the hype, this was the biggest disappointment of the year for me.�J�q
 �YDylan's dabbling in Tin Pan Alley or Stephen Foster-type songwriting just doesn't cut it.�J�q
 �He doesn't have the voice.�J�q
 �cThis kind of thing has been done to death and with far better results by the likes of Leon Redbone.�J�q
 �pI was hoping for more of the type of thing he did on Time Out of Mind or his Oscar-winning song for Wonder Boys.�J�q
 �OThis is silly posturing on the part of a true musical original and trendsetter.�J�q
 �iThe critics loved this CD, but they're probably the same ones who keep voting the now tired sounding Sgt.�J�q
 �0Peppers record the greatest thing ever recorded.�J�q
 �:This is Dylan trying to sound like Bob Dylan 60s revisted.�J�q
 �8Despite his tremendous talent, Dylan can't carry it off.�J�q
 �/The CD mainly recycles his old ideas and music.�J�q
 �:It isn't bad, but it is not very compelling or convincing.�J�q
 �cHonestly, I love Time Out Of Mind, but this CD just sounds like outtakes from those great sessions.�J�q
 �$No original ground being broke here.�J�q
 �2Better to invest in Live 1966 or some earlier Bob.�J�q
 �POr how about some of Bob's earlier contemporaries, like Richard and Mimi Farina?�J�q
 ��After Dylan's last release from the Wonder Boys sound track, I had great hopes for this record, and Dylan's transformation into some sort of country vaudeville circus clown has some interesting moments.�J�q
 �7The experiments with swing and rockabilly are a couple.�J�q
 �AHowever after repeated listening they just don't really grab you.�J�q
 �NThe first track Tweedle Dee and Tweedle Dum is a rocker and a promising start.�J�q
 ��His new voice is perfect for it, and the humor within the writing on this track and some others is reminiscent of his attitude on TOOM, but the rest is just not to up to that level.�J�q
 ��Considering what's out there right now this is not a bad album, but for a Dylan album from any of his phases it's just not up to standard.�J�q
 ��Keep the next track forward button under your finger, or better still grab a copy of Good As I've Been To You, an underrated Dylan classic.�J�q
 �~This album has some terrific songs -- "Mississippi" is worth the price, in fact -- and Dylan sound relaxed and the band sharp.�J�q
 �So why a bad review?�J�q
 �<Well, on the whole, relaxed is not how Dylan is at his best.�J�q
 �+He was relaxed on "Under the Red Sky," too.�J�q
 ��The repeated efforts at cocktail swing just don't work for me, though clearly many people like them; he can play this stuff, sure, but why would he?�J�q
 ��It's a little like an aging lounge act -- someone below actually referred positively to the Dylan cover of Dean Martin songs, but I fear the comparison can hardly be flattering.�J�q
 ��The annoying part is that the good songs on this album show that he hasn't lost his touch overall and that the harm is self-inflicted.�J�q
 �..�J�q
 �"and melodies that are pretty weak.�J�q
 �}A few old 3-chord blues patterns,the same gravelly voice, and the background arrangements all seem too much one and the same.�J�q
 �>Some of the lyrics are interesting,but that's as it should be.�J�q
 �t"Moonlight" and "PoBoy" may be the only two that rise above mainly average songs from country/folk/rock tradition...�J�q
 �ABut at the age of 60, at least Bob and the gang are still trying.�J�q
 X  This album shows the generally dismal new sounds aging rockers can produce, and it sounds embarrassing when you consider that they're still basically producing material for people now nearly two generations behind them, and with all the disparity of life experience that suggests.�J�q
 �IThis is Bob at his best - touching, funny, lyrical, and self-deprecating.�J�q
 �There is not a bad track here.�J�q
 �qThe "bonus" tracks make for an interesting contrast between young Bob Dylan in the 1961 and old Bob here in 2001.�J�q
 �-Thank you Mr. Dylan for a great, great album.�J�q
 ��It's a testament to Dylan's sheer talent and to his sidemen that this album manages to succeed despite the near-total destruction of his voice (which wasn't strong to begin with).�J�q
 �dAt times, it's grating and painful to listen to; occasionally, it's world-weary and oddly endearing.�J�q
 ��While I like the majority of tracks on Love and Theft, I can't say any stand out in my mind as being particularly remarkable, especially in light of Dylan's long and storied career.�J�q
 �)This one might sit closer to 2 1/2 stars.�J�q
 ��This CD is worth owning for many reasons, especially for the rivetting philosophic debate in "Summer Days Summer Nights" regarding the possibility of repeating the past.�J�q
 �What's that?�J�q
 �,You can't tolerate philosophy in your music?�J�q
 �8"Why don't ya just shove off if it bothers you so much?"�J�q
 ��This table is not as advertised - the actual dimensions are 24" L x 24" W x 21" H. Additionally, the legs are not tapered at the bottom.�J�q
 ��When I received the tables I order and found the size/style discrepancies I contacted Visiondecor (the vendor/manufacturer) - I thought I'd received the wrong shipment.�J�q
 ��As it turns out, the piece has been altered since the description/photo and the current incarnation is in the dimensions/style stated in this review.�J�q
 �|Visiondecor was very helpful and apologetic offering me a choice between returning the tables or keeping them at a discount.�J�q
 ��It is a very nice table, goes together easily and shipped quickly, but if you're looking for a table to fit a small space, this isn't it.�J�q
 �dWhen I got this table I was shocked to see that the table measured 24 inches wide by 24 inches deep.�J�q
 �<The product description says that it is only 14 inches deep.�J�q
 �+That was the size I wanted, not a square!!!�J�q
 �AToo bad they did not put a more accurate size in the description.�J�q
 �.Now I'm stuck with a table that is too wide!!!�J�q
 �Buy beware!!!�J�q
 �fThe eye mask part is large and comfortable, so if that's all you're looking for, this is a great deal.�J�q
 �9However, I was interested in the hot and cold gel pack...�J�q
 �AIt is too small to effectively cool or heat through the eye mask.�J�q
 �Cit's definitely not as easy if you had your typical a-frame ladder.�J�q
 ��meaning...super quick and easy to unfold.but this ladder gives lots of flexibility as to the different ways you can reach thingsthe biggest value: how easy it is to store.�J�q
 �#I kept it in my closet in my condo.�J�q
 �fit perfectly.�J�q
 �Cdidn't take up too much room at all. and made it easily accessible.�J�q
 ��especially when my storage unit in the garage was small and an overhead bin.so, if you're in an apartment or condo...especially mine, which had 17" ceilings in the main living room...�J�q
 �lthis is definitely the right and best choicethe only reason for not giving it a full five stars, is again...�J�q
 �you feel a little reluctant to take it out for things, because of all the unfolding and refolding of the ladder you have to do.�J�q
 �Lbut that's also the trade off for the easy storage ability (and flexibility)�J�q
 �$My husband and I listen to it daily.�J�q
 �?There are alternative voices and spiritual music as background.�J�q
 �The pace is perfect.�J�q
 �i.e. not too fast nor too slow.�J�q
 �@It is a great way to recite the rosary while you are commuting!!�J�q
 �$The ultimate Christian prayer on CD.�J�q
 �JIf you pray the Rasary you'll have the Bible come to life in front of you.�J�q
 ��RFA is a good book if you like PKD and are familiar with his themes, his life, and the specific concerns that he had at this point in his life.�J�q
 �!As a novel, RFA is quite lacking.�J�q
 �As art is incomplete.�J�q
 �5Valis would prove this, and would remedy the problem.�J�q
 �}I read RFA after 30 other PKD novels, so naturally I loved it, but I think that it is not the best example of PKD the artist.�J r
 �.Essentially, this is another version of VALIS.�Jr
 ��I recommend VALIS often, especially to readers who tell me they've read a book or two of his (usually Do Androids Dream of Electric Sheep?) and haven't been all that impressed, or people who are looking for something totally out of joint.�Jr
 �BVALIS, if you're even considering this book, should be read first.�Jr
 �<Radio Free Albemuth is actually an earlier version of VALIS.�Jr
 �GIt's bland, by comparison, though more firmly delineated in its themes.�Jr
 �ZSo if you've read VALIS and are having trouble grappling its web, pick this up and chisel.�Jr
 ��Many don't realize that Dick, though hardly a great prose stylist, is a formidable literary mind that speaks in very complex ways to many of the themes studied in his more respected contemporaries.�Jr
 �gI'm only part way through this book, and I'm re-reading VALIS (a book I'd rate at 10) at the same time.�Jr
 �zWhat strikes me is that despite different plot lines and different characters, both have some elements that are identical.�J	r
 �HThe protagonist is communicated to by VALIS via a beam of light in both.�J
r
 �pVALIS informes both of a birth defect in their son (identical defects, identical results, different characters).�Jr
 �>I'm going to continue and see if there are other similarities.�Jr
 ��So far it looks like RFA (posthumously published from notes found by a friend of Dick) is a rip-off from VALIS, or possibly a rejected first copy.�Jr
 �1That's not to say it isn't good or worth reading.�Jr
 �3I just have a suspiscion that it's not to original.�Jr
 �CIAO...--Terrapin�Jr
 �wTry to read "Radio Free Albemuth" and you'll need no further explanation of why it wasn't published while he was alive.�Jr
 �hEven by the slapdash standards of an unfortunately large part of Dick's writing, this is very, very bad.�Jr
 �Don't waste your time on it -- not when you have "Man in the High Castle" and "A Scanner Darkly," among others, to choose from.�Jr
 ��All writers have drafts and manuscripts that serve their purpose of getting the brain going; they make interesting footnotes for scholars and groupies, and they churn out royalties for literary estates, but they don't make literature.�Jr
 �hThis one is only a dry run for "Valis," a well-edited and gripping story that best shows the later Dick.�Jr
 �,This is my first attempt at reading PK Dick.�Jr
 ��This book tried hard to be _something_... and I tried hard to find out _what_... but the experience left me with a headache and a vague nausea.�Jr
 �@I'm going to try another of his books, but this one left me ill.�Jr
 �3I love much of PKD's writing, but this is just bad.�Jr
 ��There's very little story at all, and instead the bulk of the book is just incoherent religious ramblings and aimless political paranoia, apparently the product of PKD's deteriorating mental state after years of heavy amphetamine abuse.�Jr
 X   Other books from around this time in his life I've read (or I've tried to read but given up on) are similarly terrible, and I don't why I thought another take on the whole Valis nonsense would be better when the book VALIS itself is pretty much unreadable.�Jr
 �:some interesting ideas of course, but tiresomely paranoid.�Jr
 �@Dick is reaching for religious explanations for his own madness.�Jr
 �Nhis brain at this point is like a pinball machine that has experienced "tilt".�Jr
 �vone of my very favorite authors, but unlike many who love the Valis trilogy i don't much care for his work after 1970.�Jr
 �your mileage may vary.�J r
 ��I'm getting a bit tired of all these totalitarian regime stories, though admittedly there are some very frightening similarities with today's America and the world as a whole.�J!r
 ��However, the story is very loose, all these techno-theological references seem very naive now, and it totally lacks any nerve, leaving you wondering whether you should stop reading it or go on till the end.�J"r
 �oI don't know what it must have been like reading 20 years ago, but now it just doesn't make it for a good read.�J#r
 �YCharles Aznavour is not only a singer, but a composer and the greatest poet of all times.�J$r
 �>His artistic talents cannot even be summarized in a few lines!�J%r
 �uAnybody who has the nerve to negatively comment on great artists is obviously living in a sad sad world of their own.�J&r
 �[Now, go back to your favorite bar and listen to your despicable music Mr. Toronto, will ya?�J'r
 �RThis is probably the worst butchering of Sondheim's music I have ever come across.�J(r
 �0A waste of money, even for a Sondheim collector.�J)r
 ��I purchased this CDbecause Julie Wilson sang a previously unrecorded Sondhiem song that was cut from the show "A Funny Thing Happened On the Way to the Forum.�J*r
 �W" Unfamiliar with Ms. Wilson's work, I assume she was once a formidable cabaret singer.�J+r
 �"Unfortunately, her voice has gone.�J,r
 �]Listening to an entire CD of her raspy, not always on key singing, was not that entertaining.�J-r
 �>This is one of my favorite indie pop records of the late 90's.�J.r
 ��Superboy & Supergirl, Josie, Pop songs your new boyfriend's too stupid to know about, Sweet, Meet me in Las Vegas - classic songs - great band.�J/r
 �_Don't get me wrong, I am a huge fan of enanos but come on...they can do a lot better than this.�J0r
 �\I would not say that this is a bad album, coz enanos can't make a bad album at all, never...�J1r
 �,But here, most of the songs sounds the same.�J2r
 �69 of 12 songs are ballads, and they are all the same!!�J3r
 �7Marciano's voice is not as powerfull as in other discs.�J4r
 �m"Cuanto Poder" is such an stupid and boring song that I couldn't believe they chose it as the first single. "�J5r
 �pDel Cairo a Paris" and "Sweet Summer" seems to have been composed two minutes before the recording of the album.�J6r
 ��The only tracks which are near the real sound of enanos are the great "Balada 3D", the rocky "Mujer Maravilla", a song that reminds of the soft metal of Big Bang and�J7r
 �bGuerra Gaucha (I really miss that style), and "Una noche en Potrerillos", a moving romantic balad.�J8r
 �%"Amores Lejanos" is just a good song.�J9r
 �The rest is more of the same...�J:r
 �hPueden creer que Amores Lejanos no se editó ni se va a editar en Argentina por lo menos hasta Febrero??�J;r
 �CAsi que tuve que escuchar las canciones depués de bajarlas en mp3.�J<r
 �	El disco?�J=r
 �!Son los enanos, que esperabas...?�J>r
 �>This group has made better albums and they've also made worse.�J?r
 �%If you are already a fan, pick it up.�J@r
 �+Otherwise, you'll probably be disappointed.�JAr
 �uI read all the negative reviews before the purchase and really only one is true and that is the battery life is weak.�JBr
 �]For me, I plan on using it in my car and having power in it at all times, so it works for me.�JCr
 ��Other than that the UI is great, it shows album art which is awesome, basic camera functionality is there if you ever need it, software is easy to understand and the touch screen responds to my touch everytime.�JDr
 �TPlus, with all the above, a 20GB HHD, lots of inbox accessories and it's only $250!?�JEr
 X`  ~pros~cameratouch screenhuge displayyou can use your fingers on the touch screen~cons~BATTERY LIFE (1-2 hrs.)gets dirty easily (fingerprints)also scratches easilynot a good volume controlthe headphones make no sense at all (one headphone cord is longer then the other)I think that this mp3 player should go to people who listen to music not very often.�JFr
 �%If you use this at home, no problems.�JGr
 �2I have to say that the software is kind of cheesy.�JHr
 �\And if you had an iPod, you have to convert all of your songs to mp3 format (kinda annoying)�JIr
 �"I would NOT reccomend this at all.�JJr
 �The graphics are okay.~Pros~�JKr
 ��TouchscreenCamera~Cons~Software does NOT workTouchscreen not very responsiveBattery is horrible even with screen offCamera is a horrible quality�JLr
 �-I received this item and it looked well-made.�JMr
 ��When I placed it on my glass-top electric range, however, I noticed that it wobbled a bit and as a consequence didn't appear to heat up uniformly.�JNr
 �=I suppose if you have a gas range this would not be an issue.�JOr
 ��The product looked like just what we needed for a new induction stove, and based on one review stating yes to induction we decided to try it.�JPr
 �)This did not work on our induction range.�JQr
 �	Just FYI.�JRr
 �Looks great otherwise.�JSr
 �LI made this purchase sometime in October and to date I have not received it.�JTr
 �_I sent a request for information on when I could expect to receive and I have not yet received.�JUr
 �7I am not pleased that no one has gotten back to me yet.�JVr
 �I love this griddle.�JWr
 �I use it all the time.�JXr
 �#Here are two things I've learned.1.�JYr
 �:Make sure the surface is well lubricated before cooking.2.�JZr
 �pFor easy clean up, while the griddle is still hot, pour on one cup of hot water and let it sit for about 30 min.�J[r
 �<The very best stainless steel cooking piece I've ever owned.�J\r
 �TI have been looking for a stainless griddle pan to replace the nonstick for a while.�J]r
 �IBloomingdale's had a Calphalon similar to this one, but it was over $100.�J^r
 �YAlthough this doesn't compare to the quality of the Calphalon, it's an affordable option.�J_r
 �NThis is a heavy stainless pan, and the lid can be used on other pots and pans.�J`r
 �tBe careful to fully read the instructions, as there is a protective coating on the pan to protect it while shipping.�Jar
 �iThis has to be thoroughly washed with soap and vinegar to remove it, or the pan can permanently discolor.�Jbr
 �jThe pan heats evenly on an electric range set to just below medium, and holds the heat throughout cooking.�Jcr
 �UI use a light spray prior to using, and the pan cleans up easily with soap and water.�Jdr
 �^The clean design gives the option of an attractive serving platter for hot or cold appetizers.�Jer
 �=Chill the tray in the fridge for cold items, or warm for hot.�Jfr
 �(I OPENED THE BOX WITH GREAT EXPECTATION.�Jgr
 �IT LOOKED AND FELT GREAT.�Jhr
 �CI DID NOT SUSPECTED THAT ANYTHING COULD BE WRONG WITH THIS PRODUCT.�Jir
 �GI THROW THE BOX AWAY AND DECIDED TO USED IT IN THE NEXT COUPLE OF DAYS.�Jjr
 �WRONG MOVE!�Jkr
 �kTHREE DAYS LATER I DECIDED TO USE IT, AND TO MY DISMAY, WHILE WASHING IT, ONE OF THE HANDLES JEST FELL OFF.�Jlr
 �IT IS A SHAME THAT WAY�Jmr
 �THE HANDLES�Jnr
 �ARE ATTACHED.�Jor
 �-THERE ARE NO SCREWS OR FASTENERS OF ANY KIND.�Jpr
 �,A PIECE OF GUM WOULD HAVE DONE A BETTER JOB.�Jqr
 �FI received this griddle as a gift, and I have never been more pleased.�Jrr
 ��It heats more evenly than any other griddle on the market--you can literally use every inch of space and expect the same results, even if your burner is not exactly the same size as the griddle.�Jsr
 �)It also cleans up beautifully and easily.�Jtr
 �-I have made pancakes and french toast so far.�Jur
 �<I highly recommend this griddle; it is well worth the price.�Jvr
 �AThe handles do get just a little hot after awhile, so be careful.�Jwr
 ��Also, unlike with other pans that "radiate" heat that you can feel with your hand, this pan will not "feel" hot--and it will heat up quickly--so test with a little water first or it will get too hot on you.����      Jxr
 �Nice little griddle.�Jyr
 �bWhen you unbox it though, you need to wash it before using it just like the instructions instruct.�Jzr
 ��They advise to use vinegar and detergent to take off the industrial oils from manufacturing or it will leave a permanent brown stain.�J{r
 ��I have only stainless steel pots, so the secret to using this type of surface is to heat it slowly with a low temperature for about 5 minutes with your oil on the surface.�J|r
 �>I have gas, so I am unsure how it works with electric burners.�J}r
 �ZBut once you have a even low temperature, then it will work just like a non stick surface.�J~r
 �(It cooks the best pancakes and sausages.�Jr
 �This griddle is GREAT!�J�r
 �Heats evenly.�J�r
 �"Lid is a plus to keep things warm.�J�r
 �-Amazon had best price and delivery was quick.�J�r
 �I'd buy again.�J�r
 �1I ordered this based on so many positive reviews.�J�r
 �YOne look at it was enough to make me wonder what kind of stoves all of these people have!�J�r
 �cI have a standard 4 burner gas stove and this griddle was much too big for even the largest burner.�J�r
 �I sent it right back.�J�r
 �!The griddle is ok - used it once.�J�r
 �JBut, the glass lid I received had three dents on the metal rim of the lid.�J�r
 �HI do not believe this was done in shipping - it was fairly well wrapped.�J�r
 �=I believe this was sent out by the company in this condition.�J�r
 �QIt was on sale and I believe i received a returned griddle from another customer.�J�r
 �SI will keep the griddle and use it - but, i will never buy from this company again.�J�r
 �#This was a very entertaining title.�J�r
 �CI completely beat the game within about a week after purchasing it.�J�r
 �ZIt is lots of fun however I do wish it was a bit longer, in parts it seemed almost rushed.�J�r
 �DThe graphics are very impressive and the controls are easy to learn.�J�r
 �&This game is definately worth playing.�J�r
 �pMy 9 year old son loved the book, the movie, and the ds game but it only took him 3 days to beat all the levels.�J�r
 �*He had a hard time puuting it down though.�J�r
 �+This game in my opinion is a complete flop.�J�r
 �/saphira is just the muscle, not much flying =(.�J�r
 �Jthe magic is kinda fun but once the games over it loses its novelty value.�J�r
 �$Also it doesnt follow the darn book.�J�r
 �Do NOT buy it.�J�r
 �Enjoy smooth jazz piano music.�J�r
 �THelps me get through the work day with a positive mood and makes the day flow nicely�J�r
 ��I thought this film would be a nice aviation drama but it was so poorly done that I should just throw it away instead of letting it occupy precious space on my DVD shelves.�J�r
 �^I HAD to see this movie after hearing praises about the extraordinary character of this woman.�J�r
 �3I wasn't impressed with her character or the movie.�J�r
 �Not so extraordinary.�J�r
 �'Rockwell's first album was really good.�J�r
 �/It had a funk driven feel to it on most tracks.�J�r
 �DObscene Phone Caller was a good follow up to Somebody's Watching Me.�J�r
 �}Anybody that can get Michael Jackson to sing on your song is a winner in my book as that song is still funky 2 decades later!�J�r
 �XIt sounded very close to Michael's own hit Billie Jean in terms of the rhythm and beats.�J�r
 �NUnfortunately, Rockwell couldn't match that early success with his next album.�J�r
 �BBut this was a good start for him in his short lived music career.�J�r
 �They say there's no such thing as pefection but I beg to differ when it comes to Rockwell's debut album Somebody's watching me.�J�r
 �)This album is great from start to finish.�J�r
 �lIt has everything I love, including hot electro beats, synthesizers and some of the best new wave ever made.�J�r
 ��The best tracks are of course the title track, Obscene phone caller, Knife which displays Rockwell's softer side, Foreign Country and Wasting away but as I stated before this album flawless.�J�r
 �^Order this album you will not be sorry!PS,Captured Rockwell's 2nd album) aint half bad either!�J�r
 �:I'll admit, I purchased this CD thinking I was a Brak Fan.�J�r
 ��Don't get me wrong - I watched Cartoon Planet every week for months straight, then Space Ghost Coast To Coast after CP got shafted.�J�r
 �<As for this album: I listened to it twice, then put it away.�J�r
 �?The guest apperances were by people I don't know or care about.�J�r
 ��The content was somewhat funny, but skits can't compare to the hilarious segments from Cartoon Planet or the pieces on the previous 2 CD's.�J�r
 �.If you are a SGC2C fan, you might like this cd�J�r
 �but that's a stretch.�J�r
 �cIf you're NOT a SGC2C fan, but - say - five years old, you may enjoy this album's attempt at humor.�J�r
 �]Truly a low point in Cartoon Network CD's, followed only by those "music inspired by" albums.�J�r
 �\Brak is at his best yet, with his stupid, mindless songs to enjoy when you've had a bad day.�J�r
 �@If you're a mechanic, then "I Like Hubcaps" is the song for you!�J�r
 �>This CD is awesome and should be nominated for a Grammy Award.�J�r
 � Remember, this CD is............�J�r
 �BRAKTACULAR!!!!!!�J�r
 X_  I recently obtained 'Brak Presents the Brak Album Starring Brak', and as a HUGE fan of both SGC2C and the Brak Show, I must say that I LOVE this CD - I think this album does a great job catering to the fans - which could be a problem for those of you that have never seen SGC2C or the Brak Show, I don't think all the humor makes sense to 'outsiders'.�J�r
 �WSo to reiterate what I said above, this CD is a must have for any Brak fans out there!!�J�r
 �I love this CD.�J�r
 �It's magical and good.�J�r
 �It likes to eat the sandwiches.�J�r
 �,Should any comedy-hungry sole avoid this CD?�J�r
 �9Only if you're round on the edges and high in the middle.�J�r
 �And now, on with my pants.�J�r
 �(This one's for you, Andy!)�J�r
 �4I couldn't believe when I saw the write up for this.�J�r
 �&I-10 is NOT America's longest highway.�J�r
 �'That would be I-80,interstate category.�J�r
 �Teaneck,NJ to San Francisco,CA.�J�r
 �jI've read Gleick's Faster, and when I saw What Just Happened in the bookstore, I picked it up immediately.�J�r
 X   Gleick's candid analyses of technological triumphs is an enjoyable walk through the computer and Internet revolution of the 1990s; however, the book was lacking some of the critical interprative edge that one finds in Faster, and for that reason it fell a little short of my expectations.�J�r
 ��Although What Just Happened does offer an opportunity to step back and think about the implications of the IT revolution, I found myself reading it more for entertainment.�J�r
 � I own half a dozen Gleick books.�J�r
 �ZUnlike the others, if someone borrowed this one and didn't give it back, that would be OK.�J�r
 ��Maybe someone who would want to re-live the early, early days of the internet would like reading it, but then again, I worked for an ISP and created web pages from scratch in HTML in the mid and late 90s, and this book still was barely enjoyable.�J�r
 �5This is so much nicer than you can tell by a picture!�J�r
 �0It is extremely heavy and the etching is lovely.�J�r
 �Well worth the price.�J�r
 �MI purchased this for a wedding shower gift and am thoroughly pleased with it.�J�r
 �)I see the US version isn't very exciting.�J�r
 �8You can find the European version on Amazon.de EUR 6,50.�J�r
 �uWhich has the remixes by And One and Beborn Beton that ended up on Cleaner's Solaris album in the US as bonus tracks.�J�r
 �`Cleen (Daniel Myer of haujobb's main side project) takes a step backwards here with this single.�J�r
 �mThe Voice is a quality dancefloor EBM tune, but it's stretched a bit thin here with too many sub-par remixes.�J�r
 �(:Wumpscut:? ick!)�J�r
 ��The remix of Sunburst, however is quite good, as is the live non album track 0361.There were problems with the mastering of early shipments of the US pressing of this disc.�J�r
 �^I'm pretty sure that you _must_ have this book if you are studying advanced organic chemistry.�J�r
 ��Maybe it's not the best one to use as a study guide, but it's extremely helpful as a reference book both for undergrads and graduate students.�J�r
 �>However, one can argue that this edition is a bit out-of-date.�J�r
 �Lower level ESL?�J�r
 �TI would love to know what the writers of the book title views as intermediate level.�J�r
 �I teach lower level ESL adults.�J�r
 �WLower level means the students struggle with all words and the meanings of those words.�J�r
 �TWe are working on mastering words such as : next to, behind, Monday, run and turkey.�J�r
 �5The title of this book is "First Discussion Starters-�J�r
 �=Speaking Fluency Activities for Lower-level ESL/EFL Student".�J�r
 �eIt would appear that this is a simple, easy-does it- let's-get -comfy with the English language book.�J�r
 �RI now quote from a paragraph: "The post-master general....faced a unique dilemma."�J�r
 �Dand...." unless the expression creates a substantial disruption...."�J�r
 �Right?�J�r
 �AThese are speaking activities for "Lower-Level ESL/EFL Students"?�J�r
 �I cannot use this book.�J�r
 �iI urge the authors of this book to rename the book to more accurately describe the level of its contents.�J�r
 �	bleeeeeh!�J�r
 �XAs an ESL teacher, I was running out of ideas for conversations my students would enjoy.�J�r
 �MThis book gave me many ideas and provided exercises for written language too.�J�r
 �iThis looks like it would be an excellent language development for early speakers of the english language.�J�r
 ��It is far past the 'first' language development discussions--it is far more appropriate for older language learner content topics.�J�r
 �2I blindly bought this because "Diwan" was so good.�J�r
 �\It is a ridiculous mix of decent Middle Eastern rhythms and horribly misplaced guitar riffs.�J�r
 �;It would almost be laughable if I hadn't spent money on it.�J�r
 �nRachid Taha always wanted to be a rock and roll star and when that didn't work he jumped on the Rai bandwagon.�J�r
 �TNow he is trying to sell trendy rai music with his horrible interpretations of rock.�J�r
 �Don't fall for it like I did.�J�r
 �VI was disapointed with this CD as the artist tries too hard to be do too much in 1 CD.�J�r
 �NIt tries to incorporate too many styles without doing justice any any of them.�J�r
 �NIt isn't a dance, or techno CD and is far too Western to be culturally Arabic.�J�r
 �WI felt I got nothing of what I was looking for which was contemporary and dancible Rai.�J�r
 �<This is more atune to Alternative music with foreign lyrics.�J�r
 �tI recieved a item totally different from what you guy were advertasing, there for I'm really upset for your service.�J�r
 �/I am unable to review a product never received.�J�r
 �gI don't think its Amazon's fault because apparently the book is sent or mailed from some other company.�J�r
 �PInformation was that the book should be mailed anytime from (this is an example)�J�r
 �Nov 27 - Dec 16.�J�r
 ��I waited patiently till one day before the cutoff date to email the company that was mailing the product to inform them that I haven't yet received it.�J�r
 �nI understand that the postal service scan some of the products sent and this product was scanned as delivered.�J s
 �2Mistakes are done in delivery and other instances.�Js
 �$That could have been this situation.�Js
 �4I just know that I have never received that product.�Js
 �JQuestion asked to me by the company lead me to believe that I recieved it.�Js
 �Question I am asking.�Js
 �6Why would I want the company to send me a secong book.�Js
 �1What would one book have that the other wouldn't.�Js
 �2Bottom line is I can't review a book not received.�Js
 �cThank You,JoeLas carpetas: persecucion politica y derechos civiles en Puerto Rico (Spanish Edition)�J	s
 �hSeiko 5 SNZB73 - ordered this watch from TnT Jewelry through Amazon, it arrived in a blue box and clean.�J
s
 �HI liked the look and the stainless steel bracelet is shiny and polished.�Js
 �iThe see thru case back is a plus for a 100m water resist but I noticed It was made in Malaysia not Japan.�Js
 ��So far the quality looks same and I liked it, I wished it had a screw in crown but no just pull and push in crown, Still I never take it off when I swim or taking a shower .�Js
 �@Accuracy is acceptable, it gains 20 seconds for couple of weeks.�Js
 �RI had this watch for almost 2 months so I don't know how long this watch can live.�Js
 �Goodluck and thanks.�Js
 �ZI have had previous Seiko watches purchased in retail stores that always lasted for years.�Js
 �;I would replace them because they looked beat up from wear.�Js
 �lThis NEW Seiko SNZB73 was bought from WatchZone (which now calls itself WristWatch) lasted seven (7) months!�Js
 �BSeiko's web site warns you about buying from unauthorized dealers.�Js
 �8Now I will have the hassle of seeing if it can be fixed.�Js
 �Beware and Good Luck!Umbra�Js
 ��Tica Watch Box, Natural/MushroomSeiko Men's Retrograde Chronograph Silver-Tone Watch #SPC011Seiko Men's Alarm Chronograph Leather Strap Watch�Js
 �c#SNAB65Seiko Men's Alarm Chronograph Silver-Tone Watch #SNAA61Seiko Men's Kinetic Silver-Tone Watch�Js
 �#SKA387�Js
 ��Almost unknown semi-classic from Jack Arnold, responsible for some of the '50's best sci-fi classics such as CREATURE FROM THE BLACK LAGOON, INCREDIBLE SHRINKING MAN, IT CAME FROM OUTER SPACE and TARANTULA.�Js
 � A worthy edition to these films.�Js
 ��This is one of those titles I had seen in the movies originally on a theater screen, and then on television but hasn't been on in years...�Js
 �vSo I was overjoyed when it was released just recently by OLIVE FILMS on bluray for the first time in any video format.�Js
 �wI must say aside from minor pleasures, the film is pretty silly and hasn't been given a proper restoration of any kind.�Js
 �dThere is some minor gatefloat in portions, some scratches and a bit more grain than should be too...�Js
 �qUnless (as is the case with me for personal reasons) this film is one of your wants, I wouldn't waste your money.�J s
 ��See when I was a kid myself and my long deceased younger brother Michael used to go and see double and triple features twice a week during summer...�J!s
 �*And THAT is purely why I wanted the flick.�J"s
 �=I read classics now and then, but this one was disappointing.�J#s
 �cThe storyline is very good, but Melville is wordy, and gets caught up in dull, 20 page digressions.�J$s
 �3(Ex. difference between a humpback and a narwhale).�J%s
 �=All in all, I'm glad I read it, but it was real work at times�J&s
 �"Call me Ishmael.�J's
 �Z" It's undoubtedly one of the most widely recognized opening lines of any classical novel.�J(s
 �3Unfortunately, it's also the best line in the book.�J)s
 �gThis is a vast, slow-moving work in which all of the action takes place in the last twenty or so pages.�J*s
 �lMost of the rest is a more or less inaccurate description of the whaling industry and the anatomy of whales.�J+s
 ��The sea is perhaps one of the most colorful and compelling backdrops for a novel filled with tension and power, but Melville fails to communicate either of these except in rare moments.�J,s
 �hCoupled with this is his failure to fully develop what would have been some very interesting characters.�J-s
 �DOne of those rare cases in which the movie was better than the book.�J.s
 �'Call me Ishmael'.�J/s
 �KI suppose everyone has heard of this book until they are almost sick of it.�J0s
 �But it is worth reading.�J1s
 �uThin on character insights but full of information on whaling/whales and the technology and lifestyles of the period.�J2s
 �.A curious book, readable and worth the effort.�J3s
 �6Very good book read it a million of times AMAZING!!!!!�J4s
 �!Its just missing a little umph!!!�J5s
 �0I think very good book missing something though.�J6s
 ��While this book has a strong plot that pulls the reader into its style and context, there is a little bit too much attention to detail that does not need to be included.�J7s
 �CStill, this book is good to read and will captivate many audiences.�J8s
 �LI have a degree in English Education, and my son in law is an English major.�J9s
 �_I thought this book dragged on and on, was loaded with tedious detail, and was a waste of time.�J:s
 �SIf it had been condensed into three short chapters, it would have been much better.�J;s
 �YI finished reading it only because I am retired and it was on my "classics to read" list.�J<s
 �kWhen I told my son in law that I was reading it, he told me that he could not finish it, that it was awful.�J=s
 �0If you want to read a good novel, this isn't it.�J>s
 �#I would give it 0 stars if I could.�J?s
 �IAfter reading it, I am glad that I didn't have to read it in high school!�J@s
 �The format was strange.�JAs
 �pOne chapter is pure nonsense, supposedly funny, and the next is a manomaniac ultimately searching for his death.�JBs
 �BMaybe it is just me, but I don't understand why this is a classic?�JCs
 �QI should have checked what this publishing looked like inside before I bought it.�JDs
 �-The layout is not attractive, at least to me.�JEs
 �#The Penguin edition is much better.�JFs
 ��Melville's book is astounding, to be sure, but this version begins with the first chapter, not with the extensive front material that Melville included.�JGs
 �*In other words, it is NOT a complete text.�JHs
 �^Moby-Dick... thought it would be a good whaling book with adventure and perhaps some suspense.�JIs
 �9How dissapointed was I when I finished reading this book.�JJs
 ��First of all, Melville just loves to start discussing whales and sea and then somehow jump to an absolutely irrelevant subject, such as ponds and paintings.�JKs
 �JSecondly, if you are looking for adventure and suspense there may be some.�JLs
 �SHowever, what little there is of it, it is killed off by the very boring narration.�JMs
 �&I thought there were some good points.�JNs
 �fIt had strong symbolism, quite a good description of the life at the time and the life of the sailors.�JOs
 �lMust admit that the characters are masterfully created, especially Ahab with the solemn mystery of his life.�JPs
 ��There are certain books which become classics because of the characters they create, the stories that they tell, what they say about society, both in the time they are written, and later, or because they simply make you think.�JQs
 �+This is not one of those sorts of classics.�JRs
 �KThis book is a classic because it is a classic, not because it is any good.�JSs
 ��Oh, I suppose if one could get past the fact that it was written by a man being paid by the inch, you would find a story/allegory buried somewhere inside.�JTs
 ��But you would have to be extremely patient, enjoy torturing yourself, or just want to be able to say that you've read it in order to get there.�JUs
 ��It pains me to think of all the good books which were probably written around the same time which have faded into obscurity, while this dreck has become a classic.�JVs
 �@Have heard for years that MOBY DICK is a great American Classic.�JWs
 �Decided to try to read it.�JXs
 �Very disappointed.�JYs
 �CIf you want to read lots of meaningless whale trivia read the book.�JZs
 �FIf you want to read a good book stay as far as you can from this book.�J[s
 �yThis story will make you forget your troubles and I don't think anyone can read it and not be affected in a personal way.�J\s
 �-I plan on reading it every decade I am alive.�J]s
 �\All I have heard of Moby Dick I imagined that it must be one of the best books ever written.�J^s
 �OIt is certainly not of my favorites and it took me a long long time to read it!�J_s
 �"One tip: Buy the abridged version!�J`s
 ��I used to prefer unabridged books but this one must be shortened especially because it has whole chapters on uninteresting stuff like obsolete cetology!�Jas
 �CI have read 25% of this book and still waiting to get to the story.�Jbs
 �@All the same it is a great novel, very detailed and descriptive.�Jcs
 � I look forward to the adventure.�Jds
 �sIf you can't find/can't afford an original press with the Rockwell Kent illustrations, this is the next best thing.�Jes
 �ESame illustrations, but the cover isn't anything to write home about.�Jfs
 �$Improve my reading material, I said.�Jgs
 �,Expand my knowledge of the classics, I said.�Jhs
 �aMight need the knowledge if I'm ever the Phone A Friend on Who wants to be a Millionaire, I said.�Jis
 �Free on Kindle I said.�Jjs
 �Wrong, wrong, wrong, wrong.�Jks
 �ENever a word or sentence used where ten - or twenty - will do intead.�Jls
 �xAchingly long and tedious, and downright boring (sorry Herman), the author refers to whaling voyages taking three years.�Jms
 �8Take this book with you, you might just get it finished.�Jns
 X  While some of the passages are rather hard to get through as they go into excruciating detail about the ship on the sea, the men on the ship on the sea, the ropes on the ship on the sea with the men, the mooring-clips the ropes are attached to on the ship by the men in the sea...�Jos
 �]The action of this novel and its imagery and prose of foreshadowing make it a worthy classic.�Jps
 �/Characters are well defined and often poignant.�Jqs
 �JEverybody should at least attempt this book at least twice in their lives.�Jrs
 �CIt is easy to see why this book made it in to the list of classics.�Jss
 �nHerman Melville is a whiz at descriptive text, though at times this makes the book a little long in the tooth.�Jts
 �{This book is less about a 'story' of the white whale and more a picture of what whaling is all about and whales in general.�Jus
 �wI enjoyed reading Moby Dick, but it's not a book I'll read again and by the end of the book I was relived I'd finished.�Jvs
 �iWhoever creates these Kindle freebies doesn't even proofread them and is a terrible typist and formatter.�Jws
 �Not much else to say.�Jxs
 �k"Whenever I find myself growing grim about the mouth; whenever it is a damp, drizzly November in my soul...�Jys
 �+" I read Moby Dick, and if you haven't yet,�Jzs
 �well I say give it a try.�J{s
 ��its Moby Dick....what more needs to be said, i am trying to read the classics i missed in my youth, this is an excellent way to do it, melville is not even close to a favorite, so i am glad i didnt pay for it�J|s
 �/and it was nice that Kindle has these for free.�J}s
 �#"From hell's heart, I stab at thee!�J~s
 �)" Maybe the best line in any book ever...�Js
 �+This book is pretty fantastic in many ways.�J�s
 �=The characters are good, and the story is an interesting one.�J�s
 �\And I can hardly imagine more descriptive narrative regarding the whaling days of Nantucket.�J�s
 ��In fact, that is also the worst part - Melville can be a bit boring when goes to lengths about this set of sails or masts or whatever.�J�s
 �nBut if you stick with it, you can pretty much picture a whaling vessel at work, which is quite an achievement.�J�s
 �:It's a piece of history preserved largely by this one man.�J�s
 �uSo the story suffers a bit when he goes off on multiple chapters about the ship details, but it remains a good story.�J�s
 �`And Ahab is a fascinating character - he is surely crazy but in many ways also pretty inspiring.�J�s
 �2He is the quintessential man that won't give up...�J�s
 �It starts off as a story.�J�s
 �kThen you learn that the globe was charted by whaling ships, because there were more of them than explorers.�J�s
 ��There were some good moments when he looked at jonah and the whale, his method stirring up the crew into a frenzy about moby dick and the final battle with moby dick.�J�s
 �wThe rest reads like an encyclopedia on the working and running of a whaling ship, its crew and dissection of the whale.�J�s
 �CA diagram would have been nice and some spacing between paragraphs.�J�s
 �RIf this book was released today for the first time, it would not sell well at all.�J�s
 �mOne thing really bugs me about the Modern Library... edition, and that is the way the annotations are set up.�J�s
 �gThe very helpful endnotes follow the text of the novel, but the text contains no indicators whatsoever.�J�s
 ��I at first was not aware that this edition contained any annotations; when I found them, I was somewhat dissappointed, as although they are rather helpful, they are set up clumsily.�J�s
 �?This probably is not the best edition of _Moby-Dick_ to choose.�J�s
 �>I was excited to order Moby Dick for a long camping trip west.�J�s
 �mMy daughter had an assignment to read it over the summer, but shecan't read in the car without turning green.�J�s
 �This seemed perfect.�J�s
 �FUnfortunately, it wasn't made clear that this was an abridged version.�J�s
 �HTherefore, we couldn't use it and had to order another from the library.�J�s
 �-I'm sure that it is excellent for what it is.�J�s
 �mI really felt, however, that we should have been told more directly that thiswas a greatly shortened version.�J�s
 �Thank you,Cecilia Johnson�J�s
 �UThe story had a lot of interesting characters and the descriptions of them was vivid.�J�s
 �OI have read oly one third of the book, but am looking forward to read the rest.�J�s
 �love this book!�J�s
 �\If you have an iphone the podcast of Moby Dick is phenominal if you like to listen to books.�J�s
 �aI must say that if I wanted an education in whales and whaling, this would be the book to choose.�J�s
 �cHowever, Herman Melville belabors the point and confuses the issue with many words and repetitions.�J�s
 �bThe story line, if you can find it, is very interesting as is the education but it goes on and on.�J�s
 �2What should be a few paragraphs goes on for pages.�J�s
 �dAlthough the plot dragged all over the place, it was a basicly good book with a lot of emence detale�J�s
 �
A classic?�J�s
 �Yes.�J�s
 �Long?�J�s
 �That's an understatement.�J�s
 �<There's an entire chapter on why the color `white' is scary.�J�s
 ��If you never read this in school, and you don't know what you're getting into, I suggest talking to a few people who have read it first to see if this is your flavor.�J�s
 �KMelville is a beautiful writer, but his epic tangents were too much for me.�J�s
 �2Some people like the Mona Lisa, some people don't.�J�s
 �u"Moby Dick" has been hyped far beyond its worth because it was the first American novel with philosophic pretentions.�J�s
 �jYou could read the first ninety pages, then skip to the last ten and miss nothing but a binful of symbols.�J�s
 ��It's gauche, jejune, primitive, a graphic novel without illustrations--one of the few American novels improved as a Classics comic book.�J�s
 �UJoseph Conrad covers the same material better, and Persig better than either of them.�J�s
 �NThis is a wearying tome that no one would read if it weren't assigned reading.�J�s
 �It will teach you . . .�J�s
 �nothing.�J�s
 �EI quite enjoyed this version, but it felt like it missed some action.�J�s
 �{I don't know the book off-by-heart so I can't pinpoint exact instances, but the overall feeling was that it was incomplete.�J�s
 �$That said, it was still a good read.�J�s
 �\moby dick..is the ultimate tale of obsession...we all know the story, so i wont go into it..�J�s
 �9but this version is a great presentation of the classic..�J�s
 �SThis is the third time reading Moby Dick, an important part of American literature.�J�s
 �5The Kindle makes the book portable and more flexible.�J�s
 �( Makes it very accessible )�J�s
 �*Tale more enjoyable for me in this format.�J�s
 �No lugging around a heavy tome.�J�s
 �Excellent historical content.�J�s
 �^Melville makes a compassionate case for the Sperm Whale, without condemning the whale fishery.�J�s
 �&I'm looking forward to new selections.�J�s
 �TI'm afraid to say that I actually couldn't even get past chapter three in this book.�J�s
 �3The author is garrulous and can't get to the point.�J�s
 ��For example, he will begin to describe a character but adds so much extra stuff that you forget what the subject was in the first place.�J�s
 �EBrian Jacques is a descriptive author who makes sense, unlike Herman.�J�s
 �HAlso, the book is boring and I could tell it wasn't going to get better.�J�s
 �?If you want to read Moby Dick then check it out of the library.�J�s
 �Good books I would�J�s
 ��recommendThe Redwall series by Brian JacquesThe Warriors series by Erin HunterSoul Surfer by Bethany HamiltonHawkbeakWindclan deputy�J�s
 �It was really weird�J�s
 �3I think it could have been better the book was okay�J�s
 �\but it's just kind of boring read the books if you like Moby dick I just did not like it....�J�s
 �ZThis is a great book, which can only be appreciated well after you are out of high school.�J�s
 �LThe descriptions are wonderful and the humor sly, constant, and understated.�J�s
 �ZI kept wishing there was a really good movie out to capture these folks as contemporaries.�J�s
 �ZThe obsessions and characters are complex and deeply-drawn, and the story itself timeless.�J�s
 ��Haven't read this since my teens, forgot how technical it is about whales .. now we know much more about the sperm whale its origins and habits.�J�s
 �<These wonderful creatures are protected now thanks Heavens..�J�s
 �Timeless and not for everyone.�J�s
 �*Interesting religious and social overtones�J�s
 ��This book basically has a good plot and climax, but until you get there, OY, all the boredom in the world comvbined under one cover.�J�s
 �BGood historical background in it but once again, too many details.�J�s
 �NThere is a whole chapter about the captain's hat and about the captain's pipe!�J�s
 �6This was the second most boring book I have ever read.�J�s
 �pThis book moves along at a pretty slow pace but at the same time manages to keep you stuck to the pages somehow.�J�s
 �WIf you've got the time and the vocabulary then i suggest picking up this classic novel.�J�s
 �UI thought that Moby Dick was an all around inspiring novel that everyone should read.�J�s
 �oAll though I had to high of expectations for the book, which made me not enjoy it as well as I thought I would.�J�s
 ��All though the sensational job of Keir Dullen the narrorator made me feel that I was on the ship, and that I was actually along side Captain Ahabs hunting the white whale.�J�s
 �aThrough out the whole book captain Ahab's and his crew is in search of Moby Dick the white whale.�J�s
 �[They travel half way around the ocean in search of him and hopefully can hunt and kill him.�J�s
 ��All though you don't actually read about Moby Dick being seen in the book till the end which emphasizes his presence a lot more and make's it more interesting to read, and also more exciting.�J�s
 �\Aside from a lengthy digression on whale types, this is epic novel writing at its very best.�J�s
 �GThe real theme here is that of obsession and how it can destroy people.�J�s
 ��The bitter Captain Ahab is a tragic Shakespearean character who instead of destroying the great whale, he self-destructs due to his blind obsessional pursuit.�J�s
 �Despite being of prodigious length, the book is well-paced and moves along nicely except for the odd insertion mentioned above.�J�s
 �vMore than 150 years since its first publication, this work is as alive and entertaining to readers now as it was then.�J�s
 �DFew works truly deserve the label 'classic', however this is one....�J�s
 �Call me f____n' Ishmael!�J�s
 �&I read the unabriged 600-page version.�J�s
 ��This book is awesome, but I recomend that you get the abridged version if reading is just an extra end-of-the-day-right-before-I-pass-out-from-exhaustion activity.�J�s
 ��I gave it four stars because, while the style of the writing is unique and very interesting, and the storyline is the cooooolest, there were times when I felt my mind beginning to wander.�J�s
 �\There were a couple of times where I just had to put the book down and go do something else.�J�s
 ��I think it was because this book was written in the mid 1800s, and the way they spoke back then was just odd, with all the "Ye land lubbers" and "Avast ye maties.�J�s
 ��" If you despise the old English way of speaking that is used by the Nantucketers (there are thousands of them in this book, they're everywhere) then I suggest you read the Cliffs Notes and get the jist.�J�s
 �iI despised this book from beginning to end, this is the most torture I have ever had, and I love to read!�J�s
 ��I enjoyed the movie, but the novel was filled with so much extra rubbish that I could hardly differentiate the novel from the movie.�J�s
 �dI think Melville was a genius, yes, but the structure in which he wrote the book did not make sense.�J�s
 �[Don't read this book if you don't have to, and if you have to read it..get the cliff notes!�J�s
 �Such a boring book.�J�s
 �9I learned things about whales that I didn't want to know.�J�s
 �/Finished it, and was glad that Moby Dick won!!!�J�s
 �hThis version gets off to a bad start by omitting Melville's first two sections (Etymology and Extracts).�J�s
 �:And it even gets the title wrong ("White" doesn't belong).�J�s
 ��If you're looking for a free version, your best bet is probably to download the Kindle version from Project Gutenberg and transfer it to your Kindle.�J�s
 �.I was so terribly disappointed with Moby Dick.�J�s
 �RI was expecting a novel of tremendous stature to justify its legendary reputation.�J�s
 �"However the story drags on and on.�J�s
 ��Worse, it seems that Melville is so in love with the whaling industry that he feels compelled to give the reader a lesson on whale anatomy, the different types of whales and the differing types of blubber they produce.�J�s
 �Interesting?�J�s
 �Not for me.�J�s
 �IMost surprisingly, I found the story to get boring once they went to sea.�J�s
 ��At the beginning, while at port, I found the characterisations and the discussion about the whaling communities to be interesting.�J�s
 �ABottom line: I can't understand how anyone could like this novel.�J�s
 �It was a PAINFUL read.�J�s
 �)Moby Dick, was a horrible waiste of time.�J t
 �KAlong with its wordy paragraphs, it also talked about uninteresting issues.�Jt
 �`It is also to long, and you don't hear of them encountering the whale until the end of the book.�Jt
 �mHeres a good idea, after you read this book, go buy a vile of arsenic, drink it and you will be much happier.�Jt
 �%The only monster was the book itself.�Jt
 ��It leaves you with that, "I hate myself" feeling you get after accidentally destroying a major city with a hydrogen bomb or something, anyways, do not read it!�Jt
 �The book is a classic.�Jt
 �The publisher established.�Jt
 �5Yet, I do not recommend the purchase of this edition.�Jt
 ��"Call me Ishmael," that powerful opening sentence, is relegated to the left-hand page of the book, a cost-cutting measure more closely resembling newspaper format.�J	t
 �QNo matter how attractive the front cover, this format will not engage the reader.�J
t
 �-Please spare us from this in future editions.�Jt
 �$DO NOT RECOMMEND FOR YOUNG CHILDREN.�Jt
 �TYPE OF ENGLISH IS PROFOUND.�Jt
 �#MUST PUSH SELF TO CONTINUE READING.�Jt
 �COULD BE UPDATED.�Jt
 �LI have always wanted to read this book but never got around to it until now.�Jt
 �`I i am a fanatic at finishing what I start or I would never have wasted the time to finish this.�Jt
 X+  While I see the hidden meanings and expression of the struggle between right and wrong...good and evil in some of the work...mostly I see the pointless rambling of an individual bent on hearing himself talk and dead set on showing the world the depth of his own knowledge about 100 pointless topics.�Jt
 �athe entire meat of this book could have been handled in about 75 pages and been an exciting read.�Jt
 �+I found it painful and dull beyound belief.�Jt
 ��I love to read and generally read 4 to 6 books per month on a variety of subjectx including most of the classics and have found value in 80 to 90 percent of them but would not reccomend this one to anyone.�Jt
 �wI must admit, somewhere after the 400th page, I was getting mighty impatient for the great whale to rear its ugly head.�Jt
 X  Majestically descriptive as to give life to planks of wood and historically fascinating, "Moby Dick" is god-like in its ingenius narrative of man and whales and sometimes tiresome in its detailed descriptions of everything under the sun and aboard the whaling vessel, The Pequod.�Jt
 ��More than being a great book, I came away with a hunger to learn more of its fascinating author who remains a marvel of history and literary wizardry.�Jt
 ��I give this book a rating of seven because it is well above average, but it is certainly not close to being the best novel I've ever read.�Jt
 ��From the memorable first line, "Call me Ishmael," the tale and quirky characters hold the reader's attention; but parts of the book are SLOW (including a very long and dreary chapter on cetology.)�Jt
 �JIt's a good book to read once, but it doesn't encourage repeated readings.�Jt
 �NMoby-Dick is one of my favorite novels so, no, I'm not taking issue with that.�Jt
 �gBut with all the hype about this special new edition, I expected to be blown away by the illustrations.�Jt
 �tInstead, I found that there aren't that many, they're quite small and, except for two or three, not that impressive.�Jt
 �9(The one on the cover is probably the best of the bunch.)�Jt
 �nPerhaps the orginal, limited edition priting of this book looked better, as opposed to this paperback version.�J t
 �YGiven my choice of illustrated editions of Moby-Dick, I would stick with Rockwell Kent's.�J!t
 �+Moby Dick is a very difficult book to read.�J"t
 ��I suggest you give the book a shot but if you find yourself losing track watch the movie with Patrick Stewart it stays close to the book.�J#t
 �Melville has a very unique style of writing and for me and others I know it was difficult to understand the message being sent.�J$t
 ��To ask someone who has never read Moby Dick what they think it is about, they would probably tell you what I used to think, "It is about a white whale.".�J%t
 �ZSurprisingly, very few of the numerous chaptors were devoted to that particular leviathon.�J&t
 ��Instead, Moby Dick himself is the climax of a book that puts you on the ocean in the mid-19th century with a rather interesting crew and captain.�J't
 X-  You do not only read about a singular chase of a brutal (not all white) beast, you learn what whales are made of (at least what they knew back then), how the crew was hired and paid, the complete workings of the ship from the owners down to the carpenter, and how dangerous a vocation this really was.�J(t
 �gSymbolism abounds, and quite honestly I ignored most of it and chose instead to just enjoy the scenery.�J)t
 �3Take what you will, but Moby Dick is well worth it.�J*t
 �I've always loved this book.�J+t
 �4I'm so glad I was able to find it and read it again.�J,t
 �#It's been years since I've read it.�J-t
 �Free Kindle books are good.�J.t
 �Free Moby Dick is even better.�J/t
 � Hadn't read this book for years.�J0t
 �7Glad I had the time to journey on the Pequod once more.�J1t
 �ZVery well written, but I don't really care for deep philosophical/theological story lines.�J2t
 �-The ideology was a tad bit tiresome at times.�J3t
 �7I wanted to say "you made your point already, move on".�J4t
 �~The author felt the need to harangue on every minute detail whether it was actually relevant to the purported outcome, or not.�J5t
 �\But, with that being said, I am glad I finally had a chance to read such a leagendary novel.�J6t
 �
Thank you.�J7t
 �iI wish that I had a better appreciation for the classics, but everytime I start reading it I fall asleep.�J8t
 � I can't get past the first page.�J9t
 ��This is the best book I've read in my nearly 40 years, with the possible exception of the Oxford English Dictionary, and easily warrants a ***** rating.�J:t
 �aBut the version I read was the earlier Penguin edition, the one annotated by Prof. Harold Beaver.�J;t
 ��Why Penguin would discontinue that fantastically thorough effort, and replace it with this utterly barren one, I really cannot fathom.�J<t
 ��Anglo-American character actor William Hootkins gives a passionate and wholly intelligible reading of some six hours of great passages from MD.�J=t
 �ZIdeally suited for driving, a great way to come to know or revisit Melville's masterpiece.�J>t
 �Quite funny as well.�J?t
 �Moby Dick is a tough book.�J@t
 ��I will not for a minute pretend that I understood much of what lay below the surface of this story, because I was reading it for pleasure and did not bother to give it the time necessary to truly appreciate Melville's genius.�JAt
 �ZOn the other hand, even giving it only half my attention, the book was still entertaining.�JBt
 �iAhab's obsession, the action on the high sea, and the battle with whales were all intriguing and griping.�JCt
 �fI also learned a tremendous amount about the whaling industry and life on the sea in the 19th century.�JDt
 �There is tremendous depth to this book, but even if you only scratch the surface it is worth the time and effort to do so once.�JEt
 X)  I wouldn't recommend this book to someone who does not like 18th century literature (it can be extremely wordy and dull at times), but for anyone who has an interest in maritime novels, Melville, or is just curious why there is so much talk about Moby Dick, I would advise you to give it a chance.�JFt
 ��I just finished reading this book and I think that it definitely deserves to be labeled a 'classic,' but I also think that not many people will see it as that.�JGt
 �HThere are many biblical, historical, and mythical allusions in this book�JHt
 �"so I didn't understand alot of it.�JIt
 �gHerman Melville was a very intelligent man and I think one of the most accomplished writers in history.�JJt
 �hFor someone to really enjoy this book, I think that one must be a big fan of literature and open-minded.�JKt
 �AIf you're far from this, this is still a somewhat enjoyable book.�JLt
 �EJust be prepared to read certain sections of the book more than once.�JMt
 ��I started reading this book expecting a story about a mad man hell bent on getting revenge on the famous white whale, Moby Dick.�JNt
 �I did get this�JOt
 �tbut I also got a very strong narrative which fascinated me at first but become annoying towards the end of the book.�JPt
 ��Due to this narrative the book has over 100 chapters and it is now safe to say that I am very knowledgeable of the whale fishery.�JQt
 �vI carried on reading this book because I loved the way it was written and in turn, the way that the book made me feel.�JRt
 �vIt is very dark and gothic and I wasn't surprised at the many references to Coleridge's `Rhyme of the Ancient Mariner.�JSt
 �>'It's a book that I can say I have read but it was tough going�JTt
 �and I wouldn't recommend it.�JUt
 �What a terribly dry book.�u(JVt
 �5I cannot concieve how this book has become a classic.�JWt
 �5The author could not have written a more boring book.�JXt
 ��The action is slow if existant at all, and written in a way that you would think if it was narrated you would think a sermon was being given.�JYt
 �3I picked this book up with the greatest intentions.�JZt
 �I never read it in High School.�J[t
 �@Now I know why, my English teachers were sparing me the trouble.�J\t
 �bThis "classic" book was not what I expected it to be, it was very boring and kind of hard to read.�J]t
 �AThe real exitement does'nt even happen until the end of the book.�J^t
 �|Maybe if they updated the book a little bit so people could read and understand it more this book would definetly be better.�J_t
 �:I personally do not like this book because it was so long.�J`t
 �"it does not have a bad story line.�Jat
 �JSo i suggest that you read the younger version no matter what age you are.�Jbt
 �~The only way you should read the full thing is if you really love to read, have alot of time, and like alot of boaring points.�Jct
 �+In this book there is a whale and some men.�Jdt
 �/the men espesially one man was after the whale.�Jet
 �i will not give away any names.�Jft
 �/The whale overpowerd the people all except one.�Jgt
 �!the end was not that good eather.�Jht
 �;i suggest this for older people who do not have much to do.�Jit
 �except read a long book.�Jjt
 �It's Moby Dick.�Jkt
 �3How are you going to start a review of this book? "�Jlt
 �^Despite being a recognized classic for more than 150 years, I don't like Moby Dick because..."�Jmt
 �A:)Actually, it is the version of the book that I am recommending.�Jnt
 �DThis one comes with the classic Rockwell Kent woodcuts for the book.�Jot
 �aThese beautiful images just add a certain touch that make it that much more of a delight to read.�Jpt
 �QDo yourself a favor and spend a few bucks to get the hardcover with these images.�Jqt
 ��This is one of the indispensable books for any good home library and, while I love my Kindle too, I'm old enough that holding a thick chunk of a book brings a real satisfaction that the Kindle can't just quite capture.�Jrt
 �nHerman Melville's Moby Dick stands in relation to the American idea as Virgil's Aeneid does to the Roman idea.�Jst
 XP  In this epic masterpiece, Melville tells the story of the tormented and obsessed Captain Ahab's pursuit of the white whale, Moby Dick, who maimed him once on a whaling voyage and has since come to represent to him the whole unresolvable problem of evil, human suffering,and the heartlessness of nature, the universe, and ultimately God.�Jtt
 X  Ishmael, the narrator of the tale, follows Ahab's doomed quest and interprets philosophically the story of which he has become a part, seeing in the search for the white whale a microcosm of the state of humanity in general, and of modern, naturalistic America in particular.�Jut
 ��The beauty of the language mirrors the beauty of the world which Ishmael sees and interprets, and which so often seems deceptive in its promises of peace, friendship, and certitude.�Jvt
 �OThis classic story is one of the best books I have ever read, but not the best.�Jwt
 �LThis story didn't blow me away, but it isn't the average book (it's better).�Jxt
 �jBeyond a questionable doubt, Moby Dick is Melville's and American History's Greatest Literary Achievement.�Jyt
 �ONo other novel questions our existence as living, thinking human beings better.�Jzt
 ��Though true that the plot chronicles the quest of a mad captain seeking vengeance upon a beast that maimed him, the book's deeper meaning of our relations to the universe cannot be ignored or the literal magic is lost.�J{t
 ��The novel addresses themes our of souls, relationships with each other, religions, morals, and ideas with such purity and genius that no other medium but the written word can convey it so beautifully.�J|t
 �wMoby is God, Ahab is Man, Starbuck is caution and religion, Stubb is ambivalence, Queequeg: trust, Fedallah: the devil.�J}t
 �:Read it or lose out on the best thing that you ever could.�J~t
 �9Be sure to avoid the audio version read by Burt Reynolds.�Jt
 �tHis pseudo dialect is almost beleivable, however he is way over the top emoting and blustering through the dialogue.�J�t
 �mThen he continues on in the same monotonous bombast to read the prose sections as if they were also dialogue.�J�t
 �wThe problem with this is an extremely irritating sing-song pattern, combined with his squeeking, yelling, and mumbling.�J�t
 �9It lacks all respect for the dignity of Melville's prose.�J�t
 �/Buy the unabridged version read by Bill Bailey.�J�t
 �]It is not exciting, but it is not irritating and does not get in the way of Melville's prose.�J�t
 �tSeen through the eyes of one Ishmael, this is the story of Captain Ahab's obsession with the white whale, Moby Dick.�J�t
 �aHaving lost his leg to the whale, Ahab is now on a mission of revenge upon this mighty leviathan.�J�t
 �dThe whale exhibits odd intelligence and ferocity, having evaded hunters and taken many a man's life.�J�t
 ��While the book is exquisitely written with intriguing characters, the author tended to slip into page upon page of explanatory notes.�J�t
 �3This breaks up the story line and is a distraction.�J�t
 �,The story itself however, keeps you reading.�J�t
 �kComplete with all the gory descriptions of the whaling business, Moby Dick is a good book, but a slow read.�J�t
 �)Call me what you like, but not Ishmael...�J�t
 �This book is HORRIBLE!�J�t
 �Classic, my eye!�J�t
 �5I would love to know what's so great about this book.�J�t
 �.I have seen better writing in a Hallmark card!�J�t
 �Boring!�J�t
 �(Give me a good ole copy of Elvis and Me!�J�t
 �4A true story that really tugs at your heart strings!�J�t
 �&I sleep with that one under my pillow!�J�t
 � Keep Moby Dick away from my bed!�J�t
 �.IT IS GOOD BUT DONT WASTE YOUR TIME READING IT�J�t
 �LIKE I DID.�J�t
 �IT IS SORT OF A WASTE OF TIME.�J�t
 �!YOU COULD BE WATCHING TV INSTEAD.�J�t
 �THINK ABOUT WHICH IS MORE FUN.�J�t
 �EXACTLY!�J�t
 �bI didn't get 20 pages into this book before I said ENOUGH ALREADY GET TO THE PART WITH THE WHALES.�J�t
 �Sheesh.�J�t
 �Maybe the ending is good.�J�t
 �PMaybe Ishmael and the whale become lifelong friends and live happily ever after.�J�t
 ��I do not know because Mr. Melville wont stop rambling on and get to the POINT.There is a nice picture of a whale on the cover of my copy anyway so that rates this book as TWO STARS.�J�t
 �The rest is mostly junk.�J�t
 �Back to the drawing board!�J�t
 �6This book is supposed to be the book to end all books.�J�t
 �mAll other symbolic books or classics are held up to Moby Dick and normaly receive an unflattering comparison.�J�t
 �QHearing all this wonderful dogma, I went out to read Herman Melville's MOBY DICK.�J�t
 � I found it dry and inconsistent.�J�t
 �pMoby Dick is a sea-faring tale by an author who adamently denied any symbolic inflection whatsoever in his book.�J�t
 �{I found it very jumpy, whereas Ishmael would divulge his thoughts on many and varied subjects such as the nature of whales.�J�t
 �HSuddenly, you find yourself a month ahead of where you thought you were.�J�t
 �EThe insigts on Ahab are masterful, however, as well as the symbolism.�J�t
 �(Melville would kill us all.)�J�t
 �,That is the only thing that saves this book.�J�t
 �iIf you will take some advice from a fellow bibliophile, don't bother to read this "book to end all books.�J�t
 j�  J�t
 �It's not worth your time.�J�t
 �`I have been going back and reading some of the classics that I should have read while in school.�J�t
 �%IMO this is one of the worst written.�J�t
 �9This guy is the ultimate master of the 'run on' sentence.�J�t
 �7He seems to know all the punctuation except the period.�J�t
 �.Over uses the comma and dash way way too much.�J�t
 �LMost of the book just over describes almost every type of fish in the ocean.�J�t
 �-One of the most boring books read or written.�J�t
 �3Glad I was smart enough to skip this one in school.�J�t
 �&I should have had him in my Lit class.�J�t
 �5His writting style would have been much improved.john�J�t
 �qI have to start by saying that I like long, involving novels by great authors (e.g. Tolstoi, Dickens, Hawthorne).�J�t
 �&I actually expected to love this book.�J�t
 �<Instead, the best I have to say is that I liked parts of it.�J�t
 �nThe chapters focusing on Ahab are replete with drama and a gorgeous sense of tragedy-the stuff of Greek drama.�J�t
 �gThen Ishamael, as narrator, takes over and discusses the minutiae of whaling and even whale physignomy.�J�t
 �I don't care.�J�t
 �I simply was bored.�J�t
 ��While I can understand on an intellectual basis the way this story is structured,I have to agree with those reviewers who said the book was a mess when it came out.�J�t
 �<I have read one other work of Melville's, namely Billy Budd.�J�t
 �I hated that too.�J�t
 �-It could be that he just isn't my cup of tea.�J�t
 �6Or maybe some of us just think Moby Dick is overrated.�J�t
 �Obviously the book's a classic.�J�t
 �cHowever I thought this hard copy was going to be a (retro) small book like the one I read as a boy.�J�t
 �,This thing's huge like a medical dictionary.�J�t
 �What a waste of time!�J�t
 �This book is very, very boring.�J�t
 �MI've said it & I'll say it again: this book is very, very, extremely, boring.�J�t
 �iI refrained from giving it 1 star to the fact that yes, it has its good points...if you ever get to them.�J�t
 �8the modern library classics version is far superior !!!!�J�t
 �ofor the amazing illustrations by rockwell kent truly enhance the experience (as does the font size and spacing)�J�t
 � I didn't make it past 100 pages.�J�t
 �6The whole time I kept thinking, what the hey is this??�J�t
 �So much for the classics.�J�t
 �>I, for the life of me, can't understand why this is a classic.�J�t
 �[...]�J�t
 �Who reads Moby Dick these days?�J�t
 �hWell, maybe we need to slow down and enjoy the rich imagery and symbolism of Melville's monumental work.�J�t
 ��Taste the salt spray, hear the creaking of the ship's timbers and the straining of its lines, smell the whale blubber, feel the impending doom.�J�t
 �LI'm sure that this is a good book and that it is so famous for good reasons.�J�t
 �=And I do like reading classic authors and long-winded novels.�J�t
 � But this one was soooo boooring!�J�t
 �NI tried and tried and tried, but eventually, after 200 pages or so, I gave up.�J�t
 �Maybe too much testosterone ...�J�t
 �%THIS SOFTWARE IS WELL WORTH THE PRICE�J�t
 �COMPARED TO OTHERS.�J�t
 �%IT IS EASY TO SET UP AND EASY TO USE.�J�t
 �HDIRECT DEPOSIT FEATURE TAKES A LITTLE TIME TO SET UP WITH YOUR OWN BANK.�J�t
 �/I LIKE ALL THE FEATURES FOR PRE TAX DEDUCTIONS.�J�t
 �QFound this more difficult to use than the simple Quicken payroll I used on my PC.�J�t
 �7Switched to Checkmark b/c Quicken does not work on Mac.�J�t
 �,I preferred the print outs more in Quickpay.�J�t
 ��I found some of the data I put in as for example : hours rates were changed in the calculations and I had to go back and correct them manually.�J�t
 �7Admittedly, some of this may be due to my inexperience.�J�t
 �I need to be able to generate 1099 forms as well as W-2, but this program assumes that you never have to deal with contractors.�J�t
 �MFor crying out loud, would it really be that difficult to include a template?�J�t
 ��And of course, the competing manufacturers' business software will allow you to print 1099 forms--unless you have a Mac, like I do.�J�t
 �*The program works well and is easy to use.�J�t
 �jHowever, as a payroll professional we cannot use this program because the quality of the output is so bad.�J�t
 �4The reports are cheap looking and difficult to read.�J�t
 �IThey could definitely use some creative support in the design department.�J�t
 �pYou can get the info you need but the forms are ugly, hard to read because of the design, and out-dated looking.�J�t
 ��Although we liked the function of the program, we ultimately decided the reports were of such poor quality that we wouldn't put our name on it to give to our clients.�J�t
 �It made us look cheap!�J�t
 �+Ordered Checkmark payroll in March of 2009.�J�t
 �It came good for 2007.�J�t
 �"Needed to pay for upgrade to 2009.�J�t
 �SAmazon was good at returning it but I still have not gotten my shipping costs back.�J�t
 �3I ORDERED THIS SOFTWARE FROM AMAZON ON JULY 6TH -1)�J�t
 �wFIRST SHIPPMENT DID NOT HAVE A CD-ROM IN IT (NO SOFTWARE), I RETURNED THE SOFTWARE (AS INSTRUCTED) FOR A REPLACEMENT.2)�J�t
 �SECOND�J�t
 ��SHIPPMENT (REPLACEMENT FOR THE FIRST) DID NOT HAVE THE CD-ROM IN IT EITHER (STILL NO SOFTWARE), WHEN I WENT THROUGH THE PROCESS TO REQUEST A REPLACEMENT -�J�t
 �THEY SENT ME�J�t
 �IAN EMAIL:"I am sorry that your replacement shipment was also problematic.�J�t
 ��As it seems that the problem with this item is more widespread thanwe originally thought, we are not able to send another replacement.�J�t
 �sWe will investigate and remedy the situation with the item; however,I cannot guarantee when the error may be fixed.�J�t
 �*"I STILL DO NOT HAVE A RESOLUTION TO THIS!�J�t
 �'I have two recordings of this symphony.�J�t
 ��One is the pricey Boulez / Vienna Philharmonic recording on Deutsche Grammophon, and the other is this one, and I think this one is much better than the Boulez rendition.�J�t
 �BThe difference between the two is the most striking in the Finale.�J u
 �fFarberman's finale is taken at just the right tempo to bring out all of the details in Mahler's score.�Ju
 �;The LSO does a fine job playing it, and the price is right.�Ju
 �-Informations in the book is clear and simple.�Ju
 �WHowever if the author used japanese or chinese characters - let a linguist writes them.�Ju
 ��Most of the secret symbols are simple japanese or chinese characters and if written by a linguist they must have a meaning - however written by somebody who doesn't read or write the language and just copied it from womewhere it looks really strange.�Ju
 �Andreas�Ju
 ��It seems thru out history, that when things get off original intent and become less pure, is when things become too rigid or fanatical.�Ju
 �<And the healing arts is no exception to those possibilities.�Ju
 X  When others are charging an arm and a leg for Reiki training, attunement and the information, Steve Murray clearly stands by his vow, stated in his book, THE ULTIMATE GUIDE TO REIKI and he brings clarity, integrity, simplicity and freedom to Reiki and those desirous of learning it.�J	u
 �xSteve has written the Reiki Bible, in my opinion, I highly recommend THE ULTIMATE GUIDE TO REIKI.It set my mind at ease.�J
u
 �Nancy Glasgow�Ju
 �sAs a recently attuned level I practitioner, I hoped for some good, solid education on the actual practice of Reiki.�Ju
 ��Instead, more than half of Steve Murray's book concentrates on full-page photos of himself doing attunements or drawing symbols over his clients' heads.�Ju
 �qThe very few actual instructions he gives are wishy-washy (do it this way, unless you want to do it another way).�Ju
 �iI did enjoy the (very short) chapter, with no photos or illustrations, on using Reiki to contact spirits.�Ju
 ��In general, I think this book is a list of very vague hints about the wonders of Reiki, each of which might be expounded on in another, and then yet another, of Mr. Murray's books or CD's or videos.�Ju
 �I won't buy any more of them.�Ju
 �QI've found infinitely more helpful information in a Yahoo group devoted to Reiki.�Ju
 ��Steve's book gives a very honest look at how some Reiki Masters view the symbols as something only to be shared with someone who is willing to pay for the knowledge.�Ju
 ��I believe the Masters that would be angry with Steve's book, are totally missing the purpose behind why we are called to healing in the first place.�Ju
 �xWe who heal, do not do it for the glory for ourselves, but to give glory to God who uses us as an instrument of healing.�Ju
 �.God knows there are so many that need healing.�Ju
 �zIf some Masters become angry, well they will have to contend with this self- created negativity and I feel sorry for them.�Ju
 �>The Guide has proven to be an excellent reference book for me.�Ju
 �1You won't be sorry that you made this investment.�Ju
 �*I was greatly disappointed with this book.�Ju
 �HAs a Reiki Master Teacher, I purchased this book as part of my research.�Ju
 �YFirst and foremost, it is important to note that it is filled with many technical errors.�Ju
 �BI am also an Intuitive and I found the pictures to be very CREEPY.�Ju
 �HReiki is Spiritual (Rei) Ki (energy) and it is of a very high vibration.�Ju
 �1I could not feel the light of Reiki in this book.�Ju
 �8I recommend "The Reiki Sourcebook" instead of this book!�J u
 �pI have recently received both my Reiki Psychic Attunement and Reiki Master Attunement from Steve Murray via DVD.�J!u
 ��These, along with Steve's Reiki the Ultimate Guide (both I and II)have truly opened up many new awarenesses and experiences for me.�J"u
 ��It may seem that this is rapid progression, but I have already had the chance to help several friends and family members using the Reiki healing programs.�J#u
 �@The most exciting was a successful longdistance healing session.�J$u
 �zI am 53 years old, have always been intuitive, but never quite sure which path to take where I could help people the most.�J%u
 �iSteve Murray's programs have answered all my questions and are continuing to help me in my Reiki journey.�J&u
 �Shanti,SariZel�J'u
 �tAlthough this book is good for a reference, Reiki attunements are supposed to be passed on through physical contact.�J(u
 �dAlthough you may be doing some type of energy healing through only reading the book, it's not Reiki.�J)u
 ��I don't believe in $10,000 master level fees, and have been lucky enough to find a teacher who doesn't charge any where near that,however I feel that students who wish to maintain a true lineage should have the dedication to do the same.�J*u
 �RI Found this book to be a great source of information and very easy to understand.�J+u
 ��I admire the courage of Steve Murray to write this book and for making all the videos, dvd's cds he has made available to us the public despite all the opposition he has had to face.�J,u
 ��I was searching for the right book to buy on Reiki as there are so many but felt I was drawn to Steve Murray's book and I,m so glad I went with my instincts as his book goes into so much detail.�J-u
 �^I reccomend anyone thinking about learning Reiki to buy this book, You won't be dissappointed.�J.u
 ��p.s I've since bought His Psychic Attunement music cd - The 1st level reiki attunement DVD and the psyhic attunement DVD (in the last month)�J/u
 �HAll excellent AND EXCELLENT VALUE FOR MONEY TOO. can't wait to buy more.�J0u
 �Thanks Steve Murray.�J1u
 �jThe book `Reiki The Ultimate Guide' is very in depth and has excellent examples explained for the process.�J2u
 �NSuper presentation and Steve shares openness and depth in describing of Reiki.�J3u
 ��THe book surpassed any of my expectations and has helped me mentally and physically, With his Books and DVDS, I know that I will become a Reiki master soon, so that I can also help others too.�J4u
 �)For that I thank you Steve Murray dearly.�J5u
 �6Many thanks for making my Reiki pathway so affordable.�J6u
 ��Also, I can't ever recall a time when such personal attention was given to me from Steve Murrays website and so timely response too.�J7u
 �?It is easy to read and an overall good guide to learning Reiki.�J8u
 �3Steve Murray gives a no nonsense overview to Reiki.�J9u
 �oAnd what is nice is his approach that each method of practicing and learning reiki varies by each Reiki master.�J:u
 �YHe suggests that you follow what you learned and what speaks to you, and that is so true!�J;u
 �$I was very disappointed in this book�J<u
 �(Not to repeat what has already been said�J=u
 �:but I just want to add my support to the positive reviews.�J>u
 �.I too read the reviews here before purchasing.�J?u
 �RI visited his website and came back here because Amazon always has the best deals.�J@u
 �I took a leap of faith.�JAu
 �/The book is the foundation for the attunements.�JBu
 �UIt is very clear and personalizes the Reiki Master who is giving you the attunements.�JCu
 �TI wouldn't do the attunement without having read the corresponding part in the book.�JDu
 �'You have to know what you have to know.�JEu
 �The DVDs worked for me.�JFu
 �3I totally support this method and will continue on.�JGu
 �6I know there are different strokes for different folks�JHu
 �#but I gave it a chance it paid off.�JIu
 �OActually, it paid off big time considering how much others charge for the same.�JJu
 �(I am extremely grateful to Steve Murray.�JKu
 �He is a special and gifted guy.�JLu
 ��The first part of the book was interesting, when it gets into the actual practice, it becomes overly repetitive, you're reading a cut and paste from the previous chapter - over and over again.�JMu
 ��The general feeling of this book is it's too technical without much of the magic and spirituality you would expect from a book on reiki.�JNu
 �IThe Reiki techniques are presented in a simple easy to understand format.�JOu
 ��I appreciated the author sharing the healing and psychic attunements; other Reiki authors only mention them and never explain how they are performed.�u.